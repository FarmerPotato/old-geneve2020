module cartridge(str){
    color("beige"){
    cube([125,75,10]);
    cube([125,35,20]);
    }
    rotate([90,0,0]){
       color("red") translate([2,2,0]) cube([120,16,.25]);
       color("yellow") translate([22,5,.25]) text(str);
    }
    
}

cartridge("A-maze-ing");