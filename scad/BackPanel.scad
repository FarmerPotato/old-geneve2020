s=2.54; // standard pitch of pin headers

module din5(c, str) {
    color("black"){
        translate(c){
            rotate([90,0,0]){
                difference() {
                    cube([15,15,0.5], center=true);
                    cylinder(center=true, r=6.5,h=1);
                    
                }
                difference()  {
                    cylinder(center=true, r=5.5,h=1);
                    rotate([0,0,0]) translate([3,0,0]) cube(center=true, [2,1,2]);
                    rotate([0,0,-45]) translate([3,0,0]) cube(center=true, [2,1,2]);
                    rotate([0,0,-90]) translate([3,0,0]) cube(center=true, [2,1,2]);
                    rotate([0,0,-135]) translate([3,0,0]) cube(center=true, [2,1,2]);
                    rotate([0,0,-180]) translate([3,0,0]) cube(center=true, [2,1,2]);
                }
                translate([0,10,0])
                text(size=ts, halign="center", str);
            }
        }
    }
}

// ps/2 jack
module mini_din6(c, str, clr) {

    translate(c){
        rotate([90,0,0]){
        color(clr){
            difference() {
                    cube([10,10,0.5], center=true);
                    cylinder(center=true, r=4,h=1);
                    
            }
            difference()  {
                cylinder(center=true, r=3.5,h=0.5);
                pr=.5; // pin hole radius
                rotate([0,0,  -10]) translate([2,0,0]) cylinder(center=true, r=pr, h=2);
                rotate([0,0, 40]) translate([2,0,0]) cylinder(center=true, r=pr, h=2);
                rotate([0,0,140]) translate([2,0,0]) cylinder(center=true, r=pr, h=2);
                rotate([0,0,190]) translate([2,0,0]) cylinder(center=true, r=pr, h=2);
                rotate([0,0,-65]) translate([2,0,0]) cylinder(center=true, r=pr, h=2);
                rotate([0,0,-115]) translate([2,0,0])cylinder(center=true, r=pr, h=2);
                translate([0,.7,0]) cube([1,2,2], center=true);
                translate([0,3,0]) cube([1.8,1,2], center=true);
                translate([2.5,-2.2,0]) cube([1,1,2], center=true);
                translate([-2.5,-2.2,0]) cube([1,1,2], center=true);
                
            }
        }
        translate([0,7,0])
        color("black")
        text(size=ts, halign="center", str);
        }
    }
}

module barrel_jack(c, str) {
    d_pin = 2.5;
    d_sleeve = 6.5;  // +1 gap around 5.5 barrel
    color ("black"){
        translate(c){
            rotate([90,0,0]){
                difference() {
                    cube([8,10,0.5], center=true);
                    translate([0,0.5,0])
                    cylinder(center=true, r=d_sleeve/2,h=1, $fn=16); // sleeve
                }
                translate([0,0.5,0])
                cylinder(center=true, r=d_pin/2,h=1, $fn=16); // pin
                translate([0,10,0])
                text(size=ts, halign="center", str);
            }
        }
    }
}

module screw_mount(){
    difference(){
        union(){
            cube(center=true, [s*3,s*3,.5]);
            cylinder(r=s, h=5, $fn=16);
        }
        cylinder(center=true, r=s/2, h=20, $fn=16);
    }
        
}

module dsub_screws(n){
    h=s*2;
    b=(n-1)/2;
    t=b+1;
    x1=t/2+0.5;
    x2=b/2+0.5;
    color("silver"){
        translate([-x1*s-6, 0]) screw_mount();
        translate([ x1*s+6, 0]) screw_mount();
    }
}

module dsub_outline(n){
    h=s*2;
    b=(n-1)/2;    // 12
    t=b+1;        // 13
    x1=t/2+0.5;
    x2=b/2+0.5;
    
    hull(){
        translate([-x1*s,   h/2, 0]) cylinder(r=s, h=5);
        translate([ x1*s,   h/2, 0]) cylinder(r=s, h=5);
        translate([-x2*s,-h/2, 0]) cylinder(r=s, h=5);
        translate([ x2*s,-h/2, 0]) cylinder(r=s, h=5);
    }
}

module pin(c){
    s = 2.54;
    color("gold")
    translate(c)
    cube(center=true, [s/3,s/3,12]);
}

module pinhole(c){
    translate(c)
    cylinder(center=true, r=s/2, h=15, $fn=12);
}


// rectangular pin grid. rows x cols
module pingrid(n,m)
{
    // offset to center it
    x=-(m-1)/2*s;
    y=-(n-1)/2*s;
    
    for (i = [0:m-1]){
        for (j = [0:n-1]){
            pin([x+i*s,y+j*s]);
        }
    }
}


// the other half of pingrid (subtract this)
module pinhole_grid(n,m){
    // offset to center it
    x=-(m-1)/2*s;
    y=-(n-1)/2*s;
    
    for (i = [0:m-1]){
        for (j = [0:n-1]){
            pinhole([x+i*s,y+j*s]);
        }
    }
}


module dsub_female(n,clr){
    h=s*2;
    b=(n-1)/2;    // 12
    t=b+1;        // 13
    x1=t/2-0.5;   // 6
    x2=b/2-0.5;   // 5.5
    color(clr)
    difference(){
        scale([.95,.9,1])
        dsub_outline(n);
        for (i = [-x1:1:x1]) {
          pinhole([i*s, s/2,0.25]);
        }
        for (i = [-x2:1:x2]) {
          pinhole([i*s,-s/2,0.25]);
        }
    }
}

// hd has 3 rows of pins
module dsub_hd_female(n,clr){
    h=s;
    t=n/3;
    x1=(t-1)/2;
    color(clr)
    difference(){
        scale([.95,.9,1])
        dsub_outline(n*2/3-1);
        for (i = [-x1:1:x1]) {
          pinhole([i*s, h,0.25]);
          pinhole([(i+.5)*s,0,0.25]);
          pinhole([i*s,-h,0.25]);
        }
    }
}

// shroud and pins
module dsub_male(n,clr){
    h=s*2;
    b=(n-1)/2;    // 12
    t=b+1;        // 13
    x1=t/2-0.5;   // 6
    x2=b/2-0.5;   // 5.5
    color(clr)
    difference(){
        dsub_outline(n);
        scale([.90,.85,2]) // please adjust x for n
        dsub_outline(n);
    }
    for (i = [-x1:1:x1]) {
      pin([i*s, s/2,0]);
    }
    for (i = [-x2:1:x2]) {
      pin([i*s,-s/2,0]);
    }
}

module db25_male(c,str){
    translate(c){
        dsub_male(25,clr);
        
        color("black") translate([0,10,0]) text(size=ts, halign="center", str);
    }
}

module db25_female(c, str, clr){
    translate(c){
        rotate([90,0,0]){
            dsub_female(25, clr);
            dsub_screws(25);

            color("black") translate([0,10,0]) text(size=ts, halign="center", str);
        }
    }
}

module db9_female(c, str, clr){
    translate(c){
        rotate([90,0,0]){
            dsub_female(9, clr);

            color("black") translate([0,10,0]) text(size=ts, halign="center", str);
        }
    }
}


module db9_male(c,str,clr){
    translate(c){
        rotate([90,0,0]){
            dsub_male(9,clr);
            color("black") translate([0,10,0]) text(size=ts, halign="center", str);
        }
    }
}

// rgb connector
module de15_female(c,str,clr){
    translate(c){
        rotate([90,0,0]){
            dsub_female(15,clr);
            dsub_screws(15);

            color("black") translate([0,10,0]) text(size=ts, halign="center", str);
        }
    }
}

module vga(c,str,clr){
    translate(c){
        rotate([90,0,0]){
            dsub_hd_female(15,clr);
            dsub_screws(9);

            color("black") translate([0,10,0]) text(size=ts, halign="center", str);
        }
    }
}

module dp_shape(){
    x = 7.5/2;
    y = 4.6/2;
    ch = 1;
    linear_extrude(2)
    polygon( points = [ 
        [x, y], [x, -y+ch], [x-ch, -y],
        [-(x-ch), -y], [-x, -y+ch], [-x, y]
        ]);
}

module minidp(c, str, clr){
    translate(c) 
        rotate([90,0,0]){
            translate([0,2,0]) color(clr) difference(){
                dp_shape();
                translate([0,0,-2])
                scale([.9,.9,3]) dp_shape();
            }
            color("black") translate([0,10,0]) text(size=ts, halign="center", str);
    }
    
    
}

// 2xn keyed male header shroud
module shroud(n)
{
    // width 8
    x=(n+0.5)*s;
    y=2.5*s;
    difference(){
        union(){
            translate([-x/2,-y/2,0]) cube([x,y,5]);
            translate([0,3.5,2.5])
                cube(center=true, [2.5*s,0.5*s,5]);
        }
        translate([0, 3, 0])
        
            cube(center=true, [2*s,  .5*s, 20]);
        cube(center=true, [n*s,  2*s,  20]);
    }
}

///////////////////////////////////////////////////

module dvi(c, str, clr){
    n=3;
    m=11;
    
    x=m*s;
    y=n*s;

    translate(c){
        rotate([90,0,0]){
            color(clr)
            difference(){
                translate([-x/2,-y/2,0]) cube([x,y,5]);
                for (i = [-5:1:2]) {
                    for (j = [-1:1:1]) {
                        translate([i*s,j*s,0])
                        cube(center=true, [2,2,11]);
                    }
                }
                translate([4*s,0,0])
                scale([.6,.6,1]){
                    for (i = [-1:2:1]) {
                        for (j = [-1:2:1]) {
                            translate([i*s,j*s,0])
                            cube(center=true, [2,2,11]);
                        }
                        cube(center=true, [8,1,11]);
                    }
                }
            }
            dsub_screws(15);
            color("black") translate([0,10,0]) text(size=ts, halign="center", str);
        }
    }
}

module conn_female(c,m,n,str,d=3*s){
    
    translate(c){
        rotate([90,0,0]){
            h=s*m;
            color("blue")
            difference(){
                cube(center=true, [n*s,m*s,d]);
                x=(n-1)/2;
                y=(m-1)/2;
                for (i = [-x:1:x]) {
                    for (j = [-y:1:y]) {
                        translate([i*s,j*s,0])
                        cube(center=true, [s/2,s/2,d+.5]);
                    }
                }
            }
            color("black") translate([0,10,0]) text(size=ts, halign="center", str);
        }
    }
}

module phono_jack(c, str, clr){
    translate(c){
        rotate([90,0,0]){
            
            difference() {
                color(clr)
                    cylinder(center=true, r=4,h=5);
                    
                cylinder(center=true,r=2.5, h=6, $fn=16);
                 
            }  
            color("gold")
            difference() {
                cylinder(center=true,r=2.5, h=4, $fn=16);
                cylinder(center=true,r=1.75, h=8, $fn=16);
            }
            color("black") translate([-10,0,0]) text(size=ts, halign="center", valign="center", str);
 
        }
    }
}

module cylinder2(r1,r2,h){
    difference(){
        cylinder(center=true,r=r1,h=h,$fn=16);
        cylinder(center=true,r=r2,h=h+2,$fn=16);
    }
}

module rca_jacks(c, str){
    translate(c){
        rotate([90,0,0]){
            scale([1,1,1]){
                //color("black") cube(center=true,[12,26.5,1]);
                
                translate([0,7,0]){
                    color("gold") cylinder2(4.5, 4, h=10);
                    color("red")  cylinder2(4, 2, h=10);
                    color("black") translate([7,-2,0]) text(size=ts, halign="center", "R"); 
                }
                translate([0,-5,0]){
                    color("gold") cylinder2(4.5, 4, h=10);
                    color("white") cylinder2(4, 2, h=10);
                    color("black") translate([7,-2,0]) text(size=ts, halign="center", "L"); 
                }
            }
        }
    }
}

module sdcard(c, str, clr){
    h=2;
    w=30;
    translate(c) {
        translate([0,8,0])
        difference(){        
            union(){
                difference(){        
                    color(clr) cube(center=true,[w,w,h]);
                    scale([.9,1.1,0.9]) cube(center=true, [w,w,h]);
                }
                scale([.9,1.1,0.9]) color("black") cube(center=true, [w,w,h]);
            }
            translate([0,-27,0]) color("black") cylinder(center=true, r=18,h=2*h); // no color = leaves the cube yellow
        
        }
        translate([0,5,7]) rotate([90,0,0])
        color("black") translate([0,-2,5]) text(size=ts, halign="center", valign="center", str);
 
        
    }
}


module pio(c,str){
    translate(c){
        rotate([90,0,0]){
            
            h=s*2;
            color("blue"){
                shroud(8);
            }
            pingrid(2,8);
            color("black") translate([0,10,0]) text(size=ts, halign="center", str);
        }
    }
}

module tipi(c,str){
    translate(c){
        rotate([90,0,0]){
            
            h=s*2;
            color("blue"){
                shroud(5);
            }
            pingrid(2,5);
            color("black") translate([0,10,0]) text(size=ts, halign="center", str);
        }
    }
}

module hexbus(c,str){
    translate(c){
        rotate([90,0,0]){
            
            h=s*2;
            color("blue"){
                shroud(4);
            }
            pingrid(2,4);
            color("black") translate([0,10,0]) text(size=ts, halign="center", str);
        }
    }
}


ts=3; // text size
g=20; // regular grid spacing

// Build it
module back_panel(clr){
    color(clr) { cube([370,1,30]); } // motherboard 370
    translate([-7.5,0,0]){
        barrel_jack([ 1*g,0,11], "5VDC");
        
        // Suppose its same size as stacked PS/2
        // 5,7,7,7
        translate([0,0,1]){
            phono_jack ([ 2.*g,0, 6], "Mic", "pink");
            phono_jack ([ 2.*g,0,15], "Out", "palegreen");
            phono_jack ([ 2.*g,0,24], "In", "blue");
        }
        rca_jacks  ([ 2.75*g,0,14]);
        din5       ([3.75*g,0,13], "MIDI In");
        din5       ([4.65*g,0,13], "MIDI Out");

        sdcard     ([6.125*g,0,7], "SD1", "silver");
        sdcard     ([6.125*g,0,17], "SD2", "silver");
//        sdcard     ([6.125*g,0,27], "SD3", "silver");
        
        translate([0,0,-5]){
            //de15_female([ 6.20*g,0,15], "RGB", "dimgray");
            //dvi        ([8.4*g,0,15], "DVI", "white");
            vga        ([8*g,0,15], "RGB", "blue");
            //minidp     ([9.25*g,0,15], "DP", "white");
            
            db25_female([10.95*g,0,15], "RS232", "dimgray");
            pio        ([13*g,0,15], "PIO");
            hexbus     ([14*g,0,15], "HexBus");
            tipi       ([14.85*g,0,15], "π");
        }
        
        // Stacked module is 13.8, 28.5. Din are 13.5 apart
        d2=15.3;
        mini_din6  ([ 15.75*g,0,8+d2], "", "MediumSeaGreen");
        mini_din6  ([ 15.75*g,0,8], "Kbd/Mouse", "Plum");

        translate([0,0,-5]){
        db9_male   ([16.75*g,0,15], "Joy1", "black");
        db9_male   ([18*g,0,15], "Joy2", "black");
        }
}
    // vga? dvi-I carries vga analog on 4 pins
    // tipi
}

//back_panel("darkgray");
//color("white") { cube([350,1,30]); } // console 370
//db25_female([10.*g,0,15], "RS232", "yellow");
