include <BackPanel.scad>

s=2.54;

// module size
modw=80;  // x
modd=130; // y
modh=1.6; // pcb thickness

module board(){
    color("green") cube([modw,modd,modh]);
    // a little silk screen
    color("white") {
        t=0.5;
        z=modh+.1;
        cube([modw,t,z]);
        cube([t,modd,z]);
        translate([0,modd-t,0]) cube([modw,t,z]);
        cube([.5,modd,z]);
        translate([modw-t,t,0]) cube([t,modd,z]);
    }
}

// 2x8 box header
module bus_connector1(c){
    translate(c)
        rotate([90,0,0]){
            color("blue"){
                shroud(8);
            }
            pingrid(2,8);
            //color("black") translate([0,10,0]) text(size=ts, halign="center", str);
        }
}

// 2xn stacking header for shield

module stacking_header(m,n){
    translate([s,4*s,modh/2]){
        rotate([0,0,90]) pingrid(m,n);
        rotate([90,0,90]) conn_female([0,5,0],m,n,"");
    }
}

// pair of 2x8 headers - max 32 signals
// or are they two copies of 2x8
module bus_connector(c){
    translate([0,s*10,0]){
        stacking_header(2,8);
        translate([modw-2*s,0,0])    stacking_header(2,8);
    }
}


p = s/4;  // pin dimension

module cpupin(){
    
    color("silver") translate([-p/2,0,0]) cube([p,p/2,2.5]);
}


module dip(sz=40,w=15.6,str="DIP40",ps=2.54) {
    h = 3;
    ts= ps<2 ? 1 : 2;
    translate([0,0,.5]){
        color("#222")
            difference(){
                cube([sz/2*ps,w,h]);
                translate([-.1,(w-s)/2,h-.5]) cube([s,s,1.1]);
            }
        // index mark
        color("#444") translate([0,(w-s)/2,h-.5]) cube([s,s,.6]);
        // label
        color("white")
            translate([ps*sz/4,(w-ts)/2,h]) text(halign="center",valign="baseline",str, size=ts);
    }
    // pins
    for (i=[0:(sz/2-1)]){
        translate([(i+0.5)*ps,,0]) rotate([0,0,180]) cpupin();
        translate([(i+0.5)*ps,w,0]) cpupin();
    }
}

module smpin(){
    color("silver") translate([-p/2,p/2,0]) cube([p,s/2,p/2]);
}

module smpins(sz=20,w=7.8,ps=1.27){
    
    for (i=[0:(sz/2-1)]){
        translate([(i+0.5)*ps,,0]) rotate([0,0,180]) smpin();
        translate([(i+0.5)*ps,w,0])   smpin();
    }
}

// common width of DIP (except DIP24 narrow)
// (we're more likely to use a 24-pin ROM than an ATMEGA)
// Yamaha 64 pin is 32*ps x 17.0 ps=1.778
function guesswidth(sz) = sz<64 ? sz<24 ? 8 : 15.6 : 17.0;


module dip14(str){
    dip(14,guesswidth(14),str);
}

module dip16(str){
    dip(16,guesswidth(14),str);
}

module dip20(str){
    dip(20,guesswidth(14),str);
}

module sop(sz=20,w=4,str){
    // cheat
    dip(sz,w,str,1.27);
    smpins(sz,w);
}

module soic14(str=""){
    // actual SOIC (14) 8.65 mm × 3.91 mm
    // for LVC125A
    // model 8.89 mm x 3.91 mm
    sop(14,3.91,str);
}

module soic20(str=""){
    // actual 12.80 mm × 7.50 mm
    // for LVC245ADW
    // model 12.70 mm x 7.50 mm
    sop(20,7.5,str);
}

module so20(str=""){
    // actual 7.50 mm x 5.30 mm
    // for LVC245ANS
    // model 12.70 mm x 7.50 mm
    sop(20,5.6,str);
}

module sop14(str=""){
    // actual 10.30mm x 5.30 mm
    // for LVC125A
    // cheat
    sop(14,5.3,str);
}


module sop16(str=""){
    // cheat
    sop(16,3.91,str);
}

module sop20(str=""){
    // cheat
    sop(20,3.91,str);
}

module dip40(str){
    dip(40,str);
}

// v9938 size, not 9900 size
module dip64(str){
    dip(64,17.0,str,1.778);
}

module exec9995(c){
    
    translate(c) {
        board();
        translate([10,50,2]) rotate([0,0,90]) color("white") text("I/O processor", size=6);
        
        translate([10,25,1]) dip(40,"TMS9995NL");
        
        bus_connector([modw/2,5,5]);
    }
}


module vdp9958(c){
    
    translate(c) {
        board();
        translate([10,50,2]) rotate([0,0,90]) color("white") text("VDP9958", size=6);
        
        rotate([0,0,0]){
            translate([10*s,10*s,1]){
        translate([0,10*s,0]) dip16("4464");
        translate([0,0,0]) dip64("MSX V9958 Yamaha");
            }
        }
        bus_connector([modw/2,5,5]);
    }
}

//translate([0,0,0])  exec9995([0,0,0]);
translate([0,0,0])  vdp9958([0,0,0]);

//sop14("LVC125A");
//translate([40,0,0]) soic20("LVC245A");
//translate([60,0,0]) so20("LVC245A");
