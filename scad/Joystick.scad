
module arcade_button(c, clr){
        translate(c) color(clr) {
            difference(){
                cylinder(r=12,h=3);
                cylinder(center=true,r=10,h=7);
            }
            translate([0,0,3]) difference(){
                cylinder(center=true,r=9.5,h=5);
                translate([0,0,40]) sphere(r=40, $fn=24);
            }
        }
}

module torus2(r1, r2)
{
    rotate_extrude() translate([r1,0,0]) circle(r2);
}

module joystick(c, clr){
    color(clr) translate(c) rotate([180,0,0]) scale(.5) translate([0,0,-25]) {
        scale([1,1,.65])
        difference(){
            union(){
                torus2(10,10); 
                sphere(15);
            }
            translate([0,0,-56]) sphere(50);
        }        
        cylinder(r=9,h=20);
        
        translate([0,0,30])
            difference(){
                scale([1,1,.65]) sphere(r=24);
                translate([0,0,30]) cube(60, center=true);
            }
    }
}

module knob(c, clr, str){
    color(clr) translate(c) {
        difference(){
            linear_extrude(height=6 , center=false, scale=.9) circle(r=10, $fn=10);
            translate([0,0,19]) sphere(14);
        }
        color("black") translate([0,-13,0]) text(size=5, halign="center", valign="center", str);
    }
}

module game_controls(){
    joystick([0,0,0], "gray");
    arcade_button([-20,30,0], "red");
    arcade_button([20,30,0], "blue");
}
//game_controls();
module knobs5(){
    for (i=[1:1:4]){
            knob([19*(i-3),70,0], "gray", str(i));
        }
}
    //knobs5();