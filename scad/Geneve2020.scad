
include <BackPanel.scad>
include <Joystick.scad>

// overall size
// motherboard 370x144

width = 390;
depth1 = 154; // front to middle
depth2= 150; // middle to back
depth=depth1+depth2;

height = 20; // at front edge
part=10;  // parting mold plane height
r = 10;   // corner radius
pitch=10; // top piece
pitch1=45;


 // Cylinder with pitch angle d1,d2 on each h1,h2
module pillender(r, h1, h2, d1, d2){
    
    s1 = 1-(h1*tan(d1)/(2*r));
    s2 = 1-(h2*tan(d2)/(2*r));
    
    // upside down bottom part

    translate([0,0,h1])
    scale([1,1,-1])
    linear_extrude(height=h1 , center=false, scale=s1) circle(r=r);
    // top part
    translate([0,0,h1])
    
    linear_extrude(height=h2 , center=false, scale=s2) circle(r=r);  
}

module case_body(lidOff){
    difference(){
        hull(){
            // front corners
            translate([r,r,0])  pillender(r, part, height-part, pitch1, pitch);
            translate([width-r,r,0]) pillender(r, part, height-part, pitch1, pitch);
            // middle corners
            d=depth1;
            translate([r,d,0])  pillender(r, part, height*2-part, pitch1, pitch);
            translate([width-r,d,0]) pillender(r, part, height*2-part, pitch1, pitch);
            // back corners
            translate([r,depth-r,0]) pillender(r, part, height*2-part, pitch1, pitch);
            translate([width-r,depth-r,0]) pillender(r, part, height*2-part, pitch1, pitch);
        }    
        // cutout area for back panel
        translate([7.5,depth-10,10])
        cube([width-20,10,26]);
        lidd=depth2+4;
        if (lidOff){
        // cutout lid if given
        
        translate([10,depth-lidd-2.5,35]) cube([width-20,lidd,10]);
        
        
        // cutout interior cavity
        translate([10,depth-lidd,5]) cube([width-20,lidd,50]);
        }
    }
}

// keycaps are in w=19 box, 12 on top, 18 bottom, height 6

module keycap(c){
    r = 1;
    o = 8;
    h = 6;
    points = [ [c.x-o,c.y-o,c.z], [c.x-o,c.y+o,c.z], [c.x+o,c.y-o,c.z], [c.x+o,c.y+o,c.z] ];
    hull(){
        for (p = points){
            translate(p) cylinder(r=1, h=6);
        }
    }
}

// this is broken
function expand(r, names, list) = len(r)==0 
    ? list 
    : [ 
        [ [r[0], 1, names[0] ], 
          expand(select(r,     [1:1:(len(r)-1)]), 
                 select(names, [1:1:(len(names)-1)]),
                 list) ] 
        ];

module keyboard(){
    m = 20;
    n = 20;
    // first row x, widths, letter
    rows = [ 
        [ [3,1,"Ctrl"], [4,1,"Fctn"], [5,8,""],  [13,1,"C"], [7,1,"V"] ], 
        [ [3,3,"Shift"], [6,1,"Z"], [7,1,"X"],  [8,1,"C"], [9,1,"V"] ],
        [ [3,2,"Caps Lock"], [5,1,"A"], [6,1,"S"], [7,1,"D"], [8,1,"F"] ], 
        [ [3,3,"Shift"], [6,1,"Z"], [7,1,"X"],  [8,1,"C"], [9,1,"V"] ], 
        [ [3,3,"Shift"], [6,1,"Z"], [7,1,"X"],  [8,1,"C"], [9,1,"V"] ]  
    ];
    for (i = [0:1:4]) {
        for (k = rows[i]){
            o=(i%2)/2;
            keycap2([m*(o + k.x + (k[1]-1)/2), n*(i+0.25), height*2], k[1], k[2]);
        }
    }
    
}


// Second attempt. Go with this simpler version.

module keycap2(c,w=1,str="",ts=1){
    r = 1;
    o = 8;
    h = 6;
    c1 = [c.x + (w-1)/2*20, c.y, c.z];
    translate(c1){ 
        color("Lightgray")        cube([16*w,16,12], center=true);
        translate([-w,-2,5.7]) color("black") linear_extrude(1) text(text=str, size=8*ts, halign="center", valign="baseline");
    }
}

// str=symbol, optional 2nd symbol for above
module keycap2_btt(c,w=1,str="",ts=1){
    r = 1;
    o = 8;
    h = 6;
    c1 = [c.x + (w-1)/2*20, c.y, c.z];
    translate(c1){ 
        color("ivory")        cube([16*w,16,12], center=true);
        translate([-w,-0,5.9]) color("black") linear_extrude(1) text(text=str, size=(10-2*len(str))*ts, halign="left", valign="center", direction="btt", font="Liberation Sans");
    }
}

// Third kind. Tapers in, too much on wide keys.
// Must figure out how much to scale, based on width
module keycap3(c,w=1,str="",ts=1){
    r = 1;
    o = 8;
    h = 6;
    c1 = [c.x + (w-1)/2*20, c.y, c.z];
    translate(c1){ 
        color("ivory")       linear_extrude(height=6, center=false, scale=.8) square([16*w,16], center=true);
        translate([-w,-2,5.7]) color("black") text(text=str, size=8*ts, halign="center", valign="baseline");
    }
}

module keyboard_row(xs, y, z, names){
    for (i = [0:1:len(xs)-1]){
        keycap2_btt( [ xs[i], y, z ], 1, names[i]);
        }
}

module keyboard2()
{
    // grid spacing
    m = 20;
    n = 20;
    // regular keys
    xs = [ 
        [ for (i = [3:12]) m*(i+0.5) ],
        [ for (i = [3:13]) m*i ],
        [ for (i = [2:14]) m*(i+0.5) ],
        [ for (i = [1:13]) m*i ]
        ]; 
    ys = [ for (i = [0:4]) n*(i+2)];
    names = [
        [ "Z", "X", "C", "V", "B", "N", "M", ",>", ".<", "/?" ],
        [ "A", "S", "D", "F", "G", "H", "J", "K", "L", ";:", "'\"" ],
        [ "Q", "W", "E", "R", "T", "Y", "U", "I", "O", "P", "]}", "[{", "\\|" ],
        [ "`~", "1!", "2@", "3#", "4$", "5%", "6^", "7&", "8*", "9)", "0(", "-_", "=+" ]
    ];
    z = height+4;
    for (i = [0,1,2,3]) {
        keyboard_row( xs[i], ys[i], z, names[i]);
    }
    // big keys
    pt = 0.7;
    // left side
    keycap2( [1.25*m, 1*n, z], 1.0, "Caps", pt*.8);
    keycap2( [2.5*m, 1*n, z], 1.5, "Fn", pt);
    keycap2( [4.0*m, 1*n, z], 1.5, "Ctrl", pt);
    // space bar
    keycap2( [4.75*m, 1*n, z], 7.5, "");

    keycap2( [1.*m, 2*n, z], 2.5, "Shift", pt);
    keycap2( [1.*m, 3*n, z], 2, "Space", pt);
    keycap2( [1.*m, 4*n, z], 1.5, "Tab", pt);
    // right side
    //keycap2( [11.75*m, 1*n, z], 1.5, "Any", pt);
    keycap2( [11.5*m, 1*n, z], 1.5, "Ctrl", pt);
    keycap2( [13.*m, 1*n, z], 1.5, "Fn", pt);
    keycap2( [13.5*m, 2*n, z], 2.5, "Shift", pt);
    keycap2( [13.85*m, 3*n, z], 2.0, "Enter", pt);
    keycap2( [13.9*m, 5*n, z], 2.0, "Backspace", 0.5);
   
    // Joystick
    //translate([17.5*m, 3.5*n, z]) game_controls();
    for (i=[1:1:5]){
        knob([(13.5+i)*m, fy, z], "gray", str(i));
    }
// Editing keys
    translate([0.3*m,0.0]) {
    keycap2( [16*m, 4*n, z], 1, "Del", pt*.8);   // f1
    keycap2( [17*m, 4*n, z], 1, "End", pt*.8);
    keycap2( [18*m, 4*n, z], 1, "PgDn", pt*.8);  // f6
    
    keycap2( [16*m, 5*n, z], 1, "Ins", pt*.8);   // f2
    keycap2( [17*m, 5*n, z], 1, "Home", pt*.8);
    keycap2( [18*m, 5*n, z], 1, "PgUp", pt*.8);  // f4

// arrow keys
    keycap2( [17*m, 2*n, z], 1, "↑", 1.2);
    keycap2( [16*m, 1*n, z], 1, "←", 1.2);
    keycap2( [18*m, 1*n, z], 1, "→", 1.2);
    keycap2( [17*m, 1*n, z], 1, "↓", 1.2);
    }
    
    // F keys
    fy = 6.5*n;
    keycap2( [1*m, fy, z], 1, "Esc", pt );
    
    keycap2( [2*m, fy, z], 1, "F1", pt);
    keycap2( [3*m, fy, z], 1, "F2", pt);
    keycap2( [4*m, fy, z], 1, "F3", pt);
    keycap2( [5*m, fy, z], 1, "F4", pt);
    
    keycap2( [6*m, fy, z], 1, "F5", pt);
    keycap2( [7*m, fy, z], 1, "F6", pt);
    keycap2( [8*m, fy, z], 1, "F7", pt);
    keycap2( [9*m, fy, z], 1, "F8", pt);
    
    keycap2( [10*m, fy, z], 1, "F9", pt);
    keycap2( [11*m, fy, z], 1, "F10", pt);
    
    keycap2( [12*m, fy, z], 1, "F11", pt);
//    keycap2( [12*m, fy, z], 1, "Print", pt*.7);
//    keycap2( [12*m, fy+5, z], 1, "Turbo", pt*.7);
    keycap2( [13*m, fy, z], 1, "F12", pt);
//    keycap2( [13*m, fy, z], 1, "Break", pt*.7);
//    keycap2( [13*m, fy+5, z], 1, "NMI", pt*.7);
    
}

module cartridge(str, clr1, clr2, clr3){
    // case, sticker, text
    color(clr1){
    cube([125,75,10]);
    cube([125,35,20]);
    }
    rotate([90,0,0]){
       color(clr2) translate([2,2,0]) cube([120,16,.25]);
       color(clr3) translate([62.5,5,.25]) text(str,halign="center");
    }
}

// Never mind this
module cartridges3(){
    translate([25,-50,0]) {
    translate([0,depth-50,height*3.8 ]) rotate([-90,0,0]) cartridge("Tunnels of Doom", "beige", "red", "yellow");
    translate([0,depth-75,height*3.8 ]) rotate([-90,0,0]) cartridge("Parsec 2020", "DarkSlateGray", "purple", "yellow");
    translate([0,depth-100,height*3.8 ]) rotate([-90,0,0]) cartridge("Dragon's Lair", "DarkSlateGray", "black", "orange");
    }
}


// Do it
translate([100,0,0]){
    color("cyan") case_body(lidOff=false);
    
    translate([0,0,0]) rotate([6,0,0]) keyboard2();

    translate([width-7,depth-9,6]) rotate([0,0,180]) back_panel("lightgray");
    cartridges3();

}
