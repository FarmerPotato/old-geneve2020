EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 6950 6950 0    197  ~ 39
Geneve 2020 Block Diagram
Text Notes 5200 1300 3    79   ~ 0
16-Bit Backplane and CRU Bus
Text Notes 5800 1300 0    100  ~ 0
99105 CPU
Text Notes 5750 1700 0    50   ~ 0
9901 Interrupt Controller\n
Text Notes 5750 2050 0    50   ~ 0
BIOS\n128K EPROM 128K SRAM
Text Notes 5750 4850 0    50   ~ 0
VDP1 9958
Text Notes 5750 4600 0    50   ~ 0
VDP2 F18A mk2
Text Notes 5750 3950 0    50   ~ 0
FM Sound Card x2
Text Notes 5750 4200 0    50   ~ 0
SN76489 / Speech
Text Notes 5750 5150 0    50   ~ 0
Cartridge ROM
Text Notes 5750 5650 0    50   ~ 0
P-Box Interface
Text Notes 5750 2350 0    50   ~ 0
9902 Serial, Parallel \nHexBus I/O
Text Notes 5750 5400 0    50   ~ 0
Cartridge GROM
Text Notes 5750 2600 0    50   ~ 0
16-bit Expansion (many)
Wire Notes Line
	6800 1450 6800 1100
Wire Notes Line
	6800 1100 5700 1100
Wire Notes Line
	5700 1100 5700 1450
Wire Notes Line
	6800 1450 5700 1450
Wire Notes Line
	6800 1550 5700 1550
Wire Notes Line
	5700 1800 6800 1800
Wire Notes Line
	5700 1550 5700 1800
Wire Notes Line
	6800 1550 6800 1800
Wire Notes Line
	6800 1850 5700 1850
Wire Notes Line
	6800 2100 5700 2100
Wire Notes Line
	5700 1850 5700 2100
Wire Notes Line
	6800 1850 6800 2100
Wire Notes Line
	6800 2150 5700 2150
Wire Notes Line
	6800 2400 5700 2400
Wire Notes Line
	5700 2150 5700 2400
Wire Notes Line
	6800 2150 6800 2400
Wire Notes Line
	6800 2450 5700 2450
Wire Notes Line
	6800 2700 5700 2700
Wire Notes Line
	5700 2450 5700 2700
Wire Notes Line
	6800 2450 6800 2700
Wire Notes Line
	6800 2750 5700 2750
Wire Notes Line
	6800 3000 5700 3000
Wire Notes Line
	5700 2750 5700 3000
Wire Notes Line
	6800 2750 6800 3000
Wire Notes Line
	6800 3050 5700 3050
Wire Notes Line
	5700 4450 6550 4450
Wire Notes Line
	6550 4450 6550 4650
Wire Notes Line
	6550 4650 5700 4650
Wire Notes Line
	5700 4650 5700 4450
Wire Notes Line
	6550 5500 5700 5500
Wire Notes Line
	5700 5500 5700 5700
Wire Notes Line
	5700 5700 6550 5700
Wire Notes Line
	6550 5700 6550 5500
Wire Notes Line
	6550 5250 5700 5250
Wire Notes Line
	5700 5250 5700 5450
Wire Notes Line
	5700 5450 6550 5450
Wire Notes Line
	6550 5450 6550 5250
Wire Notes Line
	6550 4700 5700 4700
Wire Notes Line
	5700 4700 5700 4900
Wire Notes Line
	5700 4900 6550 4900
Wire Notes Line
	6550 4900 6550 4700
Wire Notes Line
	6550 3800 5700 3800
Wire Notes Line
	5700 3800 5700 4000
Wire Notes Line
	5700 4000 6550 4000
Wire Notes Line
	6550 4000 6550 3800
Wire Notes Line
	6550 4050 5700 4050
Wire Notes Line
	5700 4050 5700 4250
Wire Notes Line
	5700 4250 6550 4250
Wire Notes Line
	6550 4250 6550 4050
Wire Notes Line
	6550 5000 5700 5000
Wire Notes Line
	5700 5000 5700 5200
Wire Notes Line
	5700 5200 6550 5200
Wire Notes Line
	6550 5200 6550 5000
Text Notes 7150 3700 0    50   ~ 0
Audio\n  Out
Wire Notes Line
	7400 3500 7450 3500
Wire Notes Line
	7450 3500 7450 3550
Wire Notes Line
	7450 3550 7400 3550
Wire Notes Line
	7400 3600 7450 3600
Wire Notes Line
	7450 3600 7450 3650
Wire Notes Line
	7450 3650 7400 3650
Text Notes 7150 4750 0    50   ~ 0
Video\nOut
Wire Notes Line
	7400 4500 7450 4500
Wire Notes Line
	7450 4500 7450 4650
Wire Notes Line
	7450 4650 7400 4650
Wire Notes Line
	7400 4450 7400 4900
Wire Notes Line
	7400 4750 7450 4750
Wire Notes Line
	7450 4750 7450 4850
Wire Notes Line
	7450 4850 7400 4850
Connection ~ 5450 5100
Wire Bus Line
	5450 5100 5450 5350
Connection ~ 5450 4800
Wire Bus Line
	5450 4800 5450 5100
Connection ~ 5450 4550
Wire Bus Line
	5450 4550 5450 4800
Wire Notes Line
	7400 5000 7400 5450
Text Notes 7150 5400 0    50   ~ 0
ROM\n Cart\n Port\nGROM
Text Notes 6700 4050 1    50   ~ 0
Digital
Connection ~ 5450 1700
Wire Bus Line
	5450 1700 5450 1300
Connection ~ 5450 1950
Wire Bus Line
	5450 1950 5450 1700
Connection ~ 5450 2250
Wire Bus Line
	5450 2250 5450 1950
Connection ~ 5450 2550
Wire Bus Line
	5450 2550 5450 2250
Wire Bus Line
	5700 5350 5450 5350
Wire Bus Line
	5700 5100 5450 5100
Wire Bus Line
	5700 4800 5450 4800
Wire Bus Line
	5700 4550 5450 4550
Wire Bus Line
	5700 2550 5450 2550
Wire Bus Line
	5700 2250 5450 2250
Wire Bus Line
	5700 1950 5450 1950
Wire Bus Line
	5700 1700 5450 1700
Wire Bus Line
	5700 1300 5450 1300
Wire Bus Line
	5700 4150 5450 4150
Wire Notes Line
	7400 5500 7400 5700
Text Notes 7150 5700 0    50   ~ 0
SCSI\nCable
Text Notes 6900 3600 0    50   ~ 0
DAC
Wire Bus Line
	7150 4550 6550 4550
Wire Bus Line
	7150 4800 6550 4800
Wire Bus Line
	7150 5100 6550 5100
Wire Bus Line
	7150 5350 6550 5350
Wire Bus Line
	7150 5600 6550 5600
Wire Notes Line
	7150 2150 7400 2150
Wire Notes Line
	7400 2150 7400 2350
Wire Notes Line
	7400 2350 7150 2350
Wire Notes Line
	7150 2350 7150 2150
Wire Notes Line
	6900 3450 7050 3450
Wire Notes Line
	7150 3250 7400 3250
Wire Notes Line
	7400 3000 7150 3000
Wire Bus Line
	6800 3150 7150 3150
Wire Bus Line
	6800 2250 7150 2250
Text Notes 4050 5650 0    50   ~ 0
Device Select\n0 VDP1\n1 VDP2\n2 FM1\n3 FM2\n4 Speech/SN76489\n5 Cart ROM\n6 GROM\n7 P-Box\n
Text Notes 4050 2750 0    50   ~ 0
0000 9901\n\n1000 Master DSR/Disk\n1100 Disk\n1200\n1300 Parallel \n1340 Serial 1\n1380 Serial 2 \n1540 Serial 3\n1580 Serial 4\n1C00 SAMS\n…\n\n2000 9901 for BIOS\n2100 SD Card\n2200 HexBus\n2400 High-Speed Serial
Text Notes 4050 3700 0    50   ~ 0
CRU Parallel\n\n8-Bit Device x:\n8x00 Port 0\n8x02 Port 1\n8x04 Port 2\n8x06 Port 3\n\nA200 HexBus\nA400 High-Speed Serial\n
Text Notes 3950 4500 0    50   ~ 0
8-Bit Bus Lines\n<> D7-D0  Data Bus\n  > SS2-SS0 Device Select\n  > M1-M0 Port Select\n  > CSW* Write\n  > CSR*  Read\n <  READY
Text Notes 4000 1300 0    50   ~ 0
CRU Map\n\nMDOS/GPL\n
Text Notes 4000 2400 0    50   ~ 0
Reserved for BIOS
Text Notes 5200 3950 3    79   ~ 0
8-Bit Backplane
Wire Bus Line
	5700 5600 5450 5600
Wire Bus Line
	5700 3900 5450 3900
Wire Bus Line
	5700 3400 5450 3400
Text Notes 7150 3200 0    50   ~ 0
PS/2
Text Notes 7200 2300 0    50   ~ 0
I/O
Wire Notes Line
	6800 3500 5700 3500
Text Notes 5750 3450 0    50   ~ 0
DRAM Controller\n\nMemory Mapper\n\n16-to-8 Bit Bridge
Text Notes 5750 2900 0    50   ~ 0
DRAM 2, 8, or 32 MB
Wire Bus Line
	5700 2850 5550 2850
Wire Bus Line
	5550 3150 5700 3150
Wire Bus Line
	5450 3250 5700 3250
Wire Notes Line
	5700 3050 5700 3500
Wire Notes Line
	6800 3500 6800 3050
Text Notes 6700 3300 2    50   ~ 0
FPGA
Wire Bus Line
	5450 2550 5450 3250
Wire Bus Line
	5450 3400 5450 3900
Wire Bus Line
	5550 2850 5550 3150
Connection ~ 5450 5350
Wire Bus Line
	5450 5350 5450 5600
Connection ~ 5450 3900
Connection ~ 5450 4150
Wire Bus Line
	5450 4150 5450 4550
Wire Bus Line
	5450 3900 5450 4150
Text Notes 6750 4300 0    50   ~ 0
Analog
Wire Bus Line
	6550 3950 6650 3950
Wire Bus Line
	6650 4500 6550 4500
Wire Bus Line
	6550 3850 6650 3850
Wire Bus Line
	6650 3850 6650 3500
Wire Bus Line
	6650 4500 6650 4350
Wire Bus Line
	6650 4200 6550 4200
Wire Bus Line
	6650 3950 6650 4100
Wire Bus Line
	6650 4100 6550 4100
Wire Bus Line
	6950 3450 6950 3300
Connection ~ 6650 4350
Wire Bus Line
	6650 4350 6650 4200
Wire Notes Line
	7400 3700 7450 3700
Wire Notes Line
	7450 3700 7450 3750
Wire Notes Line
	7450 3750 7400 3750
Wire Notes Line
	7400 3450 7400 3800
Wire Notes Line
	7150 3450 7150 3800
Wire Bus Line
	7050 3750 7050 4350
Wire Bus Line
	6950 3300 6800 3300
Wire Bus Line
	7050 4350 6650 4350
Wire Notes Line
	7050 3450 7050 3700
Wire Notes Line
	7050 3700 6900 3700
Wire Notes Line
	6900 3700 6900 3450
Wire Bus Line
	7150 3600 7050 3600
Wire Bus Line
	7150 3750 7050 3750
Wire Bus Line
	7150 3500 7050 3500
Wire Notes Line
	7400 3800 7150 3800
Wire Notes Line
	7400 3450 7150 3450
Wire Notes Line
	7150 5700 7150 5500
Wire Notes Line
	7400 5500 7150 5500
Wire Notes Line
	7400 5700 7150 5700
Wire Notes Line
	7150 5450 7150 5000
Wire Notes Line
	7400 5450 7150 5450
Wire Notes Line
	7400 5000 7150 5000
Wire Notes Line
	7150 4900 7150 4450
Wire Notes Line
	7400 4900 7150 4900
Wire Notes Line
	7400 4450 7150 4450
Wire Notes Line
	7400 3050 7450 3050
Wire Notes Line
	7450 3050 7450 3100
Wire Notes Line
	7450 3100 7400 3100
Wire Notes Line
	7400 3150 7450 3150
Wire Notes Line
	7450 3150 7450 3200
Wire Notes Line
	7450 3200 7400 3200
Wire Notes Line
	7400 3000 7400 3250
Wire Notes Line
	7150 3000 7150 3250
Text Notes 5350 4500 3    50   ~ 0
Each slot has 1 interrupt line
Text Notes 5350 4350 1    50   ~ 0
Interrupts —>
Text Notes 5350 3150 1    50   ~ 0
Interrupts —>
$EndSCHEMATC
