EESchema Schematic File Version 4
LIBS:Pearl-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Gemini:41464 U4
U 1 1 5DF025F0
P 4150 4050
F 0 "U4" V 4150 4050 50  0000 L CNN
F 1 "41464" V 4250 4050 50  0000 L CNN
F 2 "Housings_DIP:DIP-18_W7.62mm_Socket_LongPads" V 4241 5044 50  0001 L CNN
F 3 "" H 4250 4050 50  0000 C CNN
	1    4150 4050
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 5E1EF8E2
P 7500 6150
F 0 "#PWR0101" H 7500 5900 50  0001 C CNN
F 1 "GND" H 7505 5977 50  0000 C CNN
F 2 "" H 7500 6150 50  0001 C CNN
F 3 "" H 7500 6150 50  0001 C CNN
	1    7500 6150
	1    0    0    -1  
$EndComp
$Comp
L Device:C C3
U 1 1 5DF0A0A0
P 8400 6000
F 0 "C3" H 8515 6046 50  0000 L CNN
F 1 "0.1uF" H 8515 5955 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D3.0mm_W2.0mm_P2.50mm" H 8438 5850 50  0001 C CNN
F 3 "~" H 8400 6000 50  0001 C CNN
	1    8400 6000
	1    0    0    -1  
$EndComp
$Comp
L Device:C C4
U 1 1 5DF0A0AA
P 8850 6000
F 0 "C4" H 8965 6046 50  0000 L CNN
F 1 "0.1uF" H 8965 5955 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D3.0mm_W2.0mm_P2.50mm" H 8888 5850 50  0001 C CNN
F 3 "~" H 8850 6000 50  0001 C CNN
	1    8850 6000
	1    0    0    -1  
$EndComp
$Comp
L Device:C C5
U 1 1 5DF0ACC5
P 9300 6000
F 0 "C5" H 9415 6046 50  0000 L CNN
F 1 "0.1uF" H 9415 5955 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D3.0mm_W2.0mm_P2.50mm" H 9338 5850 50  0001 C CNN
F 3 "~" H 9300 6000 50  0001 C CNN
	1    9300 6000
	1    0    0    -1  
$EndComp
$Comp
L Device:C C6
U 1 1 5DF0ACCF
P 9750 6000
F 0 "C6" H 9865 6046 50  0000 L CNN
F 1 "0.1uF" H 9865 5955 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D3.0mm_W2.0mm_P2.50mm" H 9788 5850 50  0001 C CNN
F 3 "~" H 9750 6000 50  0001 C CNN
	1    9750 6000
	1    0    0    -1  
$EndComp
$Comp
L Device:C C7
U 1 1 5DF0ACD9
P 10200 6000
F 0 "C7" H 10315 6046 50  0000 L CNN
F 1 "0.1uF" H 10315 5955 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D3.0mm_W2.0mm_P2.50mm" H 10238 5850 50  0001 C CNN
F 3 "~" H 10200 6000 50  0001 C CNN
	1    10200 6000
	1    0    0    -1  
$EndComp
$Comp
L Device:C C8
U 1 1 5DF0ACE3
P 10650 6000
F 0 "C8" H 10765 6046 50  0000 L CNN
F 1 "0.1uF" H 10765 5955 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D3.0mm_W2.0mm_P2.50mm" H 10688 5850 50  0001 C CNN
F 3 "~" H 10650 6000 50  0001 C CNN
	1    10650 6000
	1    0    0    -1  
$EndComp
Wire Wire Line
	10650 5850 10200 5850
Connection ~ 8400 5850
Connection ~ 8850 5850
Wire Wire Line
	8850 5850 8400 5850
Connection ~ 9300 5850
Wire Wire Line
	9300 5850 8850 5850
Connection ~ 9750 5850
Wire Wire Line
	9750 5850 9300 5850
Connection ~ 10200 5850
Wire Wire Line
	10200 5850 9750 5850
Wire Wire Line
	10650 6150 10200 6150
Connection ~ 8400 6150
Connection ~ 8850 6150
Wire Wire Line
	8850 6150 8400 6150
Connection ~ 9300 6150
Wire Wire Line
	9300 6150 8850 6150
Connection ~ 9750 6150
Wire Wire Line
	9750 6150 9300 6150
Connection ~ 10200 6150
Wire Wire Line
	10200 6150 9750 6150
$Comp
L Gemini:41464 U5
U 1 1 5DF2DB19
P 5600 4050
F 0 "U5" V 5600 4100 50  0000 L CNN
F 1 "41464" V 5700 4050 50  0000 L CNN
F 2 "Housings_DIP:DIP-18_W7.62mm_Socket_LongPads" V 5691 5044 50  0001 L CNN
F 3 "" H 5700 4050 50  0000 C CNN
	1    5600 4050
	0    1    1    0   
$EndComp
$Comp
L Gemini:41464 U3
U 1 1 5DF31866
P 2700 4050
F 0 "U3" V 2700 4050 50  0000 L CNN
F 1 "41464" V 2800 4050 50  0000 L CNN
F 2 "Housings_DIP:DIP-18_W7.62mm_Socket_LongPads" V 2791 5044 50  0001 L CNN
F 3 "" H 2800 4050 50  0000 C CNN
	1    2700 4050
	0    1    1    0   
$EndComp
$Comp
L Gemini:41464 U7
U 1 1 5DF32624
P 8500 4050
F 0 "U7" V 8500 4050 50  0000 L CNN
F 1 "41464" V 8600 4050 50  0000 L CNN
F 2 "Housings_DIP:DIP-18_W7.62mm_Socket_LongPads" V 8591 5044 50  0001 L CNN
F 3 "" H 8600 4050 50  0000 C CNN
	1    8500 4050
	0    1    1    0   
$EndComp
$Comp
L Gemini:41464 U8
U 1 1 5DF3262E
P 9950 4050
F 0 "U8" V 9950 4100 50  0000 L CNN
F 1 "41464" V 10050 4050 50  0000 L CNN
F 2 "Housings_DIP:DIP-18_W7.62mm_Socket_LongPads" V 10041 5044 50  0001 L CNN
F 3 "" H 10050 4050 50  0000 C CNN
	1    9950 4050
	0    1    1    0   
$EndComp
$Comp
L Gemini:41464 U6
U 1 1 5DF32638
P 7050 4050
F 0 "U6" V 7050 4050 50  0000 L CNN
F 1 "41464" V 7150 4050 50  0000 L CNN
F 2 "Housings_DIP:DIP-18_W7.62mm_Socket_LongPads" V 7141 5044 50  0001 L CNN
F 3 "" H 7150 4050 50  0000 C CNN
	1    7050 4050
	0    1    1    0   
$EndComp
Wire Wire Line
	3000 2750 4450 2750
Wire Wire Line
	3300 2350 4750 2350
Wire Wire Line
	2900 2650 4350 2650
Wire Wire Line
	4450 2750 5900 2750
Connection ~ 4450 2750
Wire Wire Line
	3400 2250 4850 2250
Wire Wire Line
	3500 2150 4950 2150
Wire Wire Line
	3600 2050 5050 2050
Connection ~ 4350 2650
Wire Wire Line
	4350 2650 5800 2650
Wire Wire Line
	3100 2550 4550 2550
Connection ~ 4550 2550
Wire Wire Line
	4550 2550 6000 2550
Wire Wire Line
	3200 2450 4650 2450
Connection ~ 4650 2450
Wire Wire Line
	4650 2450 6100 2450
Connection ~ 4750 2350
Wire Wire Line
	4750 2350 6200 2350
Connection ~ 4850 2250
Wire Wire Line
	4850 2250 6300 2250
Connection ~ 4950 2150
Wire Wire Line
	4950 2150 6400 2150
Connection ~ 5050 2050
Wire Wire Line
	5050 2050 6500 2050
Text GLabel 1000 4800 0    50   Input ~ 0
RD0
Text GLabel 1000 4900 0    50   Input ~ 0
RD1
Text GLabel 1000 5000 0    50   Input ~ 0
RD2
Text GLabel 1000 5100 0    50   Input ~ 0
RD3
Text GLabel 1000 5200 0    50   Input ~ 0
RD4
Text GLabel 1000 5300 0    50   Input ~ 0
RD5
Text GLabel 1000 5400 0    50   Input ~ 0
RD6
Text GLabel 1000 5500 0    50   Input ~ 0
RD7
Text Notes 7950 6950 0    200  ~ 0
V9958 VDP Memory
Text GLabel 1000 2050 0    50   Input ~ 0
AD0
Text GLabel 1000 2150 0    50   Input ~ 0
AD1
Text GLabel 1000 2250 0    50   Input ~ 0
AD2
Text GLabel 1000 2350 0    50   Input ~ 0
AD3
Text GLabel 1000 2450 0    50   Input ~ 0
AD4
Text GLabel 1000 2550 0    50   Input ~ 0
AD5
Text GLabel 1000 2650 0    50   Input ~ 0
AD6
Text GLabel 1000 2750 0    50   Input ~ 0
AD7
Wire Wire Line
	3600 4650 3600 4800
Wire Wire Line
	3600 4800 1000 4800
Wire Wire Line
	7500 5850 8400 5850
Wire Wire Line
	7500 6150 8400 6150
Wire Wire Line
	3000 2750 1000 2750
Connection ~ 3000 2750
Wire Wire Line
	2400 3450 2400 3350
Text GLabel 1000 3350 0    50   Input ~ 0
CAS0*
Text GLabel 1000 3250 0    50   Input ~ 0
CAS1*
Text GLabel 1000 3150 0    50   Input ~ 0
CASX*
Text GLabel 1000 3050 0    50   Input ~ 0
RAS*
Text GLabel 1000 2950 0    50   Input ~ 0
RW*
Wire Wire Line
	1000 3350 2400 3350
Wire Wire Line
	2500 3450 2500 3050
Wire Wire Line
	2500 3050 1000 3050
Wire Wire Line
	1000 3250 5300 3250
Wire Wire Line
	5300 3250 5300 3450
Wire Wire Line
	5300 3250 6750 3250
Wire Wire Line
	6750 3250 6750 3450
Connection ~ 5300 3250
Wire Wire Line
	3850 3450 3850 3350
Wire Wire Line
	3850 3350 2400 3350
Connection ~ 2400 3350
Wire Wire Line
	1000 3150 8200 3150
Wire Wire Line
	9650 3150 9650 3450
Connection ~ 8200 3150
Wire Wire Line
	8200 3150 9650 3150
Wire Wire Line
	8200 3150 8200 3450
Wire Wire Line
	1000 2950 2650 2950
Wire Wire Line
	2650 2950 2650 3450
Wire Wire Line
	9900 2950 9900 3450
Connection ~ 2650 2950
Wire Wire Line
	8450 3450 8450 2950
Connection ~ 8450 2950
Wire Wire Line
	8450 2950 9900 2950
Wire Wire Line
	7000 3450 7000 2950
Connection ~ 7000 2950
Wire Wire Line
	7000 2950 8450 2950
Wire Wire Line
	5550 3450 5550 2950
Connection ~ 5550 2950
Wire Wire Line
	4100 3450 4100 2950
Connection ~ 4100 2950
Wire Wire Line
	4100 2950 5550 2950
Wire Wire Line
	2500 3050 3950 3050
Wire Wire Line
	9750 3050 9750 3450
Connection ~ 2500 3050
Wire Wire Line
	8300 3450 8300 3050
Connection ~ 8300 3050
Wire Wire Line
	8300 3050 9750 3050
Wire Wire Line
	6850 3450 6850 3050
Connection ~ 6850 3050
Wire Wire Line
	6850 3050 8300 3050
Wire Wire Line
	5400 3450 5400 3050
Connection ~ 5400 3050
Wire Wire Line
	5400 3050 6850 3050
Wire Wire Line
	3950 3450 3950 3050
Connection ~ 3950 3050
Wire Wire Line
	3950 3050 5400 3050
Wire Wire Line
	2750 3450 2750 2850
Wire Wire Line
	2750 2850 2050 2850
Wire Wire Line
	2900 2650 2900 3450
Wire Wire Line
	3000 2750 3000 3450
Wire Wire Line
	3100 2550 3100 3450
Wire Wire Line
	3200 2950 3200 3450
Wire Wire Line
	3300 2350 3300 3450
Wire Wire Line
	3400 2250 3400 3450
Wire Wire Line
	3200 2450 3200 2950
Wire Wire Line
	2650 2950 4100 2950
Wire Wire Line
	2900 2650 1000 2650
Connection ~ 2900 2650
Wire Wire Line
	1000 2550 3100 2550
Connection ~ 3100 2550
Wire Wire Line
	3200 2450 1000 2450
Connection ~ 3200 2450
Connection ~ 3300 2350
Wire Wire Line
	3400 2250 1000 2250
Connection ~ 3400 2250
Wire Wire Line
	1000 2150 3500 2150
Connection ~ 3500 2150
Wire Wire Line
	3600 2050 1000 2050
Connection ~ 3600 2050
Wire Wire Line
	5800 2650 5800 3450
Wire Wire Line
	5900 2750 5900 3450
Wire Wire Line
	6000 2550 6000 3450
Wire Wire Line
	6100 2450 6100 3450
Wire Wire Line
	6200 2350 6200 3450
Wire Wire Line
	6300 2250 6300 3450
Wire Wire Line
	5550 2950 7000 2950
Wire Wire Line
	6400 2150 6400 3450
Wire Wire Line
	6500 2050 6500 3450
Wire Wire Line
	4350 2650 4350 3450
Wire Wire Line
	4450 2750 4450 3450
Wire Wire Line
	4550 2550 4550 3450
Wire Wire Line
	4650 2450 4650 3450
Wire Wire Line
	4750 2350 4750 3450
Wire Wire Line
	4850 2250 4850 3450
Wire Wire Line
	4950 2150 4950 3450
Wire Wire Line
	5050 2050 5050 3450
Wire Wire Line
	4200 3450 4200 2850
Wire Wire Line
	4200 2850 2750 2850
Connection ~ 2750 2850
Wire Wire Line
	4200 2850 5650 2850
Wire Wire Line
	10000 2850 10000 3450
Connection ~ 4200 2850
Wire Wire Line
	8550 3450 8550 2850
Connection ~ 8550 2850
Wire Wire Line
	8550 2850 10000 2850
Wire Wire Line
	7100 3450 7100 2850
Connection ~ 7100 2850
Wire Wire Line
	7100 2850 8550 2850
Wire Wire Line
	5650 3450 5650 2850
Connection ~ 5650 2850
Wire Wire Line
	5650 2850 7100 2850
Wire Wire Line
	3500 2150 3500 3450
Wire Wire Line
	3600 2050 3600 3450
Wire Wire Line
	6500 2050 7950 2050
Wire Wire Line
	10850 2050 10850 3450
Connection ~ 6500 2050
Wire Wire Line
	6400 2150 7850 2150
Wire Wire Line
	10750 2150 10750 3450
Connection ~ 6400 2150
Wire Wire Line
	6300 2250 7750 2250
Wire Wire Line
	10650 2250 10650 3450
Connection ~ 6300 2250
Wire Wire Line
	6200 2350 7650 2350
Wire Wire Line
	10550 2350 10550 3450
Connection ~ 6200 2350
Wire Wire Line
	6100 2450 7550 2450
Wire Wire Line
	10450 2450 10450 3450
Connection ~ 6100 2450
Wire Wire Line
	10350 2550 10350 3450
Wire Wire Line
	5800 2650 7250 2650
Wire Wire Line
	10150 2650 10150 3450
Connection ~ 5800 2650
Wire Wire Line
	10250 3450 10250 2750
Wire Wire Line
	10250 2750 8800 2750
Connection ~ 5900 2750
Wire Wire Line
	6000 2550 7450 2550
Connection ~ 6000 2550
Wire Wire Line
	7250 3450 7250 2650
Connection ~ 7250 2650
Wire Wire Line
	7250 2650 8700 2650
Wire Wire Line
	7350 2750 7350 3450
Connection ~ 7350 2750
Wire Wire Line
	7350 2750 5900 2750
Wire Wire Line
	7450 3450 7450 2550
Connection ~ 7450 2550
Wire Wire Line
	7450 2550 8900 2550
Wire Wire Line
	7550 3450 7550 2450
Connection ~ 7550 2450
Wire Wire Line
	7550 2450 9000 2450
Wire Wire Line
	7650 3450 7650 2350
Connection ~ 7650 2350
Wire Wire Line
	7650 2350 9100 2350
Wire Wire Line
	7750 3450 7750 2250
Connection ~ 7750 2250
Wire Wire Line
	7750 2250 9200 2250
Wire Wire Line
	7850 3450 7850 2150
Connection ~ 7850 2150
Wire Wire Line
	7850 2150 9300 2150
Wire Wire Line
	7950 3450 7950 2050
Connection ~ 7950 2050
Wire Wire Line
	7950 2050 9400 2050
Wire Wire Line
	8700 3450 8700 2650
Connection ~ 8700 2650
Wire Wire Line
	8700 2650 10150 2650
Wire Wire Line
	8800 3450 8800 2750
Connection ~ 8800 2750
Wire Wire Line
	8800 2750 7350 2750
Wire Wire Line
	8900 3450 8900 2550
Connection ~ 8900 2550
Wire Wire Line
	8900 2550 10350 2550
Wire Wire Line
	9000 3450 9000 2450
Connection ~ 9000 2450
Wire Wire Line
	9000 2450 10450 2450
Wire Wire Line
	9100 3450 9100 2350
Connection ~ 9100 2350
Wire Wire Line
	9100 2350 10550 2350
Wire Wire Line
	9200 3450 9200 2250
Connection ~ 9200 2250
Wire Wire Line
	9200 2250 10650 2250
Wire Wire Line
	9300 3450 9300 2150
Connection ~ 9300 2150
Wire Wire Line
	9300 2150 10750 2150
Wire Wire Line
	9400 3450 9400 2050
Connection ~ 9400 2050
Wire Wire Line
	9400 2050 10850 2050
Wire Wire Line
	9300 4900 9300 4650
Wire Wire Line
	3600 4800 6500 4800
Wire Wire Line
	9400 4800 9400 4650
Connection ~ 3600 4800
Wire Wire Line
	1000 5000 3400 5000
Wire Wire Line
	9200 5000 9200 4650
Wire Wire Line
	9100 4650 9100 5100
Wire Wire Line
	1000 5200 5050 5200
Wire Wire Line
	10850 5200 10850 4650
Wire Wire Line
	10750 4650 10750 5300
Wire Wire Line
	10750 5300 7850 5300
Wire Wire Line
	1000 5400 4850 5400
Wire Wire Line
	10650 5400 10650 4650
Wire Wire Line
	1000 5500 4750 5500
Wire Wire Line
	10550 5500 10550 4650
Wire Wire Line
	3500 4650 3500 4900
Wire Wire Line
	1000 4900 3500 4900
Connection ~ 3500 4900
Wire Wire Line
	3500 4900 6400 4900
Wire Wire Line
	3300 4650 3300 5100
Connection ~ 3300 5100
Wire Wire Line
	3300 5100 1000 5100
Wire Wire Line
	3300 5100 6200 5100
Wire Wire Line
	3400 4650 3400 5000
Connection ~ 3400 5000
Wire Wire Line
	3400 5000 6300 5000
Connection ~ 4750 5500
Wire Wire Line
	4750 5500 7650 5500
Wire Wire Line
	4750 4650 4750 5500
Wire Wire Line
	4850 4650 4850 5400
Connection ~ 4850 5400
Wire Wire Line
	4850 5400 7750 5400
Wire Wire Line
	4950 4650 4950 5300
Connection ~ 4950 5300
Wire Wire Line
	4950 5300 1000 5300
Wire Wire Line
	5050 4650 5050 5200
Connection ~ 5050 5200
Wire Wire Line
	5050 5200 7950 5200
Wire Wire Line
	6200 4650 6200 5100
Connection ~ 6200 5100
Wire Wire Line
	6200 5100 9100 5100
Wire Wire Line
	6300 4650 6300 5000
Connection ~ 6300 5000
Wire Wire Line
	6300 5000 9200 5000
Wire Wire Line
	6400 4650 6400 4900
Connection ~ 6400 4900
Wire Wire Line
	6400 4900 9300 4900
Wire Wire Line
	6500 4650 6500 4800
Connection ~ 6500 4800
Wire Wire Line
	6500 4800 9400 4800
Wire Wire Line
	7650 4650 7650 5500
Connection ~ 7650 5500
Wire Wire Line
	7650 5500 10550 5500
Wire Wire Line
	7750 4650 7750 5400
Connection ~ 7750 5400
Wire Wire Line
	7750 5400 10650 5400
Wire Wire Line
	7850 4650 7850 5300
Connection ~ 7850 5300
Wire Wire Line
	7850 5300 4950 5300
Wire Wire Line
	7950 4650 7950 5200
Connection ~ 7950 5200
Wire Wire Line
	7950 5200 10850 5200
Text GLabel 1000 1950 0    50   Input ~ 0
GND
Text GLabel 1000 1850 0    50   Input ~ 0
+5V
Wire Wire Line
	3700 4050 3650 4050
Wire Wire Line
	3800 4050 3750 4050
Wire Wire Line
	1000 1950 2050 1950
Wire Wire Line
	1000 2350 3300 2350
Wire Wire Line
	2350 4050 2250 4050
Wire Wire Line
	2250 4050 2250 1950
Connection ~ 2250 1950
Wire Wire Line
	9550 1950 9550 4050
Wire Wire Line
	9550 4050 9600 4050
Wire Wire Line
	9450 4050 9500 4050
Wire Wire Line
	9500 4050 9500 1850
Wire Wire Line
	3700 4050 3700 1850
Wire Wire Line
	1000 1850 3700 1850
Connection ~ 3700 1850
Wire Wire Line
	3750 4050 3750 1950
Connection ~ 3750 1950
Wire Wire Line
	3750 1950 5200 1950
Wire Wire Line
	5250 4050 5200 4050
Wire Wire Line
	5200 4050 5200 1950
Connection ~ 5200 1950
Wire Wire Line
	3700 1850 5150 1850
Wire Wire Line
	5100 4050 5150 4050
Wire Wire Line
	5150 4050 5150 1850
Connection ~ 5150 1850
Wire Wire Line
	6550 4050 6600 4050
Wire Wire Line
	6600 4050 6600 1850
Wire Wire Line
	5150 1850 6600 1850
Connection ~ 6600 1850
Wire Wire Line
	6600 1850 8050 1850
Wire Wire Line
	6700 4050 6650 4050
Wire Wire Line
	6650 4050 6650 1950
Wire Wire Line
	5200 1950 6650 1950
Connection ~ 6650 1950
Wire Wire Line
	6650 1950 8100 1950
Wire Wire Line
	8000 4050 8050 4050
Wire Wire Line
	8050 4050 8050 1850
Connection ~ 8050 1850
Wire Wire Line
	8050 1850 9500 1850
Wire Wire Line
	8150 4050 8100 4050
Wire Wire Line
	8100 4050 8100 1950
Connection ~ 8100 1950
Wire Wire Line
	8100 1950 9550 1950
Connection ~ 9500 1850
Wire Wire Line
	9500 1850 10950 1850
Wire Wire Line
	10950 1850 10950 4050
Wire Wire Line
	10950 4050 10900 4050
Wire Wire Line
	2250 1950 3750 1950
Wire Wire Line
	2050 2850 2050 1950
Connection ~ 2050 2850
Wire Wire Line
	2050 2850 1000 2850
Connection ~ 2050 1950
Wire Wire Line
	2050 1950 2250 1950
$Comp
L power:+5V #PWR03
U 1 1 5E2A2703
P 7500 5850
F 0 "#PWR03" H 7500 5700 50  0001 C CNN
F 1 "+5V" H 7515 6023 50  0000 C CNN
F 2 "" H 7500 5850 50  0001 C CNN
F 3 "" H 7500 5850 50  0001 C CNN
	1    7500 5850
	1    0    0    -1  
$EndComp
Text Notes 1200 2850 2    50   ~ 0
OE*
$EndSCHEMATC
