EESchema Schematic File Version 4
LIBS:Pearl-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:GND #PWR02
U 1 1 5DF07BA0
P 2850 7350
F 0 "#PWR02" H 2850 7100 50  0001 C CNN
F 1 "GND" H 2855 7177 50  0000 C CNN
F 2 "" H 2850 7350 50  0001 C CNN
F 3 "" H 2850 7350 50  0001 C CNN
	1    2850 7350
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 5DF09221
P 2850 7200
F 0 "C1" H 2965 7246 50  0000 L CNN
F 1 "0.1uF" H 2965 7155 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D3.0mm_W2.0mm_P2.50mm" H 2888 7050 50  0001 C CNN
F 3 "~" H 2850 7200 50  0001 C CNN
	1    2850 7200
	1    0    0    -1  
$EndComp
$Comp
L Device:C C9
U 1 1 5DF09B85
P 3300 7200
F 0 "C9" H 3415 7246 50  0000 L CNN
F 1 "0.1uF" H 3415 7155 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D3.0mm_W2.0mm_P2.50mm" H 3338 7050 50  0001 C CNN
F 3 "~" H 3300 7200 50  0001 C CNN
	1    3300 7200
	1    0    0    -1  
$EndComp
Wire Wire Line
	3300 7050 2850 7050
Connection ~ 2850 7350
Wire Wire Line
	3300 7350 2850 7350
Text GLabel 4050 3050 0    50   Input ~ 0
RESET*
Text GLabel 4050 3650 0    50   Input ~ 0
CSYNC
Text GLabel 4050 3400 0    50   Input ~ 0
BLUE
Text GLabel 4050 3200 0    50   Input ~ 0
GREEN
Text GLabel 4050 3300 0    50   Input ~ 0
RED
Wire Wire Line
	3750 4100 3350 4100
Wire Wire Line
	3750 4000 3750 4100
Wire Wire Line
	4050 4000 3750 4000
Wire Wire Line
	3750 3900 4050 3900
Wire Wire Line
	3750 3800 3750 3900
Wire Wire Line
	3350 3800 3750 3800
$Comp
L Device:Crystal Y1
U 1 1 5DF02A9A
P 3350 3950
F 0 "Y1" V 3396 3819 50  0000 R CNN
F 1 "21.47727" V 3305 3819 50  0000 R CNN
F 2 "Crystals:Crystal_HC49-U_Vertical" H 3350 3950 50  0001 C CNN
F 3 "~" H 3350 3950 50  0001 C CNN
	1    3350 3950
	0    -1   1    0   
$EndComp
Text GLabel 5450 2400 2    50   Input ~ 0
RD0
Text GLabel 5450 2500 2    50   Input ~ 0
RD1
Text GLabel 5450 2600 2    50   Input ~ 0
RD2
Text GLabel 5450 2700 2    50   Input ~ 0
RD3
Text GLabel 5450 2800 2    50   Input ~ 0
RD4
Text GLabel 5450 2900 2    50   Input ~ 0
RD5
Text GLabel 5450 3000 2    50   Input ~ 0
RD6
Text GLabel 5450 3100 2    50   Input ~ 0
RD7
Text GLabel 5450 1500 2    50   Input ~ 0
AD0
Text GLabel 5450 1600 2    50   Input ~ 0
AD1
Text GLabel 5450 1700 2    50   Input ~ 0
AD2
Text GLabel 5450 1800 2    50   Input ~ 0
AD3
Text GLabel 5450 1900 2    50   Input ~ 0
AD4
Text GLabel 5450 2000 2    50   Input ~ 0
AD5
Text GLabel 5450 2100 2    50   Input ~ 0
AD6
Text GLabel 5450 2200 2    50   Input ~ 0
AD7
Text GLabel 5450 3250 2    50   Input ~ 0
RAS*
Text GLabel 5450 3350 2    50   Input ~ 0
CAS0*
Text GLabel 5450 3450 2    50   Input ~ 0
CAS1*
Text GLabel 5450 3550 2    50   Input ~ 0
CASX*
Text GLabel 5450 3700 2    50   Input ~ 0
RW*
Text GLabel 4050 2850 0    50   Input ~ 0
VDPINT1*
Text GLabel 4050 2700 0    50   Input ~ 0
CSR1*
Text GLabel 4050 2600 0    50   Input ~ 0
CSW1*
Text GLabel 4050 1850 0    50   Input ~ 0
BCD1
Text GLabel 4050 1950 0    50   Input ~ 0
BCD2
Text GLabel 4050 2050 0    50   Input ~ 0
BCD3
Text GLabel 4050 2150 0    50   Input ~ 0
BCD4
Text GLabel 4050 2250 0    50   Input ~ 0
BCD5
Text GLabel 4050 2350 0    50   Input ~ 0
BCD6
Text GLabel 4050 2450 0    50   Input ~ 0
BCD7
Text GLabel 4050 1500 0    50   Input ~ 0
MODE0
Text GLabel 4050 1600 0    50   Input ~ 0
MODE1
Text Notes 7950 6950 0    200  ~ 0
V9958 VDP Card
$Sheet
S 5550 6950 1350 700 
U 5DFD21D6
F0 "VRAM" 50
F1 "Pearl_VRAM.sch" 50
$EndSheet
Text GLabel 2650 7350 0    50   Input ~ 0
GND
Text GLabel 9800 4450 0    50   Input ~ 0
GND
Text GLabel 9800 5150 0    50   Input ~ 0
RESET*
Text GLabel 9800 4750 0    50   Input ~ 0
VDPINT1*
$Comp
L Device:C C11
U 1 1 5E1B8BC9
P 3200 3800
F 0 "C11" V 2948 3800 50  0000 C CNN
F 1 "5 pF" V 3039 3800 50  0000 C CNN
F 2 "Capacitors_ThroughHole:C_Disc_D3.0mm_W2.0mm_P2.50mm" H 3238 3650 50  0001 C CNN
F 3 "~" H 3200 3800 50  0001 C CNN
	1    3200 3800
	0    1    1    0   
$EndComp
Connection ~ 3350 3800
Text GLabel 3050 3800 0    50   Input ~ 0
GND
Text GLabel 3050 4100 0    50   Input ~ 0
GND
$Comp
L Device:C C12
U 1 1 5E1BA06B
P 3200 4100
F 0 "C12" V 2948 4100 50  0000 C CNN
F 1 "5 pF" V 3039 4100 50  0000 C CNN
F 2 "Capacitors_ThroughHole:C_Disc_D3.0mm_W2.0mm_P2.50mm" H 3238 3950 50  0001 C CNN
F 3 "~" H 3200 4100 50  0001 C CNN
	1    3200 4100
	0    1    1    0   
$EndComp
Connection ~ 3350 4100
$Comp
L 74xx:74LS138 U9
U 1 1 5E24F819
P 7750 1450
F 0 "U9" H 7750 2231 50  0000 C CNN
F 1 "74LS138" H 7750 2140 50  0000 C CNN
F 2 "Package_SO:SOIC-16W_5.3x10.2mm_P1.27mm" H 7750 1450 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS138" H 7750 1450 50  0001 C CNN
	1    7750 1450
	1    0    0    -1  
$EndComp
Text GLabel 7250 1650 0    50   Input ~ 0
SS0
Text GLabel 7250 1250 0    50   Input ~ 0
CSR*
Text GLabel 7250 1350 0    50   Input ~ 0
CSW*
Text GLabel 7250 1150 0    50   Input ~ 0
SS1
Text Notes 6700 3400 0    63   ~ 0
VDP has no chip enable\nSS1 CSR CSW\n000 X\n001 CSR1\n010 CSW1\n011 X\n100 X\n101 CSR2\n110 CSW2\n111 X\n
Text GLabel 10200 1950 2    50   Input ~ 0
RED
Text GLabel 10200 1750 2    50   Input ~ 0
GND
Text GLabel 10200 1850 2    50   Input ~ 0
GREEN
Text GLabel 10200 2050 2    50   Input ~ 0
BLUE
Text GLabel 10200 1550 2    50   Input ~ 0
CSYNC
Text GLabel 10200 1650 2    50   Input ~ 0
GND
Wire Wire Line
	7250 1750 7250 1850
Wire Wire Line
	7250 1850 7250 2150
Wire Wire Line
	7250 2150 7750 2150
Connection ~ 7250 1850
Text GLabel 7750 2150 2    50   Input ~ 0
GND
Text GLabel 7750 850  2    50   Input ~ 0
+5V
$Comp
L power:+5V #PWR01
U 1 1 5E29B52D
P 2850 7050
F 0 "#PWR01" H 2850 6900 50  0001 C CNN
F 1 "+5V" H 2865 7223 50  0000 C CNN
F 2 "" H 2850 7050 50  0001 C CNN
F 3 "" H 2850 7050 50  0001 C CNN
	1    2850 7050
	1    0    0    -1  
$EndComp
Text GLabel 2650 7050 0    50   Input ~ 0
+5V
Connection ~ 2850 7050
Wire Wire Line
	2650 7050 2850 7050
Wire Wire Line
	2850 7350 2650 7350
NoConn ~ 4050 3750
NoConn ~ 5450 3950
NoConn ~ 5450 4050
NoConn ~ 5450 4150
NoConn ~ 5450 4250
NoConn ~ 5450 4350
NoConn ~ 5450 4450
NoConn ~ 5450 4550
NoConn ~ 5450 4650
NoConn ~ 5450 4750
NoConn ~ 8250 1150
NoConn ~ 8250 1450
NoConn ~ 8250 1550
NoConn ~ 8250 1850
Text GLabel 8250 1250 2    50   Input ~ 0
CSR1*
Text GLabel 8250 1350 2    50   Input ~ 0
CSW1*
NoConn ~ 8250 1650
NoConn ~ 8250 1750
Text GLabel 9800 4550 0    50   Input ~ 0
+5V
Text GLabel 5450 1100 2    50   Input ~ 0
+5V
Text GLabel 4450 4950 0    50   Input ~ 0
GND
Wire Wire Line
	4450 4950 4700 4950
Wire Wire Line
	4700 4950 4700 4850
Wire Wire Line
	4700 4950 4850 4950
Wire Wire Line
	4850 4950 4850 4850
Connection ~ 4700 4950
Text GLabel 9700 2150 0    50   Input ~ 0
+5V
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 5E2FCF01
P 10100 4350
F 0 "#FLG0101" H 10100 4425 50  0001 C CNN
F 1 "PWR_FLAG" V 10050 4500 50  0000 C CNN
F 2 "" H 10100 4350 50  0001 C CNN
F 3 "~" H 10100 4350 50  0001 C CNN
	1    10100 4350
	-1   0    0    -1  
$EndComp
Wire Wire Line
	9800 4450 10100 4450
Wire Wire Line
	10100 4350 10100 4450
Connection ~ 10100 4450
Wire Wire Line
	10100 4450 10200 4450
Wire Wire Line
	10200 4750 9800 4750
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 5E2FD920
P 9950 4350
F 0 "#FLG0102" H 9950 4425 50  0001 C CNN
F 1 "PWR_FLAG" V 9900 4300 50  0000 L CNN
F 2 "" H 9950 4350 50  0001 C CNN
F 3 "~" H 9950 4350 50  0001 C CNN
	1    9950 4350
	-1   0    0    -1  
$EndComp
NoConn ~ 1750 1600
Text GLabel 1750 2700 2    50   Input ~ 0
3V3
NoConn ~ 1750 1900
NoConn ~ 1750 2000
NoConn ~ 1750 2100
NoConn ~ 1250 2100
NoConn ~ 1250 2000
NoConn ~ 1250 1900
Text GLabel 1250 1700 0    50   Input ~ 0
GND
Text GLabel 1750 1700 2    50   Input ~ 0
GND
Text GLabel 1750 2200 2    50   Input ~ 0
CD7
Text GLabel 1250 2200 0    50   Input ~ 0
CD6
Text GLabel 1750 2300 2    50   Input ~ 0
CD5
Text GLabel 1250 2300 0    50   Input ~ 0
CD4
Text GLabel 1750 2400 2    50   Input ~ 0
CD3
Text GLabel 1250 2400 0    50   Input ~ 0
CD2
Text GLabel 1750 2500 2    50   Input ~ 0
CD1
Text GLabel 1250 2500 0    50   Input ~ 0
CD0
Text GLabel 1750 1400 2    50   Input ~ 0
CSR*
Text GLabel 1250 1400 0    50   Input ~ 0
CSW*
Text GLabel 1750 1300 2    50   Input ~ 0
SS1
Text GLabel 1250 1300 0    50   Input ~ 0
SS0
Text GLabel 1250 1500 0    50   Input ~ 0
MODE0
Text GLabel 1750 1500 2    50   Input ~ 0
MODE1
Text GLabel 1250 2700 0    50   Input ~ 0
3V3
Text GLabel 1250 2600 0    50   Input ~ 0
GND
Text GLabel 1750 2600 2    50   Input ~ 0
GND
$Comp
L Connector_Generic:Conn_02x15_Odd_Even J1
U 1 1 5E345446
P 1450 2000
F 0 "J1" H 1500 2900 50  0000 C CNN
F 1 "Conn_02x15_Odd_Even" H 1500 3026 50  0001 C CNN
F 2 "Pin_Headers:Pin_Header_Angled_2x15_Pitch2.54mm" H 1450 2000 50  0001 C CNN
F 3 "~" H 1450 2000 50  0001 C CNN
	1    1450 2000
	1    0    0    1   
$EndComp
$Comp
L dk_Logic-Buffers-Drivers-Receivers-Transceivers:SN74LVC245AN U10
U 1 1 5E33EA70
P 1650 3950
F 0 "U10" H 1550 4453 60  0000 C CNN
F 1 "SN74LVC245AN" H 1550 4347 60  0000 C CNN
F 2 "Package_SO:SOIC-20W_7.5x12.8mm_P1.27mm" H 1850 4150 60  0001 L CNN
F 3 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fsn74lvc245a" H 1850 4250 60  0001 L CNN
F 4 "296-8503-5-ND" H 1850 4350 60  0001 L CNN "Digi-Key_PN"
F 5 "SN74LVC245AN" H 1850 4450 60  0001 L CNN "MPN"
F 6 "Integrated Circuits (ICs)" H 1850 4550 60  0001 L CNN "Category"
F 7 "Logic - Buffers, Drivers, Receivers, Transceivers" H 1850 4650 60  0001 L CNN "Family"
F 8 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fsn74lvc245a" H 1850 4750 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/texas-instruments/SN74LVC245AN/296-8503-5-ND/377483" H 1850 4850 60  0001 L CNN "DK_Detail_Page"
F 10 "IC TXRX NON-INVERT 3.6V 20DIP" H 1850 4950 60  0001 L CNN "Description"
F 11 "Texas Instruments" H 1850 5050 60  0001 L CNN "Manufacturer"
F 12 "Active" H 1850 5150 60  0001 L CNN "Status"
	1    1650 3950
	1    0    0    -1  
$EndComp
Text Notes 750  5150 0    50   ~ 0
DIR=1 A->B
Text GLabel 1650 5150 2    50   Input ~ 0
GND
Text GLabel 1650 3650 2    50   Input ~ 0
3V3
$Comp
L power:PWR_FLAG #FLG0103
U 1 1 5E33FDE2
P 1600 7050
F 0 "#FLG0103" H 1600 7125 50  0001 C CNN
F 1 "PWR_FLAG" V 1550 7000 50  0000 L CNN
F 2 "" H 1600 7050 50  0001 C CNN
F 3 "~" H 1600 7050 50  0001 C CNN
	1    1600 7050
	1    0    0    -1  
$EndComp
Text GLabel 4050 1750 0    50   Input ~ 0
BCD0
Text GLabel 1850 4150 2    50   Input ~ 0
BCD1
Text GLabel 1850 4250 2    50   Input ~ 0
BCD2
Text GLabel 1850 4350 2    50   Input ~ 0
BCD3
Text GLabel 1850 4450 2    50   Input ~ 0
BCD4
Text GLabel 1850 4550 2    50   Input ~ 0
BCD5
Text GLabel 1850 4650 2    50   Input ~ 0
BCD6
Text GLabel 1850 4750 2    50   Input ~ 0
BCD7
Text GLabel 1850 4050 2    50   Input ~ 0
BCD0
Text GLabel 1250 4550 0    50   Input ~ 0
CD6
Text GLabel 1250 4350 0    50   Input ~ 0
CD4
Text GLabel 1250 4150 0    50   Input ~ 0
CD2
Text GLabel 1250 3950 0    50   Input ~ 0
CD0
Text GLabel 1250 4750 0    50   Input ~ 0
CSR1*
Text GLabel 1250 4850 0    50   Input ~ 0
GND
Text GLabel 1250 4650 0    50   Input ~ 0
CD7
Text GLabel 1250 4450 0    50   Input ~ 0
CD5
Text GLabel 1250 4250 0    50   Input ~ 0
CD3
Text GLabel 1250 4050 0    50   Input ~ 0
CD1
$Comp
L Connector:Conn_01x08_Male J7
U 1 1 5E260B29
P 10400 4750
F 0 "J7" H 10508 5139 50  0000 C CNN
F 1 "Conn_01x08_Male" H 10508 5040 50  0001 C CNN
F 2 "Connector_IDC:IDC-Header_2x04_P2.54mm_Horizontal" H 10400 4750 50  0001 C CNN
F 3 "~" H 10400 4750 50  0001 C CNN
	1    10400 4750
	-1   0    0    -1  
$EndComp
Text GLabel 9850 3700 2    50   Input ~ 0
+5V
$Comp
L Device:R_Small_US R7
U 1 1 5E2C4B8D
P 9850 3850
F 0 "R7" H 9918 3896 50  0000 L CNN
F 1 "100K" H 9918 3805 50  0000 L CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" H 9850 3850 50  0001 C CNN
F 3 "~" H 9850 3850 50  0001 C CNN
	1    9850 3850
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 5E2FA516
P 1700 7350
F 0 "#PWR0102" H 1700 7100 50  0001 C CNN
F 1 "GND" H 1705 7177 50  0000 C CNN
F 2 "" H 1700 7350 50  0001 C CNN
F 3 "" H 1700 7350 50  0001 C CNN
	1    1700 7350
	1    0    0    -1  
$EndComp
$Comp
L Device:C C10
U 1 1 5E2FA520
P 1700 7200
F 0 "C10" H 1815 7246 50  0000 L CNN
F 1 "0.1uF" H 1815 7155 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D3.0mm_W2.0mm_P2.50mm" H 1738 7050 50  0001 C CNN
F 3 "~" H 1700 7200 50  0001 C CNN
	1    1700 7200
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 7050 1700 7050
Connection ~ 1700 7350
Wire Wire Line
	2150 7350 1700 7350
Text GLabel 1500 7350 0    50   Input ~ 0
GND
Text GLabel 1500 7050 0    50   Input ~ 0
3V3
Connection ~ 1700 7050
Wire Wire Line
	1500 7050 1600 7050
Wire Wire Line
	1700 7350 1500 7350
Connection ~ 1600 7050
Wire Wire Line
	1600 7050 1700 7050
$Comp
L power:+3V3 #PWR0103
U 1 1 5E2FFCE0
P 1700 7050
F 0 "#PWR0103" H 1700 6900 50  0001 C CNN
F 1 "+3V3" H 1715 7223 50  0000 C CNN
F 2 "" H 1700 7050 50  0001 C CNN
F 3 "" H 1700 7050 50  0001 C CNN
	1    1700 7050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4750 1100 4750 1400
Wire Wire Line
	4650 1100 4750 1100
Wire Wire Line
	4650 1100 4650 1400
Connection ~ 4750 1100
$Comp
L Device:C C14
U 1 1 5E480B7A
P 4850 1250
F 0 "C14" H 4965 1296 50  0000 L CNN
F 1 "0.1uF" H 4965 1205 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D3.0mm_W2.0mm_P2.50mm" H 4888 1100 50  0001 C CNN
F 3 "~" H 4850 1250 50  0001 C CNN
	1    4850 1250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4750 1100 4850 1100
Connection ~ 4850 1100
Wire Wire Line
	4850 1100 5450 1100
Text Label 4850 1400 0    50   ~ 0
VBB
Text GLabel 4050 4400 0    50   Input ~ 0
VDPWAIT*
Text GLabel 1250 1600 0    50   Input ~ 0
VDPWAIT*
Text GLabel 9700 1550 0    50   Input ~ 0
HSYNC
Text GLabel 9700 2900 0    50   Input ~ 0
GND
Text GLabel 9700 2500 0    50   Input ~ 0
GND
Text GLabel 9700 1950 0    50   Input ~ 0
GND
Text GLabel 9800 5050 0    50   Input ~ 0
GND
Text GLabel 9800 4850 0    50   Input ~ 0
GND
Text GLabel 9800 4650 0    50   Input ~ 0
GND
Wire Wire Line
	10200 4650 9800 4650
Wire Wire Line
	10200 4950 9800 4950
$Comp
L Device:C C15
U 1 1 5E4AAAF4
P 9700 4050
F 0 "C15" V 9600 4150 50  0000 L CNN
F 1 "1uF" V 9800 4150 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D3.0mm_W2.0mm_P2.50mm" H 9738 3900 50  0001 C CNN
F 3 "~" H 9700 4050 50  0001 C CNN
	1    9700 4050
	0    -1   1    0   
$EndComp
Text GLabel 9550 4050 0    50   Input ~ 0
GND
Wire Wire Line
	9850 3950 9850 4050
Wire Wire Line
	9850 3700 9850 3750
Text GLabel 4050 4550 0    50   Input ~ 0
VRESET*
Text GLabel 4050 4650 0    50   Input ~ 0
HRESET*
$Comp
L Gemini:V9958 U1
U 1 1 5DF01F3B
P 4750 2950
F 0 "U1" H 4500 4950 50  0000 C CNN
F 1 "V9958" H 4750 4950 50  0000 C CNN
F 2 "Gemini:DIP-64_750_ELL" H 4750 4589 50  0001 C CNN
F 3 "" H 4750 3500 50  0000 C CNN
	1    4750 2950
	1    0    0    -1  
$EndComp
NoConn ~ 4050 4150
NoConn ~ 4050 4250
Text GLabel 4050 3550 0    50   Input ~ 0
HSYNC
Text GLabel 10850 1450 2    50   Input ~ 0
VRESET*
$Comp
L Device:R_Small_US R1
U 1 1 5E6BFB7E
P 10600 2350
F 0 "R1" H 10600 2300 50  0000 L CNN
F 1 "10K" H 10600 2500 50  0000 L CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" H 10600 2350 50  0001 C CNN
F 3 "~" H 10600 2350 50  0001 C CNN
	1    10600 2350
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small_US R2
U 1 1 5E6C0701
P 10750 2350
F 0 "R2" H 10750 2300 50  0000 L CNN
F 1 "10K" H 10600 2500 50  0000 L CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" H 10750 2350 50  0001 C CNN
F 3 "~" H 10750 2350 50  0001 C CNN
	1    10750 2350
	-1   0    0    1   
$EndComp
Wire Wire Line
	3700 7050 3300 7050
Connection ~ 3300 7050
Wire Wire Line
	3300 7350 3700 7350
Connection ~ 3300 7350
Text GLabel 9700 2700 0    50   Input ~ 0
GND
$Comp
L Device:C C2
U 1 1 5E74245B
P 3700 7200
F 0 "C2" H 3815 7246 50  0000 L CNN
F 1 "47uF" H 3815 7155 50  0000 L CNN
F 2 "Capacitors_ThroughHole:CP_Radial_D5.0mm_P2.50mm" H 3738 7050 50  0001 C CNN
F 3 "~" H 3700 7200 50  0001 C CNN
	1    3700 7200
	1    0    0    -1  
$EndComp
$Comp
L Device:C C13
U 1 1 5E747121
P 4000 7200
F 0 "C13" H 4115 7246 50  0000 L CNN
F 1 "47uF" H 4115 7155 50  0000 L CNN
F 2 "Capacitors_ThroughHole:CP_Radial_D5.0mm_P2.50mm" H 4038 7050 50  0001 C CNN
F 3 "~" H 4000 7200 50  0001 C CNN
	1    4000 7200
	1    0    0    -1  
$EndComp
Wire Wire Line
	3700 7050 4000 7050
Connection ~ 3700 7050
Wire Wire Line
	4000 7350 3700 7350
Connection ~ 3700 7350
Text Notes 1300 1000 0    50   ~ 0
PMOD FPGA
Text GLabel 10850 2150 2    50   Input ~ 0
HRESET*
Text GLabel 4050 2950 0    50   Input ~ 0
CPUCLK
Text GLabel 9700 1650 0    50   Input ~ 0
CPUCLK
Text GLabel 9700 2800 0    50   Input ~ 0
GND
NoConn ~ 9800 4950
Text GLabel 9700 3000 0    50   Input ~ 0
GND
Text GLabel 9700 2600 0    50   Input ~ 0
GND
Text GLabel 9700 1450 0    50   Input ~ 0
GND
Text GLabel 9700 3200 0    50   Input ~ 0
+5V
Text GLabel 9700 3100 0    50   Input ~ 0
+5V
Text GLabel 4050 4750 0    50   Input ~ 0
YS*
Text GLabel 9700 1750 0    50   Input ~ 0
YS*
$Comp
L Connector_Generic:Conn_02x08_Odd_Even J2
U 1 1 5E7B8BAA
P 9900 1750
F 0 "J2" H 9950 2175 50  0000 C CNN
F 1 "Conn_02x08_Odd_Even" H 9950 2176 50  0001 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x08_Pitch2.54mm" H 9900 1750 50  0001 C CNN
F 3 "~" H 9900 1750 50  0001 C CNN
	1    9900 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	10850 2150 10750 2150
Connection ~ 10750 2150
Wire Wire Line
	10750 2150 10200 2150
Text GLabel 10850 2600 2    50   Input ~ 0
+5V
Wire Wire Line
	10600 2450 10600 2600
Wire Wire Line
	10600 2600 10750 2600
Wire Wire Line
	10750 2450 10750 2600
Connection ~ 10750 2600
Wire Wire Line
	10750 2600 10850 2600
Wire Wire Line
	10750 2250 10750 2150
Wire Wire Line
	10600 2250 10600 1450
Wire Wire Line
	10600 1450 10850 1450
Wire Wire Line
	10600 1450 10200 1450
Connection ~ 10600 1450
Wire Wire Line
	9800 4550 9950 4550
Connection ~ 9850 5150
Wire Wire Line
	9950 4350 9950 4550
Connection ~ 9950 4550
Wire Wire Line
	9950 4550 10200 4550
Wire Wire Line
	10200 4850 9800 4850
Text Label 10150 4950 2    50   ~ 0
FMINT1
Wire Wire Line
	9800 5150 9850 5150
Wire Wire Line
	9850 5150 9850 5050
Wire Wire Line
	9850 5150 10200 5150
Connection ~ 9850 4050
Wire Wire Line
	9850 5050 9850 4050
Wire Wire Line
	9800 5050 10200 5050
NoConn ~ 1250 1800
NoConn ~ 1750 1800
Text Notes 1800 1850 0    50   ~ 0
3V3
Text Notes 1050 1850 0    50   ~ 0
3V3
Text GLabel 9700 1850 0    50   Input ~ 0
GND
Text GLabel 9700 2050 0    50   Input ~ 0
GND
$Comp
L Connector_Generic:Conn_02x08_Odd_Even J4
U 1 1 5E4E4CB6
P 9900 2800
F 0 "J4" H 9950 3225 50  0000 C CNN
F 1 "Conn_02x08_Odd_Even" H 9950 3226 50  0001 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x08_Pitch2.54mm" H 9900 2800 50  0001 C CNN
F 3 "~" H 9900 2800 50  0001 C CNN
	1    9900 2800
	1    0    0    -1  
$EndComp
Text GLabel 10200 2900 2    50   Input ~ 0
GND
Text GLabel 10200 2500 2    50   Input ~ 0
GND
Text GLabel 10200 2700 2    50   Input ~ 0
GND
Text GLabel 10200 2800 2    50   Input ~ 0
GND
Text GLabel 10200 3000 2    50   Input ~ 0
GND
Text GLabel 10200 2600 2    50   Input ~ 0
GND
Text GLabel 10200 3200 2    50   Input ~ 0
+5V
Text GLabel 10200 3100 2    50   Input ~ 0
+5V
$EndSCHEMATC
