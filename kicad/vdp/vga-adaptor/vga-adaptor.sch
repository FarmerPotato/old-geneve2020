EESchema Schematic File Version 4
LIBS:vga-adaptor-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Gemini:CXA2075 U1
U 1 1 5E4C6168
P 6450 4000
F 0 "U1" H 6450 4600 50  0000 C CNN
F 1 "CXA2075" H 6450 4500 50  0000 C CNN
F 2 "Gemini:SOIC-24_5.0x15.24mm_P1.27mm" H 6450 4000 50  0001 C CNN
F 3 "" H 6450 4000 50  0001 C CNN
	1    6450 4000
	1    0    0    -1  
$EndComp
Text GLabel 5200 4950 0    50   Input ~ 0
+5V
Text GLabel 8950 2300 2    50   Input ~ 0
GND
Text GLabel 8300 4950 2    50   Input ~ 0
+5V
$Comp
L power:+5V #PWR0101
U 1 1 5E4C7BCE
P 5000 6700
F 0 "#PWR0101" H 5000 6550 50  0001 C CNN
F 1 "+5V" H 5015 6873 50  0000 C CNN
F 2 "" H 5000 6700 50  0001 C CNN
F 3 "" H 5000 6700 50  0001 C CNN
	1    5000 6700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 5E4C7F89
P 5000 7250
F 0 "#PWR0102" H 5000 7000 50  0001 C CNN
F 1 "GND" H 5005 7077 50  0000 C CNN
F 2 "" H 5000 7250 50  0001 C CNN
F 3 "" H 5000 7250 50  0001 C CNN
	1    5000 7250
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 5E4C83CC
P 4600 6700
F 0 "#FLG0101" H 4600 6775 50  0001 C CNN
F 1 "PWR_FLAG" H 4600 6873 50  0000 C CNN
F 2 "" H 4600 6700 50  0001 C CNN
F 3 "~" H 4600 6700 50  0001 C CNN
	1    4600 6700
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x08_Odd_Even J2
U 1 1 5E4C8B57
P 1050 6150
F 0 "J2" H 1158 6539 50  0000 C CNN
F 1 "Conn_02x08" H 1158 6540 50  0001 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x08_Pitch2.54mm" H 1050 6150 50  0001 C CNN
F 3 "~" H 1050 6150 50  0001 C CNN
	1    1050 6150
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 5E4CC2A7
P 2100 2550
F 0 "R1" H 2170 2596 50  0000 L CNN
F 1 "470" V 2100 2500 50  0000 L CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 2030 2550 50  0001 C CNN
F 3 "~" H 2100 2550 50  0001 C CNN
	1    2100 2550
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C1
U 1 1 5E4CC7FA
P 2400 2550
F 0 "C1" H 2492 2596 50  0000 L CNN
F 1 "330pF" H 2450 2500 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D5.0mm_W2.5mm_P2.50mm" H 2400 2550 50  0001 C CNN
F 3 "~" H 2400 2550 50  0001 C CNN
	1    2400 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	1700 2300 2100 2300
Wire Wire Line
	2100 2400 2100 2300
Connection ~ 2100 2300
Wire Wire Line
	2100 2300 2400 2300
Wire Wire Line
	2400 2450 2400 2300
Connection ~ 2400 2300
Wire Wire Line
	2400 2300 2850 2300
Text GLabel 1700 2300 0    50   Input ~ 0
GND
Text GLabel 1700 2800 0    50   Input ~ 0
RED
Text GLabel 1700 3000 0    50   Input ~ 0
GREEN
Text GLabel 1700 3200 0    50   Input ~ 0
BLUE
Wire Wire Line
	1700 2800 2100 2800
Wire Wire Line
	1700 3000 2850 3000
Wire Wire Line
	1700 3200 3600 3200
Wire Wire Line
	2100 2700 2100 2800
Connection ~ 2100 2800
Wire Wire Line
	2100 2800 2400 2800
Wire Wire Line
	2400 2650 2400 2800
Connection ~ 2400 2800
$Comp
L Device:R R2
U 1 1 5E4D1A1C
P 2850 2550
F 0 "R2" H 2920 2596 50  0000 L CNN
F 1 "470" V 2850 2500 50  0000 L CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 2780 2550 50  0001 C CNN
F 3 "~" H 2850 2550 50  0001 C CNN
	1    2850 2550
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C2
U 1 1 5E4D1A26
P 3150 2550
F 0 "C2" H 3242 2596 50  0000 L CNN
F 1 "330pF" H 3200 2500 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D5.0mm_W2.5mm_P2.50mm" H 3150 2550 50  0001 C CNN
F 3 "~" H 3150 2550 50  0001 C CNN
	1    3150 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	2850 2400 2850 2300
Wire Wire Line
	3150 2450 3150 2300
Connection ~ 2850 2300
Wire Wire Line
	2850 2300 3150 2300
Connection ~ 3150 2300
Wire Wire Line
	3150 2300 3600 2300
Wire Wire Line
	2400 2800 4300 2800
$Comp
L Device:R R3
U 1 1 5E4D20D7
P 3600 2550
F 0 "R3" H 3670 2596 50  0000 L CNN
F 1 "470" V 3600 2500 50  0000 L CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 3530 2550 50  0001 C CNN
F 3 "~" H 3600 2550 50  0001 C CNN
	1    3600 2550
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C3
U 1 1 5E4D2377
P 3850 2550
F 0 "C3" H 3942 2596 50  0000 L CNN
F 1 "330pF" H 3900 2500 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D5.0mm_W2.5mm_P2.50mm" H 3850 2550 50  0001 C CNN
F 3 "~" H 3850 2550 50  0001 C CNN
	1    3850 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	3600 2400 3600 2300
Connection ~ 3600 2300
Wire Wire Line
	3600 2300 3850 2300
Wire Wire Line
	3850 2450 3850 2300
Connection ~ 3850 2300
Wire Wire Line
	3850 2650 3850 3200
Connection ~ 3850 3200
Wire Wire Line
	3850 3200 4300 3200
Wire Wire Line
	3600 2700 3600 3200
Connection ~ 3600 3200
Wire Wire Line
	3600 3200 3850 3200
Wire Wire Line
	3150 2650 3150 3000
Connection ~ 3150 3000
Wire Wire Line
	3150 3000 4300 3000
Wire Wire Line
	2850 2700 2850 3000
Connection ~ 2850 3000
Wire Wire Line
	2850 3000 3150 3000
Wire Wire Line
	4600 6700 5000 6700
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 5E4E01C5
P 4600 7250
F 0 "#FLG0102" H 4600 7325 50  0001 C CNN
F 1 "PWR_FLAG" H 4600 7423 50  0000 C CNN
F 2 "" H 4600 7250 50  0001 C CNN
F 3 "~" H 4600 7250 50  0001 C CNN
	1    4600 7250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 7250 5000 7250
$Comp
L Device:C_Small C7
U 1 1 5E4E1A68
P 4400 3000
F 0 "C7" V 4350 2850 50  0000 L CNN
F 1 "0.1uF" V 4450 3050 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D5.0mm_W2.5mm_P2.50mm" H 4400 3000 50  0001 C CNN
F 3 "~" H 4400 3000 50  0001 C CNN
	1    4400 3000
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C6
U 1 1 5E4E2B33
P 4400 2800
F 0 "C6" V 4350 2650 50  0000 L CNN
F 1 "0.1uF" V 4450 2850 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D5.0mm_W2.5mm_P2.50mm" H 4400 2800 50  0001 C CNN
F 3 "~" H 4400 2800 50  0001 C CNN
	1    4400 2800
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C8
U 1 1 5E4E2E00
P 4400 3200
F 0 "C8" V 4350 3050 50  0000 L CNN
F 1 "0.1uF" V 4450 3250 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D5.0mm_W2.5mm_P2.50mm" H 4400 3200 50  0001 C CNN
F 3 "~" H 4400 3200 50  0001 C CNN
	1    4400 3200
	0    1    1    0   
$EndComp
Wire Wire Line
	4500 2800 5050 2800
Text GLabel 1700 3400 0    50   Input ~ 0
CSYNC
$Comp
L Device:R R4
U 1 1 5E4E8C2D
P 2150 3400
F 0 "R4" V 2050 3350 50  0000 L CNN
F 1 "2.2K" V 2150 3350 50  0000 L CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 2080 3400 50  0001 C CNN
F 3 "~" H 2150 3400 50  0001 C CNN
	1    2150 3400
	0    1    1    0   
$EndComp
Wire Wire Line
	1700 3400 2000 3400
Text GLabel 1700 5600 0    50   Input ~ 0
GND
$Comp
L Device:C_Small C5
U 1 1 5E4EF38F
P 2400 3850
F 0 "C5" H 2492 3896 50  0000 L CNN
F 1 "5pF" H 2450 3800 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D5.0mm_W2.5mm_P2.50mm" H 2400 3850 50  0001 C CNN
F 3 "~" H 2400 3850 50  0001 C CNN
	1    2400 3850
	1    0    0    -1  
$EndComp
Text GLabel 1700 3600 0    50   Input ~ 0
SCCLK
$Comp
L Device:C_Small C4
U 1 1 5E4F27CF
P 2700 3850
F 0 "C4" H 2792 3896 50  0000 L CNN
F 1 "47pF" H 2750 3800 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D5.0mm_W2.5mm_P2.50mm" H 2700 3850 50  0001 C CNN
F 3 "~" H 2700 3850 50  0001 C CNN
	1    2700 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	2300 3400 2700 3400
Wire Wire Line
	2400 3750 2400 3600
Wire Wire Line
	2400 3600 4450 3600
Wire Wire Line
	2700 3400 2700 3750
Connection ~ 2700 3400
Wire Wire Line
	2700 3400 4600 3400
Text Notes 3900 4900 0    50   ~ 0
Nominal\n29pF NTSC\n19pF PAL
$Comp
L Device:L L1
U 1 1 5E50743B
P 4450 5300
F 0 "L1" H 4503 5346 50  0000 L CNN
F 1 "68uH" H 4503 5255 50  0000 L CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P5.08mm_Vertical" H 4450 5300 50  0001 C CNN
F 3 "~" H 4450 5300 50  0001 C CNN
	1    4450 5300
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C10
U 1 1 5E50EBBA
P 5350 5300
F 0 "C10" H 5400 5250 50  0000 L CNN
F 1 "0.01uF" H 5400 5400 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D5.0mm_W2.5mm_P2.50mm" H 5350 5300 50  0001 C CNN
F 3 "~" H 5350 5300 50  0001 C CNN
	1    5350 5300
	-1   0    0    1   
$EndComp
$Comp
L Device:CP1 C12
U 1 1 5E510355
P 5600 5300
F 0 "C12" H 5715 5346 50  0000 L CNN
F 1 "47uF" H 5700 5200 50  0000 L CNN
F 2 "Capacitors_ThroughHole:CP_Radial_D5.0mm_P2.50mm" H 5600 5300 50  0001 C CNN
F 3 "~" H 5600 5300 50  0001 C CNN
	1    5600 5300
	1    0    0    -1  
$EndComp
$Comp
L Device:CP1 C9
U 1 1 5E510B98
P 4450 4800
F 0 "C9" H 4565 4846 50  0000 L CNN
F 1 "29pF" H 4565 4755 50  0000 L CNN
F 2 "Capacitors_ThroughHole:CP_Radial_D5.0mm_P2.50mm" H 4450 4800 50  0001 C CNN
F 3 "~" H 4450 4800 50  0001 C CNN
	1    4450 4800
	1    0    0    -1  
$EndComp
$Comp
L Device:R R5
U 1 1 5E511080
P 2150 3600
F 0 "R5" V 2050 3550 50  0000 L CNN
F 1 "2.2K" V 2150 3550 50  0000 L CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 2080 3600 50  0001 C CNN
F 3 "~" H 2150 3600 50  0001 C CNN
	1    2150 3600
	0    1    1    0   
$EndComp
Wire Wire Line
	1700 3600 2000 3600
Wire Wire Line
	2300 3600 2400 3600
Connection ~ 2400 3600
Wire Wire Line
	5600 3400 5600 2300
Wire Wire Line
	3850 2300 5600 2300
$Comp
L Device:R R21
U 1 1 5E52804D
P 8250 2700
F 0 "R21" V 8150 2600 50  0000 L CNN
F 1 "75" V 8250 2650 50  0000 L CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 8180 2700 50  0001 C CNN
F 3 "~" H 8250 2700 50  0001 C CNN
	1    8250 2700
	0    1    1    0   
$EndComp
Wire Wire Line
	7550 2300 7550 3400
Wire Wire Line
	7550 3400 7300 3400
Wire Wire Line
	7300 3550 7650 3550
Wire Wire Line
	8400 2700 8500 2700
Text GLabel 8950 2700 2    50   Input ~ 0
RED_OUT
Wire Wire Line
	8950 2700 8800 2700
$Comp
L Device:R R22
U 1 1 5E533D5A
P 8250 3050
F 0 "R22" V 8150 2950 50  0000 L CNN
F 1 "75" V 8250 3000 50  0000 L CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 8180 3050 50  0001 C CNN
F 3 "~" H 8250 3050 50  0001 C CNN
	1    8250 3050
	0    1    1    0   
$EndComp
Wire Wire Line
	8400 3050 8500 3050
Text GLabel 8950 3050 2    50   Input ~ 0
GREEN_OUT
Wire Wire Line
	8950 3050 8800 3050
$Comp
L Device:R R23
U 1 1 5E53AA7E
P 8250 3400
F 0 "R23" V 8150 3300 50  0000 L CNN
F 1 "75" V 8250 3350 50  0000 L CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 8180 3400 50  0001 C CNN
F 3 "~" H 8250 3400 50  0001 C CNN
	1    8250 3400
	0    1    1    0   
$EndComp
Wire Wire Line
	8400 3400 8500 3400
Text GLabel 8950 3400 2    50   Input ~ 0
BLUE_OUT
Wire Wire Line
	8950 3400 8800 3400
$Comp
L Device:R R24
U 1 1 5E53D38F
P 8250 3750
F 0 "R24" V 8150 3650 50  0000 L CNN
F 1 "75" V 8250 3700 50  0000 L CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 8180 3750 50  0001 C CNN
F 3 "~" H 8250 3750 50  0001 C CNN
	1    8250 3750
	0    1    1    0   
$EndComp
Wire Wire Line
	7950 3750 8100 3750
Wire Wire Line
	8400 3750 8500 3750
$Comp
L Device:CP1 C24
U 1 1 5E53D39B
P 8650 3750
F 0 "C24" V 8550 3850 50  0000 L CNN
F 1 "220uF" V 8750 3800 50  0000 L CNN
F 2 "Capacitors_ThroughHole:CP_Radial_D5.0mm_P2.50mm" H 8650 3750 50  0001 C CNN
F 3 "~" H 8650 3750 50  0001 C CNN
	1    8650 3750
	0    -1   1    0   
$EndComp
Text GLabel 8950 3750 2    50   Input ~ 0
CVIDEO
Wire Wire Line
	8950 3750 8800 3750
$Comp
L Device:R R25
U 1 1 5E540370
P 8250 4100
F 0 "R25" V 8150 4000 50  0000 L CNN
F 1 "75" V 8250 4050 50  0000 L CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 8180 4100 50  0001 C CNN
F 3 "~" H 8250 4100 50  0001 C CNN
	1    8250 4100
	0    1    1    0   
$EndComp
Wire Wire Line
	8400 4100 8500 4100
$Comp
L Device:CP1 C25
U 1 1 5E54037C
P 8650 4100
F 0 "C25" V 8550 4200 50  0000 L CNN
F 1 "220uF" V 8750 4150 50  0000 L CNN
F 2 "Capacitors_ThroughHole:CP_Radial_D5.0mm_P2.50mm" H 8650 4100 50  0001 C CNN
F 3 "~" H 8650 4100 50  0001 C CNN
	1    8650 4100
	0    -1   1    0   
$EndComp
Text GLabel 8950 4450 2    50   Input ~ 0
SVIDEO-C
Wire Wire Line
	8950 4100 8800 4100
$Comp
L Device:R R26
U 1 1 5E543395
P 8250 4450
F 0 "R26" V 8150 4350 50  0000 L CNN
F 1 "75" V 8250 4400 50  0000 L CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 8180 4450 50  0001 C CNN
F 3 "~" H 8250 4450 50  0001 C CNN
	1    8250 4450
	0    1    1    0   
$EndComp
Wire Wire Line
	8400 4450 8500 4450
$Comp
L Device:CP1 C26
U 1 1 5E5433A1
P 8650 4450
F 0 "C26" V 8550 4550 50  0000 L CNN
F 1 "220uF" V 8750 4500 50  0000 L CNN
F 2 "Capacitors_ThroughHole:CP_Radial_D5.0mm_P2.50mm" H 8650 4450 50  0001 C CNN
F 3 "~" H 8650 4450 50  0001 C CNN
	1    8650 4450
	0    -1   1    0   
$EndComp
Text GLabel 8950 4100 2    50   Input ~ 0
SVIDEO-Y
Wire Wire Line
	8950 4450 8800 4450
Wire Wire Line
	7550 2300 8950 2300
Wire Wire Line
	7650 2700 7650 3550
Wire Wire Line
	7650 2700 8100 2700
Wire Wire Line
	7300 3650 7750 3650
Wire Wire Line
	7750 3050 8100 3050
Wire Wire Line
	7750 3050 7750 3650
Wire Wire Line
	7850 3400 7850 3750
Wire Wire Line
	7850 3750 7300 3750
Wire Wire Line
	7850 3400 8100 3400
Wire Wire Line
	7300 3850 7950 3850
Wire Wire Line
	7950 3850 7950 3750
$Comp
L Device:CP1 C23
U 1 1 5E5774D7
P 8650 3400
F 0 "C23" V 8550 3500 50  0000 L CNN
F 1 "220uF" V 8750 3450 50  0000 L CNN
F 2 "Capacitors_ThroughHole:CP_Radial_D5.0mm_P2.50mm" H 8650 3400 50  0001 C CNN
F 3 "~" H 8650 3400 50  0001 C CNN
	1    8650 3400
	0    -1   1    0   
$EndComp
$Comp
L Device:CP1 C22
U 1 1 5E577817
P 8650 3050
F 0 "C22" V 8550 3150 50  0000 L CNN
F 1 "220uF" V 8750 3100 50  0000 L CNN
F 2 "Capacitors_ThroughHole:CP_Radial_D5.0mm_P2.50mm" H 8650 3050 50  0001 C CNN
F 3 "~" H 8650 3050 50  0001 C CNN
	1    8650 3050
	0    -1   1    0   
$EndComp
$Comp
L Device:CP1 C21
U 1 1 5E5779E9
P 8650 2700
F 0 "C21" V 8550 2800 50  0000 L CNN
F 1 "220uF" V 8750 2750 50  0000 L CNN
F 2 "Capacitors_ThroughHole:CP_Radial_D5.0mm_P2.50mm" H 8650 2700 50  0001 C CNN
F 3 "~" H 8650 2700 50  0001 C CNN
	1    8650 2700
	0    -1   1    0   
$EndComp
$Comp
L Device:CP1 C18
U 1 1 5E577D86
P 7850 5350
F 0 "C18" H 7965 5396 50  0000 L CNN
F 1 "47uF" H 7950 5250 50  0000 L CNN
F 2 "Capacitors_ThroughHole:CP_Radial_D5.0mm_P2.50mm" H 7850 5350 50  0001 C CNN
F 3 "~" H 7850 5350 50  0001 C CNN
	1    7850 5350
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C17
U 1 1 5E586447
P 7600 5350
F 0 "C17" H 7650 5300 50  0000 L CNN
F 1 "0.01uF" H 7650 5450 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D5.0mm_W2.5mm_P2.50mm" H 7600 5350 50  0001 C CNN
F 3 "~" H 7600 5350 50  0001 C CNN
	1    7600 5350
	-1   0    0    1   
$EndComp
Wire Wire Line
	1700 5600 2400 5600
Wire Wire Line
	7300 4600 7600 4600
Wire Wire Line
	2400 3950 2400 5600
Connection ~ 2400 5600
Wire Wire Line
	2400 5600 2700 5600
Wire Wire Line
	2700 3950 2700 5600
Connection ~ 2700 5600
Wire Wire Line
	7600 5250 7600 4950
Wire Wire Line
	7600 5450 7600 5600
Wire Wire Line
	2700 5600 4450 5600
Connection ~ 7600 5600
Wire Wire Line
	7600 5600 7850 5600
Wire Wire Line
	7850 5500 7850 5600
Wire Wire Line
	8300 4950 7850 4950
Connection ~ 7600 4950
Wire Wire Line
	7600 4950 7600 4600
Wire Wire Line
	7850 5200 7850 4950
Connection ~ 7850 4950
Wire Wire Line
	7850 4950 7600 4950
Wire Wire Line
	4450 4950 4450 5150
Wire Wire Line
	4450 5450 4450 5600
Connection ~ 4450 5600
Connection ~ 5350 5600
Wire Wire Line
	5350 5600 5600 5600
Wire Wire Line
	4450 3950 4450 3600
Wire Wire Line
	4450 3950 5600 3950
Wire Wire Line
	4600 3850 4600 3400
Wire Wire Line
	4600 3850 5600 3850
Wire Wire Line
	4750 3200 4750 3750
Wire Wire Line
	4750 3750 5600 3750
Wire Wire Line
	4900 3650 4900 3000
Wire Wire Line
	4900 3000 4500 3000
Wire Wire Line
	4900 3650 5600 3650
Wire Wire Line
	5050 2800 5050 3550
Wire Wire Line
	5050 3550 5600 3550
$Comp
L Gemini:LM1881 U2
U 1 1 5E5FEEF0
P 6500 1450
F 0 "U2" H 6500 2031 50  0000 C CNN
F 1 "LM1881" H 6500 1940 50  0000 C CNN
F 2 "Housings_SOIC:SO-8_5.3x6.2mm_Pitch1.27mm" H 6500 1450 50  0001 C CNN
F 3 "" H 6500 1450 50  0001 C CNN
	1    6500 1450
	1    0    0    -1  
$EndComp
$Comp
L Device:R R19
U 1 1 5E5FF25C
P 8250 1100
F 0 "R19" V 8150 1000 50  0000 L CNN
F 1 "75" V 8250 1050 50  0000 L CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 8180 1100 50  0001 C CNN
F 3 "~" H 8250 1100 50  0001 C CNN
	1    8250 1100
	0    1    1    0   
$EndComp
Wire Wire Line
	8400 1100 8500 1100
Text GLabel 8950 1100 2    50   Input ~ 0
CSYNC_OUT
$Comp
L Device:CP1 C19
U 1 1 5E5FF26A
P 8650 1100
F 0 "C19" V 8550 1200 50  0000 L CNN
F 1 "220uF" V 8750 1150 50  0000 L CNN
F 2 "Capacitors_ThroughHole:CP_Radial_D5.0mm_P2.50mm" H 8650 1100 50  0001 C CNN
F 3 "~" H 8650 1100 50  0001 C CNN
	1    8650 1100
	0    -1   1    0   
$EndComp
$Comp
L Device:R R20
U 1 1 5E60D0A6
P 8250 1500
F 0 "R20" V 8150 1400 50  0000 L CNN
F 1 "75" V 8250 1450 50  0000 L CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 8180 1500 50  0001 C CNN
F 3 "~" H 8250 1500 50  0001 C CNN
	1    8250 1500
	0    1    1    0   
$EndComp
Wire Wire Line
	8400 1500 8500 1500
Text GLabel 8950 1500 2    50   Input ~ 0
VSYNC_OUT
$Comp
L Device:CP1 C20
U 1 1 5E60D0B3
P 8650 1500
F 0 "C20" V 8550 1600 50  0000 L CNN
F 1 "220uF" V 8750 1550 50  0000 L CNN
F 2 "Capacitors_ThroughHole:CP_Radial_D5.0mm_P2.50mm" H 8650 1500 50  0001 C CNN
F 3 "~" H 8650 1500 50  0001 C CNN
	1    8650 1500
	0    -1   1    0   
$EndComp
Wire Wire Line
	8000 1100 8100 1100
Wire Wire Line
	7000 1250 8000 1250
Wire Wire Line
	8000 1250 8000 1100
Wire Wire Line
	7000 1350 8000 1350
Wire Wire Line
	8000 1350 8000 1500
Wire Wire Line
	8000 1500 8100 1500
$Comp
L Device:C_Small C15
U 1 1 5E6293D7
P 5350 1250
F 0 "C15" V 5300 1100 50  0000 L CNN
F 1 "0.1uF" V 5400 1300 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D5.0mm_W2.5mm_P2.50mm" H 5350 1250 50  0001 C CNN
F 3 "~" H 5350 1250 50  0001 C CNN
	1    5350 1250
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C16
U 1 1 5E62E7C9
P 5600 1600
F 0 "C16" H 5692 1646 50  0000 L CNN
F 1 "0.1uF" H 5650 1550 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D5.0mm_W2.5mm_P2.50mm" H 5600 1600 50  0001 C CNN
F 3 "~" H 5600 1600 50  0001 C CNN
	1    5600 1600
	1    0    0    -1  
$EndComp
$Comp
L Device:R R6
U 1 1 5E62EAB2
P 5300 1600
F 0 "R6" H 5370 1646 50  0000 L CNN
F 1 "680K" V 5300 1550 50  0000 L CNN
F 2 "Potentiometers:Potentiometer_Trimmer_Piher_PT-10h2.5_Vertical_Px2.5mm_Py5.0mm" V 5230 1600 50  0001 C CNN
F 3 "~" H 5300 1600 50  0001 C CNN
	1    5300 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	5450 1250 6000 1250
Wire Wire Line
	5300 1450 5600 1450
Wire Wire Line
	5600 1450 5600 1500
Wire Wire Line
	5600 1450 6000 1450
Connection ~ 5600 1450
Wire Wire Line
	5600 1700 5600 1750
Wire Wire Line
	5600 1750 5300 1750
Wire Wire Line
	5300 1750 4950 1750
Connection ~ 5300 1750
Wire Wire Line
	5250 1250 4800 1250
Text GLabel 4800 1250 0    50   Input ~ 0
CSYNC
Text GLabel 4800 1750 0    50   Input ~ 0
GND
Wire Wire Line
	6500 1750 5600 1750
Connection ~ 5600 1750
Wire Wire Line
	6500 1050 4950 1050
Text GLabel 4800 1050 0    50   Input ~ 0
+5V
$Comp
L Device:C_Small C27
U 1 1 5E65A593
P 4950 1600
F 0 "C27" H 5042 1646 50  0000 L CNN
F 1 "0.1uF" H 5000 1550 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D5.0mm_W2.5mm_P2.50mm" H 4950 1600 50  0001 C CNN
F 3 "~" H 4950 1600 50  0001 C CNN
	1    4950 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	4950 1500 4950 1050
Connection ~ 4950 1050
Wire Wire Line
	4950 1050 4800 1050
Wire Wire Line
	4950 1700 4950 1750
Connection ~ 4950 1750
Wire Wire Line
	4950 1750 4800 1750
NoConn ~ 7000 1450
NoConn ~ 7000 1550
Text GLabel 8950 1900 2    50   Input ~ 0
HSYNC_OUT
Wire Wire Line
	4500 3200 4750 3200
Text Notes 7100 7000 0    50   ~ 0
Prototype 9958 video for Geneve2020 by Erik Olson.\n\nAnalog section based on MTX+ by Dave Stevenson.\nhttp://primrosebank.net/computers/mtx/projects/mtxplus/video/mtxplus_vdp.htm
Text GLabel 1350 6350 2    50   Input ~ 0
RED
Text GLabel 1350 6150 2    50   Input ~ 0
GND
Text GLabel 1350 6250 2    50   Input ~ 0
GREEN
Text GLabel 1350 6450 2    50   Input ~ 0
BLUE
Text GLabel 1350 5950 2    50   Input ~ 0
CSYNC
Text GLabel 1350 6050 2    50   Input ~ 0
GND
Text GLabel 850  6550 0    50   Input ~ 0
+5V
Text GLabel 850  5950 0    50   Input ~ 0
HSYNC
Text GLabel 850  7300 0    50   Input ~ 0
GND
Text GLabel 850  6900 0    50   Input ~ 0
GND
Text GLabel 850  6350 0    50   Input ~ 0
GND
Text GLabel 1350 5850 2    50   Input ~ 0
VRESET*
Text GLabel 850  7100 0    50   Input ~ 0
GND
Text GLabel 1350 6550 2    50   Input ~ 0
HRESET*
Text GLabel 850  6050 0    50   Input ~ 0
SCCLK
Text GLabel 850  7200 0    50   Input ~ 0
GND
Text GLabel 850  7400 0    50   Input ~ 0
GND
Text GLabel 850  7000 0    50   Input ~ 0
GND
Text GLabel 850  5850 0    50   Input ~ 0
GND
Text GLabel 850  7600 0    50   Input ~ 0
+5V
Text GLabel 850  7500 0    50   Input ~ 0
+5V
Text GLabel 850  6150 0    50   Input ~ 0
YS*
Text GLabel 850  6250 0    50   Input ~ 0
GND
Text GLabel 850  6450 0    50   Input ~ 0
GND
Text GLabel 1350 7300 2    50   Input ~ 0
GND
Text GLabel 1350 6900 2    50   Input ~ 0
GND
Text GLabel 1350 7100 2    50   Input ~ 0
GND
Text GLabel 1350 7200 2    50   Input ~ 0
GND
Text GLabel 1350 7400 2    50   Input ~ 0
GND
Text GLabel 1350 7000 2    50   Input ~ 0
GND
Text GLabel 1350 7600 2    50   Input ~ 0
+5V
Text GLabel 1350 7500 2    50   Input ~ 0
+5V
$Comp
L Connector_Generic:Conn_02x08_Odd_Even J4
U 1 1 5E53C0DA
P 1050 7200
F 0 "J4" H 1158 7589 50  0000 C CNN
F 1 "Conn_02x08" H 1158 7590 50  0001 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x08_Pitch2.54mm" H 1050 7200 50  0001 C CNN
F 3 "~" H 1050 7200 50  0001 C CNN
	1    1050 7200
	1    0    0    -1  
$EndComp
Wire Wire Line
	8800 1100 8950 1100
Wire Wire Line
	8800 1500 8950 1500
$Comp
L Connector_Generic:Conn_02x08_Odd_Even J3
U 1 1 5E5B65F9
P 10550 3150
F 0 "J3" H 10630 3142 50  0000 L CNN
F 1 "Conn" H 10630 3051 50  0001 L CNN
F 2 "Connector_IDC:IDC-Header_2x08_P2.54mm_Horizontal" H 10550 3150 50  0001 C CNN
F 3 "~" H 10550 3150 50  0001 C CNN
	1    10550 3150
	-1   0    0    1   
$EndComp
Text GLabel 10250 3150 0    50   Input ~ 0
RED_OUT
Text GLabel 10250 3050 0    50   Input ~ 0
GREEN_OUT
Text GLabel 10250 2950 0    50   Input ~ 0
BLUE_OUT
Text GLabel 10750 3350 2    50   Input ~ 0
GND
Text GLabel 10250 3350 0    50   Input ~ 0
VSYNC_OUT
Text GLabel 10750 2850 2    50   Input ~ 0
CVIDEO
Text GLabel 10250 3250 0    50   Input ~ 0
HSYNC_OUT
NoConn ~ 1000 5550
NoConn ~ 1000 5350
NoConn ~ 1000 5450
Text GLabel 900  5350 0    50   Input ~ 0
VRESET*
Text GLabel 900  5450 0    50   Input ~ 0
YS*
Text GLabel 900  5550 0    50   Input ~ 0
HRESET*
Wire Wire Line
	900  5350 1000 5350
Wire Wire Line
	1000 5450 900  5450
Wire Wire Line
	900  5550 1000 5550
Text Notes 10000 2550 0    50   ~ 0
Cable To Video Port
Text GLabel 10750 3450 2    50   Input ~ 0
GND
Text GLabel 10750 2950 2    50   Input ~ 0
GND
Text GLabel 10750 3050 2    50   Input ~ 0
GND
Text GLabel 10750 3150 2    50   Input ~ 0
GND
Text GLabel 10250 2750 0    50   Input ~ 0
+5V
Text GLabel 10750 2750 2    50   Input ~ 0
SVIDEO-C
Text GLabel 10250 2850 0    50   Input ~ 0
SVIDEO-Y
Text GLabel 10750 3250 2    50   Input ~ 0
GND
Text GLabel 10250 3450 0    50   Input ~ 0
CSYNC_OUT
Text Label 7250 1250 0    50   ~ 0
CSYNC_O
Text Label 7250 1350 0    50   ~ 0
VSYNC_O
Text Label 5650 1250 0    50   ~ 0
CVIDEO_IN
Text Label 5700 1450 0    50   ~ 0
RSET_IN
Text Label 7300 3550 0    50   ~ 0
ROUT
Text Label 7300 3650 0    50   ~ 0
GOUT
Text Label 7300 3750 0    50   ~ 0
BOUT
Text Label 7300 3850 0    50   ~ 0
CVOUT
Text Label 7300 4050 0    50   ~ 0
YOUT
Text Label 7300 4150 0    50   ~ 0
COUT
Text Label 5350 4050 0    50   ~ 0
YTRAP
Text Label 5350 3950 0    50   ~ 0
SCIN
Text Label 5350 3850 0    50   ~ 0
SYNCIN
Text Label 5350 3750 0    50   ~ 0
BIN
Text Label 5350 3650 0    50   ~ 0
GIN
Text Label 5350 3550 0    50   ~ 0
RIN
Text Label 5050 4350 0    50   ~ 0
NTSC_PAL
Wire Wire Line
	5600 4050 4450 4050
Wire Wire Line
	4450 4050 4450 4650
Wire Wire Line
	7300 4050 7950 4050
Wire Wire Line
	7950 4050 7950 4100
Wire Wire Line
	7950 4100 8100 4100
Wire Wire Line
	7300 4150 7850 4150
Wire Wire Line
	7850 4150 7850 4450
Wire Wire Line
	7850 4450 8100 4450
$Comp
L Connector:Conn_01x03_Male J12
U 1 1 5E76B30E
P 5050 4700
F 0 "J12" H 5100 4500 50  0000 C CNN
F 1 "Conn_01x03_Male" H 5158 4890 50  0001 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03_Pitch2.54mm" H 5050 4700 50  0001 C CNN
F 3 "~" H 5050 4700 50  0001 C CNN
	1    5050 4700
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5600 5150 5600 4950
Wire Wire Line
	5600 5450 5600 5600
Connection ~ 5600 5600
Wire Wire Line
	5600 5600 7600 5600
Connection ~ 5600 4950
Wire Wire Line
	5600 4950 5600 4600
Wire Wire Line
	5350 5400 5350 5600
Wire Wire Line
	5350 5200 5350 4950
Connection ~ 5350 4950
Wire Wire Line
	5350 4950 5600 4950
Wire Wire Line
	5200 4950 5350 4950
Text Notes 4950 4750 1    50   ~ 0
PAL
Text Notes 5150 4800 1    50   ~ 0
NTSC
Wire Wire Line
	4450 5600 4800 5600
Wire Wire Line
	5050 4150 5050 4500
Wire Wire Line
	5150 4500 5350 4500
Wire Wire Line
	4950 4500 4800 4500
Wire Wire Line
	4800 4500 4800 5600
Connection ~ 4800 5600
Wire Wire Line
	4800 5600 5350 5600
Wire Wire Line
	5050 4150 5600 4150
Wire Wire Line
	5350 4500 5350 4950
$Comp
L Device:R R7
U 1 1 5E84D442
P 8250 1900
F 0 "R7" V 8150 1800 50  0000 L CNN
F 1 "75" V 8250 1850 50  0000 L CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 8180 1900 50  0001 C CNN
F 3 "~" H 8250 1900 50  0001 C CNN
	1    8250 1900
	0    1    1    0   
$EndComp
Wire Wire Line
	8400 1900 8500 1900
$Comp
L Device:CP1 C11
U 1 1 5E84D44E
P 8650 1900
F 0 "C11" V 8550 2000 50  0000 L CNN
F 1 "220uF" V 8750 1950 50  0000 L CNN
F 2 "Capacitors_ThroughHole:CP_Radial_D5.0mm_P2.50mm" H 8650 1900 50  0001 C CNN
F 3 "~" H 8650 1900 50  0001 C CNN
	1    8650 1900
	0    -1   1    0   
$EndComp
Wire Wire Line
	8000 1900 8100 1900
Wire Wire Line
	8800 1900 8950 1900
Text GLabel 8000 1900 0    50   Input ~ 0
HSYNC
Text Notes 7250 2050 0    50   ~ 0
Does this need a buffer?
$Sheet
S 10150 3850 750  500 
U 604D8E9C
F0 "video port" 50
F1 "video_port_template.sch" 50
$EndSheet
$EndSCHEMATC
