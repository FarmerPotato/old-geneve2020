EESchema Schematic File Version 4
LIBS:rgb-adaptor-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 7450 6850 0    100  ~ 0
Cabled to vdp mezzanine
$Comp
L Connector_Generic:Conn_02x08_Odd_Even J?
U 1 1 60BB55BD
P 2000 1200
F 0 "J?" H 2080 1192 50  0000 L CNN
F 1 "Conn" H 2080 1101 50  0001 L CNN
F 2 "Connector_IDC:IDC-Header_2x08_P2.54mm_Horizontal" H 2000 1200 50  0001 C CNN
F 3 "~" H 2000 1200 50  0001 C CNN
	1    2000 1200
	-1   0    0    1   
$EndComp
Text GLabel 1700 1200 0    50   Input ~ 0
RED_OUT
Text GLabel 1700 1100 0    50   Input ~ 0
GREEN_OUT
Text GLabel 1700 1000 0    50   Input ~ 0
BLUE_OUT
Text GLabel 2200 1400 2    50   Input ~ 0
GND
Text GLabel 1700 1400 0    50   Input ~ 0
VSYNC_OUT
Text GLabel 2200 900  2    50   Input ~ 0
CVIDEO
Text GLabel 1700 1300 0    50   Input ~ 0
HSYNC_OUT
Text Notes 1450 1850 0    50   ~ 0
Cable From Video Port
Text GLabel 2200 1500 2    50   Input ~ 0
GND
Text GLabel 2200 1000 2    50   Input ~ 0
GND
Text GLabel 2200 1100 2    50   Input ~ 0
GND
Text GLabel 2200 1200 2    50   Input ~ 0
GND
Text GLabel 1700 800  0    50   Input ~ 0
+5V
Text GLabel 2200 800  2    50   Input ~ 0
SVIDEO-C
Text GLabel 1700 900  0    50   Input ~ 0
SVIDEO-Y
Text GLabel 2200 1300 2    50   Input ~ 0
GND
Text GLabel 1700 1500 0    50   Input ~ 0
CSYNC_OUT
$Comp
L Connector:DIN-8 J?
U 1 1 60BB59E2
P 1900 3300
F 0 "J?" H 1900 3781 50  0000 C CNN
F 1 "DIN-8" H 1900 3690 50  0000 C CNN
F 2 "Connect:SDS-50J" H 1900 3300 50  0001 C CNN
F 3 "http://www.mouser.com/ds/2/18/40_c091_abd_e-75918.pdf" H 1900 3300 50  0001 C CNN
	1    1900 3300
	1    0    0    -1  
$EndComp
$Comp
L Connector:SCART-F J?
U 1 1 60BBE7E7
P 7150 2950
F 0 "J?" H 7150 4370 50  0000 C CNN
F 1 "SCART-F" H 7150 4279 50  0000 C CNN
F 2 "" H 7150 3000 50  0001 C CNN
F 3 " ~" H 7150 3000 50  0001 C CNN
	1    7150 2950
	1    0    0    -1  
$EndComp
Text GLabel 1600 3300 0    50   Input ~ 0
+12V
Text Notes 1600 4000 0    50   ~ 0
Geneve 9640 RGB
Text Notes 6600 4600 0    50   ~ 0
SCART e.g. Samsung 910MP
Text Notes 1500 4150 0    50   ~ 0
+12V is for SCART Genie
Text GLabel 1900 3000 2    50   Input ~ 0
GND
NoConn ~ 2200 3300
Text GLabel 1600 3200 0    50   Input ~ 0
CVIDEO
Text GLabel 2200 3200 2    50   Input ~ 0
RED_OUT
Text GLabel 1600 3400 0    50   Input ~ 0
GREEN_OUT
Text GLabel 2200 3400 2    50   Input ~ 0
BLUE_OUT
Text GLabel 1900 3600 0    50   Input ~ 0
CSYNC_OUT
Text Label 2200 3300 0    50   ~ 0
AUDIO_OUT
Text GLabel 6150 2650 0    50   Input ~ 0
GREEN_OUT
Text GLabel 6150 2750 0    50   Input ~ 0
BLUE_OUT
Text GLabel 6150 2550 0    50   Input ~ 0
RED_OUT
Text GLabel 7750 2250 2    50   Input ~ 0
GND
Text Label 7750 3850 0    50   ~ 0
AUDIO_L
Text Label 7750 3450 0    50   ~ 0
AUDIO_R
NoConn ~ 7750 3450
NoConn ~ 7750 3850
Text GLabel 7750 3650 2    50   Input ~ 0
GND
Text GLabel 6550 3550 0    50   Input ~ 0
GND
Text GLabel 6550 3150 0    50   Input ~ 0
GND
Text GLabel 6550 2750 0    50   Input ~ 0
GND
Text GLabel 6550 2350 0    50   Input ~ 0
GND
Text GLabel 6550 1950 0    50   Input ~ 0
GND
$Comp
L Device:R_Small R?
U 1 1 60BC2D03
P 5850 3600
F 0 "R?" H 5791 3554 50  0000 R CNN
F 1 "180" H 5791 3645 50  0000 R CNN
F 2 "" H 5850 3600 50  0001 C CNN
F 3 "~" H 5850 3600 50  0001 C CNN
	1    5850 3600
	-1   0    0    1   
$EndComp
Text GLabel 5050 3150 0    50   Input ~ 0
+5V
Text Notes 1950 4900 0    50   ~ 0
http://mirrors.arcadecontrols.com/eviltim/eviltim/gamescart/gamescart.htm\nSony Playstation 1,2 have CXA1645 and 75 ohm resistor (no cap)\nNeo Geo has CXA1145, 75 ohm resistor, coupling cap.\n
$Comp
L Connector_Generic:Conn_02x04_Odd_Even J?
U 1 1 60BD479A
P 5250 3050
F 0 "J?" H 5300 3275 50  0000 C CNN
F 1 "Conn_02x04_Odd_Even" H 5300 3276 50  0001 C CNN
F 2 "" H 5250 3050 50  0001 C CNN
F 3 "~" H 5250 3050 50  0001 C CNN
	1    5250 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	6250 2950 6250 2650
Wire Wire Line
	6250 2950 6550 2950
Wire Wire Line
	6150 2750 6150 3350
Wire Wire Line
	6150 3350 6550 3350
Text GLabel 5050 3050 0    50   Input ~ 0
+5V
Wire Wire Line
	8150 4150 8150 3250
Wire Wire Line
	7750 3250 8150 3250
Text GLabel 5050 3250 0    50   Input ~ 0
+12V
Wire Wire Line
	5850 3050 5850 3500
Wire Wire Line
	6250 2650 6150 2650
Wire Wire Line
	6150 2550 6550 2550
Wire Wire Line
	5650 3250 5650 4150
Wire Wire Line
	5650 4150 8150 4150
NoConn ~ 7750 3050
Text Label 7750 3050 0    50   ~ 0
D2B
Text Label 7750 2850 0    50   ~ 0
Data1
NoConn ~ 7750 2850
Text GLabel 7750 2650 2    50   Input ~ 0
GND
Text Notes 8200 2550 0    50   ~ 0
1-3V select RGB
$Comp
L Device:R_Small R?
U 1 1 60BF542A
P 8550 2250
F 0 "R?" H 8491 2204 50  0000 R CNN
F 1 "220" H 8491 2295 50  0000 R CNN
F 2 "" H 8550 2250 50  0001 C CNN
F 3 "~" H 8550 2250 50  0001 C CNN
	1    8550 2250
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R?
U 1 1 60BF5957
P 8550 2700
F 0 "R?" H 8491 2654 50  0000 R CNN
F 1 "330" H 8491 2745 50  0000 R CNN
F 2 "" H 8550 2700 50  0001 C CNN
F 3 "~" H 8550 2700 50  0001 C CNN
	1    8550 2700
	-1   0    0    1   
$EndComp
Text GLabel 8550 2050 0    50   UnSpc ~ 0
+5V
Wire Wire Line
	8550 2350 8550 2450
Connection ~ 8550 2450
Wire Wire Line
	8550 2450 8550 2600
Wire Wire Line
	7750 2450 8550 2450
Text Label 5850 3500 1    50   ~ 0
RGBselector
Text GLabel 8550 2900 2    50   Input ~ 0
GND
Wire Wire Line
	8550 2050 8550 2150
Wire Wire Line
	8550 2800 8550 2900
NoConn ~ 6550 2150
NoConn ~ 6550 3750
NoConn ~ 6550 3950
Text Notes 6500 4100 1    50   ~ 0
Audio Down
Text Notes 7700 3600 0    50   ~ 0
Audio Gnd
Text Label 6550 2150 2    50   ~ 0
CVIDEO_DN
Text Label 7800 3250 0    50   ~ 0
ASPECT
Text Notes 8200 3350 0    50   ~ 0
0-2    OFF\n5-8    16:9\n9.5-12 4:3
$Comp
L Regulator_Switching:LT1108CS-12 U?
U 1 1 60BFAC8C
P 3850 3100
F 0 "U?" H 3406 3146 50  0000 R CNN
F 1 "LT1108CS-12" H 3406 3055 50  0000 R CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 3900 2750 50  0001 L CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/lt1108.pdf" H 3650 3700 50  0001 C CNN
	1    3850 3100
	1    0    0    -1  
$EndComp
Text Notes 3500 2550 0    50   ~ 0
12V Boost
Text GLabel 5050 2950 0    50   Input ~ 0
CSYNC_OUT
Wire Wire Line
	5550 1400 7850 1400
Wire Wire Line
	7850 1400 7850 2050
Wire Wire Line
	7850 2050 7750 2050
Wire Wire Line
	5550 3050 5850 3050
Wire Wire Line
	5650 3250 5550 3250
Wire Wire Line
	5550 1400 5550 2950
Text Notes 5150 2800 0    50   ~ 0
Jumpers
Text Label 5750 1400 0    50   ~ 0
CSYNC
Text Notes 5700 1300 0    50   ~ 0
CVIDEO UP
$EndSCHEMATC
