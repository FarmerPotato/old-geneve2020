EESchema Schematic File Version 4
LIBS:sdc-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:SD_Card J4
U 1 1 600DDCA5
P 9950 5450
F 0 "J4" H 9950 6115 50  0000 C CNN
F 1 "SD_Card" H 9950 6024 50  0000 C CNN
F 2 "Connector_Card:microSD_HC_Hirose_DM3BT-DSF-PEJS" H 9950 5450 50  0001 C CNN
F 3 "" H 9950 5450 50  0001 C CNN
	1    9950 5450
	0    1    1    0   
$EndComp
$Comp
L Connector:SD_Card J3
U 1 1 600DE2F9
P 8450 5450
F 0 "J3" H 8450 6115 50  0000 C CNN
F 1 "SD_Card" H 8450 6024 50  0000 C CNN
F 2 "Connector_Card:SD_Kyocera_145638009511859+" H 8450 5450 50  0001 C CNN
F 3 "" H 8450 5450 50  0001 C CNN
	1    8450 5450
	0    1    1    0   
$EndComp
Text GLabel 850  1600 0    50   Input ~ 0
CRUOUT
Text GLabel 11050 3850 2    50   Output ~ 0
CRUIN
Text GLabel 850  2200 0    50   Input ~ 0
A0
Text GLabel 850  2300 0    50   Input ~ 0
A1
Text GLabel 850  2400 0    50   Input ~ 0
A2
Text Notes 4000 1150 2    50   ~ 0
Control Register
Text Notes 3700 3900 2    50   ~ 0
Either Read or Write drives SCLK low.
Text GLabel 850  1300 0    50   Input ~ 0
RESOUT*
Text Notes 5100 2800 0    50   ~ 0
Card Select 1,2,3\nstay enabled across\nmany bytes
Text Notes 5750 4250 2    50   ~ 0
Normally High
NoConn ~ 10900 1400
Wire Wire Line
	2250 1500 2750 1500
Text Notes 8450 7800 0    50   ~ 0
Reference:\nhttp://elm-chan.org/docs/mmc/mmc_e.html
$Comp
L Connector:SD_Card J2
U 1 1 6034B000
P 6950 5450
F 0 "J2" H 6950 6115 50  0000 C CNN
F 1 "SD_Card" H 6950 6024 50  0000 C CNN
F 2 "Connector_Card:SD_Kyocera_145638109511859+" H 6950 5450 50  0001 C CNN
F 3 "" H 6950 5450 50  0001 C CNN
	1    6950 5450
	0    1    1    0   
$EndComp
Wire Wire Line
	850  1300 2750 1300
Text Notes 7500 3450 0    50   ~ 0
All SDC inputs must be 3.3V logic!
$Comp
L 74xx:74LS139 U1
U 2 1 60381490
P 1350 5000
F 0 "U1" H 1350 5367 50  0000 C CNN
F 1 "74LVC139" H 1350 5276 50  0000 C CNN
F 2 "Package_SO:SOIC-16_3.9x9.9mm_P1.27mm" H 1350 5000 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS139" H 1350 5000 50  0001 C CNN
	2    1350 5000
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS139 U1
U 3 1 60382224
P 900 7250
F 0 "U1" H 1300 7500 50  0000 R CNN
F 1 "74LVC139" V 1200 7400 50  0000 R CNN
F 2 "Package_SO:SOIC-16_3.9x9.9mm_P1.27mm" H 900 7250 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS139" H 900 7250 50  0001 C CNN
	3    900  7250
	-1   0    0    -1  
$EndComp
Text GLabel 800  6650 0    50   Input ~ 0
+3.3V
Text GLabel 4250 3200 0    50   Input ~ 0
GND
Text GLabel 850  5000 0    50   Input ~ 0
A5
$Comp
L Gemini:74LS259 U4
U 1 1 6010CC11
P 3250 2000
F 0 "U4" H 3250 3131 50  0000 C CNN
F 1 "74LS259" H 3250 3040 50  0000 C CNN
F 2 "Package_SO:SOIC-16_3.9x9.9mm_P1.27mm" H 3250 2250 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS259" H 3250 2250 50  0001 C CNN
	1    3250 2000
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS139 U1
U 1 1 60381BF8
P 4750 3000
F 0 "U1" H 4750 3367 50  0000 C CNN
F 1 "74LVC139" H 4750 3276 50  0000 C CNN
F 2 "Package_SO:SOIC-16_3.9x9.9mm_P1.27mm" H 4750 3000 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS139" H 4750 3000 50  0001 C CNN
	1    4750 3000
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS125 U2
U 1 1 602C022D
P 10750 3850
F 0 "U2" H 10750 4075 50  0000 C CNN
F 1 "74LVC125" H 10750 4166 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 10750 3850 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS125" H 10750 3850 50  0001 C CNN
	1    10750 3850
	1    0    0    1   
$EndComp
$Comp
L 74xx:74LS251 U6
U 1 1 600E2B1F
P 10400 1900
F 0 "U6" H 10400 2981 50  0000 C CNN
F 1 "74LS251" H 10400 2890 50  0000 C CNN
F 2 "Package_SO:SOIC-16_3.9x9.9mm_P1.27mm" H 10400 1900 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS251" H 10400 1900 50  0001 C CNN
	1    10400 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	10900 1300 11100 1300
$Comp
L 74xx:74LS125 U2
U 2 1 60549A83
P 4650 3950
F 0 "U2" H 4650 4175 50  0000 C CNN
F 1 "74LVC125" H 4650 4266 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 4650 3950 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS125" H 4650 3950 50  0001 C CNN
	2    4650 3950
	1    0    0    1   
$EndComp
Text GLabel 4650 3700 2    50   Input ~ 0
GND
Text Notes 4750 3850 0    50   ~ 0
DI Level Shifter
Text GLabel 11050 2950 1    50   Input ~ 0
+3.3V
$Comp
L Device:LED D1
U 1 1 60557052
P 7900 2100
F 0 "D1" H 7900 2200 50  0000 R CNN
F 1 "LED" V 7848 1983 50  0001 R CNN
F 2 "LEDs:LED_1206" H 7900 2100 50  0001 C CNN
F 3 "~" H 7900 2100 50  0001 C CNN
	1    7900 2100
	1    0    0    -1  
$EndComp
Text Notes 10300 3450 0    50   ~ 0
Card Select LEDs
$Comp
L Device:R R20
U 1 1 60562675
P 7500 2100
F 0 "R20" V 7550 2150 50  0000 L CNN
F 1 "100" V 7500 2050 50  0000 L CNN
F 2 "Resistors_SMD:R_1206" V 7430 2100 50  0001 C CNN
F 3 "~" H 7500 2100 50  0001 C CNN
	1    7500 2100
	0    -1   -1   0   
$EndComp
Text GLabel 6450 3350 0    50   Input ~ 0
+3.3V
Text GLabel 6450 4250 0    50   Input ~ 0
GND
NoConn ~ 3750 1900
NoConn ~ 3750 2000
NoConn ~ 3750 1700
NoConn ~ 3750 1800
Text GLabel 2650 5300 0    50   Input ~ 0
CRUOUT
Text GLabel 3150 5300 2    50   Input ~ 0
A0
Text GLabel 2650 5400 0    50   Input ~ 0
A1
Text GLabel 3150 5400 2    50   Input ~ 0
A2
Text GLabel 3150 5800 2    50   Input ~ 0
RESOUT*
Text GLabel 2650 5900 0    50   Input ~ 0
+5V
Text GLabel 2650 5600 0    50   Input ~ 0
A5
Text GLabel 3150 5700 2    50   Input ~ 0
WE*
Text GLabel 2650 5800 0    50   Input ~ 0
RD*
Text GLabel 3150 5500 2    50   Input ~ 0
A4
Text GLabel 3150 5900 2    50   Input ~ 0
+3.3V
Text GLabel 2650 5200 0    50   Input ~ 0
GND
NoConn ~ 2650 5500
Text GLabel 2650 5700 0    50   Output ~ 0
CRUIN
NoConn ~ 6850 6350
NoConn ~ 6750 6350
NoConn ~ 8250 6350
NoConn ~ 8350 6350
NoConn ~ 9750 6350
NoConn ~ 9850 6350
Text Notes 5500 1300 0    50   ~ 0
DSR ROM enable
Text Notes 5500 1400 0    50   ~ 0
Card Select LSBit
Wire Wire Line
	9800 2000 9900 2000
NoConn ~ 1850 5200
NoConn ~ 1850 5100
NoConn ~ 1850 5000
Text GLabel 3250 2900 0    50   Input ~ 0
GND
Text GLabel 800  7750 0    50   Input ~ 0
GND
Text GLabel 10400 2900 0    50   Input ~ 0
GND
Text GLabel 3250 1000 0    50   Input ~ 0
+5V
Text Label 7350 1300 0    50   ~ 0
ROMEN
Text Label 7650 2100 0    50   ~ 0
LED0
Text Label 9050 3100 0    50   ~ 0
LED2
Wire Wire Line
	7650 2100 7750 2100
$Comp
L Connector_Generic:Conn_02x08_Odd_Even J1
U 1 1 6011F32F
P 2850 5500
F 0 "J1" H 2900 6017 50  0000 C CNN
F 1 "Conn_02x08_Odd_Even" H 3200 5950 50  0000 C CNN
F 2 "Connector_IDC:IDC-Header_2x08_P2.54mm_Horizontal" H 2850 5500 50  0001 C CNN
F 3 "~" H 2850 5500 50  0001 C CNN
	1    2850 5500
	1    0    0    -1  
$EndComp
Text Label 2650 5500 0    50   ~ 0
A3
Text GLabel 850  4900 0    50   Input ~ 0
A6
Wire Wire Line
	750  4550 1950 4550
Wire Wire Line
	1950 4900 1850 4900
Text Label 1600 4550 0    50   ~ 0
CRUMSK
Text GLabel 3150 5600 2    50   Input ~ 0
A6
Text Notes 900  5450 0    50   ~ 0
Mask off 1640 and above
NoConn ~ 3750 1600
Wire Wire Line
	3950 1500 3950 2900
Wire Wire Line
	3850 1400 3850 3000
Wire Wire Line
	9900 1800 9800 1800
$Comp
L Device:C C6
U 1 1 6045E06D
P 3850 7600
F 0 "C6" H 3965 7646 50  0000 L CNN
F 1 "cap" H 3965 7555 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206" H 3888 7450 50  0001 C CNN
F 3 "~" H 3850 7600 50  0001 C CNN
	1    3850 7600
	1    0    0    -1  
$EndComp
$Comp
L Device:C C7
U 1 1 6045F43C
P 4200 7600
F 0 "C7" H 4315 7646 50  0000 L CNN
F 1 "cap" H 4315 7555 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206" H 4238 7450 50  0001 C CNN
F 3 "~" H 4200 7600 50  0001 C CNN
	1    4200 7600
	1    0    0    -1  
$EndComp
$Comp
L Device:C C8
U 1 1 6045FC1D
P 4550 7600
F 0 "C8" H 4665 7646 50  0000 L CNN
F 1 "cap" H 4665 7555 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206" H 4588 7450 50  0001 C CNN
F 3 "~" H 4550 7600 50  0001 C CNN
	1    4550 7600
	1    0    0    -1  
$EndComp
$Comp
L Device:C C9
U 1 1 6045FC27
P 4900 7600
F 0 "C9" H 5015 7646 50  0000 L CNN
F 1 "cap" H 5015 7555 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206" H 4938 7450 50  0001 C CNN
F 3 "~" H 4900 7600 50  0001 C CNN
	1    4900 7600
	1    0    0    -1  
$EndComp
$Comp
L Device:C C10
U 1 1 604650C1
P 5250 7600
F 0 "C10" H 5365 7646 50  0000 L CNN
F 1 "cap" H 5365 7555 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206" H 5288 7450 50  0001 C CNN
F 3 "~" H 5250 7600 50  0001 C CNN
	1    5250 7600
	1    0    0    -1  
$EndComp
$Comp
L Device:C C11
U 1 1 604650CB
P 5600 7600
F 0 "C11" H 5715 7646 50  0000 L CNN
F 1 "cap" H 5715 7555 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206" H 5638 7450 50  0001 C CNN
F 3 "~" H 5600 7600 50  0001 C CNN
	1    5600 7600
	1    0    0    -1  
$EndComp
$Comp
L Device:C C12
U 1 1 604650D5
P 5950 7600
F 0 "C12" H 6065 7646 50  0000 L CNN
F 1 "cap" H 6065 7555 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206" H 5988 7450 50  0001 C CNN
F 3 "~" H 5950 7600 50  0001 C CNN
	1    5950 7600
	1    0    0    -1  
$EndComp
$Comp
L Device:C C13
U 1 1 604650DF
P 6300 7600
F 0 "C13" H 6415 7646 50  0000 L CNN
F 1 "cap" H 6415 7555 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206" H 6338 7450 50  0001 C CNN
F 3 "~" H 6300 7600 50  0001 C CNN
	1    6300 7600
	1    0    0    -1  
$EndComp
Text Label 6450 6450 0    50   ~ 0
CD1
Wire Wire Line
	6300 7450 5950 7450
Connection ~ 4200 7450
Wire Wire Line
	4200 7450 3850 7450
Connection ~ 4550 7450
Wire Wire Line
	4550 7450 4200 7450
Connection ~ 4900 7450
Wire Wire Line
	4900 7450 4550 7450
Connection ~ 5250 7450
Wire Wire Line
	5250 7450 4900 7450
Connection ~ 5600 7450
Wire Wire Line
	5600 7450 5250 7450
Connection ~ 5950 7450
Wire Wire Line
	5950 7450 5600 7450
Text GLabel 9600 1000 0    50   Input ~ 0
+5V
$Comp
L Device:C C1
U 1 1 604E2D86
P 2100 7600
F 0 "C1" H 2215 7646 50  0000 L CNN
F 1 "cap" H 2215 7555 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206" H 2138 7450 50  0001 C CNN
F 3 "~" H 2100 7600 50  0001 C CNN
	1    2100 7600
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 604E2D90
P 2450 7600
F 0 "C2" H 2565 7646 50  0000 L CNN
F 1 "cap" H 2565 7555 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206" H 2488 7450 50  0001 C CNN
F 3 "~" H 2450 7600 50  0001 C CNN
	1    2450 7600
	1    0    0    -1  
$EndComp
Wire Wire Line
	2450 7450 2100 7450
Connection ~ 2100 7750
Wire Wire Line
	2100 7750 2450 7750
$Comp
L power:+5V #PWR02
U 1 1 6050B9E5
P 2650 6100
F 0 "#PWR02" H 2650 5950 50  0001 C CNN
F 1 "+5V" V 2650 6200 50  0000 L CNN
F 2 "" H 2650 6100 50  0001 C CNN
F 3 "" H 2650 6100 50  0001 C CNN
	1    2650 6100
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR01
U 1 1 6050C4F1
P 2650 4900
F 0 "#PWR01" H 2650 4650 50  0001 C CNN
F 1 "GND" V 2655 4772 50  0000 R CNN
F 2 "" H 2650 4900 50  0001 C CNN
F 3 "" H 2650 4900 50  0001 C CNN
	1    2650 4900
	0    1    1    0   
$EndComp
$Comp
L power:+3.3V #PWR03
U 1 1 6050CCDB
P 3150 6100
F 0 "#PWR03" H 3150 5950 50  0001 C CNN
F 1 "+3.3V" V 3150 6200 50  0000 L CNN
F 2 "" H 3150 6100 50  0001 C CNN
F 3 "" H 3150 6100 50  0001 C CNN
	1    3150 6100
	0    -1   -1   0   
$EndComp
$Comp
L 74xx:74LS125 U2
U 5 1 6028C3C8
P 1500 7250
F 0 "U2" H 1200 7000 50  0000 C CNN
F 1 "74LVC125" V 1250 7300 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 1500 7250 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS125" H 1500 7250 50  0001 C CNN
	5    1500 7250
	-1   0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG01
U 1 1 60515312
P 2650 5000
F 0 "#FLG01" H 2650 5075 50  0001 C CNN
F 1 "PWR_FLAG" V 2650 5127 50  0000 L CNN
F 2 "" H 2650 5000 50  0001 C CNN
F 3 "~" H 2650 5000 50  0001 C CNN
	1    2650 5000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	800  6650 900  6650
Wire Wire Line
	900  6650 900  6750
$Comp
L power:PWR_FLAG #FLG03
U 1 1 60536227
P 3150 6200
F 0 "#FLG03" H 3150 6275 50  0001 C CNN
F 1 "PWR_FLAG" V 3150 6300 50  0000 L CNN
F 2 "" H 3150 6200 50  0001 C CNN
F 3 "~" H 3150 6200 50  0001 C CNN
	1    3150 6200
	0    -1   -1   0   
$EndComp
$Comp
L power:PWR_FLAG #FLG02
U 1 1 605401EC
P 2650 6200
F 0 "#FLG02" H 2650 6275 50  0001 C CNN
F 1 "PWR_FLAG" V 2650 6300 50  0000 L CNN
F 2 "" H 2650 6200 50  0001 C CNN
F 3 "~" H 2650 6200 50  0001 C CNN
	1    2650 6200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2650 6200 2650 6100
Wire Wire Line
	2650 5000 2650 5200
Connection ~ 2650 5000
Connection ~ 2650 6100
Connection ~ 3150 6100
Wire Wire Line
	3150 6100 3150 6200
Wire Wire Line
	2650 5900 2650 6100
Wire Wire Line
	3150 5900 3150 6100
Wire Wire Line
	2650 4900 2650 5000
Text GLabel 3850 7250 0    50   Input ~ 0
+5V
Wire Wire Line
	900  6650 1500 6650
Connection ~ 900  6650
Wire Wire Line
	800  7750 900  7750
Connection ~ 900  7750
Connection ~ 2450 7450
$Comp
L Device:C C3
U 1 1 606104B9
P 2800 7600
F 0 "C3" H 2915 7646 50  0000 L CNN
F 1 "cap" H 2915 7555 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206" H 2838 7450 50  0001 C CNN
F 3 "~" H 2800 7600 50  0001 C CNN
	1    2800 7600
	1    0    0    -1  
$EndComp
$Comp
L Device:C C4
U 1 1 606104C3
P 3150 7600
F 0 "C4" H 3265 7646 50  0000 L CNN
F 1 "cap" H 3265 7555 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206" H 3188 7450 50  0001 C CNN
F 3 "~" H 3150 7600 50  0001 C CNN
	1    3150 7600
	1    0    0    -1  
$EndComp
Wire Wire Line
	2800 7750 3150 7750
Connection ~ 2800 7750
Wire Wire Line
	2450 7750 2800 7750
Connection ~ 2450 7750
Wire Wire Line
	2450 7450 2800 7450
Connection ~ 2800 7450
Wire Wire Line
	2800 7450 3150 7450
Wire Wire Line
	3150 7750 3500 7750
Connection ~ 3150 7750
$Comp
L Device:C C5
U 1 1 60641602
P 3500 7600
F 0 "C5" H 3615 7646 50  0000 L CNN
F 1 "cap" H 3615 7555 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206" H 3538 7450 50  0001 C CNN
F 3 "~" H 3500 7600 50  0001 C CNN
	1    3500 7600
	1    0    0    -1  
$EndComp
Wire Wire Line
	3500 7450 3150 7450
Connection ~ 3150 7450
Text Notes 950  1300 0    50   ~ 0
CRU Map\n1600 DSR ROM Enable (reserved)\n1602 Card Select, LSBit (0 none)\n1604 Card Select, MSBit (0 none)\n1606 Card Detect (of selected) if 0\n1608 Write Protect (of selected) if 0\n1610 Alias of 1600\n1620-163E  SPI transfer, MSBit first \n            (backwards from 9900!)\n1640-16FE  Ignored\n
Text Notes 9600 6450 0    50   ~ 0
SDC3 microSD PCB
Wire Wire Line
	7150 4550 7150 3950
Wire Wire Line
	6650 4350 8050 4350
Wire Wire Line
	8150 4350 8150 4550
Wire Wire Line
	6850 4150 6850 4550
Wire Wire Line
	6750 4550 6750 4250
Wire Wire Line
	6750 4250 7050 4250
Wire Wire Line
	8250 4250 8250 4550
Wire Wire Line
	6850 4150 8350 4150
Wire Wire Line
	8350 4150 8350 4550
Connection ~ 6850 4150
Wire Wire Line
	6950 4550 6950 4050
Wire Wire Line
	6950 4050 8050 4050
Wire Wire Line
	8450 4050 8450 4550
Wire Wire Line
	7050 4550 7050 4250
Connection ~ 7050 4250
Wire Wire Line
	7050 4250 8250 4250
Wire Wire Line
	7150 3950 8650 3950
Wire Wire Line
	8650 3950 8650 4550
Wire Wire Line
	8550 4550 8550 4250
Wire Wire Line
	8550 4250 8250 4250
Connection ~ 8250 4250
Wire Wire Line
	8850 3850 8850 4550
Wire Wire Line
	7350 3850 7350 4550
Connection ~ 8850 3850
Wire Wire Line
	8550 4250 9750 4250
Wire Wire Line
	9750 4250 9750 4550
Connection ~ 8550 4250
Wire Wire Line
	9750 4250 10050 4250
Wire Wire Line
	10050 4250 10050 4550
Wire Wire Line
	9650 4350 9650 4550
Wire Wire Line
	8150 4350 9550 4350
Connection ~ 8150 4350
Wire Wire Line
	8350 4150 9850 4150
Wire Wire Line
	9850 4150 9850 4550
Connection ~ 8350 4150
Wire Wire Line
	9950 4050 9950 4550
Connection ~ 8450 4050
Wire Wire Line
	8650 3950 10150 3950
Wire Wire Line
	10150 3950 10150 4550
Connection ~ 8650 3950
Wire Wire Line
	10350 3850 10350 4550
Connection ~ 6950 4050
Connection ~ 7150 3950
Text Notes 5500 1500 0    50   ~ 0
Card Select MSBit
Text Label 6450 6550 0    50   ~ 0
CD2
Wire Wire Line
	8650 6350 8650 6550
Text Label 6450 6650 0    50   ~ 0
CD3
Wire Wire Line
	9800 1900 9900 1900
Text Label 6450 6750 0    50   ~ 0
WP1
Text Label 6450 6850 0    50   ~ 0
WP2
Text Label 7350 1500 0    50   ~ 0
CS1
Text Label 7350 1400 0    50   ~ 0
CS0
Text Label 6450 6950 0    50   ~ 0
WP3
$Comp
L Device:R R11
U 1 1 60366AAB
P 6200 6850
F 0 "R11" V 6150 6650 50  0000 L CNN
F 1 "200" V 6200 6800 50  0000 L CNN
F 2 "Resistors_SMD:R_1206" V 6130 6850 50  0001 C CNN
F 3 "~" H 6200 6850 50  0001 C CNN
	1    6200 6850
	0    1    1    0   
$EndComp
$Comp
L Device:R R10
U 1 1 603717D0
P 6200 6750
F 0 "R10" V 6150 6550 50  0000 L CNN
F 1 "200" V 6200 6700 50  0000 L CNN
F 2 "Resistors_SMD:R_1206" V 6130 6750 50  0001 C CNN
F 3 "~" H 6200 6750 50  0001 C CNN
	1    6200 6750
	0    1    1    0   
$EndComp
Connection ~ 2750 2300
Connection ~ 2750 2400
Connection ~ 9750 4250
Text Notes 2750 4950 0    50   ~ 0
Cable to CRU I/O card
Connection ~ 2750 2200
Text Label 10650 3200 0    50   ~ 0
LED3
Wire Wire Line
	10650 3200 10750 3200
$Comp
L Device:LED D4
U 1 1 6055797B
P 10900 3200
F 0 "D4" H 10950 3100 50  0000 R CNN
F 1 "LED" V 10848 3083 50  0001 R CNN
F 2 "LEDs:LED_1206" H 10900 3200 50  0001 C CNN
F 3 "~" H 10900 3200 50  0001 C CNN
	1    10900 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	7150 6350 7150 6450
Wire Wire Line
	10150 6350 10150 6650
Wire Wire Line
	7050 6350 7050 6750
Wire Wire Line
	7050 6750 6350 6750
Wire Wire Line
	8550 6350 8550 6850
$Comp
L Device:R R12
U 1 1 60A8A0D1
P 6200 6950
F 0 "R12" V 6150 6750 50  0000 L CNN
F 1 "200" V 6200 6900 50  0000 L CNN
F 2 "Resistors_SMD:R_1206" V 6130 6950 50  0001 C CNN
F 3 "~" H 6200 6950 50  0001 C CNN
	1    6200 6950
	0    1    1    0   
$EndComp
$Comp
L Device:R R8
U 1 1 60A8A358
P 6200 6550
F 0 "R8" V 6150 6350 50  0000 L CNN
F 1 "200" V 6200 6500 50  0000 L CNN
F 2 "Resistors_SMD:R_1206" V 6130 6550 50  0001 C CNN
F 3 "~" H 6200 6550 50  0001 C CNN
	1    6200 6550
	0    1    1    0   
$EndComp
$Comp
L Device:R R7
U 1 1 60A8A362
P 6200 6450
F 0 "R7" V 6150 6250 50  0000 L CNN
F 1 "200" V 6200 6400 50  0000 L CNN
F 2 "Resistors_SMD:R_1206" V 6130 6450 50  0001 C CNN
F 3 "~" H 6200 6450 50  0001 C CNN
	1    6200 6450
	0    1    1    0   
$EndComp
$Comp
L Device:R R9
U 1 1 60A8A36C
P 6200 6650
F 0 "R9" V 6150 6450 50  0000 L CNN
F 1 "200" V 6200 6600 50  0000 L CNN
F 2 "Resistors_SMD:R_1206" V 6130 6650 50  0001 C CNN
F 3 "~" H 6200 6650 50  0001 C CNN
	1    6200 6650
	0    1    1    0   
$EndComp
Text Label 7350 1700 0    50   ~ 0
WP
Text Label 7350 1600 0    50   ~ 0
CD
Wire Wire Line
	9600 1000 9800 1000
Wire Wire Line
	9800 2000 9800 1900
Connection ~ 9800 1000
Wire Wire Line
	9800 1000 10400 1000
Connection ~ 9800 1800
Wire Wire Line
	9800 1800 9800 1000
Connection ~ 9800 1900
Wire Wire Line
	9800 1900 9800 1800
$Comp
L Device:R R1
U 1 1 60C7CC93
P 3550 6450
F 0 "R1" V 3500 6250 50  0000 L CNN
F 1 "10K" V 3550 6400 50  0000 L CNN
F 2 "Resistors_SMD:R_1206" V 3480 6450 50  0001 C CNN
F 3 "~" H 3550 6450 50  0001 C CNN
	1    3550 6450
	0    1    1    0   
$EndComp
$Comp
L Device:R R2
U 1 1 60CA352D
P 3550 6550
F 0 "R2" V 3500 6350 50  0000 L CNN
F 1 "10K" V 3550 6500 50  0000 L CNN
F 2 "Resistors_SMD:R_1206" V 3480 6550 50  0001 C CNN
F 3 "~" H 3550 6550 50  0001 C CNN
	1    3550 6550
	0    1    1    0   
$EndComp
$Comp
L Device:R R3
U 1 1 60CA3596
P 3550 6650
F 0 "R3" V 3500 6450 50  0000 L CNN
F 1 "10K" V 3550 6600 50  0000 L CNN
F 2 "Resistors_SMD:R_1206" V 3480 6650 50  0001 C CNN
F 3 "~" H 3550 6650 50  0001 C CNN
	1    3550 6650
	0    1    1    0   
$EndComp
$Comp
L Device:R R4
U 1 1 60CA35FF
P 3550 6750
F 0 "R4" V 3500 6550 50  0000 L CNN
F 1 "10K" V 3550 6700 50  0000 L CNN
F 2 "Resistors_SMD:R_1206" V 3480 6750 50  0001 C CNN
F 3 "~" H 3550 6750 50  0001 C CNN
	1    3550 6750
	0    1    1    0   
$EndComp
$Comp
L Device:R R5
U 1 1 60CA3668
P 3550 6850
F 0 "R5" V 3500 6650 50  0000 L CNN
F 1 "10K" V 3550 6800 50  0000 L CNN
F 2 "Resistors_SMD:R_1206" V 3480 6850 50  0001 C CNN
F 3 "~" H 3550 6850 50  0001 C CNN
	1    3550 6850
	0    1    1    0   
$EndComp
$Comp
L Device:R R6
U 1 1 60CA36D1
P 3550 6950
F 0 "R6" V 3500 6750 50  0000 L CNN
F 1 "10K" V 3550 6900 50  0000 L CNN
F 2 "Resistors_SMD:R_1206" V 3480 6950 50  0001 C CNN
F 3 "~" H 3550 6950 50  0001 C CNN
	1    3550 6950
	0    1    1    0   
$EndComp
Wire Wire Line
	3400 6450 3300 6450
Wire Wire Line
	3300 6950 3400 6950
Wire Wire Line
	3400 6850 3300 6850
Connection ~ 3300 6850
Wire Wire Line
	3300 6850 3300 6950
Wire Wire Line
	3400 6750 3300 6750
Wire Wire Line
	3300 6750 3300 6850
Wire Wire Line
	3400 6650 3300 6650
Wire Wire Line
	3300 6450 3300 6550
Wire Wire Line
	3400 6550 3300 6550
Connection ~ 3300 6550
Wire Wire Line
	3300 6550 3300 6650
Connection ~ 3300 6450
Wire Wire Line
	5850 6450 3700 6450
Connection ~ 5850 6450
Wire Wire Line
	3700 6550 5750 6550
Connection ~ 5750 6550
Wire Wire Line
	3700 6750 5150 6750
Connection ~ 5150 6750
Wire Wire Line
	5050 6850 3700 6850
Connection ~ 5050 6850
Text GLabel 2950 10400 0    50   Input ~ 0
GND
Text GLabel 3200 6750 0    50   Input ~ 0
+3.3V
$Comp
L 74xx:74LS125 U2
U 4 1 603842BB
P 3350 4150
F 0 "U2" H 3350 4375 50  0000 C CNN
F 1 "74LVC125" H 3350 4466 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 3350 4150 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS125" H 3350 4150 50  0001 C CNN
	4    3350 4150
	1    0    0    1   
$EndComp
$Comp
L 74xx:74LS125 U2
U 3 1 6031F2B1
P 2750 4350
F 0 "U2" H 2750 4575 50  0000 C CNN
F 1 "74LVC125" H 2750 4666 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 2750 4350 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS125" H 2750 4350 50  0001 C CNN
	3    2750 4350
	1    0    0    1   
$EndComp
Text GLabel 2450 4350 0    50   Input ~ 0
GND
Wire Wire Line
	6050 1700 6050 5050
Wire Wire Line
	750  4100 750  4550
Wire Wire Line
	850  4100 750  4100
Text Label 1900 4000 0    50   ~ 0
SPI_WE*
Text Label 1900 3900 0    50   ~ 0
SPI_RD*
Text Label 1900 3600 0    50   ~ 0
REG_WE*
Text Label 1900 3500 0    50   ~ 0
REG_RD*
Text GLabel 1350 3100 0    50   Input ~ 0
+5V
Text GLabel 1350 4400 0    50   Input ~ 0
GND
$Comp
L 74xx:74LS138 U3
U 1 1 6024F7B2
P 1350 3700
F 0 "U3" H 1350 4481 50  0000 C CNN
F 1 "74LS138" H 1350 4390 50  0000 C CNN
F 2 "Package_SO:SOIC-16_3.9x9.9mm_P1.27mm" H 1350 3700 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS138" H 1350 3700 50  0001 C CNN
	1    1350 3700
	1    0    0    -1  
$EndComp
Text GLabel 850  4000 0    50   Input ~ 0
CRUBAS*
Text GLabel 850  3600 0    50   Input ~ 0
A4
Text GLabel 850  3500 0    50   Input ~ 0
RD*
Text GLabel 850  3400 0    50   Input ~ 0
WE*
Text GLabel 850  3900 0    50   Input ~ 0
+5V
NoConn ~ 1850 3700
NoConn ~ 1850 4100
NoConn ~ 1850 3400
NoConn ~ 1850 3800
Wire Wire Line
	1850 3600 2250 3600
Wire Wire Line
	1850 3500 3650 3500
Wire Wire Line
	850  1600 2350 1600
Connection ~ 2350 1600
Wire Wire Line
	2350 1600 2750 1600
Wire Wire Line
	2250 1500 2250 3600
Text GLabel 3050 4150 0    50   Input ~ 0
GND
Wire Wire Line
	4950 3950 7150 3950
Connection ~ 3650 4150
$Comp
L Device:R R15
U 1 1 60421161
P 6850 3600
F 0 "R15" V 6850 3350 50  0000 L CNN
F 1 "10K" V 6850 3550 50  0000 L CNN
F 2 "Resistors_SMD:R_1206" V 6780 3600 50  0001 C CNN
F 3 "~" H 6850 3600 50  0001 C CNN
	1    6850 3600
	-1   0    0    1   
$EndComp
Wire Wire Line
	6850 3750 6850 4150
$Comp
L Device:R R16
U 1 1 6126353D
P 7150 3600
F 0 "R16" V 7150 3350 50  0000 L CNN
F 1 "10K" V 7150 3550 50  0000 L CNN
F 2 "Resistors_SMD:R_1206" V 7080 3600 50  0001 C CNN
F 3 "~" H 7150 3600 50  0001 C CNN
	1    7150 3600
	-1   0    0    1   
$EndComp
Wire Wire Line
	6750 4250 6450 4250
Connection ~ 6750 4250
Wire Wire Line
	7150 3950 7150 3750
Wire Wire Line
	7350 3750 7350 3850
Connection ~ 7350 3850
Wire Wire Line
	6650 3350 6650 3450
Wire Wire Line
	7350 3350 7350 3450
Connection ~ 6650 3350
Wire Wire Line
	7150 3450 7150 3350
Connection ~ 7150 3350
Wire Wire Line
	6650 3350 6850 3350
Wire Wire Line
	6850 3450 6850 3350
Connection ~ 6850 3350
Wire Wire Line
	6850 3350 6950 3350
Connection ~ 6950 3350
Wire Wire Line
	6950 3350 6950 4050
Wire Wire Line
	6950 3350 7150 3350
Wire Wire Line
	10450 3850 10350 3850
Connection ~ 10350 3850
Wire Wire Line
	8850 3850 10350 3850
Wire Wire Line
	2750 2300 9900 2300
Wire Wire Line
	2750 2400 9900 2400
Wire Wire Line
	3750 1400 3850 1400
Wire Wire Line
	3750 1500 3950 1500
Wire Wire Line
	850  2400 2750 2400
Wire Wire Line
	850  2300 2750 2300
Wire Wire Line
	850  2200 2750 2200
Wire Wire Line
	3650 4150 6850 4150
Wire Wire Line
	2750 4000 1850 4000
Wire Wire Line
	1850 3900 3350 3900
Wire Wire Line
	3650 4350 3050 4350
Wire Wire Line
	3650 4150 3650 4350
Wire Wire Line
	2750 4100 2750 4000
Connection ~ 4950 6950
Connection ~ 5650 6650
Wire Wire Line
	3700 6950 4950 6950
Wire Wire Line
	5650 6650 3700 6650
Wire Wire Line
	5850 6450 6050 6450
Wire Wire Line
	5750 6550 6050 6550
Wire Wire Line
	5650 6650 6050 6650
Wire Wire Line
	5150 6750 6050 6750
Wire Wire Line
	5050 6850 6050 6850
Wire Wire Line
	4950 6950 6050 6950
Wire Wire Line
	4550 6250 4550 6150
Connection ~ 6250 5650
Wire Wire Line
	6250 5650 6250 5500
Text GLabel 6250 5500 0    50   Input ~ 0
+5V
Wire Wire Line
	4150 5650 4150 6350
Wire Wire Line
	5450 6150 5450 6350
Wire Wire Line
	4750 6150 4750 6350
Wire Wire Line
	4950 6150 4950 6950
Wire Wire Line
	5050 6150 5050 6850
Wire Wire Line
	5150 6150 5150 6750
Wire Wire Line
	5650 6150 5650 6650
Wire Wire Line
	5750 6150 5750 6550
Wire Wire Line
	5850 6150 5850 6450
Wire Wire Line
	5950 6250 6250 6250
Wire Wire Line
	5950 6150 5950 6250
Wire Wire Line
	6250 5650 6250 6250
$Comp
L 74xx:74LS253 U5
U 1 1 602FBF31
P 5250 5650
F 0 "U5" V 5700 4750 50  0000 L CNN
F 1 "74LS253" V 5600 4750 50  0000 L CNN
F 2 "Package_SO:SOIC-16_3.9x9.9mm_P1.27mm" H 5250 5650 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS253" H 5250 5650 50  0001 C CNN
	1    5250 5650
	0    1    -1   0   
$EndComp
Wire Wire Line
	6350 6850 8550 6850
Wire Wire Line
	6350 6650 10150 6650
Wire Wire Line
	7150 6450 6350 6450
Wire Wire Line
	6350 6550 8650 6550
Wire Wire Line
	5950 1600 9900 1600
Wire Wire Line
	6050 1700 9900 1700
Wire Wire Line
	6050 5050 5250 5050
Wire Wire Line
	5250 5050 5250 5150
Wire Wire Line
	5950 1600 5950 5150
Wire Wire Line
	3650 2600 3650 3500
Connection ~ 3850 1400
Connection ~ 3950 1500
Wire Wire Line
	3850 1400 9900 1400
Wire Wire Line
	3950 1500 9900 1500
Wire Wire Line
	3850 3000 4250 3000
Wire Wire Line
	3850 6250 4550 6250
Wire Wire Line
	3950 2900 4250 2900
Wire Wire Line
	3950 6150 4450 6150
Wire Wire Line
	1950 4550 1950 4900
Wire Wire Line
	2450 10400 5600 10400
Wire Wire Line
	1500 6750 1500 6650
Connection ~ 1500 6650
Wire Wire Line
	1500 6650 2100 6650
Wire Wire Line
	5450 6350 5250 6350
Connection ~ 4750 6350
Wire Wire Line
	4750 6350 4150 6350
Text GLabel 4150 6350 0    50   Input ~ 0
GND
Wire Wire Line
	3500 7750 3850 7750
Connection ~ 3500 7750
Connection ~ 3850 7750
Wire Wire Line
	3850 7750 4200 7750
Connection ~ 4200 7750
Wire Wire Line
	4200 7750 4550 7750
Connection ~ 4550 7750
Wire Wire Line
	4550 7750 4900 7750
Connection ~ 4900 7750
Wire Wire Line
	4900 7750 5250 7750
Connection ~ 5250 7750
Wire Wire Line
	5250 7750 5600 7750
Connection ~ 5600 7750
Wire Wire Line
	5600 7750 5950 7750
Connection ~ 5950 7750
Wire Wire Line
	5950 7750 6300 7750
Wire Wire Line
	3850 7250 3850 7450
Connection ~ 3850 7450
Wire Wire Line
	3650 2600 9900 2600
Connection ~ 3350 3900
Wire Wire Line
	2350 3700 4350 3700
Wire Wire Line
	4350 3700 4350 3950
Wire Wire Line
	2350 1600 2350 3700
Wire Wire Line
	3350 3600 3350 3900
Text Label 9650 3600 0    50   ~ 0
SPI_RD*
Wire Wire Line
	7350 3850 8550 3850
$Comp
L Memory_EEPROM:M95256-WMN6P U7
U 1 1 601C4949
P 7850 900
F 0 "U7" H 8100 1250 50  0000 C CNN
F 1 "M95256-WMN6P" H 8200 1150 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 7850 900 50  0001 C CNN
F 3 "http://www.st.com/content/ccc/resource/technical/document/datasheet/9d/75/f0/3e/76/00/4c/0b/CD00103810.pdf/files/CD00103810.pdf/jcr:content/translations/en.CD00103810.pdf" H 7850 900 50  0001 C CNN
	1    7850 900 
	1    0    0    -1  
$EndComp
Wire Wire Line
	8650 3950 8650 900 
Wire Wire Line
	8350 800  8350 4150
Wire Wire Line
	8250 800  8350 800 
Wire Wire Line
	8250 900  8650 900 
Wire Wire Line
	8250 1000 8550 1000
Text GLabel 7350 600  0    50   Input ~ 0
+3.3V
Wire Wire Line
	7850 600  7350 600 
Wire Wire Line
	7450 1000 6850 1000
Wire Wire Line
	6850 1000 6850 2900
Wire Wire Line
	6850 2900 5250 2900
Text GLabel 7850 1200 0    50   Input ~ 0
GND
Wire Wire Line
	5250 3000 7250 3000
Text GLabel 11050 1300 2    50   Output ~ 0
CRUIN
Text Notes 7900 1300 0    50   ~ 0
DSR ROM\n32Kbyte or more
Connection ~ 3850 3000
Connection ~ 3950 2900
Wire Wire Line
	3850 3000 3850 6250
Wire Wire Line
	3950 2900 3950 6150
Wire Wire Line
	5250 3100 8750 3100
Wire Wire Line
	8750 3100 8750 4550
Connection ~ 8750 3100
Wire Wire Line
	5250 3200 10250 3200
Connection ~ 7250 3000
Wire Wire Line
	7150 3350 7350 3350
Wire Wire Line
	7250 3350 7250 4550
Wire Wire Line
	7250 3000 7250 3350
Wire Wire Line
	7350 2800 7250 2800
Wire Wire Line
	7250 2800 7250 3000
Wire Wire Line
	2750 2200 9900 2200
Wire Wire Line
	8050 2100 8050 2800
Connection ~ 8050 2800
Wire Wire Line
	7350 2100 7250 2100
Wire Wire Line
	7250 2100 7250 1300
Connection ~ 7250 1300
Wire Wire Line
	7250 1300 9900 1300
Wire Wire Line
	8550 3850 8550 1000
Connection ~ 8550 3850
Wire Wire Line
	8550 3850 8850 3850
$Comp
L Device:LED D3
U 1 1 606996EE
P 9300 3100
F 0 "D3" H 9250 3150 50  0000 R CNN
F 1 "LED" V 9248 2983 50  0001 R CNN
F 2 "LEDs:LED_1206" H 9300 3100 50  0001 C CNN
F 3 "~" H 9300 3100 50  0001 C CNN
	1    9300 3100
	1    0    0    -1  
$EndComp
$Comp
L Device:R R22
U 1 1 606996F8
P 8900 3100
F 0 "R22" V 8850 2900 50  0000 L CNN
F 1 "100" V 8900 3050 50  0000 L CNN
F 2 "Resistors_SMD:R_1206" V 8830 3100 50  0001 C CNN
F 3 "~" H 8900 3100 50  0001 C CNN
	1    8900 3100
	0    1    1    0   
$EndComp
Connection ~ 8050 4050
Wire Wire Line
	8050 4050 8450 4050
Wire Wire Line
	3350 3600 10750 3600
Wire Wire Line
	7350 3000 7250 3000
$Comp
L Device:R R19
U 1 1 60739206
P 8900 2900
F 0 "R19" V 8900 2650 50  0000 L CNN
F 1 "10K" V 8900 2850 50  0000 L CNN
F 2 "Resistors_SMD:R_1206" V 8830 2900 50  0001 C CNN
F 3 "~" H 8900 2900 50  0001 C CNN
	1    8900 2900
	0    1    1    0   
$EndComp
Wire Wire Line
	11050 2950 11050 3050
$Comp
L Device:R R23
U 1 1 60562DA0
P 10500 3200
F 0 "R23" V 10550 3250 50  0000 L CNN
F 1 "100" V 10500 3150 50  0000 L CNN
F 2 "Resistors_SMD:R_1206" V 10430 3200 50  0001 C CNN
F 3 "~" H 10500 3200 50  0001 C CNN
	1    10500 3200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	10250 3200 10250 4550
Wire Wire Line
	10250 3200 10350 3200
Connection ~ 10250 3200
$Comp
L Device:R R24
U 1 1 607B9FF4
P 10500 3050
F 0 "R24" V 10500 2800 50  0000 L CNN
F 1 "10K" V 10500 3000 50  0000 L CNN
F 2 "Resistors_SMD:R_1206" V 10430 3050 50  0001 C CNN
F 3 "~" H 10500 3050 50  0001 C CNN
	1    10500 3050
	0    1    1    0   
$EndComp
Wire Wire Line
	10350 3050 10250 3050
Wire Wire Line
	10250 3050 10250 3200
Wire Wire Line
	10650 3050 11050 3050
Connection ~ 11050 3050
Wire Wire Line
	11050 3050 11050 3200
Wire Wire Line
	8050 2800 8050 3000
Text Label 7650 3000 0    50   ~ 0
LED1
$Comp
L Device:LED D2
U 1 1 6084869E
P 7900 3000
F 0 "D2" H 7850 3050 50  0000 R CNN
F 1 "LED" V 7848 2883 50  0001 R CNN
F 2 "LEDs:LED_1206" H 7900 3000 50  0001 C CNN
F 3 "~" H 7900 3000 50  0001 C CNN
	1    7900 3000
	1    0    0    -1  
$EndComp
$Comp
L Device:R R21
U 1 1 608486A8
P 7500 3000
F 0 "R21" V 7450 2800 50  0000 L CNN
F 1 "100" V 7500 2950 50  0000 L CNN
F 2 "Resistors_SMD:R_1206" V 7430 3000 50  0001 C CNN
F 3 "~" H 7500 3000 50  0001 C CNN
	1    7500 3000
	0    1    1    0   
$EndComp
$Comp
L Device:R R17
U 1 1 608486B2
P 7500 2800
F 0 "R17" V 7500 2550 50  0000 L CNN
F 1 "10K" V 7500 2750 50  0000 L CNN
F 2 "Resistors_SMD:R_1206" V 7430 2800 50  0001 C CNN
F 3 "~" H 7500 2800 50  0001 C CNN
	1    7500 2800
	0    1    1    0   
$EndComp
Wire Wire Line
	7650 2800 8050 2800
Wire Wire Line
	8750 2900 8750 3100
Connection ~ 8050 3000
Wire Wire Line
	8050 3000 8050 4050
Wire Wire Line
	3750 1300 7250 1300
Wire Wire Line
	7250 1300 7250 800 
Wire Wire Line
	7250 800  7450 800 
Wire Wire Line
	7450 900  7350 900 
Wire Wire Line
	7350 900  7350 600 
Text GLabel 3200 6450 0    50   Input ~ 0
GND
Text Notes 3700 6950 0    50   ~ 0
1=No Card or Protected Lock
Text Notes 3700 6650 0    50   ~ 0
1=Card Inserted
Wire Wire Line
	3200 6750 3300 6750
Connection ~ 3300 6750
$Comp
L Gemini:Keystone_7790_RA_Screw J6
U 1 1 6092AB0E
P 750 6250
F 0 "J6" H 1150 6515 50  0000 C CNN
F 1 "Keystone_7790_RA_Screw" H 1150 6424 50  0000 C CNN
F 2 "Gemini:Keystone_7790_and_9203" H 1400 6350 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/1/7790.pdf" H 1400 6250 50  0001 L CNN
F 4 "Terminals KEYSTONE PCVB METRIC SCREW TRM HORIZ" H 1400 6150 50  0001 L CNN "Description"
F 5 "7.9" H 1400 6050 50  0001 L CNN "Height"
F 6 "534-7790" H 1400 5950 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.com/Search/Refine.aspx?Keyword=534-7790" H 1400 5850 50  0001 L CNN "Mouser Price/Stock"
F 8 "Keystone Electronics" H 1400 5750 50  0001 L CNN "Manufacturer_Name"
F 9 "7790" H 1400 5650 50  0001 L CNN "Manufacturer_Part_Number"
	1    750  6250
	1    0    0    -1  
$EndComp
NoConn ~ 1550 6250
NoConn ~ 1550 6350
NoConn ~ 750  6350
NoConn ~ 750  6250
$Comp
L Gemini:Keystone_7790_RA_Screw J5
U 1 1 60976394
P 750 5800
F 0 "J5" H 1150 6065 50  0000 C CNN
F 1 "Keystone_7790_RA_Screw" H 1150 5974 50  0000 C CNN
F 2 "Gemini:Keystone_7790_and_9203" H 1400 5900 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/1/7790.pdf" H 1400 5800 50  0001 L CNN
F 4 "Terminals KEYSTONE PCVB METRIC SCREW TRM HORIZ" H 1400 5700 50  0001 L CNN "Description"
F 5 "7.9" H 1400 5600 50  0001 L CNN "Height"
F 6 "534-7790" H 1400 5500 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.com/Search/Refine.aspx?Keyword=534-7790" H 1400 5400 50  0001 L CNN "Mouser Price/Stock"
F 8 "Keystone Electronics" H 1400 5300 50  0001 L CNN "Manufacturer_Name"
F 9 "7790" H 1400 5200 50  0001 L CNN "Manufacturer_Part_Number"
	1    750  5800
	1    0    0    -1  
$EndComp
NoConn ~ 1550 5800
NoConn ~ 1550 5900
NoConn ~ 750  5900
NoConn ~ 750  5800
Text Notes 4350 5150 0    50   ~ 0
CD and WP multiplexer
Wire Wire Line
	3200 6450 3300 6450
Text GLabel 850  5200 0    50   Input ~ 0
CRUBAS*
Text GLabel 3150 5200 2    50   Input ~ 0
CRUBAS*
Wire Notes Line
	2100 4750 2100 6250
Wire Notes Line
	2100 6250 3650 6250
Wire Notes Line
	3650 6250 3650 4750
Wire Notes Line
	3650 4750 2100 4750
Wire Wire Line
	900  7750 1500 7750
Connection ~ 1500 7750
Wire Wire Line
	1500 7750 2100 7750
Wire Wire Line
	2100 7450 2100 6650
Connection ~ 2100 7450
Text Notes 1400 3250 0    50   ~ 0
RD/WR Decoder
Wire Wire Line
	7650 3000 7750 3000
Wire Wire Line
	9450 2900 9450 3100
Connection ~ 9450 4050
Wire Wire Line
	9450 4050 9950 4050
Wire Wire Line
	8450 4050 9450 4050
Wire Wire Line
	9050 2900 9450 2900
Connection ~ 9450 3100
Wire Wire Line
	9450 3100 9450 4050
Wire Wire Line
	9050 3100 9150 3100
Wire Wire Line
	5250 6150 5250 6350
Connection ~ 5250 6350
Wire Wire Line
	5250 6350 4750 6350
Text Notes 7250 6400 0    50   ~ 0
SDC1 on PCB
Text Notes 8700 6400 0    50   ~ 0
SDC2 2nd PCB
Text Notes 7350 1950 0    50   ~ 0
Back voltage 1.7V ok
Text Notes 9600 3100 0    50   ~ 0
70% luminosity\nat 14 mA 1.9 V
Text GLabel 8050 2100 2    50   Input ~ 0
+3.3V
Text GLabel 9450 2900 2    50   Input ~ 0
+3.3V
Connection ~ 6650 4350
Wire Wire Line
	6650 3750 6650 4350
$Comp
L Device:R R14
U 1 1 6126346B
P 6650 3600
F 0 "R14" V 6650 3350 50  0000 L CNN
F 1 "10K" V 6650 3550 50  0000 L CNN
F 2 "Resistors_SMD:R_1206" V 6580 3600 50  0001 C CNN
F 3 "~" H 6650 3600 50  0001 C CNN
	1    6650 3600
	-1   0    0    1   
$EndComp
$Comp
L Device:R R18
U 1 1 6126360E
P 7350 3600
F 0 "R18" V 7350 3350 50  0000 L CNN
F 1 "10K" V 7350 3550 50  0000 L CNN
F 2 "Resistors_SMD:R_1206" V 7280 3600 50  0001 C CNN
F 3 "~" H 7350 3600 50  0001 C CNN
	1    7350 3600
	-1   0    0    1   
$EndComp
Wire Wire Line
	6450 3350 6650 3350
Wire Wire Line
	6650 4350 6550 4350
Wire Wire Line
	6650 4350 6650 4550
Wire Wire Line
	6550 4350 6550 4550
Connection ~ 8050 4350
Wire Wire Line
	8050 4350 8150 4350
Connection ~ 9550 4350
Wire Wire Line
	9550 4350 9650 4350
Wire Wire Line
	9550 4350 9550 4550
Wire Wire Line
	8050 4350 8050 4550
Text Label 8200 4350 0    50   ~ 0
DAT01
$Comp
L Connector_Generic:Conn_02x06_Odd_Even J8
U 1 1 60284CC9
P 12950 7050
F 0 "J8" H 13000 7467 50  0000 C CNN
F 1 "Conn_02x06_Top_Bottom" H 13000 7376 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x06_Pitch2.54mm" H 12950 7050 50  0001 C CNN
F 3 "~" H 12950 7050 50  0001 C CNN
	1    12950 7050
	1    0    0    -1  
$EndComp
Text GLabel 13250 6850 2    50   Input ~ 0
DO
Text GLabel 13250 6950 2    50   Input ~ 0
DI
Text GLabel 12750 7250 0    50   Input ~ 0
SCLK
Text GLabel 12750 6850 0    50   Input ~ 0
GND
Text GLabel 13250 7150 2    50   Input ~ 0
+3.3V
Text GLabel 8250 3850 0    50   Input ~ 0
DO
Text GLabel 8250 3950 0    50   Input ~ 0
DI
Text GLabel 5250 4150 0    50   Input ~ 0
SCLK
Text GLabel 5400 3000 2    50   Input ~ 0
SS1
Text GLabel 5400 3100 2    50   Input ~ 0
SS2
Text GLabel 5400 3200 2    50   Input ~ 0
SS3
Text GLabel 5400 2900 2    50   Input ~ 0
EEPROM*
Text GLabel 12750 6950 0    50   Input ~ 0
SS2
Text GLabel 13250 7050 2    50   Input ~ 0
CD2
Text GLabel 13250 7350 2    50   Input ~ 0
WP2
$Comp
L Connector_Generic:Conn_02x06_Odd_Even J7
U 1 1 602AC909
P 12950 5950
F 0 "J7" H 13000 6367 50  0000 C CNN
F 1 "Conn_02x06_Top_Bottom" H 13000 6276 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x06_Pitch2.54mm" H 12950 5950 50  0001 C CNN
F 3 "~" H 12950 5950 50  0001 C CNN
	1    12950 5950
	1    0    0    -1  
$EndComp
Text GLabel 12750 7350 0    50   Input ~ 0
WP3
Text GLabel 13250 7250 2    50   Input ~ 0
DAT01
Text GLabel 12750 7050 0    50   Input ~ 0
CD3
Text Notes 12900 7500 0    50   ~ 0
Board Stacker
Text GLabel 12750 7150 0    50   Input ~ 0
GND
Text GLabel 12750 6150 0    50   Input ~ 0
SCLK
Text GLabel 12750 5750 0    50   Input ~ 0
GND
Text GLabel 12750 5850 0    50   Input ~ 0
SS2
Text GLabel 12750 6250 0    50   Input ~ 0
WP3
Text GLabel 12750 5950 0    50   Input ~ 0
CD3
Text GLabel 12750 6050 0    50   Input ~ 0
GND
Text GLabel 13250 5750 2    50   Input ~ 0
DO
Text GLabel 13250 5850 2    50   Input ~ 0
DI
Text GLabel 13250 6050 2    50   Input ~ 0
+3.3V
Text GLabel 13250 5950 2    50   Input ~ 0
CD2
Text GLabel 13250 6250 2    50   Input ~ 0
WP2
Text GLabel 13250 6150 2    50   Input ~ 0
DAT01
$EndSCHEMATC
