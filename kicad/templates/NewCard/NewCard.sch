EESchema Schematic File Version 4
LIBS:NewCard-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 700  3750 0    100  ~ 0
Data Bus Transceiver\n(Required)
Text Notes 9650 1150 0    50   ~ 0
On BMO high, card must yield\nthe address/data/cru bus. Connect it to \na low-enable of the address encoder.
$Comp
L 74xx:74LS138 U5
U 1 1 5E6D064F
P 8400 1900
F 0 "U5" H 8400 2681 50  0000 C CNN
F 1 "74LS138" H 8400 2590 50  0000 C CNN
F 2 "Package_SO:SOIC-16_3.9x9.9mm_P1.27mm" H 8400 1900 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS138" H 8400 1900 50  0001 C CNN
	1    8400 1900
	1    0    0    -1  
$EndComp
Text Notes 8000 1050 0    100  ~ 0
Address Decoder\n(CPU Memory)
Text GLabel 1150 7150 0    50   Input ~ 0
B_RDBENA*
Text Notes 7800 2950 0    50   ~ 0
For CRU decoding, set E1=SERENA* or PARENA*\nFor CPU RAM decoding, set E1*=MEM*
Text GLabel 7900 2300 0    50   Input ~ 0
MEM*
Text GLabel 7900 1800 0    50   Input ~ 0
LA0
Text GLabel 1600 5700 2    50   Input ~ 0
GND
Text GLabel 8400 2600 2    50   Input ~ 0
GND
Text GLabel 1650 7550 2    50   Input ~ 0
GND
Text GLabel 1650 5950 0    50   Input ~ 0
VCC
Text GLabel 1600 4100 0    50   Input ~ 0
VCC
Text GLabel 8400 1300 0    50   Input ~ 0
VCC
Text GLabel 7350 6700 0    50   Input ~ 0
VCC
Text GLabel 7200 6900 2    50   Input ~ 0
GND
Text Notes 950  7100 0    50   ~ 0
LSB
Text GLabel 2150 6950 2    50   Input ~ 0
D0
Text GLabel 2150 6850 2    50   Input ~ 0
D1
Text GLabel 2150 6750 2    50   Input ~ 0
D2
Text GLabel 2150 6650 2    50   Input ~ 0
D3
Text GLabel 2150 6550 2    50   Input ~ 0
D4
Text GLabel 2150 6450 2    50   Input ~ 0
D5
Text GLabel 2150 6350 2    50   Input ~ 0
D6
Text GLabel 2150 6250 2    50   Input ~ 0
D7
Text GLabel 1150 6250 0    50   Input ~ 0
B_D7
Text Notes 850  4350 0    50   ~ 0
MSB
Text GLabel 1150 6950 0    50   Input ~ 0
B_D0_OUT
Text GLabel 1150 6350 0    50   Input ~ 0
B_D6
Text GLabel 1150 6450 0    50   Input ~ 0
B_D5
Text GLabel 1150 6550 0    50   Input ~ 0
B_D4
Text GLabel 1150 6650 0    50   Input ~ 0
B_D3
Text GLabel 1150 6750 0    50   Input ~ 0
B_D2
Text GLabel 1150 6850 0    50   Input ~ 0
B_D1
Text GLabel 2100 4400 2    50   Input ~ 0
D15
Text GLabel 2100 4500 2    50   Input ~ 0
D14
Text GLabel 2100 4600 2    50   Input ~ 0
D13
Text GLabel 2100 4700 2    50   Input ~ 0
D12
Text GLabel 2100 4800 2    50   Input ~ 0
D11
Text GLabel 2100 4900 2    50   Input ~ 0
D10
Text GLabel 2100 5000 2    50   Input ~ 0
D9
Text GLabel 2100 5100 2    50   Input ~ 0
D8
NoConn ~ 8900 1800
NoConn ~ 8900 1900
NoConn ~ 8900 2000
NoConn ~ 8900 2100
NoConn ~ 8900 2200
NoConn ~ 8900 2300
Text GLabel 7900 2100 0    50   Input ~ 0
VCC
Text GLabel 7900 1700 0    50   Input ~ 0
GND
Text GLabel 7900 1600 0    50   Input ~ 0
GND
Text Notes 9650 1450 0    50   ~ 0
Remember to include\nLPSEL* as an address bit!
Text Notes 9650 2050 0    50   ~ 0
Typically memory chips have\nOE* = RD*\nCS* = EN_0000*\nWE* = WE_IOCLK*\n\n
Text GLabel 8900 1600 2    50   Input ~ 0
ROMCS*
Text GLabel 8900 1700 2    50   Input ~ 0
RAMCS*
Text GLabel 1100 5300 0    50   Input ~ 0
B_RDBENA*
$Sheet
S 1000 1550 1450 600 
U 5ED05929
F0 "Kontron" 50
F1 "Kontron.sch" 50
$EndSheet
Text Notes 4100 1200 0    100  ~ 0
Address Buffer\n(Recommended)
Text GLabel 4700 3150 2    50   Input ~ 0
GND
Text GLabel 4750 5000 2    50   Input ~ 0
GND
Text GLabel 4750 3400 0    50   Input ~ 0
VCC
Text GLabel 4700 1550 0    50   Input ~ 0
VCC
Text Notes 4050 4550 0    50   ~ 0
LSB
Text GLabel 5250 4400 2    50   Input ~ 0
A0
Text GLabel 5250 4300 2    50   Input ~ 0
A1
Text GLabel 5250 4200 2    50   Input ~ 0
A2
Text GLabel 5250 4100 2    50   Input ~ 0
A3
Text GLabel 5250 4000 2    50   Input ~ 0
A4
Text GLabel 5250 3900 2    50   Input ~ 0
A5
Text GLabel 5250 3800 2    50   Input ~ 0
A6
Text GLabel 5250 3700 2    50   Input ~ 0
A7
Text GLabel 5200 1850 2    50   Input ~ 0
CRUIN
Text GLabel 5200 1950 2    50   Input ~ 0
A14
Text GLabel 5200 2050 2    50   Input ~ 0
A13
Text GLabel 5200 2150 2    50   Input ~ 0
A12
Text GLabel 5200 2250 2    50   Input ~ 0
A11
Text GLabel 5200 2350 2    50   Input ~ 0
A10
Text GLabel 5200 2450 2    50   Input ~ 0
A9
Text GLabel 5200 2550 2    50   Input ~ 0
A8
Text Notes 2350 4450 0    50   ~ 0
MSB
Text Notes 2350 7000 0    50   ~ 0
LSB
Text GLabel 1100 5100 0    50   Input ~ 0
B_D8
Text GLabel 1100 5000 0    50   Input ~ 0
B_D9
Text GLabel 1100 4900 0    50   Input ~ 0
B_D10
Text GLabel 1100 4800 0    50   Input ~ 0
B_D11
Text GLabel 1100 4700 0    50   Input ~ 0
B_D12
Text GLabel 1100 4600 0    50   Input ~ 0
B_D13
Text GLabel 1100 4500 0    50   Input ~ 0
B_D14
Text GLabel 1100 4400 0    50   Input ~ 0
B_D15_IN
Text GLabel 4250 3800 0    50   Input ~ 0
B_A6
Text GLabel 4250 4000 0    50   Input ~ 0
B_A4
Text GLabel 4250 4200 0    50   Input ~ 0
B_A2
Text GLabel 4250 3900 0    50   Input ~ 0
B_A5
Text Notes 800  1000 0    50   ~ 0
All bit numbering is 0 LSBit.\nData bus lines are normalized for 16-bit word D0-D15.\n8-bit operations are in the MSB D8-D15, which is the even address.
Text Notes 5450 2000 0    50   ~ 0
MSBit
Text GLabel 4250 4300 0    50   Input ~ 0
B_A1
Text GLabel 4250 4400 0    50   Input ~ 0
B_A0
Text GLabel 4250 4100 0    50   Input ~ 0
B_A3
Text GLabel 4250 3700 0    50   Input ~ 0
B_A7
Text GLabel 4200 2550 0    50   Input ~ 0
B_A8
Text GLabel 4200 2450 0    50   Input ~ 0
B_A9
Text GLabel 4200 2350 0    50   Input ~ 0
B_A10
Text GLabel 4200 2250 0    50   Input ~ 0
B_A11
Text GLabel 4200 2150 0    50   Input ~ 0
B_A12
Text GLabel 4200 2050 0    50   Input ~ 0
B_A13
Text GLabel 4200 1950 0    50   Input ~ 0
B_A14
Text GLabel 4200 1850 0    50   Input ~ 0
B_PSEL*
Text GLabel 1100 5400 0    50   Input ~ 0
CARDSEL*
Text GLabel 1150 7250 0    50   Input ~ 0
CARDSEL*
Text GLabel 6150 4000 0    50   Input ~ 0
BMO
$Comp
L Gemini:74ALS645A U6
U 1 1 5EE4BE9F
P 6250 6650
F 0 "U6" H 6250 7631 50  0000 C CNN
F 1 "74ALS645A" H 6250 7540 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-20W_7.5x12.8mm_Pitch1.27mm" H 6250 6650 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS245" H 6250 6650 50  0001 C CNN
	1    6250 6650
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS245 U1
U 1 1 5EE90910
P 4700 2350
F 0 "U1" H 4700 3331 50  0000 C CNN
F 1 "74ALS645" H 4700 3240 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-20W_7.5x12.8mm_Pitch1.27mm" H 4700 2350 50  0001 C CNN
F 3 "https://www.ti.com/lit/ds/symlink/sn74als645a.pdf" H 4700 2350 50  0001 C CNN
	1    4700 2350
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS245 U2
U 1 1 5EE91582
P 4750 4200
F 0 "U2" H 4750 5181 50  0000 C CNN
F 1 "74ALS645" H 4750 5090 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-20W_7.5x12.8mm_Pitch1.27mm" H 4750 4200 50  0001 C CNN
F 3 "https://www.ti.com/lit/ds/symlink/sn74als645a.pdf" H 4750 4200 50  0001 C CNN
	1    4750 4200
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS688 U?
U 1 1 5EE94DE5
P 6650 3100
F 0 "U?" H 7194 3146 50  0000 L CNN
F 1 "74LS688" H 7194 3055 50  0000 L CNN
F 2 "Package_SO:SOIC-20W_7.5x12.8mm_P1.27mm" H 6650 3100 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS688" H 6650 3100 50  0001 C CNN
	1    6650 3100
	1    0    0    -1  
$EndComp
Text Notes 6350 1700 0    50   ~ 0
Base Address
Text GLabel 6650 1900 0    50   Input ~ 0
VCC
Text GLabel 6650 4300 2    50   Input ~ 0
GND
Text GLabel 6150 2200 0    50   Input ~ 0
A14
Text GLabel 6150 2300 0    50   Input ~ 0
A13
Text GLabel 6150 2400 0    50   Input ~ 0
A12
Text GLabel 6150 2500 0    50   Input ~ 0
A11
Text GLabel 6150 2600 0    50   Input ~ 0
A10
Text GLabel 6150 2700 0    50   Input ~ 0
A9
Text GLabel 6150 2800 0    50   Input ~ 0
A8
Text GLabel 4250 4700 0    50   Input ~ 0
CARDSEL*
Text GLabel 4200 2850 0    50   Input ~ 0
CARDSEL*
Text Notes 3700 1350 0    50   ~ 0
An alternative is two '573 latches on B_D0 to B_D15,\nwhich follow AD0-AD15 during ALATCH high.
$Comp
L 74xx:74LS245 U3
U 1 1 5E6CF7CF
P 1600 4900
F 0 "U3" H 1600 5881 50  0000 C CNN
F 1 "74ALS645" H 1600 5790 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-20W_7.5x12.8mm_Pitch1.27mm" H 1600 4900 50  0001 C CNN
F 3 "https://www.ti.com/lit/ds/symlink/sn74als645a.pdf" H 1600 4900 50  0001 C CNN
	1    1600 4900
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS245 U?
U 1 1 5EE94D1B
P 1650 6750
F 0 "U?" H 1650 7731 50  0000 C CNN
F 1 "74ALS645" H 1650 7640 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-20W_7.5x12.8mm_Pitch1.27mm" H 1650 6750 50  0001 C CNN
F 3 "https://www.ti.com/lit/ds/symlink/sn74als645a.pdf" H 1650 6750 50  0001 C CNN
	1    1650 6750
	1    0    0    -1  
$EndComp
Text Notes 600  7700 0    50   ~ 0
How many loads on B_RDBENA\nare allowed? One or two per card?
Text GLabel 4200 2750 0    50   Input ~ 0
VCC
Text GLabel 4250 4600 0    50   Input ~ 0
VCC
Text GLabel 7150 2300 2    50   Input ~ 0
CARDSEL*
Wire Wire Line
	7900 2200 7150 2200
Wire Wire Line
	7150 2300 7150 2200
Connection ~ 7150 2200
Text Notes 7850 3250 0    50   ~ 0
Given CARDSEL*, a mapped peripheral card \nwould respond to PARENA* , or MEM* and SPAMM.
Text Notes 7650 3550 0    50   ~ 0
Speaking of SPAMM, the CPU card should pull-up\nthese bus signals in case Memory card is not present.
$EndSCHEMATC
