EESchema Schematic File Version 4
LIBS:NewCard-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 4850 2200 0    50   Input ~ 0
+5V
Text GLabel 6000 4200 0    50   Input ~ 0
INTREQ*
Text GLabel 4850 5300 0    50   Input ~ 0
GND
Text GLabel 6000 4100 0    50   Input ~ 0
NMI*
Text GLabel 4850 2900 0    50   Input ~ 0
B_D2
Text GLabel 4850 3100 0    50   Input ~ 0
B_D4
Text GLabel 4850 3300 0    50   Input ~ 0
B_D6
Text GLabel 4850 2700 0    50   Input ~ 0
B_D0_OUT
Text GLabel 3700 4900 0    50   Input ~ 0
B_ALATCH
Text GLabel 6000 5300 0    50   Input ~ 0
GND
Text GLabel 6000 2200 0    50   Input ~ 0
+5V
Text GLabel 6000 5200 0    50   Input ~ 0
RESET*
Text GLabel 3700 4600 0    50   Input ~ 0
B_BST2
Text GLabel 4850 3400 0    50   Input ~ 0
B_D7
Text GLabel 4850 3200 0    50   Input ~ 0
B_D5
Text GLabel 4850 3000 0    50   Input ~ 0
B_D3
Text GLabel 4850 2800 0    50   Input ~ 0
B_D1
Text GLabel 3700 3000 0    50   Input ~ 0
B_A6
Text GLabel 6000 2900 0    50   Input ~ 0
B_A8
Text GLabel 3700 3900 0    50   Input ~ 0
LA0
Text GLabel 6000 4800 0    50   Input ~ 0
B_A12
Text GLabel 3700 2800 0    50   Input ~ 0
B_A4
Text GLabel 3700 2700 0    50   Input ~ 0
B_A2
Text GLabel 3700 5000 0    50   Input ~ 0
B_A13
Text GLabel 6000 3000 0    50   Input ~ 0
B_A7
Text GLabel 3700 5100 0    50   Input ~ 0
B_A9
Text GLabel 3700 2900 0    50   Input ~ 0
B_A5
Text GLabel 6000 2700 0    50   Input ~ 0
B_A3
Text GLabel 6000 2800 0    50   Input ~ 0
B_A1
Text GLabel 6000 4900 0    50   Input ~ 0
B_PSEL*
NoConn ~ 6000 3700
NoConn ~ 6000 3200
NoConn ~ 3700 3800
NoConn ~ 3700 3300
Text GLabel 3700 5300 0    50   Input ~ 0
GND
Text GLabel 3700 2200 0    50   Input ~ 0
+5V
Text GLabel 6000 5000 0    50   Input ~ 0
CLKOUT
Text GLabel 3700 3100 0    50   Input ~ 0
READY
Text GLabel 3700 4700 0    50   Input ~ 0
B_BST3
Text GLabel 3700 4400 0    50   Input ~ 0
B_BST1
Text GLabel 6000 4300 0    50   Input ~ 0
B_WE*_IOCLK*
Text GLabel 6000 4500 0    50   Input ~ 0
B_RD*
$Comp
L Gemini:DIN-41612_Kontron_Card J?
U 3 1 5F02AAE1
P 6000 5300
AR Path="/5F02AAE1" Ref="J?"  Part="3" 
AR Path="/5F025AB5/5F02AAE1" Ref="J2"  Part="3" 
AR Path="/5ED05929/5F02AAE1" Ref="J1"  Part="3" 
F 0 "J1" V 5347 3750 50  0000 C CNN
F 1 "DIN-41612_Kontron_Card" V 5256 3750 50  0000 C CNN
F 2 "Connector_DIN:DIN41612_C_3x32_Male_Horizontal_THT" H 6400 2100 50  0001 L CNN
F 3 "https://www.arrow.com/en/products/86092645113755elf/amphenol-fci" H 5150 2200 50  0001 L CNN
	3    6000 5300
	-1   0    0    1   
$EndComp
$Comp
L Gemini:DIN-41612_Kontron_Card J?
U 1 1 5F02AAE7
P 3700 5300
AR Path="/5F02AAE7" Ref="J?"  Part="1" 
AR Path="/5F025AB5/5F02AAE7" Ref="J2"  Part="1" 
AR Path="/5ED05929/5F02AAE7" Ref="J1"  Part="1" 
F 0 "J1" V 3047 3750 50  0000 C CNN
F 1 "DIN-41612_Kontron_Card" V 2956 3750 50  0000 C CNN
F 2 "Connector_DIN:DIN41612_C_3x32_Male_Horizontal_THT" H 4100 2100 50  0001 L CNN
F 3 "https://www.arrow.com/en/products/86092645113755elf/amphenol-fci" H 2850 2200 50  0001 L CNN
	1    3700 5300
	-1   0    0    1   
$EndComp
$Comp
L Gemini:DIN-41612_Kontron_Card J?
U 2 1 5F02AAED
P 4850 5300
AR Path="/5F02AAED" Ref="J?"  Part="2" 
AR Path="/5F025AB5/5F02AAED" Ref="J2"  Part="2" 
AR Path="/5ED05929/5F02AAED" Ref="J1"  Part="2" 
F 0 "J1" V 4227 3750 50  0000 C CNN
F 1 "DIN-41612_Kontron_Card" V 4136 3750 50  0000 C CNN
F 2 "Connector_DIN:DIN41612_C_3x32_Male_Horizontal_THT" H 5250 2100 50  0001 L CNN
F 3 "https://www.arrow.com/en/products/86092645113755elf/amphenol-fci" H 4000 2200 50  0001 L CNN
	2    4850 5300
	-1   0    0    1   
$EndComp
Text GLabel 3700 2400 0    50   Input ~ 0
B_D14
Text GLabel 3700 2300 0    50   Input ~ 0
B_D13
Text GLabel 3700 2600 0    50   Input ~ 0
B_D12
Text GLabel 3700 2500 0    50   Input ~ 0
B_D11
Text GLabel 6000 2500 0    50   Input ~ 0
B_D10
Text GLabel 6000 3500 0    50   Input ~ 0
B_D9
Text GLabel 6000 2300 0    50   Input ~ 0
B_D8
Text GLabel 6000 2400 0    50   Input ~ 0
B_D15_IN
Text Notes 600  4500 0    50   ~ 0
In 8-bit  CRU access we\nwant the byte on D0-D7\nnot D8-D15\nDefine D0-D7 as the even word.\nThough D7 is still most significant.
Text GLabel 6000 5100 0    50   UnSpc ~ 0
B_MEM*
Text Notes 600  5150 0    50   ~ 0
Address word is rotated 1 right.\nPSEL* is moved to \nA15 MSBit of address\n(not symmetric with data bus)
Text GLabel 3700 3200 0    50   Input ~ 0
HOLD*
Text GLabel 6000 3100 0    50   Input ~ 0
FA11
Text GLabel 6000 3300 0    50   Input ~ 0
FA12
Text GLabel 6000 3400 0    50   Input ~ 0
FA13
Text GLabel 6000 4400 0    50   UnSpc ~ 0
FA15
Text GLabel 6000 4000 0    50   Input ~ 0
FA16
Text GLabel 3700 3500 0    50   Input ~ 0
FA14
Text GLabel 3700 4200 0    50   Input ~ 0
FA17
Text GLabel 3700 4300 0    50   Input ~ 0
FA18
Text Notes 600  5650 0    50   ~ 0
BMO   Bus Master Override\nPARENA Parallel CRU Enable (except /IORQ)\nSERENA  Serial CRU Enable
Text GLabel 3700 4800 0    50   Input ~ 0
IORQ*
Text GLabel 4850 4400 0    50   Input ~ 0
BMO
Text GLabel 4850 4600 0    50   Input ~ 0
INTA*
Text GLabel 4850 2300 0    50   Input ~ 0
FA19
Text GLabel 4850 2400 0    50   Input ~ 0
FA20
Text GLabel 4850 2500 0    50   Input ~ 0
FA21
Text GLabel 4850 2600 0    50   Input ~ 0
FA22
Text GLabel 4850 5100 0    50   Input ~ 0
FA23
Text GLabel 4850 5200 0    50   Input ~ 0
PRO
Text GLabel 3700 3700 0    50   Input ~ 0
PBPTHP
Text GLabel 6000 3600 0    50   Input ~ 0
SPAMM
Text GLabel 4850 4200 0    50   Input ~ 0
IC0
Text GLabel 4850 4100 0    50   Input ~ 0
IC1
Text GLabel 4850 4000 0    50   Input ~ 0
IC2
Text GLabel 4850 3900 0    50   Input ~ 0
IC3
Text GLabel 4850 3800 0    50   Input ~ 0
INT1
Text GLabel 4850 3700 0    50   Input ~ 0
INT2
Text GLabel 4850 3600 0    50   Input ~ 0
INT3
Text GLabel 4850 3500 0    50   Input ~ 0
INT4
Text GLabel 3700 4100 0    50   Input ~ 0
IAQ*
Text GLabel 3700 5200 0    50   Input ~ 0
B_HOLDA*
Text GLabel 4850 4900 0    50   Input ~ 0
SERENA*
Text GLabel 4850 5000 0    50   Input ~ 0
PARENA*
Text Notes 650  3900 0    50   ~ 0
Memory mapper outputs effective address \non A16-A28.   A0 is LSB here.\n\nBank.................... Word Address\nA14 A13 A12 A11 A10..A0\n8 bit Page. ..4K\nA23 .. A16  A15\nExtended Address\n\nA0  \\n...  } Word Address\nA10 /\n\nA11  \\nA12  4} Bank Reg#\nA13   }\nA14  /\nA15  } PSEL* or Map Enable\n\nEffective Address\n\nA16  FA11 4K half-page\n\nA17  FA12 \\n...       8} 8K page#\nA24  FA19 /\n\nA25  \\nA26   } 32MB Expansion\nA27   }\nA28  /\n     \ .. 3 mode bits ..\nA29  } RO      Read-Only\nA30  } PBPTHP  P-Box Pass-Thru Page\nA31  } SPAMM   Set page memory-mapped
NoConn ~ 3700 3400
NoConn ~ 3700 4000
NoConn ~ 3700 4500
NoConn ~ 4850 4800
NoConn ~ 4850 4700
NoConn ~ 4850 4500
NoConn ~ 4850 4300
Text GLabel 6000 4600 0    50   Input ~ 0
B_RDBENA*
Text Notes 600  5950 0    50   ~ 0
HALT is unused right?
Text GLabel 8200 3050 0    50   Input ~ 0
FA11
Text GLabel 8200 3150 0    50   Input ~ 0
FA12
Text GLabel 8200 3250 0    50   Input ~ 0
FA13
Text GLabel 8950 3150 0    50   Input ~ 0
SPAMM
Text GLabel 8200 3450 0    50   Input ~ 0
FA15
Text GLabel 8950 3250 0    50   Input ~ 0
PRO
Text GLabel 8200 4250 0    50   Input ~ 0
FA23
Text GLabel 8200 3850 0    50   Input ~ 0
FA19
Text GLabel 8200 3950 0    50   Input ~ 0
FA20
Text GLabel 8200 4050 0    50   Input ~ 0
FA21
Text GLabel 8200 4150 0    50   Input ~ 0
FA22
Text GLabel 8200 3350 0    50   Input ~ 0
FA14
Text GLabel 8950 3350 0    50   Input ~ 0
PBPTHP
Text GLabel 8200 3650 0    50   Input ~ 0
FA17
Text GLabel 8200 3750 0    50   Input ~ 0
FA18
Text GLabel 8200 3550 0    50   Input ~ 0
FA16
NoConn ~ 8200 3050
NoConn ~ 8200 3150
NoConn ~ 8200 3250
NoConn ~ 8200 3350
NoConn ~ 8200 3450
NoConn ~ 8200 3550
NoConn ~ 8200 3650
NoConn ~ 8200 3750
NoConn ~ 8200 3850
NoConn ~ 8200 3950
NoConn ~ 8200 4050
NoConn ~ 8200 4150
NoConn ~ 8200 4250
NoConn ~ 8950 3150
NoConn ~ 8950 3250
NoConn ~ 8950 3350
Text GLabel 8950 3800 0    50   Input ~ 0
INT1
Text GLabel 8950 3700 0    50   Input ~ 0
INT2
Text GLabel 8950 3600 0    50   Input ~ 0
INT3
Text GLabel 8950 3500 0    50   Input ~ 0
INT4
NoConn ~ 8950 3500
NoConn ~ 8950 3600
NoConn ~ 8950 3700
NoConn ~ 8950 3800
Text GLabel 8950 3050 0    50   Input ~ 0
BMO
NoConn ~ 8950 3050
NoConn ~ 3700 3600
Text Notes 8500 2850 2    50   ~ 0
Unused:
Text GLabel 6000 4700 0    50   Input ~ 0
RESOUT*
Text Notes 6600 2300 0    50   ~ 0
LSBit of MSByte (even byte)
Text Notes 6600 2400 0    50   ~ 0
MSBit of MSByte (even byte)
Text Notes 7950 2750 0    50   ~ 0
B_D15 D7    MSBit\nB_D14 D6\nB_D13 D5\nB_D12 D4\nB_D11 D3\nB_D10 D2\nB_D9  D1\nB_D8  D0\nB_D7 D15\nB_D6 D14\nB_D5 D13\nB_D4 D12\nB_D3 D11\nB_D2 D10\nB_D1 D9\nB_D0 D8      LSBit\n
Text Notes 6600 2600 0    50   ~ 0
LSBit, A0 addressing the word
Text GLabel 6000 3800 0    50   Input ~ 0
B_A11
Text GLabel 6000 3900 0    50   Input ~ 0
B_A10
Text Notes 3250 1700 0    50   ~ 0
Kontron Bus used in ECB RetroBrewComputing.net\n\nPins are numbered with 0 least significant.\nData bus 0-7 is from 8-bit bus, so it becomes the even (assuming big-endian), MSB of 16-bit bus.\nA0 is the LSBit of a word address. There is no byte address bit because 16-bit bus.\n99105 IN and OUT pins are in the expected place in the data bus word (not the addr).\n\nMEM* asserts for a CPU memory cycle. CRU requires decoding BST1-3 for IO.\nSERENA* and PARENA* assert for a bit-serial or byte/word-parallel CRU cycle.\nIORQ* asserts for a byte/word-parallel cycle to addresses 00-1FE (MSX style port)\n\nA card can use the data bus as AD0-AD15/PSEL of the 99105,\nor use separate address and data buses.
Text Notes 2750 6000 0    118  ~ 0
All Cards Except CPU will name least significiant with 0!\nB_A0 is least significant word address (9900 and 99105 have no A15)\nB_D0 is least significant data bit\n
Text GLabel 6000 2600 0    50   Input ~ 0
B_A0
$EndSCHEMATC
