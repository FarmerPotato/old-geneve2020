EESchema Schematic File Version 4
LIBS:BIOS-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 74xx:74LS259 U10
U 1 1 5EB245E5
P 3450 6100
F 0 "U10" H 3200 6750 50  0000 C CNN
F 1 "74LS259B" H 3700 6750 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-16_3.9x9.9mm_Pitch1.27mm" H 3450 6100 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS259" H 3450 6100 50  0001 C CNN
	1    3450 6100
	1    0    0    -1  
$EndComp
Text Notes 7300 6900 0    100  ~ 0
CRU Base Decoder and 8-Bit Register 
$Comp
L 74xx:74LS138 U?
U 1 1 5EB245EC
P 4100 1700
AR Path="/5EB245EC" Ref="U?"  Part="1" 
AR Path="/5EB213FB/5EB245EC" Ref="U5"  Part="1" 
F 0 "U5" H 3850 2200 50  0000 C CNN
F 1 "74LS138" H 4350 2200 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-16_3.9x9.9mm_Pitch1.27mm" H 4100 1700 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS138" H 4100 1700 50  0001 C CNN
	1    4100 1700
	1    0    0    -1  
$EndComp
Text GLabel 1050 1900 0    50   Output ~ 0
B_A7
Text GLabel 1050 3550 0    50   Output ~ 0
B_A8
Text GLabel 1050 3450 0    50   Output ~ 0
B_A4
Text GLabel 1050 3350 0    50   Output ~ 0
B_A10
Text GLabel 1050 3250 0    50   Output ~ 0
B_A11
Text GLabel 1050 3150 0    50   Output ~ 0
B_A12
Text GLabel 1050 3050 0    50   Output ~ 0
B_A13
Text GLabel 1050 6100 0    50   Input ~ 0
B_A2
Text GLabel 1050 6000 0    50   Input ~ 0
B_A1
Text GLabel 1050 5900 0    50   Input ~ 0
B_A0
Text GLabel 1050 5700 0    50   Input ~ 0
B_D0_OUT
Text GLabel 7750 5700 2    50   Input ~ 0
B_D15_IN
Wire Wire Line
	1050 3150 1800 3150
Wire Wire Line
	1050 3250 1800 3250
Wire Wire Line
	1800 3350 1050 3350
Wire Wire Line
	1050 3450 1800 3450
Wire Wire Line
	1800 3550 1050 3550
Text GLabel 900  7500 0    50   Input ~ 0
ZERO
$Comp
L power:GND #PWR0102
U 1 1 5EB2CF13
P 1000 7500
F 0 "#PWR0102" H 1000 7250 50  0001 C CNN
F 1 "GND" H 1005 7327 50  0000 C CNN
F 2 "" H 1000 7500 50  0001 C CNN
F 3 "" H 1000 7500 50  0001 C CNN
	1    1000 7500
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0103
U 1 1 5EB2D6EC
P 2300 2750
F 0 "#PWR0103" H 2300 2600 50  0001 C CNN
F 1 "+5V" H 2315 2923 50  0000 C CNN
F 2 "" H 2300 2750 50  0001 C CNN
F 3 "" H 2300 2750 50  0001 C CNN
	1    2300 2750
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0104
U 1 1 5EB2D886
P 1450 7500
F 0 "#PWR0104" H 1450 7350 50  0001 C CNN
F 1 "+5V" H 1465 7673 50  0000 C CNN
F 2 "" H 1450 7500 50  0001 C CNN
F 3 "" H 1450 7500 50  0001 C CNN
	1    1450 7500
	1    0    0    -1  
$EndComp
Text GLabel 1350 7500 0    50   Input ~ 0
ONE
Wire Wire Line
	1350 7500 1450 7500
Wire Wire Line
	900  7500 1000 7500
Text GLabel 1050 4850 0    50   Input ~ 0
SERENA*
Text GLabel 1050 6500 0    50   Input ~ 0
B_RESOUT*
Wire Wire Line
	1050 5900 2450 5900
Wire Wire Line
	1050 6000 2350 6000
Wire Wire Line
	2950 6100 2250 6100
Text Notes 2150 7800 0    50   ~ 0
Reminder: pin 3 is MSBit\nLowest CRU address is LSBit of register\n
Wire Wire Line
	2950 5700 1050 5700
Wire Wire Line
	2950 6500 1050 6500
$Comp
L power:+5V #PWR0108
U 1 1 5EB4068D
P 3450 5400
F 0 "#PWR0108" H 3450 5250 50  0001 C CNN
F 1 "+5V" H 3465 5573 50  0000 C CNN
F 2 "" H 3450 5400 50  0001 C CNN
F 3 "" H 3450 5400 50  0001 C CNN
	1    3450 5400
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0109
U 1 1 5EB417FB
P 4100 1100
F 0 "#PWR0109" H 4100 950 50  0001 C CNN
F 1 "+5V" H 4115 1273 50  0000 C CNN
F 2 "" H 4100 1100 50  0001 C CNN
F 3 "" H 4100 1100 50  0001 C CNN
	1    4100 1100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 5EB2CD73
P 2300 5150
F 0 "#PWR0101" H 2300 4900 50  0001 C CNN
F 1 "GND" H 2305 4977 50  0000 C CNN
F 2 "" H 2300 5150 50  0001 C CNN
F 3 "" H 2300 5150 50  0001 C CNN
	1    2300 5150
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS688 U?
U 1 1 5EB245F8
P 2300 3950
AR Path="/5EB245F8" Ref="U?"  Part="1" 
AR Path="/5EB213FB/5EB245F8" Ref="U11"  Part="1" 
F 0 "U11" H 1950 5050 50  0000 L CNN
F 1 "74LS688" H 2100 4050 50  0000 L CNN
F 2 "Housings_SOIC:SOIC-20W_7.5x12.8mm_Pitch1.27mm" H 2300 3950 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS688" H 2300 3950 50  0001 C CNN
	1    2300 3950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0110
U 1 1 5EB42A64
P 4100 2400
F 0 "#PWR0110" H 4100 2150 50  0001 C CNN
F 1 "GND" H 4105 2227 50  0000 C CNN
F 2 "" H 4100 2400 50  0001 C CNN
F 3 "" H 4100 2400 50  0001 C CNN
	1    4100 2400
	1    0    0    -1  
$EndComp
Text Notes 950  4700 0    50   ~ 0
 Base >2100
Text GLabel 1800 3950 0    50   Input ~ 0
ZERO
Text GLabel 1800 4450 0    50   Input ~ 0
ZERO
Text GLabel 1800 4350 0    50   Input ~ 0
ZERO
Text GLabel 1800 4250 0    50   Input ~ 0
ZERO
Text GLabel 1800 4150 0    50   Input ~ 0
ZERO
Wire Wire Line
	1800 4850 1050 4850
$Comp
L Device:LED D7
U 1 1 5EB490E0
P 4300 7400
F 0 "D7" V 4247 7478 50  0000 L CNN
F 1 "LED" V 4338 7478 50  0000 L CNN
F 2 "LEDs:LED_D3.0mm_Horizontal_O6.35mm_Z10.0mm" H 4300 7400 50  0001 C CNN
F 3 "~" H 4300 7400 50  0001 C CNN
	1    4300 7400
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0111
U 1 1 5EB4AEC9
P 4300 7550
F 0 "#PWR0111" H 4300 7300 50  0001 C CNN
F 1 "GND" H 4305 7377 50  0000 C CNN
F 2 "" H 4300 7550 50  0001 C CNN
F 3 "" H 4300 7550 50  0001 C CNN
	1    4300 7550
	1    0    0    -1  
$EndComp
Text Notes 5150 7750 0    50   ~ 0
Convention: SBO 0\nDSR ROM Enable\nAn indicator light\nQ0 says page 1 is active\nQ7 is a spare bit
$Comp
L 74xx:74LS251 U7
U 1 1 5EB52DE4
P 5950 6300
F 0 "U7" H 5950 7381 50  0000 C CNN
F 1 "74LS251" H 5950 7290 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-16_3.9x9.9mm_Pitch1.27mm" H 5950 6300 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS251" H 5950 6300 50  0001 C CNN
	1    5950 6300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3950 5800 4400 5800
Wire Wire Line
	3950 5700 4300 5700
Wire Wire Line
	5450 5900 4500 5900
Wire Wire Line
	3950 6200 4800 6200
Wire Wire Line
	3950 6000 4600 6000
Wire Wire Line
	7750 5700 6450 5700
Wire Wire Line
	5450 6600 2450 6600
Wire Wire Line
	2450 6600 2450 5900
Connection ~ 2450 5900
Wire Wire Line
	2450 5900 2950 5900
Wire Wire Line
	5450 6700 2350 6700
Wire Wire Line
	2350 6700 2350 6000
Connection ~ 2350 6000
Wire Wire Line
	2350 6000 2950 6000
Connection ~ 2250 6100
Wire Wire Line
	2250 6100 1050 6100
$Comp
L power:GND #PWR0105
U 1 1 5EB3E50B
P 3450 7400
F 0 "#PWR0105" H 3450 7150 50  0001 C CNN
F 1 "GND" H 3455 7227 50  0000 C CNN
F 2 "" H 3450 7400 50  0001 C CNN
F 3 "" H 3450 7400 50  0001 C CNN
	1    3450 7400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0113
U 1 1 5EB615F0
P 5950 7300
F 0 "#PWR0113" H 5950 7050 50  0001 C CNN
F 1 "GND" H 5955 7127 50  0000 C CNN
F 2 "" H 5950 7300 50  0001 C CNN
F 3 "" H 5950 7300 50  0001 C CNN
	1    5950 7300
	1    0    0    -1  
$EndComp
Wire Wire Line
	2250 6100 2250 6800
Wire Wire Line
	3450 6800 3450 7400
Wire Wire Line
	1050 3650 1800 3650
Wire Wire Line
	1050 3750 1800 3750
Text GLabel 1800 4050 0    50   Input ~ 0
ONE
Text Notes 2700 1350 0    50   ~ 0
Combine RD* and WE*\nwith chip enable
Text Notes 2900 4750 0    79   ~ 0
Bit-Serial CRU register
Wire Wire Line
	1050 3050 1800 3050
Text GLabel 1800 4550 0    50   Input ~ 0
ZERO
Text GLabel 1050 3650 0    50   Output ~ 0
B_A6
Text GLabel 1050 3750 0    50   Output ~ 0
B_A5
$Comp
L Device:R_Small R6
U 1 1 5EF212E5
P 4300 7150
F 0 "R6" H 4359 7196 50  0000 L CNN
F 1 "470" H 4359 7105 50  0000 L CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" H 4300 7150 50  0001 C CNN
F 3 "~" H 4300 7150 50  0001 C CNN
	1    4300 7150
	1    0    0    -1  
$EndComp
Wire Wire Line
	2250 6800 5450 6800
Text Notes 700  5100 0    50   ~ 0
This card has only\na serial register
NoConn ~ 4600 1700
NoConn ~ 4600 1800
NoConn ~ 4600 1900
NoConn ~ 4600 2000
NoConn ~ 4600 2100
NoConn ~ 6450 5800
Text GLabel 4300 5550 1    50   Output ~ 0
ROMPAGE0
Text GLabel 4400 5550 1    50   Output ~ 0
ROMPAGE1
Text GLabel 4500 5550 1    50   Output ~ 0
ROMPAGE2
Text GLabel 4600 5550 1    50   Output ~ 0
RAMPAGE0
Text GLabel 4700 5550 1    50   Output ~ 0
RAMPAGE1
Text GLabel 4800 5550 1    50   Output ~ 0
RAMPAGE2
Text GLabel 4900 5550 1    50   Output ~ 0
RAMPAGE3
Wire Wire Line
	4300 5550 4300 5700
Wire Wire Line
	3950 6400 5000 6400
Wire Wire Line
	3950 6300 4900 6300
Wire Wire Line
	4400 5550 4400 5800
Connection ~ 4400 5800
Wire Wire Line
	4400 5800 5450 5800
Wire Wire Line
	4500 5550 4500 5900
Connection ~ 4500 5900
Wire Wire Line
	4500 5900 3950 5900
Wire Wire Line
	4600 5550 4600 6000
Connection ~ 4600 6000
Wire Wire Line
	4600 6000 5450 6000
Wire Wire Line
	4700 5550 4700 6100
Wire Wire Line
	4700 6100 3950 6100
Wire Wire Line
	4800 5550 4800 6200
Wire Wire Line
	4900 5550 4900 6300
Connection ~ 4900 6300
Wire Wire Line
	4900 6300 5450 6300
Connection ~ 4300 5700
Wire Wire Line
	4300 5700 5450 5700
Wire Wire Line
	4300 5700 4300 6400
$Comp
L Device:R_Small R1
U 1 1 5F00B071
P 5000 7150
F 0 "R1" H 5059 7196 50  0000 L CNN
F 1 "470" H 5059 7105 50  0000 L CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" H 5000 7150 50  0001 C CNN
F 3 "~" H 5000 7150 50  0001 C CNN
	1    5000 7150
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D1
U 1 1 5F00B340
P 5000 7400
F 0 "D1" V 4947 7478 50  0000 L CNN
F 1 "LED" V 5038 7478 50  0000 L CNN
F 2 "LEDs:LED_D3.0mm_Horizontal_O6.35mm_Z10.0mm" H 5000 7400 50  0001 C CNN
F 3 "~" H 5000 7400 50  0001 C CNN
	1    5000 7400
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR01
U 1 1 5F00B6A9
P 5000 7550
F 0 "#PWR01" H 5000 7300 50  0001 C CNN
F 1 "GND" H 5005 7377 50  0000 C CNN
F 2 "" H 5000 7550 50  0001 C CNN
F 3 "" H 5000 7550 50  0001 C CNN
	1    5000 7550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4300 6400 4300 7050
Wire Wire Line
	5000 7050 5000 6400
Connection ~ 5000 6400
Wire Wire Line
	5000 6400 5450 6400
Text GLabel 5950 5400 0    50   Output ~ 0
+5V
Text GLabel 3450 5400 0    50   Output ~ 0
+5V
Text GLabel 1050 2000 0    50   Output ~ 0
B_A9
Text GLabel 1050 1600 0    50   Output ~ 0
B_A3
Text GLabel 4900 1600 2    50   Output ~ 0
WE_SREG0*
Wire Wire Line
	2950 6300 2150 6300
Text GLabel 1050 1400 0    50   Output ~ 0
B_WE*
Text GLabel 4900 1500 2    50   Output ~ 0
RD_SREG0*
Text GLabel 1050 1500 0    50   Output ~ 0
B_RD*
Wire Wire Line
	3100 2100 3600 2100
Wire Wire Line
	3100 2100 3100 3050
Wire Wire Line
	2800 3050 3100 3050
Text GLabel 2150 6300 0    50   Output ~ 0
WE_SREG0*
Text GLabel 5450 7000 0    50   Output ~ 0
RD_SREG0*
Wire Wire Line
	4900 1600 4600 1600
Connection ~ 4800 6200
Wire Wire Line
	4800 6200 5450 6200
Wire Wire Line
	5450 6100 4700 6100
Connection ~ 4700 6100
Text Label 5000 6900 0    50   ~ 0
LED7
Text Notes 700  2850 0    50   ~ 0
no BMO. this is the BIOS!\nInclude A6 to fix alias at >80
Wire Wire Line
	1050 1400 3600 1400
Wire Wire Line
	1050 1500 3600 1500
Wire Wire Line
	1050 1600 3600 1600
Wire Wire Line
	4600 1500 4900 1500
NoConn ~ 4600 1400
Wire Wire Line
	1050 1900 3600 1900
Wire Wire Line
	1050 2000 3600 2000
Text GLabel 1800 4650 0    50   Input ~ 0
ZERO
Text Notes 600  4300 0    50   ~ 0
Moved A7 to E3\nit is a ONE and E3 \nwas unused
Text Notes 4700 2000 0    50   ~ 0
A3 would make this RD_SREG1*\nand WE_SREG1*
Text Notes 1550 1900 0    50   ~ 0
A7 the x1xx in the base address
Text Label 2800 3050 0    50   ~ 0
CRUBASE*
Text Notes 600  4000 0    50   ~ 0
Swapped A4 and A9\nmade routing easier
$EndSCHEMATC
