Name     bios.pld ;
PartNo   00 ;
Date     8/5/2020 ;
Revision 01 ;
Designer Engineer ;
Company  Erik Olson ;
Assembly None ;
Location  ;
Device   g22v10lcc ;

/* options:   cbld -e | grep g22v10      OE D AR SP */

/* *************** INPUT PINS *********************/
/* positive logic IN USE: MEM means MEM* == 0) */
PIN 2  = clock;        /* Register Clock is WE*  */
PIN [3,13,16,9,4] = [A0,A1,A2,A10,A13];      /* Address Inputs     */
PIN [5,6] = [BST1, BST2];                   /* Bus Status */
PIN  7  = !MEM;
PIN 10  = !PSEL;
PIN 11  = !SEL2100;
PIN 12  = D0;
PIN 17 = !MEMSEL;
PIN 26 = CRUIN;
PIN [25,19,21,23,18,20,24] = [Q0..6]; /* ROMPAGE0..2,RAMPAGE0..3 */
PIN 27 = NOT_A13;

field output = [Q6..0]; /* internal 8-bit register */
field sel = [A2..0];    /* bit read-write selector */

/** Logic Equations, positive logic **/
NOT_A13 = !A13;
CRUEN = SEL2100 & !A13 & !A10; /* anywhere in >2100 - >21fe , no other aliases */
MACRO = !(BST1 # BST2 # MEM);
GM    = !(!MEM # PSEL);
/* */
!MEMSEL = !(MACRO # GM); /* still positive logic */

/* next state logic, for clocked registers */
output.d  =  [Q6, Q5, Q4, Q3, Q2, Q1, D0]  &  sel:0 & CRUEN
          #  [Q6, Q5, Q4, Q3, Q2, D0, Q0]  &  sel:1 & CRUEN
          #  [Q6, Q5, Q4, Q3, D0, Q1, Q0]  &  sel:2 & CRUEN
          #  [Q6, Q5, Q4, D0, Q2, Q1, Q0]  &  sel:3 & CRUEN
          #  [Q6, Q5, D0, Q3, Q2, Q1, Q0]  &  sel:4 & CRUEN
          #  [Q6, D0, Q4, Q3, Q2, Q1, Q0]  &  sel:5 & CRUEN
          #  [D0, Q5, Q4, Q3, Q2, Q1, Q0]  &  sel:6 & CRUEN
          #  [Q6, Q5, Q4, Q3, Q2, Q1, Q0]  &  sel:7 & CRUEN
          #  [Q6, Q5, Q4, Q3, Q2, Q1, Q0]  &         !CRUEN;


output.sp = 'h'00;             /* synchronous preset  not used */

output.oe = 'h'ff;         /* tri-state control  always on */

output.ar = 'h'00;              /* asynchronous reset not used  */


/* Readout of registered bit (cru input of the cpu) */
CRUIN =  Q0  &  sel:0
      #  Q1  &  sel:1
      #  Q2  &  sel:2
      #  Q3  &  sel:3
      #  Q4  &  sel:4
      #  Q5  &  sel:5
      #  Q6  &  sel:6;

