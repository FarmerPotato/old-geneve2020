EESchema Schematic File Version 4
LIBS:BIOS-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Gemini:74ALS645 U?
U 1 1 5F13AD88
P 4900 2350
F 0 "U?" H 4900 3331 50  0000 C CNN
F 1 "74ALS645" H 4900 3240 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-20W_7.5x12.8mm_Pitch1.27mm" H 4900 2350 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS245" H 4900 2350 50  0001 C CNN
	1    4900 2350
	1    0    0    -1  
$EndComp
$Comp
L Gemini:74ALS645 U?
U 1 1 5F13AD8E
P 4900 4250
F 0 "U?" H 4900 5231 50  0000 C CNN
F 1 "74ALS645" H 4900 5140 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-20W_7.5x12.8mm_Pitch1.27mm" H 4900 4250 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS245" H 4900 4250 50  0001 C CNN
	1    4900 4250
	1    0    0    -1  
$EndComp
$Comp
L Gemini:74ALS645 U?
U 1 1 5F13AD94
P 4900 6250
F 0 "U?" H 4900 7231 50  0000 C CNN
F 1 "74ALS645" H 4900 7140 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-20W_7.5x12.8mm_Pitch1.27mm" H 4900 6250 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS245" H 4900 6250 50  0001 C CNN
	1    4900 6250
	1    0    0    -1  
$EndComp
Text GLabel 2250 3800 0    50   Input ~ 0
B_A8
Text GLabel 2250 2550 0    50   Input ~ 0
B_A7
Text GLabel 2250 2150 0    50   Input ~ 0
B_A3
Text GLabel 2250 1950 0    50   Input ~ 0
B_A1
Text GLabel 4400 3950 0    50   Input ~ 0
B_D10
Text GLabel 4400 3750 0    50   Input ~ 0
B_D8
Text GLabel 4400 4450 0    50   Input ~ 0
B_D15_IN
Text GLabel 2250 1850 0    50   Input ~ 0
B_A0
Text GLabel 2250 4100 0    50   Input ~ 0
B_A11
Text GLabel 2250 4000 0    50   Input ~ 0
B_A10
Text GLabel 2250 4200 0    50   Input ~ 0
B_A12
Text GLabel 2250 4500 0    50   Input ~ 0
B_PSEL*
Text GLabel 4400 5750 0    50   UnSpc ~ 0
B_MEM*
Text GLabel 4400 5850 0    50   Input ~ 0
B_RESOUT*
Text GLabel 4400 5950 0    50   Input ~ 0
SERENA*
Text GLabel 4400 6050 0    50   Input ~ 0
BMO
Text GLabel 4400 2050 0    50   Input ~ 0
B_D2
Text GLabel 4400 2250 0    50   Input ~ 0
B_D4
Text GLabel 4400 2450 0    50   Input ~ 0
B_D6
Text GLabel 4400 1850 0    50   Input ~ 0
B_D0_OUT
Text GLabel 4400 2550 0    50   Input ~ 0
B_D7
Text GLabel 4400 2350 0    50   Input ~ 0
B_D5
Text GLabel 4400 2150 0    50   Input ~ 0
B_D3
Text GLabel 4400 1950 0    50   Input ~ 0
B_D1
Text GLabel 2250 2450 0    50   Input ~ 0
B_A6
Text GLabel 2250 2250 0    50   Input ~ 0
B_A4
Text GLabel 2250 2050 0    50   Input ~ 0
B_A2
Text GLabel 2250 2350 0    50   Input ~ 0
B_A5
Text GLabel 4400 4350 0    50   Input ~ 0
B_D14
Text GLabel 4400 4250 0    50   Input ~ 0
B_D13
Text GLabel 4400 4150 0    50   Input ~ 0
B_D12
Text GLabel 4400 4050 0    50   Input ~ 0
B_D11
Text GLabel 2250 4400 0    50   Input ~ 0
B_A14
Text GLabel 4400 6150 0    50   Input ~ 0
B_BST1
Text GLabel 4400 6250 0    50   Input ~ 0
B_BST2
Text GLabel 2250 4300 0    50   Input ~ 0
B_A13
Text GLabel 2250 3900 0    50   Input ~ 0
B_A9
$Comp
L Gemini:74ALS645 U?
U 1 1 5F13ADBF
P 2750 2350
F 0 "U?" H 2750 3331 50  0000 C CNN
F 1 "74ALS645" H 2750 3240 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-20W_7.5x12.8mm_Pitch1.27mm" H 2750 2350 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS245" H 2750 2350 50  0001 C CNN
	1    2750 2350
	1    0    0    -1  
$EndComp
$Comp
L Gemini:74ALS645 U?
U 1 1 5F13ADC5
P 2750 4300
F 0 "U?" H 2750 5281 50  0000 C CNN
F 1 "74ALS645" H 2750 5190 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-20W_7.5x12.8mm_Pitch1.27mm" H 2750 4300 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS245" H 2750 4300 50  0001 C CNN
	1    2750 4300
	1    0    0    -1  
$EndComp
Text GLabel 4400 3850 0    50   Input ~ 0
B_D9
$EndSCHEMATC
