EESchema Schematic File Version 4
LIBS:BIOS-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 7400 6900 0    100  ~ 0
16-Bit Backplane Bus Signals
Text GLabel 1450 2300 2    50   Input ~ 0
RD*
Text GLabel 1450 2400 2    50   Input ~ 0
WE*_IOCLK*
Text GLabel 1450 2500 2    50   Input ~ 0
MEM*
Text GLabel 1450 3400 2    50   Input ~ 0
AD13
Text GLabel 1450 3500 2    50   Input ~ 0
AD11
Text GLabel 1450 3600 2    50   Input ~ 0
AD9
Text GLabel 1450 3800 2    50   Input ~ 0
AD7
Text GLabel 1450 3900 2    50   Input ~ 0
AD5
Text GLabel 1450 4100 2    50   Input ~ 0
AD1
Text GLabel 1450 4000 2    50   Input ~ 0
AD3
Text GLabel 1450 2600 2    50   Input ~ 0
BST1
Text GLabel 1450 2700 2    50   Input ~ 0
BST3
Text GLabel 1450 2800 2    50   Input ~ 0
READY
Text GLabel 1450 2900 2    50   Input ~ 0
CLKOUT
Text GLabel 1450 3000 2    50   Input ~ 0
ALATCH
Text GLabel 1450 2100 2    50   Input ~ 0
+5V
Text GLabel 1450 3300 2    50   Input ~ 0
PSEL*_AD15_OUT
Text GLabel 3150 3950 2    50   Input ~ 0
INT1
Text GLabel 1450 5200 2    50   Input ~ 0
GND
Text GLabel 3150 5150 2    50   Input ~ 0
GND
Text GLabel 4900 4950 2    50   Input ~ 0
EXBAE
$Comp
L BIOS-rescue:DIN-41612_C_3x32_RA_Header-Gemini J?
U 1 1 5EB5704C
P 2150 2100
AR Path="/5EB5704C" Ref="J?"  Part="1" 
AR Path="/5EB6AE9B/5EB5704C" Ref="J1"  Part="1" 
F 0 "J1" H 2378 2146 50  0000 L CNN
F 1 "DIN-41612_C_3x32_RA_Header" H 2378 2055 50  0000 L CNN
F 2 "Connector_DIN:DIN41612_C_3x32_Male_Horizontal_THT" H 4500 2200 50  0001 L CNN
F 3 "https://www.arrow.com/en/products/86092645113755elf/amphenol-fci" H 2100 2100 50  0001 L CNN
	1    2150 2100
	1    0    0    -1  
$EndComp
$Comp
L BIOS-rescue:DIN-41612_C_3x32_RA_Header-Gemini J?
U 2 1 5EB58079
P 2350 2050
AR Path="/5EB58079" Ref="J?"  Part="2" 
AR Path="/5EB6AE9B/5EB58079" Ref="J1"  Part="2" 
F 0 "J1" H 2758 2315 50  0000 C CNN
F 1 "DIN-41612_C_3x32_RA_Header" H 2758 2224 50  0000 C CNN
F 2 "Connector_DIN:DIN41612_C_3x32_Male_Horizontal_THT" H 4700 2150 50  0001 L CNN
F 3 "https://www.arrow.com/en/products/86092645113755elf/amphenol-fci" H 2300 2050 50  0001 L CNN
	2    2350 2050
	1    0    0    -1  
$EndComp
$Comp
L BIOS-rescue:DIN-41612_C_3x32_RA_Header-Gemini J?
U 3 1 5EB59147
P 2950 2050
AR Path="/5EB59147" Ref="J?"  Part="3" 
AR Path="/5EB6AE9B/5EB59147" Ref="J1"  Part="3" 
F 0 "J1" H 4508 2315 50  0000 C CNN
F 1 "DIN-41612_C_3x32_RA_Header" H 4508 2224 50  0000 C CNN
F 2 "Connector_DIN:DIN41612_C_3x32_Male_Horizontal_THT" H 5300 2150 50  0001 L CNN
F 3 "https://www.arrow.com/en/products/86092645113755elf/amphenol-fci" H 2900 2050 50  0001 L CNN
	3    2950 2050
	1    0    0    -1  
$EndComp
NoConn ~ 1450 3200
NoConn ~ 1450 3700
NoConn ~ 4900 3050
NoConn ~ 4900 3550
Text GLabel 3150 4950 2    50   Input ~ 0
CRUENA*
Text GLabel 1450 4200 2    50   Input ~ 0
LA15
Text GLabel 1450 4300 2    50   Input ~ 0
LA13
Text GLabel 1450 4400 2    50   Input ~ 0
LA11
Text GLabel 1450 4500 2    50   Input ~ 0
LA9
Text GLabel 1450 4700 2    50   Input ~ 0
LA5
Text GLabel 1450 4600 2    50   Input ~ 0
LA7
Text GLabel 1450 4800 2    50   Input ~ 0
LA3
Text GLabel 1450 4900 2    50   Input ~ 0
LA1
Text GLabel 3150 3850 2    50   Input ~ 0
INT2
Text GLabel 3150 3750 2    50   Input ~ 0
INT3
Text GLabel 3150 3650 2    50   Input ~ 0
INT4
Text GLabel 3150 3550 2    50   Input ~ 0
INT5
Text GLabel 3150 3450 2    50   Input ~ 0
INT6
Text GLabel 3150 3350 2    50   Input ~ 0
INT7
Text GLabel 4900 4150 2    50   Input ~ 0
LA14
Text GLabel 4900 4250 2    50   Input ~ 0
LA12
Text GLabel 4900 4350 2    50   Input ~ 0
LA10
Text GLabel 4900 4750 2    50   Input ~ 0
LA2
Text GLabel 4900 4850 2    50   Input ~ 0
LA0
Text GLabel 4900 4550 2    50   Input ~ 0
LA6
Text GLabel 4900 4450 2    50   Input ~ 0
LA8
Text GLabel 4900 4650 2    50   Input ~ 0
LA4
Text GLabel 4900 3250 2    50   Input ~ 0
AD14
Text GLabel 4900 3350 2    50   Input ~ 0
AD12
Text GLabel 4900 3450 2    50   Input ~ 0
AD10
Text GLabel 4900 3650 2    50   Input ~ 0
AD8
Text GLabel 4900 3750 2    50   Input ~ 0
AD6
Text GLabel 4900 4050 2    50   Input ~ 0
AD0_IN
Text GLabel 4900 3950 2    50   Input ~ 0
AD2
Text GLabel 4900 3850 2    50   Input ~ 0
AD4
Text GLabel 4900 2550 2    50   Input ~ 0
CLK3
Text GLabel 4900 2450 2    50   Input ~ 0
BST2
Text GLabel 4900 5150 2    50   Input ~ 0
GND
Text GLabel 4900 5050 2    50   Input ~ 0
RESET*
Text GLabel 3150 2050 2    50   Input ~ 0
+5V
Text GLabel 4900 2050 2    50   Input ~ 0
+5V
$Comp
L Gemini:TMS99105 U6
U 1 1 5EB555B9
P 8900 3200
F 0 "U6" H 8900 4781 50  0000 C CNN
F 1 "TMS99105" H 8900 4690 50  0000 C CNN
F 2 "Housings_DIP:DIP-40_W15.24mm_Socket_LongPads" H 8900 3200 50  0001 C CNN
F 3 "" H 8900 3200 50  0001 C CNN
	1    8900 3200
	1    0    0    -1  
$EndComp
Text GLabel 8050 3100 0    50   Input ~ 0
IC0
Text GLabel 8050 3200 0    50   Input ~ 0
IC1
Text GLabel 8050 3300 0    50   Input ~ 0
IC2
Text GLabel 8050 3400 0    50   Input ~ 0
IC3
Text GLabel 8050 2900 0    50   Input ~ 0
INTREQ*
Text GLabel 8050 2300 0    50   Input ~ 0
WE*_IOCLK*
Text GLabel 8050 2500 0    50   Input ~ 0
RESET*
Text GLabel 9750 3000 2    50   Input ~ 0
ALATCH
Text GLabel 9750 2900 2    50   Input ~ 0
CLKOUT
Text GLabel 8050 2700 0    50   Input ~ 0
HOLD*
Text GLabel 8050 2800 0    50   Input ~ 0
READY
Text GLabel 8050 3000 0    50   Input ~ 0
NMI*
Text GLabel 8050 2400 0    50   Input ~ 0
RD*
Text GLabel 9750 2300 2    50   Input ~ 0
MEM*
Text GLabel 8950 1800 2    50   Input ~ 0
VCC
Text Notes 7800 2050 0    50   ~ 0
Tie RESET*,APP* for \nMacrostore Prototyping
Text GLabel 9750 2400 2    50   Input ~ 0
BST1
Text GLabel 9750 2500 2    50   Input ~ 0
BST2
Text GLabel 9750 2600 2    50   Input ~ 0
BST3
Text GLabel 9750 2700 2    50   Input ~ 0
XTAL1
Text GLabel 9750 2800 2    50   Input ~ 0
XTAL2
Text GLabel 9050 4600 2    50   Input ~ 0
GND
Wire Wire Line
	8050 2600 8050 2500
Text GLabel 9750 3100 2    50   Input ~ 0
PSEL*_AD15_OUT
Text GLabel 8050 3600 0    50   Input ~ 0
AD0_IN
Text GLabel 8050 4000 0    50   Input ~ 0
AD4
Text GLabel 8050 3700 0    50   Input ~ 0
AD1
Text GLabel 8050 3800 0    50   Input ~ 0
AD2
Text GLabel 8050 3900 0    50   Input ~ 0
AD3
Text GLabel 9750 4100 2    50   Input ~ 0
AD5
Text GLabel 9750 4000 2    50   Input ~ 0
AD6
Text GLabel 9750 3900 2    50   Input ~ 0
AD7
Text GLabel 9750 3800 2    50   Input ~ 0
AD8
Text GLabel 9750 3700 2    50   Input ~ 0
AD9
Text GLabel 9750 3600 2    50   Input ~ 0
AD10
Text GLabel 9750 3500 2    50   Input ~ 0
AD11
Text GLabel 9750 3400 2    50   Input ~ 0
AD12
Text GLabel 9750 3300 2    50   Input ~ 0
AD13
Text GLabel 9750 3200 2    50   Input ~ 0
AD14
NoConn ~ 8050 3500
$Comp
L Device:Crystal Y1
U 1 1 5EB62998
P 5100 7050
F 0 "Y1" V 5054 7181 50  0000 L CNN
F 1 "24 MHz" V 5145 7181 50  0000 L CNN
F 2 "Crystals:Crystal_HC49-U_Vertical" H 5100 7050 50  0001 C CNN
F 3 "~" H 5100 7050 50  0001 C CNN
	1    5100 7050
	0    1    1    0   
$EndComp
Wire Wire Line
	4450 7050 4950 7050
Wire Wire Line
	4950 7050 4950 7200
Wire Wire Line
	4450 6950 4950 6950
Wire Wire Line
	4950 6950 4950 6900
Text GLabel 3150 6450 0    50   Input ~ 0
GND
$Comp
L Device:R R3
U 1 1 5EB629A4
P 1650 6800
F 0 "R3" V 1650 6650 50  0000 C CNN
F 1 "10K" V 1650 6800 50  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" V 1580 6800 50  0001 C CNN
F 3 "~" H 1650 6800 50  0001 C CNN
	1    1650 6800
	0    -1   -1   0   
$EndComp
Text GLabel 1500 6700 0    50   Input ~ 0
VCC
$Comp
L Device:R R2
U 1 1 5EB629AB
P 1650 6700
F 0 "R2" V 1650 6550 50  0000 C CNN
F 1 "10K" V 1650 6700 50  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" V 1580 6700 50  0001 C CNN
F 3 "~" H 1650 6700 50  0001 C CNN
	1    1650 6700
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R4
U 1 1 5EB629B1
P 1650 6900
F 0 "R4" V 1650 6750 50  0000 C CNN
F 1 "10K" V 1650 6900 50  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" V 1580 6900 50  0001 C CNN
F 3 "~" H 1650 6900 50  0001 C CNN
	1    1650 6900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C_Small C3
U 1 1 5EB629B7
P 5550 6900
F 0 "C3" V 5321 6900 50  0000 C CNN
F 1 "5pF" V 5412 6900 50  0000 C CNN
F 2 "Capacitors_SMD:C_1206_HandSoldering" H 5550 6900 50  0001 C CNN
F 3 "~" H 5550 6900 50  0001 C CNN
	1    5550 6900
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C4
U 1 1 5EB629BD
P 5550 7200
F 0 "C4" V 5321 7200 50  0000 C CNN
F 1 "5pF" V 5412 7200 50  0000 C CNN
F 2 "Capacitors_SMD:C_1206_HandSoldering" H 5550 7200 50  0001 C CNN
F 3 "~" H 5550 7200 50  0001 C CNN
	1    5550 7200
	0    1    1    0   
$EndComp
Wire Wire Line
	5650 6900 5650 7050
Text GLabel 5700 7050 2    50   Input ~ 0
GND
$Comp
L Connector:Barrel_Jack J2
U 1 1 5EB629C7
P 5100 6150
F 0 "J2" H 4850 6450 50  0000 C CNN
F 1 "Barrel_Jack" H 5100 6350 50  0000 C CNN
F 2 "Connect:Barrel_Jack_CUI_PJ-102AH" H 5150 6110 50  0001 C CNN
F 3 "~" H 5150 6110 50  0001 C CNN
	1    5100 6150
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0112
U 1 1 5EB629CD
P 5400 6050
F 0 "#PWR0112" H 5400 5900 50  0001 C CNN
F 1 "VCC" H 5417 6223 50  0000 C CNN
F 2 "" H 5400 6050 50  0001 C CNN
F 3 "" H 5400 6050 50  0001 C CNN
	1    5400 6050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0114
U 1 1 5EB629D3
P 5400 6250
F 0 "#PWR0114" H 5400 6000 50  0001 C CNN
F 1 "GND" H 5405 6077 50  0000 C CNN
F 2 "" H 5400 6250 50  0001 C CNN
F 3 "" H 5400 6250 50  0001 C CNN
	1    5400 6250
	1    0    0    -1  
$EndComp
Text GLabel 1900 6900 2    50   Input ~ 0
HOLD*
Text GLabel 1900 6700 2    50   Input ~ 0
READY
Text GLabel 1900 6800 2    50   Input ~ 0
INTREQ*
Wire Wire Line
	1800 6900 1900 6900
Wire Wire Line
	1900 6700 1800 6700
Wire Wire Line
	1800 6800 1900 6800
Text GLabel 1500 6800 0    50   Input ~ 0
VCC
Text GLabel 5400 6050 2    50   Input ~ 0
VCC
Text GLabel 5400 6250 2    50   Input ~ 0
GND
Text GLabel 4450 6950 0    50   Input ~ 0
XTAL1
Text GLabel 4450 7050 0    50   Input ~ 0
XTAL2
Wire Wire Line
	5700 7050 5650 7050
Connection ~ 5650 7050
Wire Wire Line
	5650 7050 5650 7200
$Comp
L Device:C_Small C2
U 1 1 5EB629E7
P 3400 6350
F 0 "C2" V 3171 6350 50  0000 C CNN
F 1 "4.7uF" V 3262 6350 50  0000 C CNN
F 2 "Capacitors_SMD:C_1206_HandSoldering" H 3400 6350 50  0001 C CNN
F 3 "~" H 3400 6350 50  0001 C CNN
	1    3400 6350
	-1   0    0    1   
$EndComp
$Comp
L Switch:SW_SPST SW2
U 1 1 5EB629ED
P 3150 6250
F 0 "SW2" H 3150 6400 50  0000 C CNN
F 1 "SW_SPST" H 3150 6116 50  0001 C CNN
F 2 "Buttons_Switches_ThroughHole:SW_TH_Tactile_Omron_B3F-10xx" H 3150 6250 50  0001 C CNN
F 3 "~" H 3150 6250 50  0001 C CNN
	1    3150 6250
	0    -1   1    0   
$EndComp
Wire Wire Line
	3150 6450 3400 6450
Text GLabel 2750 6050 0    50   Input ~ 0
VCC
$Comp
L Device:R R5
U 1 1 5EB629F5
P 2900 6050
F 0 "R5" V 2900 5900 50  0000 C CNN
F 1 "10K" V 2900 6050 50  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" V 2830 6050 50  0001 C CNN
F 3 "~" H 2900 6050 50  0001 C CNN
	1    2900 6050
	0    -1   -1   0   
$EndComp
Text GLabel 3400 6050 2    50   Input ~ 0
NMI*
Wire Wire Line
	3400 6250 3400 6050
Text GLabel 1500 6900 0    50   Input ~ 0
VCC
Text GLabel 1900 6450 0    50   Input ~ 0
GND
$Comp
L Device:C_Small C1
U 1 1 5EB62A05
P 2150 6350
F 0 "C1" V 1921 6350 50  0000 C CNN
F 1 "4.7uF" V 2012 6350 50  0000 C CNN
F 2 "Capacitors_SMD:C_1206_HandSoldering" H 2150 6350 50  0001 C CNN
F 3 "~" H 2150 6350 50  0001 C CNN
	1    2150 6350
	-1   0    0    1   
$EndComp
$Comp
L Switch:SW_SPST SW1
U 1 1 5EB62A0B
P 1900 6250
F 0 "SW1" H 1900 6400 50  0000 C CNN
F 1 "SW_SPST" H 1900 6116 50  0001 C CNN
F 2 "Buttons_Switches_ThroughHole:SW_TH_Tactile_Omron_B3F-10xx" H 1900 6250 50  0001 C CNN
F 3 "~" H 1900 6250 50  0001 C CNN
	1    1900 6250
	0    -1   1    0   
$EndComp
Wire Wire Line
	1900 6450 2150 6450
Text GLabel 1500 6050 0    50   Input ~ 0
VCC
$Comp
L Device:R R1
U 1 1 5EB62A13
P 1650 6050
F 0 "R1" V 1650 5900 50  0000 C CNN
F 1 "10K" V 1650 6050 50  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" V 1580 6050 50  0001 C CNN
F 3 "~" H 1650 6050 50  0001 C CNN
	1    1650 6050
	0    -1   -1   0   
$EndComp
Text GLabel 2150 6050 2    50   Input ~ 0
RESET*
Wire Wire Line
	2150 6250 2150 6050
Wire Wire Line
	8850 4600 9050 4600
Wire Wire Line
	4950 6900 5450 6900
Wire Wire Line
	3050 6050 3400 6050
Wire Wire Line
	4950 7200 5450 7200
Wire Wire Line
	1800 6050 2150 6050
$EndSCHEMATC
