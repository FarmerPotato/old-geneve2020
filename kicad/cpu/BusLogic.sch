EESchema Schematic File Version 4
LIBS:Steven-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 9000 1300 2    50   Input ~ 0
SERENA*
Text GLabel 9000 1700 2    50   Input ~ 0
PARENA*
Text GLabel 6800 1600 2    50   Input ~ 0
IAQ*
Text GLabel 9000 2800 2    50   Input ~ 0
IORQ*
$Comp
L 74xx:74LS138 U8
U 1 1 5F13B6CB
P 6050 1600
F 0 "U8" H 6050 2381 50  0000 C CNN
F 1 "74ALS138" H 6050 2290 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-16_3.9x9.9mm_Pitch1.27mm" H 6050 1600 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS138" H 6050 1600 50  0001 C CNN
	1    6050 1600
	1    0    0    -1  
$EndComp
Text GLabel 5550 3500 0    50   Input ~ 0
MEM*
Text GLabel 5550 1500 0    50   Input ~ 0
BST1
Text GLabel 5550 1400 0    50   Input ~ 0
BST2
Text GLabel 5550 1300 0    50   Input ~ 0
BST3
Text GLabel 5550 1900 0    50   Input ~ 0
ALATCH
NoConn ~ 6550 1300
NoConn ~ 6550 1400
NoConn ~ 6550 1900
Text GLabel 6800 1800 2    50   Input ~ 0
INTA*
Text Notes 9550 4150 0    50   ~ 0
IORQ* is a CRU\naccess in >8000->81FE
$Comp
L 74xx:74LS138 U7
U 1 1 5F13E3C4
P 8500 1600
F 0 "U7" H 8500 2381 50  0000 C CNN
F 1 "74ALS138" H 8500 2290 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-16_3.9x9.9mm_Pitch1.27mm" H 8500 1600 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS138" H 8500 1600 50  0001 C CNN
	1    8500 1600
	1    0    0    -1  
$EndComp
Text GLabel 8000 1500 0    50   Input ~ 0
LA0
$Comp
L 74xx:74LS688 U2
U 1 1 5F141EB9
P 8500 3700
F 0 "U2" H 9044 3746 50  0000 L CNN
F 1 "74ALS688" H 9044 3655 50  0000 L CNN
F 2 "Gemini:SO-20_5.3x12.6mm_P1.27mm" H 8500 3700 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS688" H 8500 3700 50  0001 C CNN
	1    8500 3700
	1    0    0    -1  
$EndComp
Text GLabel 8000 2900 0    50   Input ~ 0
LA0
Text GLabel 8000 3000 0    50   Input ~ 0
LA1
Text GLabel 8000 3100 0    50   Input ~ 0
LA2
Text GLabel 8000 3200 0    50   Input ~ 0
LA3
Text GLabel 8000 3300 0    50   Input ~ 0
LA4
Text GLabel 8000 3400 0    50   Input ~ 0
LA5
Text GLabel 8000 3500 0    50   Input ~ 0
LA6
Wire Wire Line
	7550 2000 7550 2800
Wire Wire Line
	7550 2000 8000 2000
Text GLabel 8500 2500 2    50   Input ~ 0
+5V
Text GLabel 8500 4900 2    50   Input ~ 0
GND
Text GLabel 6050 2300 2    50   Input ~ 0
GND
Text GLabel 8500 2300 2    50   Input ~ 0
GND
Text GLabel 8500 1000 2    50   Input ~ 0
+5V
Text GLabel 6050 1000 2    50   Input ~ 0
+5V
Text GLabel 8000 1400 0    50   Input ~ 0
LA1
Text GLabel 8000 1300 0    50   Input ~ 0
ZERO
Text GLabel 8000 1900 0    50   Input ~ 0
HOLDA
Text GLabel 8000 1800 0    50   Input ~ 0
ONE
NoConn ~ 9000 1400
NoConn ~ 9000 1500
NoConn ~ 9000 1600
NoConn ~ 9000 1800
NoConn ~ 9000 1900
NoConn ~ 9000 2000
Text Label 6550 3300 0    50   ~ 0
IO*
Text GLabel 8000 4400 0    50   Input ~ 0
ZERO
Text GLabel 8000 4300 0    50   Input ~ 0
ZERO
Text GLabel 8000 4200 0    50   Input ~ 0
ZERO
Text GLabel 8000 4100 0    50   Input ~ 0
ZERO
Text GLabel 8000 4000 0    50   Input ~ 0
ZERO
Text GLabel 8000 3900 0    50   Input ~ 0
ZERO
Text GLabel 8000 3800 0    50   Input ~ 0
ZERO
Text GLabel 8000 3700 0    50   Input ~ 0
ONE
Text GLabel 6800 3700 2    50   Input ~ 0
HOLDA*
Text Notes 10350 2250 2    50   ~ 0
How many loads can a '138 drive?\nDo these signals need a bus driver?
Text Notes -1150 3950 0    50   ~ 0
In HOLD state, these CPU\npins are tri-stated:\nMEM*\nR_W*\nBST1-3\nWE*_IOCLK*\nRD*\nAD0-AD15\n\nat next cycle, ALATCH is Hi-Z \nany Hi-Z would make HOLDA* undefined
Text Notes 9250 900  2    50   ~ 0
To simplify cards, CRU enabled\n0000-3FFE\n8000-BFFE
Text GLabel 5550 2000 0    50   Input ~ 0
HOLDA
Text Notes 5250 2050 2    50   ~ 0
is there a race here?
$Comp
L 74xx:74LS74 U10
U 1 1 5F3052E7
P 2100 5300
F 0 "U10" H 2100 5781 50  0000 C CNN
F 1 "74ALS74" H 2100 5690 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 2100 5300 50  0001 C CNN
F 3 "74xx/74hc_hct74.pdf" H 2100 5300 50  0001 C CNN
	1    2100 5300
	1    0    0    -1  
$EndComp
Text GLabel 900  5000 0    50   Input ~ 0
HOLDA*
Text GLabel 4000 5750 2    50   Input ~ 0
HOLDA
Text Notes 3200 4700 2    50   ~ 0
Latch HOLDA* before bus goes Hi-Z
Text GLabel 900  5300 0    50   Input ~ 0
HOLD*
Text Notes 1000 5950 0    50   ~ 0
Set on falling HOLDA* (pull-up)\nReset on rising HOLD*\nOutput is B_HOLDA*\nInitialize to zero on rising RESET*\n
Text Notes 4800 5800 2    50   ~ 0
on-board
Text Notes 4800 5600 2    50   ~ 0
off-board
Text GLabel 900  5600 0    50   Input ~ 0
RESET*
Text GLabel 900  5200 0    50   Input ~ 0
ZERO
Wire Wire Line
	900  5300 1800 5300
Wire Wire Line
	1800 5200 900  5200
Text Notes 1750 4700 2    79   ~ 0
HOLD circuit
Text Notes 5600 950  2    79   ~ 0
Bus State Decoder
Text GLabel 8000 4600 0    50   Input ~ 0
HOLDA
Wire Wire Line
	900  5000 2100 5000
$Comp
L 74xx:74LS74 U10
U 2 1 5EC36336
P 3050 7050
F 0 "U10" H 3050 7531 50  0000 C CNN
F 1 "74ALS74" H 3050 7440 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 3050 7050 50  0001 C CNN
F 3 "74xx/74hc_hct74.pdf" H 3050 7050 50  0001 C CNN
	2    3050 7050
	1    0    0    -1  
$EndComp
Text Notes 650  7650 0    50   ~ 0
Until RD*, the data buffer \npasses AD0-15, for latching \noff-board.\n\nInitialize to 0 on rising ALATCH\nSet on falling RD*
Text Notes 1050 6500 0    79   ~ 0
Delayed R/W* Circuit
Text GLabel 1850 7050 0    50   Input ~ 0
ALATCH
Wire Wire Line
	2750 7050 1850 7050
Text GLabel 1850 6950 0    50   Input ~ 0
ZERO
Wire Wire Line
	1850 6950 2750 6950
Text GLabel 1850 6750 0    50   Input ~ 0
RD*
Wire Wire Line
	1850 6750 3050 6750
Text GLabel 3500 6950 2    50   Input ~ 0
RDBENA
Wire Wire Line
	3350 6950 3500 6950
Text Notes 4300 7000 2    50   ~ 0
on-board
Text GLabel 1850 7400 0    50   Input ~ 0
ONE
Wire Wire Line
	1850 7400 3050 7400
Wire Wire Line
	3050 7400 3050 7350
$Comp
L 74xx:74LS74 U10
U 3 1 5EC4F9C0
P 4850 7050
F 0 "U10" H 4550 7500 50  0000 C CNN
F 1 "74ALS74" H 4600 7400 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 4850 7050 50  0001 C CNN
F 3 "74xx/74hc_hct74.pdf" H 4850 7050 50  0001 C CNN
	3    4850 7050
	1    0    0    -1  
$EndComp
Text GLabel 4850 7450 2    50   Input ~ 0
GND
Text GLabel 4850 6650 2    50   Input ~ 0
+5V
Text GLabel 3500 7150 2    50   Input ~ 0
RDBENA*
Wire Wire Line
	3350 7150 3500 7150
Text Notes 4350 7200 2    50   ~ 0
off-board
Wire Wire Line
	8000 2800 7550 2800
Wire Wire Line
	900  5600 2100 5600
Text GLabel 4000 5350 2    50   Input ~ 0
ONE
Text GLabel 4000 5250 2    50   Input ~ 0
ZERO
Text Notes 2500 5000 0    50   ~ 0
DMA enabled/disabled
NoConn ~ 6550 1500
Text Notes 4100 3900 0    50   ~ 0
       BST\nMEM* 1 2 3\n 0    0 0 0  SOPL \n 0    0 0 1  SOP  \n 0    0 1 0  IOP  \n 0    0 1 1  IAQ* \n 0    1 0 0  DOP  \n 0    1 0 1  INTA \n 0    1 1 0  WS   \n 0    1 1 1  GM   \n 1    0 0 0  AUMSL\n 1    0 0 1  AUMS \n 1    0 1 0  RESET\n 1    0 1 1  IO   \n 1    1 0 0  WP   \n 1    1 0 1  ST   \n 1    1 1 0  MID  \n 1    1 1 1  HOLDA
Text Notes 3450 6900 0    50   ~ 0
CPU reads the bus
Text Notes 3450 7300 0    50   ~ 0
To DIR pin of '245
Text Label 2400 5400 0    50   ~ 0
JP2_HOLDA*
Wire Wire Line
	2400 5400 2850 5400
Text Label 2400 5200 0    50   ~ 0
JP1_HOLDA
Text Notes 2550 6500 0    50   ~ 0
Allows external cards to use AD0-AD15,\ninstead of separate A and D buses.
$Comp
L 74xx:74LS138 U11
U 1 1 5EF48D0D
P 6050 3300
F 0 "U11" H 6050 4081 50  0000 C CNN
F 1 "74ALS138" H 6050 3990 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-16_3.9x9.9mm_Pitch1.27mm" H 6050 3300 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS138" H 6050 3300 50  0001 C CNN
	1    6050 3300
	1    0    0    -1  
$EndComp
Text GLabel 5550 3200 0    50   Input ~ 0
BST1
Text GLabel 5550 3100 0    50   Input ~ 0
BST2
Text GLabel 5550 3000 0    50   Input ~ 0
BST3
Text GLabel 5550 3600 0    50   Input ~ 0
ALATCH
Text GLabel 5550 3700 0    50   Input ~ 0
HOLDA
$Comp
L dk_Logic-Gates-and-Inverters:SN74LS04N U12
U 1 1 5EF617F5
P 4250 1800
F 0 "U12" H 4250 2603 60  0000 C CNN
F 1 "74LS04" H 4250 2497 60  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 4450 2000 60  0001 L CNN
F 3 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fsn74ls04" H 4450 2100 60  0001 L CNN
F 4 "296-1629-5-ND" H 4450 2200 60  0001 L CNN "Digi-Key_PN"
F 5 "SN74LS04N" H 4450 2300 60  0001 L CNN "MPN"
F 6 "Integrated Circuits (ICs)" H 4450 2400 60  0001 L CNN "Category"
F 7 "Logic - Gates and Inverters" H 4450 2500 60  0001 L CNN "Family"
F 8 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fsn74ls04" H 4450 2600 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/texas-instruments/SN74LS04N/296-1629-5-ND/277275" H 4450 2700 60  0001 L CNN "DK_Detail_Page"
F 10 "IC INVERTER 6CH 6-INP 14DIP" H 4450 2800 60  0001 L CNN "Description"
F 11 "Texas Instruments" H 4450 2900 60  0001 L CNN "Manufacturer"
F 12 "Active" H 4450 3000 60  0001 L CNN "Status"
	1    4250 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 1800 5550 1800
Text Label 5350 1800 0    50   ~ 0
!MEM*
Text GLabel 3700 1800 0    50   Input ~ 0
MEM*
Wire Wire Line
	3700 1800 3950 1800
Text Label 6550 1300 0    50   ~ 0
SOPL
Text Label 6550 1400 0    50   ~ 0
SOP
Text Label 6550 1500 0    50   ~ 0
IOP
Text Label 6550 1700 0    50   ~ 0
DOP
Wire Wire Line
	6550 1600 6800 1600
Wire Wire Line
	6550 1800 6800 1800
Text Label 6550 1900 0    50   ~ 0
WS
Text Label 6550 2000 0    50   ~ 0
GM
Text Label 6550 3000 0    50   ~ 0
AUMSL
Text Label 6550 3100 0    50   ~ 0
AUMS
Text GLabel 6800 3200 2    50   Input ~ 0
RESOUT*
Wire Wire Line
	6550 3200 6800 3200
Text Label 6550 3400 0    50   ~ 0
WP
Text Label 6550 3500 0    50   ~ 0
ST
Text Label 6550 3600 0    50   ~ 0
MID
Wire Wire Line
	6800 3700 6550 3700
Wire Wire Line
	7550 3300 7550 2800
Wire Wire Line
	6550 3300 7550 3300
Connection ~ 7550 2800
Text GLabel 6050 2700 2    50   Input ~ 0
+5V
Text GLabel 6050 4000 2    50   Input ~ 0
GND
Text GLabel 4250 2300 2    50   Input ~ 0
GND
Text GLabel 4250 1200 2    50   Input ~ 0
+5V
NoConn ~ 4550 2000
NoConn ~ 4550 1900
NoConn ~ 4550 1700
NoConn ~ 4550 1600
NoConn ~ 4550 1500
Text GLabel 3700 1500 0    50   Input ~ 0
ZERO
Text GLabel 3700 1600 0    50   Input ~ 0
ZERO
Text GLabel 3700 1700 0    50   Input ~ 0
ZERO
Text GLabel 3700 1900 0    50   Input ~ 0
ZERO
Text GLabel 3700 2000 0    50   Input ~ 0
ZERO
Wire Wire Line
	3700 1500 3950 1500
Wire Wire Line
	3950 1600 3700 1600
Wire Wire Line
	3700 1700 3950 1700
Wire Wire Line
	3950 1900 3700 1900
Wire Wire Line
	3700 2000 3950 2000
$Comp
L Switch:SW_DIP_x02 SW3
U 1 1 5F019C01
P 3150 5350
F 0 "SW3" H 3150 5717 50  0000 C CNN
F 1 "SW_DIP_x02" H 3150 5626 50  0000 C CNN
F 2 "Buttons_Switches_ThroughHole:SW_DIP_x2_W7.62mm_Slide" H 3150 5350 50  0001 C CNN
F 3 "~" H 3150 5350 50  0001 C CNN
	1    3150 5350
	1    0    0    -1  
$EndComp
Wire Wire Line
	2850 5400 2850 5350
Wire Wire Line
	2850 5200 2850 5250
Wire Wire Line
	2400 5200 2850 5200
$Comp
L Device:R R?
U 1 1 5F0222C1
P 3750 5250
AR Path="/5F0222C1" Ref="R?"  Part="1" 
AR Path="/5F13AC65/5F0222C1" Ref="R6"  Part="1" 
F 0 "R6" V 3750 5100 50  0000 C CNN
F 1 "1K" V 3750 5250 50  0000 C CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 3680 5250 50  0001 C CNN
F 3 "~" H 3750 5250 50  0001 C CNN
	1    3750 5250
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R?
U 1 1 5F022AF8
P 3750 5350
AR Path="/5F022AF8" Ref="R?"  Part="1" 
AR Path="/5F13AC65/5F022AF8" Ref="R7"  Part="1" 
F 0 "R7" V 3750 5200 50  0000 C CNN
F 1 "1K" V 3750 5350 50  0000 C CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 3680 5350 50  0001 C CNN
F 3 "~" H 3750 5350 50  0001 C CNN
	1    3750 5350
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3900 5250 4000 5250
Wire Wire Line
	3900 5350 4000 5350
Wire Wire Line
	3450 5250 3500 5250
Wire Wire Line
	3600 5350 3550 5350
Text GLabel 4000 5550 2    50   Input ~ 0
B_HOLDA*
Wire Wire Line
	3550 5350 3550 5550
Connection ~ 3550 5350
Wire Wire Line
	3550 5550 4000 5550
Wire Wire Line
	3500 5750 4000 5750
Wire Wire Line
	3500 5250 3500 5350
Connection ~ 3500 5250
Wire Wire Line
	3500 5250 3600 5250
Wire Wire Line
	3500 5350 3500 5750
Wire Wire Line
	3450 5350 3550 5350
NoConn ~ 6550 2000
NoConn ~ 6550 1700
NoConn ~ 6550 3000
NoConn ~ 6550 3100
NoConn ~ 6550 3400
NoConn ~ 6550 3500
NoConn ~ 6550 3600
$EndSCHEMATC
