EESchema Schematic File Version 4
LIBS:Steven-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 4850 2200 0    50   Input ~ 0
+5V
Text GLabel 6000 4200 0    50   Input ~ 0
INTREQ*
Text GLabel 4850 5300 0    50   Input ~ 0
GND
Text GLabel 6000 4100 0    50   Input ~ 0
NMI*
Text GLabel 4850 2900 0    50   Input ~ 0
B_D2
Text GLabel 4850 3100 0    50   Input ~ 0
B_D4
Text GLabel 4850 3300 0    50   Input ~ 0
B_D6
Text GLabel 4850 2700 0    50   Input ~ 0
B_D0_OUT
Text GLabel 3700 4900 0    50   Input ~ 0
B_ALATCH
Text GLabel 6000 5300 0    50   Input ~ 0
GND
Text GLabel 6000 2200 0    50   Input ~ 0
+5V
Text GLabel 6000 5200 0    50   Input ~ 0
RESET*
Text GLabel 3700 4600 0    50   Input ~ 0
B_BST2
Text GLabel 4850 3400 0    50   Input ~ 0
B_D7
Text GLabel 4850 3200 0    50   Input ~ 0
B_D5
Text GLabel 4850 3000 0    50   Input ~ 0
B_D3
Text GLabel 4850 2800 0    50   Input ~ 0
B_D1
Text GLabel 6000 3900 0    50   Input ~ 0
LA4
Text GLabel 3700 3000 0    50   Input ~ 0
LA8
Text GLabel 6000 2900 0    50   Input ~ 0
LA6
Text GLabel 3700 3900 0    50   Input ~ 0
LA0
Text GLabel 6000 4800 0    50   Input ~ 0
LA2
Text GLabel 3700 2800 0    50   Input ~ 0
LA10
Text GLabel 3700 2700 0    50   Input ~ 0
LA12
Text GLabel 3700 5000 0    50   Input ~ 0
LA1
Text GLabel 6000 3800 0    50   Input ~ 0
LA3
Text GLabel 6000 3000 0    50   Input ~ 0
LA7
Text GLabel 3700 5100 0    50   Input ~ 0
LA5
Text GLabel 3700 2900 0    50   Input ~ 0
LA9
Text GLabel 6000 2700 0    50   Input ~ 0
LA11
Text GLabel 6000 2800 0    50   Input ~ 0
LA13
Text GLabel 6000 4900 0    50   Input ~ 0
B_PSEL*
NoConn ~ 6000 3700
NoConn ~ 6000 3200
NoConn ~ 3700 3800
NoConn ~ 3700 3300
Text GLabel 3700 5300 0    50   Input ~ 0
GND
Text GLabel 3700 2200 0    50   Input ~ 0
+5V
Text GLabel 6000 5000 0    50   Input ~ 0
CLKOUT
Text GLabel 3700 3100 0    50   Input ~ 0
READY
Text GLabel 3700 4700 0    50   Input ~ 0
B_BST3
Text GLabel 3700 4400 0    50   Input ~ 0
B_BST1
Text GLabel 6000 4300 0    50   Input ~ 0
B_WE*_IOCLK*
Text GLabel 6000 4500 0    50   Input ~ 0
B_RD*
$Comp
L Gemini:DIN-41612_Kontron_Card J?
U 3 1 5F02AAE1
P 6000 5300
AR Path="/5F02AAE1" Ref="J?"  Part="3" 
AR Path="/5F025AB5/5F02AAE1" Ref="J2"  Part="3" 
F 0 "J2" V 5347 3750 50  0000 C CNN
F 1 "DIN-41612_Kontron_Card" V 5256 3750 50  0000 C CNN
F 2 "Connector_DIN:DIN41612_C_3x32_Male_Horizontal_THT" H 6400 2100 50  0001 L CNN
F 3 "https://www.arrow.com/en/products/86092645113755elf/amphenol-fci" H 5150 2200 50  0001 L CNN
	3    6000 5300
	-1   0    0    1   
$EndComp
$Comp
L Gemini:DIN-41612_Kontron_Card J?
U 1 1 5F02AAE7
P 3700 5300
AR Path="/5F02AAE7" Ref="J?"  Part="1" 
AR Path="/5F025AB5/5F02AAE7" Ref="J2"  Part="1" 
F 0 "J2" V 3047 3750 50  0000 C CNN
F 1 "DIN-41612_Kontron_Card" V 2956 3750 50  0000 C CNN
F 2 "Connector_DIN:DIN41612_C_3x32_Male_Horizontal_THT" H 4100 2100 50  0001 L CNN
F 3 "https://www.arrow.com/en/products/86092645113755elf/amphenol-fci" H 2850 2200 50  0001 L CNN
	1    3700 5300
	-1   0    0    1   
$EndComp
$Comp
L Gemini:DIN-41612_Kontron_Card J?
U 2 1 5F02AAED
P 4850 5300
AR Path="/5F02AAED" Ref="J?"  Part="2" 
AR Path="/5F025AB5/5F02AAED" Ref="J2"  Part="2" 
F 0 "J2" V 4227 3750 50  0000 C CNN
F 1 "DIN-41612_Kontron_Card" V 4136 3750 50  0000 C CNN
F 2 "Connector_DIN:DIN41612_C_3x32_Male_Horizontal_THT" H 5250 2100 50  0001 L CNN
F 3 "https://www.arrow.com/en/products/86092645113755elf/amphenol-fci" H 4000 2200 50  0001 L CNN
	2    4850 5300
	-1   0    0    1   
$EndComp
Text GLabel 3700 2400 0    50   Input ~ 0
B_D14
Text GLabel 3700 2300 0    50   Input ~ 0
B_D13
Text GLabel 3700 2600 0    50   Input ~ 0
B_D12
Text GLabel 3700 2500 0    50   Input ~ 0
B_D11
Text GLabel 6000 2500 0    50   Input ~ 0
B_D10
Text GLabel 6000 3500 0    50   Input ~ 0
B_D9
Text GLabel 6000 2300 0    50   Input ~ 0
B_D8
Text GLabel 6000 2400 0    50   Input ~ 0
B_D15_IN
Text Notes 600  4500 0    50   ~ 0
In 8-bit  CRU access we\nwant the byte on D0-D7\nnot D8-D15\nDefine D0-D7 as the even word.\nThough D7 is still most significant.
Text GLabel 6000 5100 0    50   UnSpc ~ 0
B_MEM*
Text Notes 600  5150 0    50   ~ 0
Address word is rotated 1 right.\nPSEL* is moved to \nA15 MSBit of address\n(not symmetric with data bus)
Text GLabel 3700 3200 0    50   Input ~ 0
HOLD*
Text GLabel 6000 3100 0    50   Input ~ 0
FA11
Text GLabel 6000 3300 0    50   Input ~ 0
FA12
Text GLabel 6000 3400 0    50   Input ~ 0
FA13
Text GLabel 6000 4400 0    50   UnSpc ~ 0
FA15
Text GLabel 6000 4000 0    50   Input ~ 0
FA16
Text GLabel 3700 3500 0    50   Input ~ 0
FA14
Text GLabel 3700 4200 0    50   Input ~ 0
FA17
Text GLabel 3700 4300 0    50   Input ~ 0
FA18
Text Notes 600  5650 0    50   ~ 0
BMO   Bus Master Override\nPARENA Parallel CRU Enable (except /IORQ)\nSERENA  Serial CRU Enable
Text GLabel 3700 4800 0    50   Input ~ 0
IORQ*
Text GLabel 4850 4400 0    50   Input ~ 0
BMO
Text GLabel 4850 4600 0    50   Input ~ 0
INTA*
Text GLabel 4850 2300 0    50   Input ~ 0
FA19
Text GLabel 4850 2400 0    50   Input ~ 0
FA20
Text GLabel 4850 2500 0    50   Input ~ 0
FA21
Text GLabel 4850 2600 0    50   Input ~ 0
FA22
Text GLabel 4850 5100 0    50   Input ~ 0
FA23
Text GLabel 4850 5200 0    50   Input ~ 0
PRO
Text GLabel 3700 3700 0    50   Input ~ 0
PBPTHP
Text GLabel 6000 3600 0    50   Input ~ 0
SPAMM
Text GLabel 4850 4200 0    50   Input ~ 0
IC0
Text GLabel 4850 4100 0    50   Input ~ 0
IC1
Text GLabel 4850 4000 0    50   Input ~ 0
IC2
Text GLabel 4850 3900 0    50   Input ~ 0
IC3
Text GLabel 4850 3800 0    50   Input ~ 0
INT1
Text GLabel 4850 3700 0    50   Input ~ 0
INT2
Text GLabel 4850 3600 0    50   Input ~ 0
INT3
Text GLabel 4850 3500 0    50   Input ~ 0
INT4
Text GLabel 3700 4100 0    50   Input ~ 0
IAQ*
Text GLabel 3700 5200 0    50   Input ~ 0
B_HOLDA*
Text GLabel 4850 4900 0    50   Input ~ 0
SERENA*
Text GLabel 4850 5000 0    50   Input ~ 0
PARENA*
Text Notes 650  3900 0    50   ~ 0
CPU outputs L_PSEL,  LA0 .. LA14 onto \nA15,A14...A0 where A0 is LSbit (Motorola)\n\nMemory mapper outputs 16-bit page\nregister on A16-A31.\n\nBank.................... Word Address\nA14 A13 A12 A11 A10..A0\n8 bit Page. ..4K\nA23 .. A16  A15\nExtended Address\n\nA0  \\n...  } Word Address\nA10 /\n\nA11  \\nA12  4} Bank Reg#\nA13   }\nA14  /\nA15  } PSEL* or Map Enable\n\nEffective Address\n\nA16  FA11 4K half-page\n\nA17  FA12 \\n...       8} 8K page#\nA24  FA19 /\n\nA25  \\nA26   } 32MB Expansion\nA27   }\nA28  /\n     \ .. 3 mode bits ..\nA29  } PRO      Page Read-Only\nA30  } PBPTHP  P-Box Pass-Thru Page\nA31  } SPAMM   Some ports are memory-mapped
NoConn ~ 3700 3400
NoConn ~ 3700 4000
NoConn ~ 3700 4500
NoConn ~ 4850 4800
NoConn ~ 4850 4700
NoConn ~ 4850 4500
NoConn ~ 4850 4300
Text GLabel 6000 4600 0    50   Input ~ 0
B_RDBENA*
Text Notes 600  5950 0    50   ~ 0
HALT is unused right?
Text GLabel 4850 6450 0    50   Input ~ 0
FA11
Text GLabel 4850 6550 0    50   Input ~ 0
FA12
Text GLabel 4850 6650 0    50   Input ~ 0
FA13
Text GLabel 5700 6400 0    50   Input ~ 0
SPAMM
Text GLabel 4850 6850 0    50   Input ~ 0
FA15
Text GLabel 5700 6500 0    50   Input ~ 0
PRO
Text GLabel 4850 7650 0    50   Input ~ 0
FA23
Text GLabel 4850 7250 0    50   Input ~ 0
FA19
Text GLabel 4850 7350 0    50   Input ~ 0
FA20
Text GLabel 4850 7450 0    50   Input ~ 0
FA21
Text GLabel 4850 7550 0    50   Input ~ 0
FA22
Text GLabel 4850 6750 0    50   Input ~ 0
FA14
Text GLabel 5700 6600 0    50   Input ~ 0
PBPTHP
Text GLabel 4850 7050 0    50   Input ~ 0
FA17
Text GLabel 4850 7150 0    50   Input ~ 0
FA18
Text GLabel 4850 6950 0    50   Input ~ 0
FA16
NoConn ~ 4850 6450
NoConn ~ 4850 6550
NoConn ~ 4850 6650
NoConn ~ 4850 6750
NoConn ~ 4850 6850
NoConn ~ 4850 6950
NoConn ~ 4850 7050
NoConn ~ 4850 7150
NoConn ~ 4850 7250
NoConn ~ 4850 7350
NoConn ~ 4850 7450
NoConn ~ 4850 7550
NoConn ~ 4850 7650
NoConn ~ 5700 6400
NoConn ~ 5700 6500
NoConn ~ 5700 6600
Text GLabel 5700 7050 0    50   Input ~ 0
INT1
Text GLabel 5700 6950 0    50   Input ~ 0
INT2
Text GLabel 5700 6850 0    50   Input ~ 0
INT3
Text GLabel 5700 6750 0    50   Input ~ 0
INT4
NoConn ~ 5700 6750
NoConn ~ 5700 6850
NoConn ~ 5700 6950
NoConn ~ 5700 7050
Text GLabel 5700 6300 0    50   Input ~ 0
BMO
NoConn ~ 5700 6300
NoConn ~ 3700 3600
Text Notes 5200 6200 2    50   ~ 0
Unused by BIOS:
Text GLabel 6000 4700 0    50   Input ~ 0
RESOUT*
Text GLabel 6000 2600 0    50   Input ~ 0
LA14
Text Notes 6550 2450 0    50   ~ 0
MSBit even word
Text Notes 6550 2350 0    50   ~ 0
LSBit even word
Text Notes 3500 5700 0    50   ~ 0
Address bits begin with LA14 on A0 because the 99105 has only word addresses. \nThis makes it possible to address 8-bit devices at even addresses, especially I/O \ndevices during CRU cycles.
Text Notes 4300 2750 0    50   ~ 0
LSB
$EndSCHEMATC
