EESchema Schematic File Version 4
LIBS:Steven-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R R2
U 1 1 5EC40C59
P 10250 1950
F 0 "R2" V 10250 1800 50  0000 C CNN
F 1 "10K" V 10250 1950 50  0000 C CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 10180 1950 50  0001 C CNN
F 3 "~" H 10250 1950 50  0001 C CNN
	1    10250 1950
	0    -1   -1   0   
$EndComp
Text GLabel 10500 1950 2    50   Input ~ 0
HOLD*
Text GLabel 10500 2050 2    50   Input ~ 0
READY
Wire Wire Line
	10400 1950 10500 1950
Wire Wire Line
	10500 2050 10400 2050
Text Notes 9550 6950 2    100  ~ 0
TMS99105 CPU Card
Text GLabel 2500 4650 0    50   Input ~ 0
HOLDA
Text Notes 9350 6500 0    50   ~ 0
Timing   t_w  t_su t_h t_pd   at LE\nF573     6     2    3\nLS573   n/a\nALS573  10    10    7  \nHCT573  25   13   5   52  SOIC $1  , SOP  $1\n\nLS645    -   -   -   15\nALS645A  -   -  -    10   SOIC $1.70  SO $2\n\nLVC245  -   -   -   10\nALS74A  5   5   0  -  SOIC,SO $0.75\nPackage codes\nSOIC=DW 12.9x7.6/10.63\nSOP=NS  12.9x5.6/8.2
Text Notes 4650 1600 0    50   ~ 0
CPU: D and B\nBUS: Q and A\nDIR = R/W*\nL: B to A   CPU write\nH: A to B   CPU read
$Comp
L Gemini:74ALS645A U6
U 1 1 5EBF3089
P 3000 4150
F 0 "U6" H 2700 5000 50  0000 C CNN
F 1 "74ALS645" H 2700 4900 50  0000 C CNN
F 2 "Gemini:SO-20_5.3x12.6mm_P1.27mm" H 3000 4150 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS245" H 3000 4150 50  0001 C CNN
	1    3000 4150
	1    0    0    -1  
$EndComp
$Comp
L Gemini:74ALS645A U4
U 1 1 5EBF47AC
P 3000 6250
F 0 "U4" H 2700 7050 50  0000 C CNN
F 1 "74ALS645" H 2700 6950 50  0000 C CNN
F 2 "Gemini:SO-20_5.3x12.6mm_P1.27mm" H 3000 6250 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS245" H 3000 6250 50  0001 C CNN
	1    3000 6250
	1    0    0    -1  
$EndComp
Wire Wire Line
	8250 3600 8350 3600
Connection ~ 8250 3600
Text GLabel 7350 2500 0    50   Input ~ 0
R_W*
Wire Wire Line
	8150 3600 8250 3600
Text GLabel 9050 2200 2    50   Input ~ 0
AD14
Text GLabel 9050 2300 2    50   Input ~ 0
AD13
Text GLabel 9050 2400 2    50   Input ~ 0
AD12
Text GLabel 9050 2500 2    50   Input ~ 0
AD11
Text GLabel 9050 2600 2    50   Input ~ 0
AD10
Text GLabel 9050 2700 2    50   Input ~ 0
AD9
Text GLabel 9050 2800 2    50   Input ~ 0
AD8
Text GLabel 9050 2900 2    50   Input ~ 0
AD7
Text GLabel 9050 3000 2    50   Input ~ 0
AD6
Text GLabel 9050 3100 2    50   Input ~ 0
AD5
Text GLabel 7350 3000 0    50   Input ~ 0
AD3
Text GLabel 7350 2900 0    50   Input ~ 0
AD2
Text GLabel 7350 2800 0    50   Input ~ 0
AD1
Text GLabel 7350 3100 0    50   Input ~ 0
AD4
Text GLabel 7350 2700 0    50   Input ~ 0
AD0_IN
Text GLabel 9050 2100 2    50   Input ~ 0
PSEL*_D15_OUT
Text GLabel 8350 3600 2    50   Input ~ 0
GND
Text GLabel 9050 1700 2    50   Input ~ 0
XTAL2
Text GLabel 9050 1600 2    50   Input ~ 0
XTAL1
Text GLabel 9050 1500 2    50   Input ~ 0
BST3
Text GLabel 9050 1400 2    50   Input ~ 0
BST2
Text GLabel 9050 1300 2    50   Input ~ 0
BST1
Text GLabel 8250 800  2    50   Input ~ 0
+5V
Text GLabel 9050 1200 2    50   Input ~ 0
MEM*
Text GLabel 7350 1300 0    50   Input ~ 0
RD*
Text GLabel 7350 2000 0    50   Input ~ 0
NMI*
Text GLabel 7350 1800 0    50   Input ~ 0
READY
Text GLabel 7350 1600 0    50   Input ~ 0
HOLD*
Text GLabel 9050 1800 2    50   Input ~ 0
CLKOUT
Text GLabel 9050 2000 2    50   Input ~ 0
ALATCH
Text GLabel 7350 1400 0    50   Input ~ 0
RESET*
Text GLabel 7350 1200 0    50   Input ~ 0
WE*_IOCLK*
Text GLabel 7350 1900 0    50   Input ~ 0
INTREQ*
Text GLabel 7350 2400 0    50   Input ~ 0
IC3
Text GLabel 7350 2300 0    50   Input ~ 0
IC2
Text GLabel 7350 2200 0    50   Input ~ 0
IC1
Text GLabel 7350 2100 0    50   Input ~ 0
IC0
$Comp
L Gemini:TMS99105 U?
U 1 1 5EC3F39B
P 8200 2200
AR Path="/5EC3CEE8/5EC3F39B" Ref="U?"  Part="1" 
AR Path="/5EC3F39B" Ref="U1"  Part="1" 
F 0 "U1" H 8200 3781 50  0000 C CNN
F 1 "TMS99105" H 8200 3690 50  0000 C CNN
F 2 "Housings_DIP:DIP-40_W15.24mm_Socket_LongPads" H 8200 2200 50  0001 C CNN
F 3 "" H 8200 2200 50  0001 C CNN
	1    8200 2200
	1    0    0    -1  
$EndComp
$Comp
L Gemini:74ALS573C U5
U 1 1 5EB70E69
P 5500 4150
F 0 "U5" H 5200 4950 50  0000 C CNN
F 1 "74ALS573" H 5200 4850 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-20W_7.5x12.8mm_Pitch1.27mm" H 5500 4150 50  0001 C CNN
F 3 "74xx/74hc573.pdf" H 5500 4150 50  0001 C CNN
	1    5500 4150
	1    0    0    -1  
$EndComp
Text GLabel 6000 5750 2    50   UnSpc ~ 0
LA0
Text GLabel 6000 5850 2    50   UnSpc ~ 0
LA1
Text GLabel 6000 5950 2    50   UnSpc ~ 0
LA2
Text GLabel 6000 6050 2    50   UnSpc ~ 0
LA3
Text GLabel 6000 6150 2    50   UnSpc ~ 0
LA4
Text GLabel 6000 6250 2    50   UnSpc ~ 0
LA5
Text GLabel 6000 6350 2    50   UnSpc ~ 0
LA6
Text GLabel 6000 6450 2    50   UnSpc ~ 0
LA7
Text GLabel 6000 3650 2    50   UnSpc ~ 0
LA8
Text GLabel 6000 3750 2    50   UnSpc ~ 0
LA9
Text GLabel 6000 3850 2    50   UnSpc ~ 0
LA10
Text GLabel 6000 3950 2    50   UnSpc ~ 0
LA11
Text GLabel 6000 4050 2    50   UnSpc ~ 0
LA12
Text GLabel 6000 4150 2    50   UnSpc ~ 0
LA13
Text GLabel 6000 4250 2    50   UnSpc ~ 0
LA14
Text GLabel 6000 4350 2    50   UnSpc ~ 0
B_PSEL*
$Comp
L Gemini:74ALS573C U3
U 1 1 5EB71CCE
P 5500 6250
F 0 "U3" H 5150 7050 50  0000 L CNN
F 1 "74ALS573" H 5000 6950 50  0000 L CNN
F 2 "Housings_SOIC:SOIC-20W_7.5x12.8mm_Pitch1.27mm" H 5500 6250 50  0001 C CNN
F 3 "74xx/74hc573.pdf" H 5500 6250 50  0001 C CNN
	1    5500 6250
	1    0    0    -1  
$EndComp
Text GLabel 2500 5850 0    50   Input ~ 0
B_D14
Text GLabel 2500 6050 0    50   Input ~ 0
B_D12
Text GLabel 2500 6250 0    50   Input ~ 0
B_D10
Text GLabel 2500 6450 0    50   Input ~ 0
B_D8
Text GLabel 2500 4350 0    50   Input ~ 0
B_D0_OUT
Text GLabel 2500 6350 0    50   Input ~ 0
B_D9
Text GLabel 2500 6150 0    50   Input ~ 0
B_D11
Text GLabel 2500 5950 0    50   Input ~ 0
B_D13
Text GLabel 2500 5750 0    50   Input ~ 0
B_D15_IN
Text GLabel 5000 6050 0    50   Input ~ 0
AD3
Text GLabel 5000 5950 0    50   Input ~ 0
AD2
Text GLabel 5000 5850 0    50   Input ~ 0
AD1
Text GLabel 5000 6150 0    50   Input ~ 0
AD4
Text GLabel 5000 5750 0    50   Input ~ 0
AD0_IN
Text GLabel 3500 6050 2    50   Input ~ 0
AD3
Text GLabel 3500 5950 2    50   Input ~ 0
AD2
Text GLabel 3500 5850 2    50   Input ~ 0
AD1
Text GLabel 3500 6150 2    50   Input ~ 0
AD4
Text GLabel 3500 5750 2    50   Input ~ 0
AD0_IN
Text GLabel 5000 6650 0    50   Input ~ 0
ALATCH
Text GLabel 3500 4250 2    50   Input ~ 0
AD14
Text GLabel 3500 4150 2    50   Input ~ 0
AD13
Text GLabel 3500 4050 2    50   Input ~ 0
AD12
Text GLabel 3500 3950 2    50   Input ~ 0
AD11
Text GLabel 3500 3850 2    50   Input ~ 0
AD10
Text GLabel 3500 3750 2    50   Input ~ 0
AD9
Text GLabel 3500 3650 2    50   Input ~ 0
AD8
Text GLabel 3500 6450 2    50   Input ~ 0
AD7
Text GLabel 3500 6350 2    50   Input ~ 0
AD6
Text GLabel 3500 6250 2    50   Input ~ 0
AD5
Text GLabel 3500 4350 2    50   Input ~ 0
PSEL*_D15_OUT
Text GLabel 2500 4550 0    50   Input ~ 0
RDBENA
Text GLabel 2500 6750 0    50   Input ~ 0
HOLDA
Text GLabel 5000 4650 0    50   Input ~ 0
HOLDA
Text GLabel 5000 4550 0    50   Input ~ 0
ALATCH
Text GLabel 5000 6450 0    50   Input ~ 0
AD7
Text GLabel 5000 6350 0    50   Input ~ 0
AD6
Text GLabel 5000 6250 0    50   Input ~ 0
AD5
Text GLabel 5000 4250 0    50   Input ~ 0
AD14
Text GLabel 5000 4150 0    50   Input ~ 0
AD13
Text GLabel 5000 4050 0    50   Input ~ 0
AD12
Text GLabel 5000 3950 0    50   Input ~ 0
AD11
Text GLabel 5000 3850 0    50   Input ~ 0
AD10
Text GLabel 5000 3750 0    50   Input ~ 0
AD9
Text GLabel 5000 3650 0    50   Input ~ 0
AD8
$Sheet
S 700  1450 1250 350 
U 5F025AB5
F0 "Kontron" 50
F1 "Kontron.sch" 50
$EndSheet
$Sheet
S 700  700  1250 350 
U 5F13AC65
F0 "BusLogic" 50
F1 "BusLogic.sch" 50
$EndSheet
Text GLabel 3800 2000 2    50   Input ~ 0
RD*
Text GLabel 3800 1700 2    50   Input ~ 0
WE*_IOCLK*
Text GLabel 3800 1900 2    50   Input ~ 0
RDBENA*
Text GLabel 3800 1800 2    50   Input ~ 0
ALATCH
Text GLabel 2800 1800 0    50   Input ~ 0
B_ALATCH
Text GLabel 2800 1300 0    50   Input ~ 0
B_MEM*
Text GLabel 2800 1600 0    50   Input ~ 0
B_BST1
Text GLabel 2800 1500 0    50   Input ~ 0
B_BST2
Text GLabel 2800 1400 0    50   Input ~ 0
B_BST3
Text GLabel 2800 1700 0    50   Input ~ 0
B_WE*_IOCLK*
Text GLabel 2800 2000 0    50   Input ~ 0
B_RD*
Text GLabel 2800 1900 0    50   Input ~ 0
B_RDBENA*
Text GLabel 2800 2300 0    50   Input ~ 0
HOLDA
Text GLabel 5500 3350 2    50   Input ~ 0
+5V
Text GLabel 5500 5450 2    50   Input ~ 0
+5V
Text GLabel 3000 5450 2    50   Input ~ 0
+5V
Text GLabel 3300 1000 2    50   Input ~ 0
+5V
Text GLabel 5500 4950 2    50   Input ~ 0
GND
Text GLabel 3300 2600 2    50   Input ~ 0
GND
Text GLabel 5500 7050 2    50   Input ~ 0
GND
Text GLabel 3000 7050 2    50   Input ~ 0
GND
Text GLabel 3000 4950 2    50   Input ~ 0
GND
Text GLabel 1100 6850 2    50   Input ~ 0
+5V
Text GLabel 1100 7250 2    50   Input ~ 0
GND
$Comp
L power:+5V #PWR0101
U 1 1 5F1CFF47
P 800 6750
F 0 "#PWR0101" H 800 6600 50  0001 C CNN
F 1 "+5V" H 815 6923 50  0000 C CNN
F 2 "" H 800 6750 50  0001 C CNN
F 3 "" H 800 6750 50  0001 C CNN
	1    800  6750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 5F1D0221
P 800 7450
F 0 "#PWR0102" H 800 7200 50  0001 C CNN
F 1 "GND" H 805 7277 50  0000 C CNN
F 2 "" H 800 7450 50  0001 C CNN
F 3 "" H 800 7450 50  0001 C CNN
	1    800  7450
	1    0    0    -1  
$EndComp
Text GLabel 3000 3350 2    50   Input ~ 0
+5V
Text GLabel 10100 2050 0    50   Input ~ 0
+5V
Text GLabel 10100 1950 0    50   Input ~ 0
+5V
$Comp
L Device:Crystal Y1
U 1 1 5EC40C41
P 10200 1250
F 0 "Y1" V 10154 1381 50  0000 L CNN
F 1 "24 MHz" V 10245 1381 50  0000 L CNN
F 2 "Crystals:Crystal_HC49-U_Vertical" H 10200 1250 50  0001 C CNN
F 3 "~" H 10200 1250 50  0001 C CNN
	1    10200 1250
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C23
U 1 1 5EC40C5F
P 10650 1100
F 0 "C23" V 10421 1100 50  0000 C CNN
F 1 "5pF" V 10512 1100 50  0000 C CNN
F 2 "Capacitors_ThroughHole:C_Disc_D3.0mm_W2.0mm_P2.50mm" H 10650 1100 50  0001 C CNN
F 3 "~" H 10650 1100 50  0001 C CNN
	1    10650 1100
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C24
U 1 1 5EC40C65
P 10650 1400
F 0 "C24" V 10421 1400 50  0000 C CNN
F 1 "5pF" V 10512 1400 50  0000 C CNN
F 2 "Capacitors_ThroughHole:C_Disc_D3.0mm_W2.0mm_P2.50mm" H 10650 1400 50  0001 C CNN
F 3 "~" H 10650 1400 50  0001 C CNN
	1    10650 1400
	0    1    1    0   
$EndComp
Wire Wire Line
	10750 1100 10750 1250
Text GLabel 10800 1250 2    50   Input ~ 0
GND
Text GLabel 10050 1100 0    50   Input ~ 0
XTAL1
Text GLabel 10050 1400 0    50   Input ~ 0
XTAL2
Wire Wire Line
	10800 1250 10750 1250
Connection ~ 10750 1250
Wire Wire Line
	10750 1250 10750 1400
Wire Wire Line
	10050 1100 10200 1100
Wire Wire Line
	10050 1400 10200 1400
Connection ~ 10200 1100
Wire Wire Line
	10200 1100 10550 1100
Connection ~ 10200 1400
Wire Wire Line
	10200 1400 10550 1400
Text GLabel 5000 6750 0    50   Input ~ 0
HOLDA
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 5F259D9F
P 800 6850
F 0 "#FLG0101" H 800 6925 50  0001 C CNN
F 1 "PWR_FLAG" H 800 7023 50  0000 C CNN
F 2 "" H 800 6850 50  0001 C CNN
F 3 "~" H 800 6850 50  0001 C CNN
	1    800  6850
	0    -1   -1   0   
$EndComp
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 5F25EB9A
P 800 7250
F 0 "#FLG0102" H 800 7325 50  0001 C CNN
F 1 "PWR_FLAG" H 800 7423 50  0000 C CNN
F 2 "" H 800 7250 50  0001 C CNN
F 3 "~" H 800 7250 50  0001 C CNN
	1    800  7250
	0    -1   -1   0   
$EndComp
Text GLabel 1100 7350 2    50   Input ~ 0
ZERO
Text GLabel 1100 6950 2    50   Input ~ 0
ONE
Wire Wire Line
	1100 7350 1000 7350
Wire Wire Line
	1000 7350 1000 7250
Connection ~ 1000 7250
Wire Wire Line
	1000 7250 1100 7250
Wire Wire Line
	1100 6950 1000 6950
Wire Wire Line
	1000 6950 1000 6850
Connection ~ 1000 6850
Wire Wire Line
	1000 6850 1100 6850
Text Notes 2800 7050 2    50   ~ 0
Latch is disabled\nduring HOLD state
Text Notes 3050 2600 2    50   ~ 0
Driver is disabled\nduring HOLD state
Text Notes 2450 4900 2    50   ~ 0
Transceiver is disabled\nduring HOLD state
Text Notes 10750 6850 2    50   ~ 0
Buffer more outputs\nProtect from inputs
Text Notes 5950 3050 2    79   ~ 0
Address Latches
$Comp
L Gemini:74ALS645A U9
U 1 1 5F29246F
P 3300 1800
F 0 "U9" H 3000 2600 50  0000 C CNN
F 1 "74ALS645A" H 3000 2500 50  0000 C CNN
F 2 "Gemini:SO-20_5.3x12.6mm_P1.27mm" H 3300 1800 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS245" H 3300 1800 50  0001 C CNN
	1    3300 1800
	1    0    0    -1  
$EndComp
Text GLabel 2800 2200 0    50   Input ~ 0
ZERO
Text GLabel 3800 1400 2    50   Input ~ 0
BST3
Text GLabel 3800 1500 2    50   Input ~ 0
BST2
Text GLabel 3800 1600 2    50   Input ~ 0
BST1
Text GLabel 3800 1300 2    50   Input ~ 0
MEM*
$Comp
L Device:C C5
U 1 1 5F386FA9
P 950 3400
F 0 "C5" H 900 3750 50  0000 L CNN
F 1 "0.1uF" H 900 3650 50  0000 L CNN
F 2 "Gemini:C_Disc_D3.8mm_W2.6mm_P4.00mm" H 988 3250 50  0001 C CNN
F 3 "~" H 950 3400 50  0001 C CNN
	1    950  3400
	0    1    1    0   
$EndComp
$Comp
L Device:C C6
U 1 1 5F38B951
P 950 3650
F 0 "C6" H 900 4000 50  0000 L CNN
F 1 "0.1uF" H 900 3900 50  0000 L CNN
F 2 "Gemini:C_Disc_D3.8mm_W2.6mm_P4.00mm" H 988 3500 50  0001 C CNN
F 3 "~" H 950 3650 50  0001 C CNN
	1    950  3650
	0    1    1    0   
$EndComp
$Comp
L Device:C C7
U 1 1 5F38C6A4
P 950 3900
F 0 "C7" H 900 4250 50  0000 L CNN
F 1 "0.1uF" H 900 4150 50  0000 L CNN
F 2 "Gemini:C_Disc_D3.8mm_W2.6mm_P4.00mm" H 988 3750 50  0001 C CNN
F 3 "~" H 950 3900 50  0001 C CNN
	1    950  3900
	0    1    1    0   
$EndComp
$Comp
L Device:C C8
U 1 1 5F38C6AE
P 950 4150
F 0 "C8" H 900 4500 50  0000 L CNN
F 1 "0.1uF" H 900 4400 50  0000 L CNN
F 2 "Gemini:C_Disc_D3.8mm_W2.6mm_P4.00mm" H 988 4000 50  0001 C CNN
F 3 "~" H 950 4150 50  0001 C CNN
	1    950  4150
	0    1    1    0   
$EndComp
$Comp
L Device:C C9
U 1 1 5F38D55E
P 950 4400
F 0 "C9" H 900 4750 50  0000 L CNN
F 1 "0.1uF" H 900 4650 50  0000 L CNN
F 2 "Gemini:C_Disc_D3.8mm_W2.6mm_P4.00mm" H 988 4250 50  0001 C CNN
F 3 "~" H 950 4400 50  0001 C CNN
	1    950  4400
	0    1    1    0   
$EndComp
$Comp
L Device:C C10
U 1 1 5F38D568
P 950 4650
F 0 "C10" H 900 5000 50  0000 L CNN
F 1 "0.1uF" H 900 4900 50  0000 L CNN
F 2 "Gemini:C_Disc_D3.8mm_W2.6mm_P4.00mm" H 988 4500 50  0001 C CNN
F 3 "~" H 950 4650 50  0001 C CNN
	1    950  4650
	0    1    1    0   
$EndComp
$Comp
L Device:C C11
U 1 1 5F38D572
P 950 4900
F 0 "C11" H 900 5250 50  0000 L CNN
F 1 "0.1uF" H 900 5150 50  0000 L CNN
F 2 "Gemini:C_Disc_D3.8mm_W2.6mm_P4.00mm" H 988 4750 50  0001 C CNN
F 3 "~" H 950 4900 50  0001 C CNN
	1    950  4900
	0    1    1    0   
$EndComp
$Comp
L Device:C C12
U 1 1 5F38D57C
P 950 5150
F 0 "C12" H 900 5500 50  0000 L CNN
F 1 "0.1uF" H 900 5400 50  0000 L CNN
F 2 "Gemini:C_Disc_D3.8mm_W2.6mm_P4.00mm" H 988 5000 50  0001 C CNN
F 3 "~" H 950 5150 50  0001 C CNN
	1    950  5150
	0    1    1    0   
$EndComp
$Comp
L Device:C C1
U 1 1 5F38F6FA
P 950 5400
F 0 "C1" H 900 5750 50  0000 L CNN
F 1 "0.1uF" H 900 5650 50  0000 L CNN
F 2 "Gemini:C_Disc_D3.8mm_W2.6mm_P4.00mm" H 988 5250 50  0001 C CNN
F 3 "~" H 950 5400 50  0001 C CNN
	1    950  5400
	0    1    1    0   
$EndComp
$Comp
L Device:C C2
U 1 1 5F38F704
P 950 5650
F 0 "C2" H 900 6000 50  0000 L CNN
F 1 "0.1uF" H 900 5900 50  0000 L CNN
F 2 "Gemini:C_Disc_D3.8mm_W2.6mm_P4.00mm" H 988 5500 50  0001 C CNN
F 3 "~" H 950 5650 50  0001 C CNN
	1    950  5650
	0    1    1    0   
$EndComp
$Comp
L Device:C C3
U 1 1 5F38F70E
P 950 5900
F 0 "C3" H 900 6250 50  0000 L CNN
F 1 "0.1uF" H 900 6150 50  0000 L CNN
F 2 "Gemini:C_Disc_D3.8mm_W2.6mm_P4.00mm" H 988 5750 50  0001 C CNN
F 3 "~" H 950 5900 50  0001 C CNN
	1    950  5900
	0    1    1    0   
$EndComp
$Comp
L Device:C C4
U 1 1 5F38F718
P 950 6150
F 0 "C4" H 900 6500 50  0000 L CNN
F 1 "0.1uF" H 900 6400 50  0000 L CNN
F 2 "Gemini:C_Disc_D3.8mm_W2.6mm_P4.00mm" H 988 6000 50  0001 C CNN
F 3 "~" H 950 6150 50  0001 C CNN
	1    950  6150
	0    1    1    0   
$EndComp
Connection ~ 1100 3650
Wire Wire Line
	1100 3650 1100 3900
Connection ~ 1100 3900
Wire Wire Line
	1100 3900 1100 4150
Connection ~ 1100 4150
Wire Wire Line
	1100 4150 1100 4400
Connection ~ 1100 4400
Wire Wire Line
	1100 4400 1100 4650
Connection ~ 1100 4650
Wire Wire Line
	1100 4650 1100 4900
Connection ~ 1100 4900
Wire Wire Line
	1100 4900 1100 5150
Connection ~ 1100 5150
Wire Wire Line
	1100 5150 1100 5400
Connection ~ 1100 5400
Wire Wire Line
	1100 5400 1100 5650
Connection ~ 1100 5650
Wire Wire Line
	1100 5650 1100 5900
Connection ~ 1100 5900
Wire Wire Line
	1100 5900 1100 6150
Wire Wire Line
	800  6150 800  5900
Connection ~ 800  3650
Connection ~ 800  3900
Wire Wire Line
	800  3900 800  3650
Connection ~ 800  4150
Wire Wire Line
	800  4150 800  3900
Connection ~ 800  4400
Wire Wire Line
	800  4400 800  4150
Connection ~ 800  4650
Wire Wire Line
	800  4650 800  4400
Connection ~ 800  4900
Wire Wire Line
	800  4900 800  4650
Connection ~ 800  5150
Wire Wire Line
	800  5150 800  4900
Connection ~ 800  5400
Wire Wire Line
	800  5400 800  5150
Connection ~ 800  5650
Wire Wire Line
	800  5650 800  5400
Connection ~ 800  5900
Wire Wire Line
	800  5900 800  5650
$Comp
L power:GND #PWR0103
U 1 1 5F39C3BD
P 800 3100
F 0 "#PWR0103" H 800 2850 50  0001 C CNN
F 1 "GND" H 805 2927 50  0000 C CNN
F 2 "" H 800 3100 50  0001 C CNN
F 3 "" H 800 3100 50  0001 C CNN
	1    800  3100
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR0104
U 1 1 5F39C7C6
P 1100 3100
F 0 "#PWR0104" H 1100 2950 50  0001 C CNN
F 1 "+5V" H 1115 3273 50  0000 C CNN
F 2 "" H 1100 3100 50  0001 C CNN
F 3 "" H 1100 3100 50  0001 C CNN
	1    1100 3100
	0    1    1    0   
$EndComp
$Comp
L Device:R R3
U 1 1 5EC40C53
P 10250 2050
F 0 "R3" V 10250 1900 50  0000 C CNN
F 1 "10K" V 10250 2050 50  0000 C CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 10180 2050 50  0001 C CNN
F 3 "~" H 10250 2050 50  0001 C CNN
	1    10250 2050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	800  3100 800  3400
Wire Wire Line
	1100 3100 1100 3400
Connection ~ 1100 3400
Wire Wire Line
	1100 3400 1100 3650
Connection ~ 800  3400
Wire Wire Line
	800  3400 800  3650
Text GLabel 8800 5250 2    50   Input ~ 0
AD14
Text GLabel 8800 5350 2    50   Input ~ 0
AD13
Text GLabel 8800 5450 2    50   Input ~ 0
AD12
Text GLabel 8800 5550 2    50   Input ~ 0
AD11
Text GLabel 8800 5650 2    50   Input ~ 0
AD10
Text GLabel 8800 5750 2    50   Input ~ 0
AD9
Text GLabel 8800 5850 2    50   Input ~ 0
AD8
Text GLabel 8800 5950 2    50   Input ~ 0
AD7
Text GLabel 8800 6050 2    50   Input ~ 0
AD6
Text GLabel 8800 6150 2    50   Input ~ 0
AD5
Text GLabel 8800 5150 2    50   Input ~ 0
PSEL*_D15_OUT
Text GLabel 8800 4550 2    50   Input ~ 0
BST3
Text GLabel 8800 4450 2    50   Input ~ 0
BST2
Text GLabel 8800 4350 2    50   Input ~ 0
BST1
Text GLabel 8800 4250 2    50   Input ~ 0
MEM*
Text GLabel 8800 4850 2    50   Input ~ 0
CLKOUT
Text GLabel 8800 5050 2    50   Input ~ 0
ALATCH
$Comp
L Connector_Generic:Conn_01x20 J1
U 1 1 5EC9689C
P 7800 5150
F 0 "J1" H 7750 6200 50  0000 L CNN
F 1 "Conn_01x20" H 7600 6200 50  0001 L CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x20_Pitch2.54mm" H 7800 5150 50  0001 C CNN
F 3 "~" H 7800 5150 50  0001 C CNN
	1    7800 5150
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x20 J3
U 1 1 5EC7B205
P 8600 5250
F 0 "J3" H 8600 4150 50  0000 C CNN
F 1 "Conn_01x20" H 8518 4116 50  0001 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x20_Pitch2.54mm" H 8600 5250 50  0001 C CNN
F 3 "~" H 8600 5250 50  0001 C CNN
	1    8600 5250
	-1   0    0    1   
$EndComp
Text GLabel 7600 5550 0    50   Input ~ 0
R_W*
Text GLabel 7600 6050 0    50   Input ~ 0
AD3
Text GLabel 7600 5950 0    50   Input ~ 0
AD2
Text GLabel 7600 5850 0    50   Input ~ 0
AD1
Text GLabel 7600 6150 0    50   Input ~ 0
AD4
Text GLabel 7600 5750 0    50   Input ~ 0
AD0_IN
Text GLabel 7600 4350 0    50   Input ~ 0
RD*
Text GLabel 7600 5050 0    50   Input ~ 0
NMI*
Text GLabel 7600 4850 0    50   Input ~ 0
READY
Text GLabel 7600 4650 0    50   Input ~ 0
HOLD*
Text GLabel 7600 4450 0    50   Input ~ 0
RESET*
Text GLabel 7600 4250 0    50   Input ~ 0
WE*_IOCLK*
Text GLabel 7600 4950 0    50   Input ~ 0
INTREQ*
Text GLabel 7600 5450 0    50   Input ~ 0
IC3
Text GLabel 7600 5350 0    50   Input ~ 0
IC2
Text GLabel 7600 5250 0    50   Input ~ 0
IC1
Text GLabel 7600 5150 0    50   Input ~ 0
IC0
Text Notes 3650 3050 2    79   ~ 0
Data Bus\nTransceivers
Text Notes 2650 800  0    79   ~ 0
Signal Drivers
Text Notes 7450 3950 0    79   ~ 0
Test Headers
Wire Bus Line
	4100 7250 4100 1050
Wire Bus Line
	4100 1050 6750 1050
Wire Bus Line
	6750 1050 6750 6400
Text GLabel 8800 4950 2    50   Input ~ 0
GND
Text GLabel 7600 4750 0    50   Input ~ 0
GND
Text GLabel 7600 5650 0    50   Input ~ 0
+5V
Text GLabel 2500 6650 0    50   Input ~ 0
RDBENA
Text GLabel 10100 2150 0    50   Input ~ 0
+5V
Wire Wire Line
	10400 2150 10500 2150
Text GLabel 10500 2150 2    50   Input ~ 0
INTREQ*
$Comp
L Device:R R5
U 1 1 5EC40C4C
P 10250 2150
F 0 "R5" V 10250 2000 50  0000 C CNN
F 1 "10K" V 10250 2150 50  0000 C CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 10180 2150 50  0001 C CNN
F 3 "~" H 10250 2150 50  0001 C CNN
	1    10250 2150
	0    -1   -1   0   
$EndComp
Text GLabel 7350 1500 0    50   Input ~ 0
APP*
$Sheet
S 700  2150 1250 350 
U 5F142239
F0 "Front Panel" 50
F1 "StevenFrontPanel.sch" 50
$EndSheet
Text GLabel 2500 3650 0    50   Input ~ 0
B_D7
Text GLabel 2500 3850 0    50   Input ~ 0
B_D5
Text GLabel 2500 4250 0    50   Input ~ 0
B_D1
Text GLabel 2500 4050 0    50   Input ~ 0
B_D3
Text GLabel 2500 3950 0    50   Input ~ 0
B_D4
Text GLabel 2500 4150 0    50   Input ~ 0
B_D2
Text GLabel 2500 3750 0    50   Input ~ 0
B_D6
Text Notes 1900 5800 0    50   ~ 0
MSB
Text Notes 1900 4400 0    50   ~ 0
LSB
Text Notes 6300 4300 0    50   ~ 0
LSB
Text Notes 6200 5700 0    50   ~ 0
MSB
Wire Wire Line
	800  7450 800  7250
Connection ~ 800  7250
Wire Wire Line
	800  6750 800  6850
Connection ~ 800  6850
Wire Wire Line
	800  7250 1000 7250
Wire Wire Line
	800  6850 1000 6850
Text GLabel 5000 4350 0    50   Input ~ 0
PSEL*_D15_OUT
Text GLabel 7600 4550 0    50   Input ~ 0
APP*
Text Notes 4150 4400 0    50   ~ 0
LSB
Text Notes 9700 2150 0    50   ~ 0
LSB
Text Notes -2700 2600 0    50   ~ 0
LSB
NoConn ~ 8800 4650
NoConn ~ 8800 4750
Text Notes 8850 4750 0    50   ~ 0
Skip XTAL1,2 to reduce\nload capacitance
$EndSCHEMATC
