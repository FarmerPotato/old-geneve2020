EESchema Schematic File Version 4
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 1450 2400 0    50   Input ~ 0
RD*
Text GLabel 1450 2500 0    50   Input ~ 0
WE*_IOCLK*
Text GLabel 1450 2600 0    50   Input ~ 0
MEM*
Text GLabel 1950 2300 2    50   Input ~ 0
GND
Text GLabel 1450 3400 0    50   Input ~ 0
AD13
Text GLabel 1450 3500 0    50   Input ~ 0
AD11
Text GLabel 1450 3600 0    50   Input ~ 0
AD9
Text GLabel 1450 3700 0    50   Input ~ 0
AD7
Text GLabel 1450 3800 0    50   Input ~ 0
AD5
Text GLabel 1450 4000 0    50   Input ~ 0
AD1
Text GLabel 1450 3900 0    50   Input ~ 0
AD3
$Comp
L Connector_Generic:Conn_02x20_Odd_Even J?
U 1 1 5E92AB5E
P 1650 3200
AR Path="/5E92AB5E" Ref="J?"  Part="1" 
AR Path="/5E905665/5E92AB5E" Ref="J?"  Part="1" 
AR Path="/5E8FF889/5E92AB5E" Ref="J1"  Part="1" 
F 0 "J1" H 1350 4250 50  0000 L CNN
F 1 "Slot 1: 9901" H 1450 4250 50  0000 L BNN
F 2 "Pin_Headers:Pin_Header_Straight_2x20_Pitch2.54mm" H 1650 3200 50  0001 C CNN
F 3 "~" H 1650 3200 50  0001 C CNN
	1    1650 3200
	1    0    0    -1  
$EndComp
Text GLabel 1450 2700 0    50   Input ~ 0
BST1
Text GLabel 1450 2800 0    50   Input ~ 0
BST3
Text GLabel 1950 2700 2    50   Input ~ 0
BST2
Text GLabel 1450 2300 0    50   Input ~ 0
RESET*
Text GLabel 1950 2400 2    50   Input ~ 0
GND
Text GLabel 1950 2600 2    50   Input ~ 0
GND
Text GLabel 1950 2900 2    50   Input ~ 0
GND
Text GLabel 1950 2500 2    50   Input ~ 0
GND
Text GLabel 1450 2900 0    50   Input ~ 0
READY
Text GLabel 1450 3000 0    50   Input ~ 0
CLKOUT
Text GLabel 1950 2800 2    50   Input ~ 0
CLK3
Text GLabel 1950 3200 2    50   Input ~ 0
GND
Text GLabel 1950 3100 2    50   Input ~ 0
GND
Text GLabel 1950 3000 2    50   Input ~ 0
GND
Text GLabel 1450 3100 0    50   Input ~ 0
ALATCH
Text GLabel 1950 4100 2    50   Input ~ 0
EXBAE
Text GLabel 1950 3800 2    50   Input ~ 0
AD4
Text GLabel 1950 3900 2    50   Input ~ 0
AD2
Text GLabel 1950 4000 2    50   Input ~ 0
AD0_IN
Text GLabel 1950 3700 2    50   Input ~ 0
AD6
Text GLabel 1950 3600 2    50   Input ~ 0
AD8
Text GLabel 1950 3500 2    50   Input ~ 0
AD10
Text GLabel 1950 3400 2    50   Input ~ 0
AD12
Text GLabel 1950 3300 2    50   Input ~ 0
AD14
Text GLabel 1450 4200 0    50   Input ~ 0
VCC
Text Notes 1250 4600 0    50   ~ 0
EXBAE from FPGA inhibits\nmemory module output
$Comp
L Connector_Generic:Conn_02x20_Odd_Even J?
U 1 1 5E97FD59
P 3550 3250
AR Path="/5E97FD59" Ref="J?"  Part="1" 
AR Path="/5E905665/5E97FD59" Ref="J?"  Part="1" 
AR Path="/5E8FF889/5E97FD59" Ref="J2"  Part="1" 
F 0 "J2" H 3250 4300 50  0000 L CNN
F 1 "Slot 2" H 3350 4300 50  0000 L CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x20_Pitch2.54mm" H 3550 3250 50  0001 C CNN
F 3 "~" H 3550 3250 50  0001 C CNN
	1    3550 3250
	1    0    0    -1  
$EndComp
Text Notes 3000 1350 0    100  ~ 0
Vertical connector 2
Text GLabel 1450 3300 0    50   Input ~ 0
PSEL*_AD15_OUT
$Comp
L Connector_Generic:Conn_02x20_Odd_Even J?
U 1 1 5EA95ED1
P 7300 3250
AR Path="/5EA95ED1" Ref="J?"  Part="1" 
AR Path="/5E905665/5EA95ED1" Ref="J?"  Part="1" 
AR Path="/5E8FF889/5EA95ED1" Ref="J4"  Part="1" 
F 0 "J4" H 7000 4300 50  0000 L CNN
F 1 "Slot 4" H 7100 4300 50  0000 L CNN
F 2 "Pin_Headers:Pin_Header_Angled_2x20_Pitch2.54mm" H 7300 3250 50  0001 C CNN
F 3 "~" H 7300 3250 50  0001 C CNN
	1    7300 3250
	1    0    0    -1  
$EndComp
Text Notes 4900 1350 0    100  ~ 0
Vertical connector 3
$Comp
L Connector_Generic:Conn_02x20_Odd_Even J?
U 1 1 5EA98303
P 5500 3250
AR Path="/5EA98303" Ref="J?"  Part="1" 
AR Path="/5E905665/5EA98303" Ref="J?"  Part="1" 
AR Path="/5E8FF889/5EA98303" Ref="J3"  Part="1" 
F 0 "J3" H 5200 4300 50  0000 L CNN
F 1 "Slot 3" H 5300 4300 50  0000 L CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x20_Pitch2.54mm" H 5500 3250 50  0001 C CNN
F 3 "~" H 5500 3250 50  0001 C CNN
	1    5500 3250
	1    0    0    -1  
$EndComp
Text GLabel 1450 4100 0    50   Input ~ 0
INT1
Text GLabel 1450 3200 0    50   Input ~ 0
INT2
Text Notes 1100 1350 0    100  ~ 0
Vertical connector 1
Text Notes 6800 1500 0    100  ~ 0
Vertical connector 3\nor Horizontal
Text GLabel 1450 1750 0    50   Input ~ 0
IC0
Text GLabel 1950 1750 2    50   Input ~ 0
IC1
Text GLabel 1450 1850 0    50   Input ~ 0
IC2
Text GLabel 1450 1650 0    50   Input ~ 0
INTREQ*
Text Notes 750  1950 0    50   ~ 0
To CPU
Text GLabel 1950 1650 2    50   Input ~ 0
GND
$Comp
L Connector_Generic:Conn_02x04_Odd_Even J?
U 1 1 5E6B3A89
P 1650 1750
AR Path="/5E6B3A89" Ref="J?"  Part="1" 
AR Path="/5E8FF889/5E6B3A89" Ref="J6"  Part="1" 
F 0 "J6" H 1700 2067 50  0000 C CNN
F 1 "Conn_02x04_Odd_Even" H 1700 1976 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x04_Pitch2.54mm" H 1650 1750 50  0001 C CNN
F 3 "~" H 1650 1750 50  0001 C CNN
	1    1650 1750
	1    0    0    -1  
$EndComp
Text GLabel 1950 1850 2    50   Input ~ 0
IC3
Text GLabel 1950 1950 2    50   Input ~ 0
GND
Text GLabel 1950 4200 2    50   Input ~ 0
GND
Text GLabel 3350 2450 0    50   Input ~ 0
RD*
Text GLabel 3350 2550 0    50   Input ~ 0
WE*_IOCLK*
Text GLabel 3350 2650 0    50   Input ~ 0
MEM*
Text GLabel 3350 3450 0    50   Input ~ 0
AD13
Text GLabel 3350 3550 0    50   Input ~ 0
AD11
Text GLabel 3350 3650 0    50   Input ~ 0
AD9
Text GLabel 3350 3750 0    50   Input ~ 0
AD7
Text GLabel 3350 3850 0    50   Input ~ 0
AD5
Text GLabel 3350 4050 0    50   Input ~ 0
AD1
Text GLabel 3350 3950 0    50   Input ~ 0
AD3
Text GLabel 3350 2750 0    50   Input ~ 0
BST1
Text GLabel 3350 2850 0    50   Input ~ 0
BST3
Text GLabel 3350 2350 0    50   Input ~ 0
RESET*
Text GLabel 3350 2950 0    50   Input ~ 0
READY
Text GLabel 3350 3050 0    50   Input ~ 0
CLKOUT
Text GLabel 3350 3150 0    50   Input ~ 0
ALATCH
Text GLabel 3350 4250 0    50   Input ~ 0
VCC
Text GLabel 3350 3350 0    50   Input ~ 0
PSEL*_AD15_OUT
Text GLabel 3350 4150 0    50   Input ~ 0
INT1
Text GLabel 3350 3250 0    50   Input ~ 0
INT2
Text GLabel 5300 2450 0    50   Input ~ 0
RD*
Text GLabel 5300 2550 0    50   Input ~ 0
WE*_IOCLK*
Text GLabel 5300 2650 0    50   Input ~ 0
MEM*
Text GLabel 5300 3450 0    50   Input ~ 0
AD13
Text GLabel 5300 3550 0    50   Input ~ 0
AD11
Text GLabel 5300 3650 0    50   Input ~ 0
AD9
Text GLabel 5300 3750 0    50   Input ~ 0
AD7
Text GLabel 5300 3850 0    50   Input ~ 0
AD5
Text GLabel 5300 4050 0    50   Input ~ 0
AD1
Text GLabel 5300 3950 0    50   Input ~ 0
AD3
Text GLabel 5300 2750 0    50   Input ~ 0
BST1
Text GLabel 5300 2850 0    50   Input ~ 0
BST3
Text GLabel 5300 2350 0    50   Input ~ 0
RESET*
Text GLabel 5300 2950 0    50   Input ~ 0
READY
Text GLabel 5300 3050 0    50   Input ~ 0
CLKOUT
Text GLabel 5300 3150 0    50   Input ~ 0
ALATCH
Text GLabel 5300 4250 0    50   Input ~ 0
VCC
Text GLabel 5300 3350 0    50   Input ~ 0
PSEL*_AD15_OUT
Text GLabel 5300 4150 0    50   Input ~ 0
INT1
Text GLabel 5300 3250 0    50   Input ~ 0
INT2
Text GLabel 7100 2450 0    50   Input ~ 0
RD*
Text GLabel 7100 2550 0    50   Input ~ 0
WE*_IOCLK*
Text GLabel 7100 2650 0    50   Input ~ 0
MEM*
Text GLabel 7100 3450 0    50   Input ~ 0
AD13
Text GLabel 7100 3550 0    50   Input ~ 0
AD11
Text GLabel 7100 3650 0    50   Input ~ 0
AD9
Text GLabel 7100 3750 0    50   Input ~ 0
AD7
Text GLabel 7100 3850 0    50   Input ~ 0
AD5
Text GLabel 7100 4050 0    50   Input ~ 0
AD1
Text GLabel 7100 3950 0    50   Input ~ 0
AD3
Text GLabel 7100 2750 0    50   Input ~ 0
BST1
Text GLabel 7100 2850 0    50   Input ~ 0
BST3
Text GLabel 7100 2350 0    50   Input ~ 0
RESET*
Text GLabel 7100 2950 0    50   Input ~ 0
READY
Text GLabel 7100 3050 0    50   Input ~ 0
CLKOUT
Text GLabel 7100 3150 0    50   Input ~ 0
ALATCH
Text GLabel 7100 4250 0    50   Input ~ 0
VCC
Text GLabel 7100 3350 0    50   Input ~ 0
PSEL*_AD15_OUT
Text GLabel 7100 4150 0    50   Input ~ 0
INT1
Text GLabel 7100 3250 0    50   Input ~ 0
INT2
Text GLabel 3850 2350 2    50   Input ~ 0
GND
Text GLabel 3850 2750 2    50   Input ~ 0
BST2
Text GLabel 3850 2450 2    50   Input ~ 0
GND
Text GLabel 3850 2650 2    50   Input ~ 0
GND
Text GLabel 3850 2950 2    50   Input ~ 0
GND
Text GLabel 3850 2550 2    50   Input ~ 0
GND
Text GLabel 3850 2850 2    50   Input ~ 0
CLK3
Text GLabel 3850 3250 2    50   Input ~ 0
GND
Text GLabel 3850 3150 2    50   Input ~ 0
GND
Text GLabel 3850 3050 2    50   Input ~ 0
GND
Text GLabel 3850 4150 2    50   Input ~ 0
EXBAE
Text GLabel 3850 3850 2    50   Input ~ 0
AD4
Text GLabel 3850 3950 2    50   Input ~ 0
AD2
Text GLabel 3850 4050 2    50   Input ~ 0
AD0_IN
Text GLabel 3850 3750 2    50   Input ~ 0
AD6
Text GLabel 3850 3650 2    50   Input ~ 0
AD8
Text GLabel 3850 3550 2    50   Input ~ 0
AD10
Text GLabel 3850 3450 2    50   Input ~ 0
AD12
Text GLabel 3850 3350 2    50   Input ~ 0
AD14
Text GLabel 3850 4250 2    50   Input ~ 0
GND
Text GLabel 5800 2350 2    50   Input ~ 0
GND
Text GLabel 5800 2750 2    50   Input ~ 0
BST2
Text GLabel 5800 2450 2    50   Input ~ 0
GND
Text GLabel 5800 2650 2    50   Input ~ 0
GND
Text GLabel 5800 2950 2    50   Input ~ 0
GND
Text GLabel 5800 2550 2    50   Input ~ 0
GND
Text GLabel 5800 2850 2    50   Input ~ 0
CLK3
Text GLabel 5800 3250 2    50   Input ~ 0
GND
Text GLabel 5800 3150 2    50   Input ~ 0
GND
Text GLabel 5800 3050 2    50   Input ~ 0
GND
Text GLabel 5800 4150 2    50   Input ~ 0
EXBAE
Text GLabel 5800 3850 2    50   Input ~ 0
AD4
Text GLabel 5800 3950 2    50   Input ~ 0
AD2
Text GLabel 5800 4050 2    50   Input ~ 0
AD0_IN
Text GLabel 5800 3750 2    50   Input ~ 0
AD6
Text GLabel 5800 3650 2    50   Input ~ 0
AD8
Text GLabel 5800 3550 2    50   Input ~ 0
AD10
Text GLabel 5800 3450 2    50   Input ~ 0
AD12
Text GLabel 5800 3350 2    50   Input ~ 0
AD14
Text GLabel 5800 4250 2    50   Input ~ 0
GND
Text GLabel 7600 2350 2    50   Input ~ 0
GND
Text GLabel 7600 2750 2    50   Input ~ 0
BST2
Text GLabel 7600 2450 2    50   Input ~ 0
GND
Text GLabel 7600 2650 2    50   Input ~ 0
GND
Text GLabel 7600 2950 2    50   Input ~ 0
GND
Text GLabel 7600 2550 2    50   Input ~ 0
GND
Text GLabel 7600 2850 2    50   Input ~ 0
CLK3
Text GLabel 7600 3250 2    50   Input ~ 0
GND
Text GLabel 7600 3150 2    50   Input ~ 0
GND
Text GLabel 7600 3050 2    50   Input ~ 0
GND
Text GLabel 7600 4150 2    50   Input ~ 0
EXBAE
Text GLabel 7600 3850 2    50   Input ~ 0
AD4
Text GLabel 7600 3950 2    50   Input ~ 0
AD2
Text GLabel 7600 4050 2    50   Input ~ 0
AD0_IN
Text GLabel 7600 3750 2    50   Input ~ 0
AD6
Text GLabel 7600 3650 2    50   Input ~ 0
AD8
Text GLabel 7600 3550 2    50   Input ~ 0
AD10
Text GLabel 7600 3450 2    50   Input ~ 0
AD12
Text GLabel 7600 3350 2    50   Input ~ 0
AD14
Text GLabel 7600 4250 2    50   Input ~ 0
GND
Text GLabel 1450 1950 0    50   Input ~ 0
VCC
$Comp
L Device:R_Network_Dividers_x06_SIP RN1
U 1 1 5E8B9614
P 9600 1950
F 0 "RN1" V 8783 1950 50  0000 C CNN
F 1 "R_Network_Dividers_x06_SIP" V 8874 1950 50  0000 C CNN
F 2 "Resistors_ThroughHole:R_Array_SIP8" V 10275 1950 50  0001 C CNN
F 3 "http://www.vishay.com/docs/31509/csc.pdf" H 9600 1950 50  0001 C CNN
	1    9600 1950
	0    1    1    0   
$EndComp
Text GLabel 9300 1550 0    50   Input ~ 0
RD*
Text GLabel 9300 1750 0    50   Input ~ 0
WE*_IOCLK*
Text GLabel 9300 1950 0    50   Input ~ 0
MEM*
Text GLabel 9300 2150 0    50   Input ~ 0
BST1
Text GLabel 9300 2350 0    50   Input ~ 0
BST3
Text GLabel 9300 1350 0    50   Input ~ 0
RESET*
Text GLabel 9900 1350 2    50   Input ~ 0
GND
Text GLabel 9900 2450 2    50   Input ~ 0
VCC
$Comp
L Device:R_Network_Dividers_x06_SIP RN2
U 1 1 5E8BCD8D
P 10850 2850
F 0 "RN2" V 10033 2850 50  0000 C CNN
F 1 "R_Network_Dividers_x06_SIP" V 10124 2850 50  0000 C CNN
F 2 "Resistors_ThroughHole:R_Array_SIP8" V 11525 2850 50  0001 C CNN
F 3 "http://www.vishay.com/docs/31509/csc.pdf" H 10850 2850 50  0001 C CNN
	1    10850 2850
	0    1    1    0   
$EndComp
Text GLabel 11150 3350 2    50   Input ~ 0
VCC
Text GLabel 11150 2250 2    50   Input ~ 0
GND
Text GLabel 10550 2250 0    50   Input ~ 0
BST2
Text GLabel 10550 2450 0    50   Input ~ 0
CLK3
Text GLabel 10550 2650 0    50   Input ~ 0
READY
Text GLabel 10550 2850 0    50   Input ~ 0
CLKOUT
Text GLabel 10550 3050 0    50   Input ~ 0
ALATCH
Text GLabel 10550 3250 0    50   Input ~ 0
INT2
$Comp
L Device:R_Network_Dividers_x06_SIP RN3
U 1 1 5E8BF0F6
P 9600 4600
F 0 "RN3" V 8783 4600 50  0000 C CNN
F 1 "R_Network_Dividers_x06_SIP" V 8874 4600 50  0000 C CNN
F 2 "Resistors_ThroughHole:R_Array_SIP8" V 10275 4600 50  0001 C CNN
F 3 "http://www.vishay.com/docs/31509/csc.pdf" H 9600 4600 50  0001 C CNN
	1    9600 4600
	0    1    1    0   
$EndComp
Text GLabel 9900 5100 2    50   Input ~ 0
VCC
Text GLabel 9900 4000 2    50   Input ~ 0
GND
Text GLabel 9300 4000 0    50   Input ~ 0
PSEL*_AD15_OUT
Text GLabel 9300 4200 0    50   Input ~ 0
AD14
Text GLabel 9300 4400 0    50   Input ~ 0
AD13
Text GLabel 9300 4800 0    50   Input ~ 0
AD11
Text GLabel 9300 4600 0    50   Input ~ 0
AD12
Text GLabel 9300 5000 0    50   Input ~ 0
AD10
$Comp
L Device:R_Network_Dividers_x06_SIP RN4
U 1 1 5E8C1891
P 9600 5950
F 0 "RN4" V 8783 5950 50  0000 C CNN
F 1 "R_Network_Dividers_x06_SIP" V 8874 5950 50  0000 C CNN
F 2 "Resistors_ThroughHole:R_Array_SIP8" V 10275 5950 50  0001 C CNN
F 3 "http://www.vishay.com/docs/31509/csc.pdf" H 9600 5950 50  0001 C CNN
	1    9600 5950
	0    1    1    0   
$EndComp
Text GLabel 9900 6450 2    50   Input ~ 0
VCC
Text GLabel 9900 5350 2    50   Input ~ 0
GND
$Comp
L Device:R_Network_Dividers_x06_SIP RN5
U 1 1 5E8C3CC5
P 10700 5950
F 0 "RN5" V 9883 5950 50  0000 C CNN
F 1 "R_Network_Dividers_x06_SIP" V 9974 5950 50  0000 C CNN
F 2 "Resistors_ThroughHole:R_Array_SIP8" V 11375 5950 50  0001 C CNN
F 3 "http://www.vishay.com/docs/31509/csc.pdf" H 10700 5950 50  0001 C CNN
	1    10700 5950
	0    1    1    0   
$EndComp
Text GLabel 11000 6450 2    50   Input ~ 0
VCC
Text GLabel 11000 5350 2    50   Input ~ 0
GND
Text GLabel 9300 5350 0    50   Input ~ 0
AD9
Text GLabel 9300 5550 0    50   Input ~ 0
AD7
Text GLabel 9300 5750 0    50   Input ~ 0
AD5
Text GLabel 9300 6150 0    50   Input ~ 0
AD1
Text GLabel 9300 5950 0    50   Input ~ 0
AD3
Text GLabel 9300 6350 0    50   Input ~ 0
INT1
Text GLabel 10400 6350 0    50   Input ~ 0
EXBAE
Text GLabel 10400 5750 0    50   Input ~ 0
AD4
Text GLabel 10400 5950 0    50   Input ~ 0
AD2
Text GLabel 10400 6150 0    50   Input ~ 0
AD0_IN
Text GLabel 10400 5550 0    50   Input ~ 0
AD6
Text GLabel 10400 5350 0    50   Input ~ 0
AD8
$Sheet
S 3600 4900 800  400 
U 5EB261DB
F0 "BusSignals" 50
F1 "../bios/BusSignals.sch" 50
$EndSheet
$EndSCHEMATC
