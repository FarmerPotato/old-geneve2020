EESchema Schematic File Version 4
LIBS:Sapphire-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 3650 3250 2    50   Input ~ 0
RD*
Text GLabel 3650 3350 2    50   Input ~ 0
WE*_IOCLK*
Text GLabel 3150 3350 0    50   Input ~ 0
MEM*
Text GLabel 3150 3250 0    50   Input ~ 0
GND
Text GLabel 3150 4350 0    50   Input ~ 0
AD13
Text GLabel 3150 4450 0    50   Input ~ 0
AD11
Text GLabel 3150 4550 0    50   Input ~ 0
AD9
Text GLabel 3150 4650 0    50   Input ~ 0
AD7
Text GLabel 3150 4750 0    50   Input ~ 0
AD5
Text GLabel 3150 4950 0    50   Input ~ 0
AD1
Text GLabel 3150 4850 0    50   Input ~ 0
AD3
Text GLabel 3650 5150 2    50   Input ~ 0
VCC
$Comp
L Connector_Generic:Conn_02x20_Odd_Even J?
U 1 1 5E6F4312
P 3350 4150
AR Path="/5E6F4312" Ref="J?"  Part="1" 
AR Path="/5E905665/5E6F4312" Ref="J?"  Part="1" 
AR Path="/5E8FF889/5E6F4312" Ref="J?"  Part="1" 
F 0 "J?" H 3050 5200 50  0000 L CNN
F 1 "Slot 1: 9901" H 3150 5200 50  0000 L BNN
F 2 "Pin_Headers:Pin_Header_Straight_2x20_Pitch2.54mm" H 3350 4150 50  0001 C CNN
F 3 "~" H 3350 4150 50  0001 C CNN
	1    3350 4150
	1    0    0    -1  
$EndComp
Text GLabel 5350 3550 2    50   Input ~ 0
BST1
Text GLabel 5350 3650 2    50   Input ~ 0
BST3
Text GLabel 5250 3550 0    50   Input ~ 0
BST2
Text GLabel 5050 3000 0    50   Input ~ 0
RESET*
Text GLabel 2450 3400 0    50   Input ~ 0
GND
Text GLabel 2450 3600 0    50   Input ~ 0
GND
Text GLabel 2450 3700 0    50   Input ~ 0
GND
Text GLabel 2450 3500 0    50   Input ~ 0
GND
Text GLabel 3150 5050 0    50   Input ~ 0
READY
Text GLabel 5250 3650 0    50   Input ~ 0
CLKOUT
Text GLabel 5050 2700 0    50   Input ~ 0
CLK3
Text GLabel 2450 4000 0    50   Input ~ 0
GND
Text GLabel 2450 3900 0    50   Input ~ 0
GND
Text GLabel 2450 3800 0    50   Input ~ 0
GND
Text GLabel 4450 3650 2    50   Input ~ 0
ALATCH
Text GLabel 3650 5050 2    50   Input ~ 0
RDBENA
Text GLabel 3650 4750 2    50   Input ~ 0
AD4
Text GLabel 3650 4850 2    50   Input ~ 0
AD2
Text GLabel 3650 4950 2    50   Input ~ 0
AD0_IN
Text GLabel 3650 4650 2    50   Input ~ 0
AD6
Text GLabel 3650 4550 2    50   Input ~ 0
AD8
Text GLabel 3650 4450 2    50   Input ~ 0
AD10
Text GLabel 3650 4350 2    50   Input ~ 0
AD12
Text GLabel 3650 4250 2    50   Input ~ 0
AD14
Text GLabel 3150 5150 0    50   Input ~ 0
VCC
Text Notes 2950 5550 0    50   ~ 0
RDBENA from FPGA inhibits\nmemory module output
Text GLabel 3150 4250 0    50   Input ~ 0
PSEL*_AD15_OUT
Text GLabel 5550 2900 2    50   Input ~ 0
EXT1
Text GLabel 5550 3000 2    50   Input ~ 0
EXT2
Text GLabel 3150 2800 0    50   Input ~ 0
IC0
Text GLabel 3650 2800 2    50   Input ~ 0
IC1
Text GLabel 3150 2900 0    50   Input ~ 0
IC2
Text GLabel 3150 2700 0    50   Input ~ 0
INTREQ*
Text Notes 2900 2850 2    50   ~ 0
To CPU
Text GLabel 2200 2600 0    50   Input ~ 0
GND
Text GLabel 3650 2600 2    50   Input ~ 0
GND
$Comp
L Connector_Generic:Conn_02x04_Odd_Even J?
U 1 1 5E6F433D
P 3350 2700
AR Path="/5E6F433D" Ref="J?"  Part="1" 
AR Path="/5E8FF889/5E6F433D" Ref="J?"  Part="1" 
AR Path="/5E6F255E/5E6F433D" Ref="J?"  Part="1" 
F 0 "J?" H 3400 3017 50  0000 C CNN
F 1 "Conn_02x04_Odd_Even" H 3400 2926 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x04_Pitch2.54mm" H 3350 2700 50  0001 C CNN
F 3 "~" H 3350 2700 50  0001 C CNN
	1    3350 2700
	1    0    0    -1  
$EndComp
Text GLabel 3650 2900 2    50   Input ~ 0
IC3
Text GLabel 3650 2700 2    50   Input ~ 0
GND
Text GLabel 3650 4150 2    50   Input ~ 0
D0
Text GLabel 3650 4050 2    50   Input ~ 0
D2
Text GLabel 3650 3950 2    50   Input ~ 0
D4
Text GLabel 3650 3850 2    50   Input ~ 0
D6
Text GLabel 3650 3750 2    50   Input ~ 0
D8
Text GLabel 3650 3650 2    50   Input ~ 0
D10
Text GLabel 3650 3550 2    50   Input ~ 0
D12
Text GLabel 3650 3450 2    50   Input ~ 0
D14
Text GLabel 3150 3450 0    50   Input ~ 0
D15
Text GLabel 3150 3550 0    50   Input ~ 0
D13
Text GLabel 3150 3650 0    50   Input ~ 0
D11
Text GLabel 3150 3750 0    50   Input ~ 0
D9
Text GLabel 3150 3850 0    50   Input ~ 0
D7
Text GLabel 3150 3950 0    50   Input ~ 0
D5
Text GLabel 3150 4050 0    50   Input ~ 0
D3
Text GLabel 3150 4150 0    50   Input ~ 0
D1
$Comp
L Connector_Generic:Conn_02x04_Odd_Even J?
U 1 1 5E6F65A8
P 5250 2800
AR Path="/5E6F65A8" Ref="J?"  Part="1" 
AR Path="/5E8FF889/5E6F65A8" Ref="J?"  Part="1" 
AR Path="/5E6F255E/5E6F65A8" Ref="J?"  Part="1" 
F 0 "J?" H 5300 3117 50  0000 C CNN
F 1 "Conn_02x04_Odd_Even" H 5300 3026 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x04_Pitch2.54mm" H 5250 2800 50  0001 C CNN
F 3 "~" H 5250 2800 50  0001 C CNN
	1    5250 2800
	1    0    0    -1  
$EndComp
Text GLabel 4500 5150 0    50   Input ~ 0
3V3
Text GLabel 4750 5150 2    50   Input ~ 0
+5V
$EndSCHEMATC
