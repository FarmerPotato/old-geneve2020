EESchema Schematic File Version 4
LIBS:Garnet-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R_Small R?
U 1 1 5F9BB496
P 3250 6950
AR Path="/5EEDD9EE/5F9BB496" Ref="R?"  Part="1" 
AR Path="/5F9B7653/5F9BB496" Ref="R5"  Part="1" 
AR Path="/5F1CEF26/5F9BB496" Ref="R2"  Part="1" 
F 0 "R2" H 3309 6996 50  0000 L CNN
F 1 "220" H 3309 6905 50  0000 L CNN
F 2 "Resistors_SMD:R_1206" H 3250 6950 50  0001 C CNN
F 3 "~" H 3250 6950 50  0001 C CNN
	1    3250 6950
	0    1    -1   0   
$EndComp
Connection ~ 4300 4050
Wire Wire Line
	4300 4000 4300 4050
Wire Wire Line
	4300 3800 4300 3750
Wire Wire Line
	4100 4050 4300 4050
Text GLabel 3800 4350 2    50   Input ~ 0
GND
$Comp
L Device:R_Small R?
U 1 1 5F9BB4A2
P 4300 3900
AR Path="/5EEDD9EE/5F9BB4A2" Ref="R?"  Part="1" 
AR Path="/5F9B7653/5F9BB4A2" Ref="R4"  Part="1" 
AR Path="/5F1CEF26/5F9BB4A2" Ref="R3"  Part="1" 
F 0 "R3" H 4359 3946 50  0000 L CNN
F 1 "280" H 4359 3855 50  0000 L CNN
F 2 "Resistors_SMD:R_1206" H 4300 3900 50  0001 C CNN
F 3 "~" H 4300 3900 50  0001 C CNN
	1    4300 3900
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5F9BB4A8
P 2950 3750
AR Path="/5EEDD9EE/5F9BB4A8" Ref="R?"  Part="1" 
AR Path="/5F9B7653/5F9BB4A8" Ref="R2"  Part="1" 
AR Path="/5F1CEF26/5F9BB4A8" Ref="R1"  Part="1" 
F 0 "R1" H 3009 3796 50  0000 L CNN
F 1 "220" H 3009 3705 50  0000 L CNN
F 2 "Resistors_SMD:R_1206" H 2950 3750 50  0001 C CNN
F 3 "~" H 2950 3750 50  0001 C CNN
	1    2950 3750
	0    1    -1   0   
$EndComp
Wire Wire Line
	3300 3950 3300 3750
Wire Wire Line
	3300 3750 3150 3750
Wire Wire Line
	3300 4350 3300 4150
$Comp
L Device:D D?
U 1 1 5F9BB4B1
P 3150 4050
AR Path="/5EEDD9EE/5F9BB4B1" Ref="D?"  Part="1" 
AR Path="/5F9B7653/5F9BB4B1" Ref="D1"  Part="1" 
AR Path="/5F1CEF26/5F9BB4B1" Ref="D1"  Part="1" 
F 0 "D1" V 3104 3971 50  0000 R CNN
F 1 "1N4148" V 3195 3971 50  0000 R CNN
F 2 "Diodes_SMD:D_SOD-323_HandSoldering" H 3150 4050 50  0001 C CNN
F 3 "~" H 3150 4050 50  0001 C CNN
	1    3150 4050
	0    -1   1    0   
$EndComp
Wire Wire Line
	3150 4200 3150 4350
Text GLabel 3800 3600 2    50   Input ~ 0
+5V
$Comp
L Isolator:H11L3 U?
U 1 1 5F9BB4BA
P 3800 4050
AR Path="/5EEDD9EE/5F9BB4BA" Ref="U?"  Part="1" 
AR Path="/5F9B7653/5F9BB4BA" Ref="U15"  Part="1" 
AR Path="/5F1CEF26/5F9BB4BA" Ref="U2"  Part="1" 
F 0 "U2" H 3600 4300 50  0000 L CNN
F 1 "H11L3" H 3850 4300 50  0000 L CNN
F 2 "Housings_DIP:DIP-6_W7.62mm_SMDSocket_SmallPads" H 3710 4050 50  0001 C CNN
F 3 "https://www.fairchildsemi.com/datasheets/H1/H11L1M.pdf" H 3710 4050 50  0001 C CNN
	1    3800 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 3750 4300 3750
Wire Wire Line
	3300 3950 3500 3950
Wire Wire Line
	3500 4150 3300 4150
Wire Wire Line
	3800 3750 3800 3600
Text Notes 2950 3350 0    50   ~ 0
Review R values
Text Label 4300 4450 0    50   ~ 0
OPTIN
Wire Wire Line
	2350 3750 2350 4050
Wire Wire Line
	2350 4150 2350 4350
Connection ~ 3150 4350
Wire Wire Line
	3150 4350 3300 4350
Wire Wire Line
	2750 4350 3150 4350
Wire Wire Line
	3150 3900 3150 3750
Connection ~ 3150 3750
Wire Wire Line
	3150 3750 3050 3750
$Comp
L Device:R_Small R?
U 1 1 5F9BB500
P 4550 6500
AR Path="/5EEDD9EE/5F9BB500" Ref="R?"  Part="1" 
AR Path="/5F9B7653/5F9BB500" Ref="R1"  Part="1" 
AR Path="/5F1CEF26/5F9BB500" Ref="R4"  Part="1" 
F 0 "R4" H 4609 6546 50  0000 L CNN
F 1 "220" H 4609 6455 50  0000 L CNN
F 2 "Resistors_SMD:R_1206" H 4550 6500 50  0001 C CNN
F 3 "~" H 4550 6500 50  0001 C CNN
	1    4550 6500
	-1   0    0    1   
$EndComp
Text GLabel 4550 6250 0    50   Input ~ 0
+5V
Wire Wire Line
	4550 6250 4550 6400
$Comp
L 74xx:74LS14 U?
U 5 1 5F9BB50A
P 2850 6300
AR Path="/5EEDD9EE/5F9BB50A" Ref="U?"  Part="1" 
AR Path="/5F9B7653/5F9BB50A" Ref="U16"  Part="5" 
AR Path="/5F1CEF26/5F9BB50A" Ref="U1"  Part="5" 
F 0 "U1" V 2804 6480 50  0000 L CNN
F 1 "74LS14" V 2895 6480 50  0000 L CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 2850 6300 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS14" H 2850 6300 50  0001 C CNN
	5    2850 6300
	1    0    0    -1  
$EndComp
Text Label 3650 6950 0    39   ~ 0
BUFOUT
Text Label 3250 3750 0    50   ~ 0
OPTO-
Text Label 3300 4350 0    50   ~ 0
OPTO+
$Comp
L 74xx:74LS14 U?
U 6 1 5F9BB510
P 2250 6300
AR Path="/5EEDD9EE/5F9BB510" Ref="U?"  Part="2" 
AR Path="/5F9B7653/5F9BB510" Ref="U16"  Part="6" 
AR Path="/5F1CEF26/5F9BB510" Ref="U1"  Part="6" 
F 0 "U1" V 2204 6480 50  0000 L CNN
F 1 "74LS14" V 2295 6480 50  0000 L CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 2250 6300 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS14" H 2250 6300 50  0001 C CNN
	6    2250 6300
	1    0    0    -1  
$EndComp
Text Label 3100 6950 2    39   ~ 0
MIDI_OUT
Text Label 2200 4150 0    39   ~ 0
MIDI_IN
$Comp
L Device:C_Small C4
U 1 1 5EF6C0C3
P 2650 3750
F 0 "C4" V 2421 3750 50  0000 C CNN
F 1 "C_Small" V 2512 3750 50  0000 C CNN
F 2 "Capacitors_SMD:C_1206" H 2650 3750 50  0001 C CNN
F 3 "~" H 2650 3750 50  0001 C CNN
	1    2650 3750
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C3
U 1 1 5EF6C5F5
P 2650 4350
F 0 "C3" V 2421 4350 50  0000 C CNN
F 1 "C_Small" V 2512 4350 50  0000 C CNN
F 2 "Capacitors_SMD:C_1206" H 2650 4350 50  0001 C CNN
F 3 "~" H 2650 4350 50  0001 C CNN
	1    2650 4350
	0    1    1    0   
$EndComp
Text Notes 2400 3900 0    39   ~ 0
DIN-5\npin 4
Text Notes 2400 4350 0    39   ~ 0
DIN-5\npin 5
Wire Wire Line
	2350 3750 2550 3750
Wire Wire Line
	2750 3750 2850 3750
Wire Wire Line
	2550 4350 2350 4350
Text Notes 4600 6800 2    39   ~ 0
DIN-5\npin 4
Text Notes 2800 6950 2    39   ~ 0
DIN-5\npin 5
Text Notes 3450 5200 2    39   ~ 0
H11L3 is inverting
$Comp
L 74xx:74LS14 U?
U 4 1 5F9BB4CF
P 2500 5250
AR Path="/5EEDD9EE/5F9BB4CF" Ref="U?"  Part="6" 
AR Path="/5F9B7653/5F9BB4CF" Ref="U16"  Part="4" 
AR Path="/5F1CEF26/5F9BB4CF" Ref="U1"  Part="4" 
F 0 "U1" V 2546 5070 50  0000 R CNN
F 1 "74LS14" V 2455 5070 50  0000 R CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 2500 5250 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS14" H 2500 5250 50  0001 C CNN
	4    2500 5250
	-1   0    0    1   
$EndComp
Wire Wire Line
	1800 5250 2200 5250
Wire Wire Line
	1800 4150 2350 4150
Wire Wire Line
	1800 4050 2350 4050
Text Notes 700  2950 0    50   ~ 0
Possible MIDI connectors\n\nPC game port D-sub 15 carried MIDI. Here is an adaptor:\nhttps://www.excelvalley.com/product/midi-joystick-audio-cable-1-8m/\nThe DB15 is 40mm, same width as 2 DIN 41652 ports.\n\nHere is a TRS jack adaptor. There is more than one type?? Beatstep/Beatstep Pro\nhttps://www.arturia.com/store/accessories/beatstepproreplacementcablekit\nhttps://www.excelvalley.com/product/midi-adapter-breakout-cables/\n\n\n\nTradeoff\nPC game port - same size, extra cost, peripherals rare\nTRS jack - bit smaller, but extra dongle needed\nSerial - already need a dongle.. when for direct to DB9?\nIf there were a DB9 to MIDI in/out...\n\nCred - nothing says MIDI like a DIN-5\n\nYamaha Reface has one mini-DIN-6 . Also on Tenori, sw1000xg\nhttps://www.yamahasynth.com/ask-a-question/reface-midi-mini-din-pin-out\nHere is a breakout for $14\nhttps://www.fullcompass.com/prod/517038-yamaha-zp893500-cable-midi-breakout-cable-for-reface-cp-reface-dx\nPinout here: https://tonnemansblog.wordpress.com/tenori-on-midi-cable-pin-layout/\n\nhttps://www.yamahasynth.com/ask-a-question/reface-midi-din-adaptor\n\nsw1000xg was 1998 Yamaha professional studio card.
Text HLabel 1800 5250 0    50   Input ~ 0
MIDI_IN
Text HLabel 1800 6300 0    50   Input ~ 0
MIDI_OUT
Text HLabel 1800 4050 0    50   Input ~ 0
MIDI_IN_P4
Text HLabel 1800 4150 0    50   Input ~ 0
MIDI_IN_P5
Wire Wire Line
	3650 6950 3350 6950
Text HLabel 1800 7050 0    50   Input ~ 0
MIDI_OUT_P4
Text HLabel 1800 6950 0    50   Input ~ 0
MIDI_OUT_P5
Wire Wire Line
	1800 6950 3150 6950
Connection ~ 3800 3750
Wire Wire Line
	4300 4050 4300 5250
Wire Wire Line
	2800 5250 4300 5250
Text HLabel 1750 7550 0    50   Input ~ 0
MIDI_IN_P2
Text HLabel 1750 7300 0    50   Input ~ 0
MIDI_OUT_P2
$Comp
L Device:C_Small C1
U 1 1 5F22FDE9
P 2200 7300
F 0 "C1" V 1971 7300 50  0000 C CNN
F 1 "0.1uF" V 2062 7300 50  0000 C CNN
F 2 "Capacitors_SMD:C_1206" H 2200 7300 50  0001 C CNN
F 3 "~" H 2200 7300 50  0001 C CNN
	1    2200 7300
	0    1    1    0   
$EndComp
Wire Wire Line
	1750 7300 2100 7300
Text GLabel 5400 7550 2    50   Input ~ 0
GND
$Comp
L 74xx:74LS14 U?
U 7 1 5F231BA9
P 5250 6750
AR Path="/5EEDD9EE/5F231BA9" Ref="U?"  Part="2" 
AR Path="/5F9B7653/5F231BA9" Ref="U?"  Part="6" 
AR Path="/5F1CEF26/5F231BA9" Ref="U1"  Part="7" 
F 0 "U1" V 5204 6930 50  0000 L CNN
F 1 "74LS14" V 5295 6930 50  0000 L CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 5250 6750 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS14" H 5250 6750 50  0001 C CNN
	7    5250 6750
	1    0    0    -1  
$EndComp
Wire Wire Line
	3650 6950 3650 6300
Wire Wire Line
	3650 6300 3150 6300
Wire Wire Line
	1800 6300 1950 6300
Wire Wire Line
	4550 6600 4550 7050
Wire Wire Line
	1800 7050 4550 7050
Wire Wire Line
	2300 7300 5250 7300
Wire Wire Line
	5250 6250 4550 6250
$Comp
L Device:C_Small C2
U 1 1 5F245927
P 2200 7550
F 0 "C2" V 1971 7550 50  0000 C CNN
F 1 "0.1uF" V 2062 7550 50  0000 C CNN
F 2 "Capacitors_SMD:C_1206" H 2200 7550 50  0001 C CNN
F 3 "~" H 2200 7550 50  0001 C CNN
	1    2200 7550
	0    1    1    0   
$EndComp
Wire Wire Line
	1750 7550 2100 7550
Wire Wire Line
	5250 7550 5250 7300
Wire Wire Line
	5400 7550 5250 7550
Connection ~ 5250 7550
Wire Wire Line
	2300 7550 5250 7550
Wire Wire Line
	5250 7250 5250 7300
Connection ~ 5250 7300
$EndSCHEMATC
