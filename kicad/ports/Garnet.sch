EESchema Schematic File Version 4
LIBS:Garnet-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:DIN-5_180degree J6
U 1 1 5F009D44
P 2350 4100
F 0 "J6" H 2350 3825 50  0000 C CNN
F 1 "MIDI IN" H 2350 3734 50  0000 C CNN
F 2 "Connect:SDS-50J" H 2350 4100 50  0001 C CNN
F 3 "http://www.mouser.com/ds/2/18/40_c091_abd_e-75918.pdf" H 2350 4100 50  0001 C CNN
	1    2350 4100
	-1   0    0    1   
$EndComp
$Comp
L Connector:DIN-5_180degree J7
U 1 1 5F00A0B9
P 1450 4100
F 0 "J7" H 1450 3825 50  0000 C CNN
F 1 "MIDI OUT" H 1450 3734 50  0000 C CNN
F 2 "Connect:SDS-50J" H 1450 4100 50  0001 C CNN
F 3 "http://www.mouser.com/ds/2/18/40_c091_abd_e-75918.pdf" H 1450 4100 50  0001 C CNN
	1    1450 4100
	-1   0    0    1   
$EndComp
Text Notes 7100 7050 0    79   ~ 0
What's on other cards:\nVideo\n4x RCA jacks\nDigital Audio
$Comp
L Gemini:PS2_Keyboard_Mouse_Stacked J2
U 1 1 5F098F73
P 1700 6050
F 0 "J2" H 1594 7026 50  0000 C CNN
F 1 "PS2_Keyboard_Mouse_Stacked" H 1594 6935 50  0000 C CNN
F 2 "Gemini:PS2_Keyboard_Mouse_Stacked" H 1700 6070 50  0001 C CNN
F 3 "" H 1700 6070 50  0000 C CNN
	1    1700 6050
	1    0    0    -1  
$EndComp
Text Notes 9450 7000 0    100  ~ 0
On CRU Front Panel:\nFTDI\nPMOD
$Sheet
S 5900 3150 900  900 
U 5F1CEF26
F0 "MIDI Ports" 50
F1 "MIDI_Ports.sch" 50
F2 "MIDI_IN" I L 5900 3600 50 
F3 "MIDI_OUT" I L 5900 3700 50 
F4 "MIDI_IN_P4" I L 5900 3500 50 
F5 "MIDI_IN_P5" I L 5900 3400 50 
F6 "MIDI_OUT_P4" I L 5900 3300 50 
F7 "MIDI_OUT_P5" I L 5900 3200 50 
F8 "MIDI_IN_P2" I L 5900 3800 50 
F9 "MIDI_OUT_P2" I L 5900 3900 50 
$EndSheet
Wire Wire Line
	5900 3200 1000 3200
Wire Wire Line
	1000 3200 1000 4200
Wire Wire Line
	1000 4200 1150 4200
Wire Wire Line
	1750 4200 1850 4200
Wire Wire Line
	1850 4200 1850 3300
Wire Wire Line
	1850 3300 5900 3300
Wire Wire Line
	5900 3400 1950 3400
Wire Wire Line
	2050 4200 1950 4200
Wire Wire Line
	1950 4200 1950 3400
Wire Wire Line
	2650 4200 2750 4200
Wire Wire Line
	2750 4200 2750 3500
Wire Wire Line
	5900 3700 5550 3700
Wire Wire Line
	5450 3600 5900 3600
Text GLabel 5800 1850 0    50   Input ~ 0
GAME_ALPHA
Text GLabel 6300 2150 2    50   Input ~ 0
GND
Text GLabel 6300 2250 2    50   Input ~ 0
GND
Wire Wire Line
	2750 3500 5900 3500
Wire Wire Line
	2350 4400 5050 4400
Wire Wire Line
	5050 4400 5050 3800
Wire Wire Line
	5050 3800 5900 3800
Wire Wire Line
	5150 3900 5150 4500
Wire Wire Line
	1450 4500 1450 4400
Wire Wire Line
	5150 3900 5900 3900
Wire Wire Line
	1450 4500 5150 4500
Text Notes 8150 7400 0    100  ~ 0
Not Included:\nHexBus 1,2\nDB-25 RS232\nPIO
$Comp
L Connector_Generic:Conn_01x10 J1
U 1 1 5F2976AD
P 6000 5150
F 0 "J1" H 6000 4500 50  0000 C CNN
F 1 "COM1_HDR" H 5950 5700 50  0000 C CNN
F 2 "Connector_IDC:IDC-Header_2x05_P2.54mm_Horizontal" H 6000 5150 50  0001 C CNN
F 3 "~" H 6000 5150 50  0001 C CNN
	1    6000 5150
	1    0    0    1   
$EndComp
Text Notes 7200 2300 0    79   ~ 0
Atari Joysticks
Text Label 2000 5950 0    50   ~ 0
B_KBD_CLK
Text Label 2000 6150 0    50   ~ 0
B_KBD_DATA
Text GLabel 1400 6050 0    50   Input ~ 0
+5V
NoConn ~ 1400 5950
NoConn ~ 1400 6150
NoConn ~ 2000 6050
Text Label 2000 5450 0    50   ~ 0
B_MUS_CLK
Text Label 2000 5650 0    50   ~ 0
B_MUS_DATA
Text GLabel 1400 5550 0    50   Input ~ 0
+5V
NoConn ~ 2000 5550
NoConn ~ 1400 5650
NoConn ~ 1400 5450
Text Notes 12000 5700 0    50   ~ 0
Use dual footprint?\n   DB15 plus two DB9 option?\n\nDB15 mounted underneath? \n   keep right side up..\n   use board header.. \n   expensive, optional\n   makes DB9 the cheaper option\n
Text Notes 12000 6300 0    50   ~ 0
Use AUD_RET for LINE IN.\n\nPC uses it to go to back \npanel speaker output.\nSpeaker switched off by \nheadphone insertion.
$Comp
L Connector:Conn_01x15_Male J9
U 1 1 5F0E8952
P 8300 6000
F 0 "J9" V 9000 6500 50  0000 C CNN
F 1 "4A Keyboard" V 9000 5900 50  0000 C CNN
F 2 "Connector_IDC:IDC-Header_2x08_P2.54mm_Horizontal" H 8300 6000 50  0001 C CNN
F 3 "~" H 8300 6000 50  0001 C CNN
	1    8300 6000
	0    -1   -1   0   
$EndComp
Text GLabel 7600 5800 1    50   Input ~ 0
KEY_ROW2
Text GLabel 7700 5800 1    50   Input ~ 0
KEY_ROW3
Text GLabel 7800 5800 1    50   Input ~ 0
KEY_ROW5
Text GLabel 7900 5800 1    50   Input ~ 0
KEY_ROW1
Text GLabel 8000 5800 1    50   Input ~ 0
KEY_ROW0
Text GLabel 8100 5800 1    50   Input ~ 0
KEY_ALPHA
Text GLabel 8200 5800 1    50   Input ~ 0
KEY_ROW4
Text GLabel 8300 5800 1    50   Input ~ 0
KEY_COL5
Text GLabel 8400 5800 1    50   Input ~ 0
KEY_COL4
Text GLabel 8500 5800 1    50   Input ~ 0
KEY_ROW6
Text GLabel 8600 5800 1    50   Input ~ 0
KEY_ROW7
Text GLabel 8700 5800 1    50   Input ~ 0
KEY_COL0
Text GLabel 8800 5800 1    50   Input ~ 0
KEY_COL1
Text GLabel 8900 5800 1    50   Input ~ 0
KEY_COL2
Text GLabel 9000 5800 1    50   Input ~ 0
KEY_COL3
Text Notes 10400 2050 2    50   ~ 0
http://www.unige.ch/medecine/nouspikel/ti99/keyboard.htm
$Sheet
S 10100 600  1050 1150
U 5F1062E1
F0 "Joystick Keyboard" 50
F1 "JoystickKeyboard.sch" 50
$EndSheet
$Comp
L Connector:DB9_Male_MountingHoles J4
U 1 1 5F0E3601
P 8900 4850
F 0 "J4" V 9100 4550 50  0000 L CNN
F 1 "Joystick2" V 9100 4900 50  0000 L CNN
F 2 "Connector_Dsub:DSUB-9_Male_Horizontal_P2.77x2.54mm_EdgePinOffset9.40mm" H 8900 4850 50  0001 C CNN
F 3 " ~" H 8900 4850 50  0001 C CNN
	1    8900 4850
	0    1    1    0   
$EndComp
$Comp
L Connector:DB9_Male_MountingHoles J3
U 1 1 5F11EB66
P 7550 4850
F 0 "J3" V 7750 4550 50  0000 L CNN
F 1 "Joystick 1" V 7750 4889 50  0000 L CNN
F 2 "Connector_Dsub:DSUB-9_Male_Horizontal_P2.77x2.54mm_EdgePinOffset9.40mm" H 7550 4850 50  0001 C CNN
F 3 " ~" H 7550 4850 50  0001 C CNN
	1    7550 4850
	0    1    1    0   
$EndComp
Text GLabel 9600 3650 2    50   Input ~ 0
JOY_B1
Text GLabel 9600 3350 2    50   Input ~ 0
JOY_LEFT
Text GLabel 9600 3250 2    50   Input ~ 0
JOY_DOWN
Text GLabel 9600 3450 2    50   Input ~ 0
JOY_RIGHT
Text GLabel 9600 2950 2    50   Input ~ 0
JOY2_TEST
Text GLabel 9600 2850 2    50   Input ~ 0
JOY1_TEST
Text GLabel 9600 3150 2    50   Input ~ 0
JOY_UP
Wire Wire Line
	9600 3150 8500 3150
Wire Wire Line
	7150 3150 8500 3150
Connection ~ 8500 3150
Wire Wire Line
	9600 3250 8700 3250
Connection ~ 8700 3250
Wire Wire Line
	8700 3250 7350 3250
Wire Wire Line
	9600 3350 8900 3350
Connection ~ 8900 3350
Wire Wire Line
	8900 3350 7550 3350
Wire Wire Line
	9600 3450 9100 3450
Connection ~ 9100 3450
Wire Wire Line
	9100 3450 7750 3450
Wire Wire Line
	9600 3650 8600 3650
Wire Wire Line
	8600 3650 7250 3650
Connection ~ 8600 3650
Wire Wire Line
	9600 2850 7650 2850
Wire Wire Line
	9600 2950 9000 2950
Wire Wire Line
	9600 2650 8800 2650
Wire Wire Line
	8800 2650 7450 2650
Connection ~ 8800 2650
Text GLabel 9600 4150 2    50   Input ~ 0
PADDLE2
Text GLabel 9600 4250 2    50   Input ~ 0
PADDLE1
Wire Wire Line
	7150 3150 7150 4550
Wire Wire Line
	7250 3650 7250 4550
Wire Wire Line
	7350 3250 7350 4550
Wire Wire Line
	7450 2650 7450 4550
Wire Wire Line
	7550 3350 7550 4550
Wire Wire Line
	7650 2850 7650 4550
Wire Wire Line
	7750 3450 7750 4550
Wire Wire Line
	8500 3150 8500 4550
Wire Wire Line
	8700 3250 8700 4550
Wire Wire Line
	8600 3650 8600 4550
Wire Wire Line
	8800 2650 8800 4550
Wire Wire Line
	8900 3350 8900 4550
Wire Wire Line
	9000 2950 9000 4550
Wire Wire Line
	9100 3450 9100 4550
Wire Wire Line
	9600 4150 9300 4150
Wire Wire Line
	9300 4150 9300 4550
Wire Wire Line
	9300 4150 7950 4150
Wire Wire Line
	7950 4150 7950 4550
Connection ~ 9300 4150
Wire Wire Line
	9600 4250 9200 4250
Wire Wire Line
	9200 4250 9200 4550
Wire Wire Line
	9200 4250 7850 4250
Wire Wire Line
	7850 4250 7850 4550
Connection ~ 9200 4250
Text Notes 10050 2700 0    50   ~ 0
C-64 +5V, MSX B2
Text Notes 10050 2900 0    50   ~ 0
2600 GND
Text Notes 10050 3000 0    50   ~ 0
2600 GND, C-64 GND
Text Notes 10050 3200 0    50   ~ 0
2600 UP
Text Notes 10500 3300 2    50   ~ 0
2600 DOWN
Text Notes 10050 3400 0    50   ~ 0
2600 LEFT
Text Notes 10050 3500 0    50   ~ 0
2600 RIGHT
Text Notes 10050 3700 0    50   ~ 0
2600 FIRE
Text Notes 10050 4200 0    50   ~ 0
2600 RIGHT PADDLE, C-64 Y
Text Notes 10050 4300 0    50   ~ 0
2600 LEFT PADDLE, C-64 X
Text Notes 10050 4650 0    50   ~ 0
Two paddles share one DB9.\nFour paddles can be plugged\nin at once. 
Wire Wire Line
	5450 3600 5450 1950
Wire Wire Line
	5450 1950 5800 1950
Wire Wire Line
	5550 3700 5550 2250
Wire Wire Line
	5550 2250 5800 2250
Text GLabel 6300 2350 2    50   Input ~ 0
GAME_ROW2
Text GLabel 6300 2450 2    50   Input ~ 0
GAME_ROW0
Text GLabel 5800 2450 0    50   Input ~ 0
GAME_ROW1
Text GLabel 6300 1850 2    50   Input ~ 0
GAME_COL2
Text GLabel 5800 2350 0    50   Input ~ 0
GAME_ROW3
Text GLabel 5800 2550 0    50   Input ~ 0
GAME_COL1
Text GLabel 5800 2150 0    50   Input ~ 0
GAME_ROW4
Text GLabel 6300 2550 2    50   Input ~ 0
GAME_COL0
Text GLabel 5800 2050 0    50   Input ~ 0
GAME_ROW6
Text GLabel 6300 2050 2    50   Input ~ 0
GAME_ROW5
Text GLabel 6300 1950 2    50   Input ~ 0
GAME_ROW7
Text GLabel 3900 6050 3    50   Input ~ 0
GND
Text GLabel 4900 5750 1    50   Input ~ 0
GND
Text Label 4800 5750 1    50   ~ 0
RI
Text Label 4700 5750 1    50   ~ 0
DTR
Text Label 4600 5750 1    50   ~ 0
CTS
Text Label 4500 5750 1    50   ~ 0
TXD
Text Label 4400 5750 1    50   ~ 0
RTS
Text Label 4300 5750 1    50   ~ 0
RXD
Text Label 4200 5750 1    50   ~ 0
DSR
Text Label 4100 5750 1    50   ~ 0
DCD
$Comp
L Connector:DB9_Male_MountingHoles J8
U 1 1 5F002EBC
P 4500 6050
F 0 "J8" V 4700 5750 50  0000 L CNN
F 1 "RS232 DTE" V 4700 6100 50  0000 L CNN
F 2 "Connector_Dsub:DSUB-9_Male_Horizontal_P2.77x2.84mm_EdgePinOffset4.94mm_Housed_MountingHolesOffset7.48mm" H 4500 6050 50  0001 C CNN
F 3 " ~" H 4500 6050 50  0001 C CNN
	1    4500 6050
	0    1    1    0   
$EndComp
Wire Wire Line
	5800 4750 4800 4750
Wire Wire Line
	4800 4750 4800 5750
Wire Wire Line
	5800 4850 4600 4850
Wire Wire Line
	4600 4850 4600 5750
Wire Wire Line
	5800 4950 4400 4950
Wire Wire Line
	4400 4950 4400 5750
Wire Wire Line
	5800 5050 4200 5050
Wire Wire Line
	4200 5050 4200 5750
Wire Wire Line
	5800 5150 4900 5150
Wire Wire Line
	4900 5150 4900 5750
Wire Wire Line
	5800 5250 4700 5250
Wire Wire Line
	4700 5250 4700 5750
Wire Wire Line
	5800 5350 4500 5350
Wire Wire Line
	4500 5350 4500 5750
Wire Wire Line
	5800 5450 4300 5450
Wire Wire Line
	4300 5450 4300 5750
Wire Wire Line
	5800 5550 4100 5550
Wire Wire Line
	4100 5550 4100 5750
NoConn ~ 5800 4650
NoConn ~ 8300 4850
NoConn ~ 6950 4850
NoConn ~ 2650 4100
NoConn ~ 2050 4100
NoConn ~ 1750 4100
NoConn ~ 1150 4100
$Comp
L Connector_Generic:Conn_02x08_Odd_Even J5
U 1 1 5F24CA3B
P 6100 2250
F 0 "J5" H 6150 1625 50  0000 C CNN
F 1 "GamePortHdr" H 6150 1716 50  0000 C CNN
F 2 "Connector_IDC:IDC-Header_2x08_P2.54mm_Vertical" H 6100 2250 50  0001 C CNN
F 3 "~" H 6100 2250 50  0001 C CNN
	1    6100 2250
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_02x04_Top_Bottom J14
U 1 1 5F1F14FB
P 3050 5650
F 0 "J14" H 3100 5225 50  0000 C CNN
F 1 "KBD_MOUSE_HDR" H 3100 5316 50  0000 C CNN
F 2 "Connector_IDC:IDC-Header_2x04_P2.54mm_Vertical" H 3050 5650 50  0001 C CNN
F 3 "~" H 3050 5650 50  0001 C CNN
	1    3050 5650
	-1   0    0    1   
$EndComp
Wire Wire Line
	2750 5400 2750 5450
Wire Wire Line
	2750 5650 2000 5650
Wire Wire Line
	2000 5450 2000 5400
Wire Wire Line
	2000 5400 2750 5400
Wire Wire Line
	2000 5950 3400 5950
Wire Wire Line
	3400 5950 3400 5450
Wire Wire Line
	3400 5450 3250 5450
Wire Wire Line
	3250 5650 3300 5650
Wire Wire Line
	3300 5650 3300 6150
Wire Wire Line
	3300 6150 2000 6150
Text GLabel 3250 5550 2    50   Input ~ 0
GND
Text GLabel 3250 5750 2    50   Input ~ 0
GND
Text GLabel 2750 5550 0    50   Input ~ 0
GND
Text GLabel 2750 5750 0    50   Input ~ 0
GND
Text Notes 7300 2450 0    50   ~ 0
https://www.epanorama.net/documents/joystick/ataristick.html
Text GLabel 9600 2650 2    50   Input ~ 0
JOY_B2
NoConn ~ 1000 400 
$Comp
L Motor:Fan M1
U 1 1 5F407E98
P 1000 1750
F 0 "M1" V 705 1800 50  0000 C CNN
F 1 "Fan" V 796 1800 50  0000 C CNN
F 2 "Connect:Fan_Pin_Header_Straight_1x03" H 1000 1760 50  0001 C CNN
F 3 "~" H 1000 1760 50  0001 C CNN
	1    1000 1750
	0    1    1    0   
$EndComp
$Comp
L Connector_Generic:Conn_02x10_Top_Bottom J13
U 1 1 5F407E9E
P 2050 1250
F 0 "J13" H 2100 1867 50  0000 C CNN
F 1 "ATX Power" H 2100 1776 50  0000 C CNN
F 2 "Connectors_Molex:Molex_MiniFit-JR-5556-20A_2x10x4.20mm_Straight" H 2050 1250 50  0001 C CNN
F 3 "~" H 2050 1250 50  0001 C CNN
	1    2050 1250
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_SPST_Lamp SW1
U 1 1 5F407EA4
P 3250 1250
F 0 "SW1" H 3250 1580 50  0000 C CNN
F 1 "SW_SPST_Lamp" H 3250 1489 50  0000 C CNN
F 2 "Connectors_Molex:Molex_KK-6410-04_04x2.54mm_Straight" H 3250 1550 50  0001 C CNN
F 3 "~" H 3250 1550 50  0001 C CNN
	1    3250 1250
	1    0    0    -1  
$EndComp
Text GLabel 1850 1050 0    50   Input ~ 0
GND
Text GLabel 1850 1150 0    50   Input ~ 0
+5V
Text GLabel 1850 1250 0    50   Input ~ 0
GND
Text GLabel 1850 1350 0    50   Input ~ 0
+5V
Text GLabel 1850 1450 0    50   Input ~ 0
GND
Text GLabel 2350 1650 2    50   Input ~ 0
+5V
Text GLabel 2350 1750 2    50   Input ~ 0
+5V
Text GLabel 2350 1250 2    50   Input ~ 0
GND
Text GLabel 2350 1350 2    50   Input ~ 0
GND
Text GLabel 2350 1450 2    50   Input ~ 0
GND
NoConn ~ 2350 1550
Text GLabel 1850 850  0    50   Input ~ 0
+3.3V
Text GLabel 1850 950  0    50   Input ~ 0
+3.3V
Text GLabel 2350 850  2    50   Input ~ 0
+3.3V
Wire Wire Line
	3450 1150 3450 1050
Wire Wire Line
	3450 1050 2350 1050
Wire Wire Line
	2350 1150 3050 1150
Text Label 2650 1150 0    50   ~ 0
PWR_ON*
Wire Wire Line
	800  1750 700  1750
Wire Wire Line
	700  1450 1850 1450
Text GLabel 2350 1050 2    50   Input ~ 0
GND
Wire Wire Line
	700  1450 700  1750
$Comp
L power:+5V #PWR01
U 1 1 5F407EC3
P 700 1350
F 0 "#PWR01" H 700 1200 50  0001 C CNN
F 1 "+5V" H 715 1523 50  0000 C CNN
F 2 "" H 700 1350 50  0001 C CNN
F 3 "" H 700 1350 50  0001 C CNN
	1    700  1350
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG01
U 1 1 5F407EC9
P 1250 1350
F 0 "#FLG01" H 1250 1425 50  0001 C CNN
F 1 "PWR_FLAG" H 1250 1523 50  0000 C CNN
F 2 "" H 1250 1350 50  0001 C CNN
F 3 "~" H 1250 1350 50  0001 C CNN
	1    1250 1350
	1    0    0    -1  
$EndComp
Wire Wire Line
	1250 1350 700  1350
Wire Wire Line
	1250 1350 1850 1350
Connection ~ 1250 1350
NoConn ~ 1850 1550
NoConn ~ 1850 1650
NoConn ~ 2350 950 
Text Label 1800 1750 2    50   ~ 0
+12V1
$Comp
L Motor:Fan M2
U 1 1 5F410948
P 1000 2350
F 0 "M2" V 705 2400 50  0000 C CNN
F 1 "Fan" V 796 2400 50  0000 C CNN
F 2 "Connect:Fan_Pin_Header_Straight_1x03" H 1000 2360 50  0001 C CNN
F 3 "~" H 1000 2360 50  0001 C CNN
	1    1000 2350
	0    1    1    0   
$EndComp
$Comp
L Motor:Fan M3
U 1 1 5F410CFE
P 1000 2950
F 0 "M3" V 705 3000 50  0000 C CNN
F 1 "Fan" V 796 3000 50  0000 C CNN
F 2 "Connect:Fan_Pin_Header_Straight_1x03" H 1000 2960 50  0001 C CNN
F 3 "~" H 1000 2960 50  0001 C CNN
	1    1000 2950
	0    1    1    0   
$EndComp
Wire Wire Line
	1300 2950 1300 2350
Connection ~ 1300 1750
Connection ~ 1300 2350
Wire Wire Line
	1300 2350 1300 1750
Wire Wire Line
	800  2950 800  2350
Connection ~ 800  1750
Connection ~ 800  2350
Wire Wire Line
	800  2350 800  1750
$Comp
L Mechanical:MountingHole H1
U 1 1 5F4BEB56
P 7300 650
F 0 "H1" H 7400 696 50  0000 L CNN
F 1 "Spacer" H 7400 605 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3" H 7300 650 50  0001 C CNN
F 3 "~" H 7300 650 50  0001 C CNN
	1    7300 650 
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 5F4DBB7C
P 7300 850
F 0 "H2" H 7400 896 50  0000 L CNN
F 1 "Spacer" H 7400 805 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3" H 7300 850 50  0001 C CNN
F 3 "~" H 7300 850 50  0001 C CNN
	1    7300 850 
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 5F4E1388
P 7300 1050
F 0 "H3" H 7400 1096 50  0000 L CNN
F 1 "Spacer" H 7400 1005 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3" H 7300 1050 50  0001 C CNN
F 3 "~" H 7300 1050 50  0001 C CNN
	1    7300 1050
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H4
U 1 1 5F4E1392
P 7300 1250
F 0 "H4" H 7400 1296 50  0000 L CNN
F 1 "Spacer" H 7400 1205 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3" H 7300 1250 50  0001 C CNN
F 3 "~" H 7300 1250 50  0001 C CNN
	1    7300 1250
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H5
U 1 1 5F4E6D30
P 7300 1450
F 0 "H5" H 7400 1496 50  0000 L CNN
F 1 "Spacer" H 7400 1405 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3" H 7300 1450 50  0001 C CNN
F 3 "~" H 7300 1450 50  0001 C CNN
	1    7300 1450
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H6
U 1 1 5F4E6D3A
P 7300 1650
F 0 "H6" H 7400 1696 50  0000 L CNN
F 1 "Spacer" H 7400 1605 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3" H 7300 1650 50  0001 C CNN
F 3 "~" H 7300 1650 50  0001 C CNN
	1    7300 1650
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H7
U 1 1 5F4E6D44
P 7750 650
F 0 "H7" H 7850 696 50  0000 L CNN
F 1 "Spacer" H 7850 605 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3" H 7750 650 50  0001 C CNN
F 3 "~" H 7750 650 50  0001 C CNN
	1    7750 650 
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H8
U 1 1 5F4E6D4E
P 7750 850
F 0 "H8" H 7850 896 50  0000 L CNN
F 1 "Spacer" H 7850 805 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3" H 7750 850 50  0001 C CNN
F 3 "~" H 7750 850 50  0001 C CNN
	1    7750 850 
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H9
U 1 1 5F5E2207
P 8250 650
F 0 "H9" H 8350 696 50  0000 L CNN
F 1 "Spacer" H 8350 605 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3" H 8250 650 50  0001 C CNN
F 3 "~" H 8250 650 50  0001 C CNN
	1    8250 650 
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H10
U 1 1 5F5E2211
P 8250 850
F 0 "H10" H 8350 896 50  0000 L CNN
F 1 "Spacer" H 8350 805 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3" H 8250 850 50  0001 C CNN
F 3 "~" H 8250 850 50  0001 C CNN
	1    8250 850 
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H11
U 1 1 5F5E221B
P 8250 1050
F 0 "H11" H 8350 1096 50  0000 L CNN
F 1 "Spacer" H 8350 1005 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3" H 8250 1050 50  0001 C CNN
F 3 "~" H 8250 1050 50  0001 C CNN
	1    8250 1050
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H12
U 1 1 5F5E2225
P 8250 1250
F 0 "H12" H 8350 1296 50  0000 L CNN
F 1 "Spacer" H 8350 1205 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3" H 8250 1250 50  0001 C CNN
F 3 "~" H 8250 1250 50  0001 C CNN
	1    8250 1250
	1    0    0    -1  
$EndComp
$Sheet
S 9900 5200 1150 1000
U 5F8F85DB
F0 "Sheet5F8F85DA" 50
F1 "MMC.sch" 50
$EndSheet
$Comp
L Connector_Generic:Conn_02x08_Top_Bottom J17
U 1 1 5F9095BA
P 4500 6700
F 0 "J17" V 4504 7080 50  0000 L CNN
F 1 "PIO" V 4595 7080 50  0000 L CNN
F 2 "Connector_IDC:IDC-Header_2x08_P2.54mm_Horizontal" H 4500 6700 50  0001 C CNN
F 3 "~" H 4500 6700 50  0001 C CNN
	1    4500 6700
	0    1    1    0   
$EndComp
$Comp
L Mechanical:MountingHole H13
U 1 1 5F91E481
P 8250 1450
F 0 "H13" H 8350 1496 50  0000 L CNN
F 1 "Spacer" H 8350 1405 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3" H 8250 1450 50  0001 C CNN
F 3 "~" H 8250 1450 50  0001 C CNN
	1    8250 1450
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H14
U 1 1 5F91E48B
P 8250 1650
F 0 "H14" H 8350 1696 50  0000 L CNN
F 1 "Spacer" H 8350 1605 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3" H 8250 1650 50  0001 C CNN
F 3 "~" H 8250 1650 50  0001 C CNN
	1    8250 1650
	1    0    0    -1  
$EndComp
Wire Wire Line
	1300 1750 1850 1750
Text Notes 4100 1250 0    50   ~ 0
Not sure. Does R1966 type\nhave 1 anode for LED. \n1 common GND?
$Comp
L Device:R_Small R?
U 1 1 5F150BC8
P 3500 1450
AR Path="/5F1062E1/5F150BC8" Ref="R?"  Part="1" 
AR Path="/5F150BC8" Ref="R39"  Part="1" 
F 0 "R39" V 3304 1450 50  0000 C CNN
F 1 "470" V 3395 1450 50  0000 C CNN
F 2 "Resistors_SMD:R_1206" H 3500 1450 50  0001 C CNN
F 3 "~" H 3500 1450 50  0001 C CNN
	1    3500 1450
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 1250 3500 1250
Wire Wire Line
	2350 1650 3500 1650
Wire Wire Line
	3050 1250 2350 1250
Wire Wire Line
	3500 1350 3500 1250
Wire Wire Line
	3500 1550 3500 1650
$Comp
L Connector_Generic:Conn_02x08_Odd_Even J16
U 1 1 5F26A03F
P 6250 7150
AR Path="/5F26A03F" Ref="J16"  Part="1" 
AR Path="/5F1062E1/5F26A03F" Ref="J?"  Part="1" 
F 0 "J16" H 6300 6525 50  0000 C CNN
F 1 "PIOHdr" H 6300 6616 50  0000 C CNN
F 2 "Connector_IDC:IDC-Header_2x08_P2.54mm_Horizontal" H 6250 7150 50  0001 C CNN
F 3 "~" H 6250 7150 50  0001 C CNN
	1    6250 7150
	-1   0    0    1   
$EndComp
$EndSCHEMATC
