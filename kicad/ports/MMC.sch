EESchema Schematic File Version 4
LIBS:Garnet-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:SD_Card J?
U 1 1 5F9051FC
P 6250 2850
AR Path="/5F9051FC" Ref="J?"  Part="1" 
AR Path="/5F8F85DB/5F9051FC" Ref="J12"  Part="1" 
F 0 "J12" H 6250 3515 50  0000 C CNN
F 1 "MMC2" H 6250 3424 50  0000 C CNN
F 2 "Connect:SD_Card_Receptacle" H 6250 2850 50  0001 C CNN
F 3 "http://portal.fciconnect.com/Comergent//fci/drawing/10067847.pdf" H 6250 2850 50  0001 C CNN
	1    6250 2850
	-1   0    0    -1  
$EndComp
$Comp
L Connector:SD_Card J?
U 1 1 5F905202
P 3450 2850
AR Path="/5F905202" Ref="J?"  Part="1" 
AR Path="/5F8F85DB/5F905202" Ref="J11"  Part="1" 
F 0 "J11" H 3450 3515 50  0000 C CNN
F 1 "MMC1" H 3450 3424 50  0000 C CNN
F 2 "Connect:SD_Card_Receptacle" H 3450 2850 50  0001 C CNN
F 3 "http://portal.fciconnect.com/Comergent//fci/drawing/10067847.pdf" H 3450 2850 50  0001 C CNN
	1    3450 2850
	-1   0    0    -1  
$EndComp
Text GLabel 2550 3050 3    50   Input ~ 0
GND
Text GLabel 5350 3050 3    50   Input ~ 0
GND
Wire Wire Line
	5350 2950 5350 3050
Wire Wire Line
	2550 2950 2550 3050
NoConn ~ 4350 3250
NoConn ~ 4350 2450
Text Notes 6250 3550 2    50   ~ 0
http://elm-chan.org/docs/mmc/mmc_e.html
NoConn ~ 7150 3250
NoConn ~ 7150 2450
Text GLabel 7850 3250 0    50   Input ~ 0
+3V3
Text GLabel 8350 2750 2    50   Input ~ 0
GND
$Comp
L Connector_Generic:Conn_02x08_Odd_Even J?
U 1 1 5F905213
P 8050 2950
AR Path="/5F905213" Ref="J?"  Part="1" 
AR Path="/5F8F85DB/5F905213" Ref="J10"  Part="1" 
F 0 "J10" H 8100 2325 50  0000 C CNN
F 1 "MMC_Hdr" H 8100 2416 50  0000 C CNN
F 2 "Connector_IDC:IDC-Header_2x08_P2.54mm_Horizontal" H 8050 2950 50  0001 C CNN
F 3 "~" H 8050 2950 50  0001 C CNN
	1    8050 2950
	1    0    0    1   
$EndComp
Text GLabel 5350 2750 0    50   Input ~ 0
SPWP2
Text GLabel 5350 2650 0    50   Input ~ 0
SPCD2
Text GLabel 2550 2650 0    50   Input ~ 0
SPCD1
Text GLabel 2550 2750 0    50   Input ~ 0
SPWP1
Text GLabel 8350 3050 2    50   Input ~ 0
SPWP1
Text GLabel 8350 2650 2    50   Input ~ 0
SPCD1
Text GLabel 8350 2950 2    50   Input ~ 0
SPCD2
Text GLabel 8350 2550 2    50   Input ~ 0
SPWP2
Text GLabel 7150 2550 2    50   Input ~ 0
SPCS2
Text GLabel 8350 2850 2    50   Input ~ 0
GND
Text GLabel 4350 2750 2    50   Input ~ 0
GND
Text GLabel 7150 2650 2    50   Input ~ 0
SPDI2
Text GLabel 7150 3150 2    50   Input ~ 0
SPDO2
Text GLabel 7150 2950 2    50   Input ~ 0
SPCLK2
Text GLabel 7150 2850 2    50   Input ~ 0
+3V3
Text GLabel 7150 2750 2    50   Input ~ 0
GND
Text GLabel 7150 3050 2    50   Input ~ 0
GND
Text GLabel 8350 3150 2    50   Input ~ 0
SPCS2
Text GLabel 8350 3250 2    50   Input ~ 0
+3V3
Text GLabel 4350 2550 2    50   Input ~ 0
SPCS1
Text GLabel 7850 2550 0    50   Input ~ 0
SPCS1
Text GLabel 7850 2650 0    50   Input ~ 0
SPDI1
Text GLabel 7850 2750 0    50   Input ~ 0
SPDO2
Text GLabel 7850 2850 0    50   Input ~ 0
SPCLK2
Text GLabel 7850 2950 0    50   Input ~ 0
SPCLK1
Text GLabel 7850 3050 0    50   Input ~ 0
SPDO1
Text GLabel 7850 3150 0    50   Input ~ 0
SPDI2
Text GLabel 4350 2650 2    50   Input ~ 0
SPDI1
Text GLabel 4350 2950 2    50   Input ~ 0
SPCLK1
Text GLabel 4350 3150 2    50   Input ~ 0
SPDO1
Text GLabel 4350 2850 2    50   Input ~ 0
+3V3
Text GLabel 4350 3050 2    50   Input ~ 0
GND
$EndSCHEMATC
