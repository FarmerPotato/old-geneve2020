EESchema Schematic File Version 4
LIBS:Garnet-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_02x06_Top_Bottom J?
U 1 1 5F4C9D33
P 4850 3850
F 0 "J?" H 4800 4350 50  0000 L CNN
F 1 "Audio_3_Stacked" H 4950 3550 50  0000 L CNN
F 2 "Gemini:Phono_3_Stacked" H 4850 3850 50  0001 C CNN
F 3 "~" H 4850 3850 50  0001 C CNN
	1    4850 3850
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J?
U 1 1 5F4C9D39
P 6600 3850
F 0 "J?" H 6650 3425 50  0000 C CNN
F 1 "AC97_HDR" H 6650 3516 50  0000 C CNN
F 2 "Connector_IDC:IDC-Header_2x05_P2.54mm_Vertical" H 6600 3850 50  0001 C CNN
F 3 "~" H 6600 3850 50  0001 C CNN
	1    6600 3850
	-1   0    0    1   
$EndComp
Text GLabel 6800 3950 2    50   Input ~ 0
AUD_MIC_BIAS
Text GLabel 6800 4050 2    50   Input ~ 0
AUD_MIC
Text GLabel 6800 3850 2    50   Input ~ 0
AUD_FPOUT_R
Text GLabel 6800 3650 2    50   Input ~ 0
AUD_FPOUT_L
Text GLabel 6300 3650 0    50   Input ~ 0
AUD_RET_L
Text GLabel 6300 4050 0    50   Input ~ 0
AUD_GND
NoConn ~ 6800 3750
NoConn ~ 6300 3750
Text Notes 6100 3800 0    50   ~ 0
KEY
Text GLabel 6300 3850 0    50   Input ~ 0
AUD_RET_R
Text Notes 5050 3300 2    50   ~ 0
13 pins, unknown
Text GLabel 4550 4050 0    50   Input ~ 0
AUD_RET_L
Text GLabel 4550 3950 0    50   Input ~ 0
AUD_RET_R
Text GLabel 4550 3850 0    50   Input ~ 0
AUD_GND
Text GLabel 5050 4050 2    50   Input ~ 0
AUD_MIC_BIAS
Text GLabel 5050 3950 2    50   Input ~ 0
AUD_MIC
Text GLabel 5050 3550 2    50   Input ~ 0
AUD_FPOUT_L
Text GLabel 5050 3650 2    50   Input ~ 0
AUD_FPOUT_R
NoConn ~ 4550 3650
NoConn ~ 4550 3550
NoConn ~ 4550 3750
NoConn ~ 5050 3750
NoConn ~ 5050 3850
NoConn ~ 6350 3950
Text Label 6300 3950 2    50   ~ 0
AUD_VCC
Text Notes 6150 3550 2    50   ~ 0
Line In
$EndSCHEMATC
