EESchema Schematic File Version 4
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 74xx:74LS32 U?
U 5 1 607CCA71
P 3200 5300
AR Path="/607CCA71" Ref="U?"  Part="1" 
AR Path="/604F6A55/607CCA71" Ref="U?"  Part="5" 
AR Path="/604F6A55/607B7273/607CCA71" Ref="U?"  Part="5" 
F 0 "U?" H 3430 5346 50  0000 L CNN
F 1 "74LS32" H 3430 5255 50  0000 L CNN
F 2 "" H 3200 5300 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS32" H 3200 5300 50  0001 C CNN
	5    3200 5300
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS86 U?
U 5 1 607CCA77
P 4400 5300
AR Path="/607CCA77" Ref="U?"  Part="1" 
AR Path="/604F6A55/607CCA77" Ref="U?"  Part="5" 
AR Path="/604F6A55/607B7273/607CCA77" Ref="U?"  Part="5" 
F 0 "U?" H 4630 5346 50  0000 L CNN
F 1 "74LS86" H 4630 5255 50  0000 L CNN
F 2 "" H 4400 5300 50  0001 C CNN
F 3 "74xx/74ls86.pdf" H 4400 5300 50  0001 C CNN
	5    4400 5300
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS86 U?
U 4 1 607CCA7D
P 4250 3500
AR Path="/607CCA7D" Ref="U?"  Part="1" 
AR Path="/604F6A55/607CCA7D" Ref="U?"  Part="4" 
AR Path="/604F6A55/607B7273/607CCA7D" Ref="U?"  Part="4" 
F 0 "U?" H 4250 3825 50  0000 C CNN
F 1 "74LS86" H 4250 3734 50  0000 C CNN
F 2 "" H 4250 3500 50  0001 C CNN
F 3 "74xx/74ls86.pdf" H 4250 3500 50  0001 C CNN
	4    4250 3500
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS86 U?
U 3 1 607CCA83
P 4250 2900
AR Path="/607CCA83" Ref="U?"  Part="1" 
AR Path="/604F6A55/607CCA83" Ref="U?"  Part="3" 
AR Path="/604F6A55/607B7273/607CCA83" Ref="U?"  Part="3" 
F 0 "U?" H 4250 3225 50  0000 C CNN
F 1 "74LS86" H 4250 3134 50  0000 C CNN
F 2 "" H 4250 2900 50  0001 C CNN
F 3 "74xx/74ls86.pdf" H 4250 2900 50  0001 C CNN
	3    4250 2900
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS86 U?
U 2 1 607CCA89
P 4250 2300
AR Path="/607CCA89" Ref="U?"  Part="1" 
AR Path="/604F6A55/607CCA89" Ref="U?"  Part="2" 
AR Path="/604F6A55/607B7273/607CCA89" Ref="U?"  Part="2" 
F 0 "U?" H 4250 2625 50  0000 C CNN
F 1 "74LS86" H 4250 2534 50  0000 C CNN
F 2 "" H 4250 2300 50  0001 C CNN
F 3 "74xx/74ls86.pdf" H 4250 2300 50  0001 C CNN
	2    4250 2300
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS86 U?
U 1 1 607CCA8F
P 4250 1700
AR Path="/607CCA8F" Ref="U?"  Part="1" 
AR Path="/604F6A55/607CCA8F" Ref="U?"  Part="1" 
AR Path="/604F6A55/607B7273/607CCA8F" Ref="U?"  Part="1" 
F 0 "U?" H 4250 2025 50  0000 C CNN
F 1 "74LS86" H 4250 1934 50  0000 C CNN
F 2 "" H 4250 1700 50  0001 C CNN
F 3 "74xx/74ls86.pdf" H 4250 1700 50  0001 C CNN
	1    4250 1700
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS32 U?
U 3 1 607CCA95
P 6300 2600
AR Path="/607CCA95" Ref="U?"  Part="1" 
AR Path="/604F6A55/607CCA95" Ref="U?"  Part="3" 
AR Path="/604F6A55/607B7273/607CCA95" Ref="U?"  Part="3" 
F 0 "U?" H 6300 2925 50  0000 C CNN
F 1 "74LS32" H 6300 2834 50  0000 C CNN
F 2 "" H 6300 2600 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS32" H 6300 2600 50  0001 C CNN
	3    6300 2600
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS32 U?
U 2 1 607CCA9B
P 5300 3000
AR Path="/607CCA9B" Ref="U?"  Part="1" 
AR Path="/604F6A55/607CCA9B" Ref="U?"  Part="2" 
AR Path="/604F6A55/607B7273/607CCA9B" Ref="U?"  Part="2" 
F 0 "U?" H 5300 3325 50  0000 C CNN
F 1 "74LS32" H 5300 3234 50  0000 C CNN
F 2 "" H 5300 3000 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS32" H 5300 3000 50  0001 C CNN
	2    5300 3000
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS32 U?
U 1 1 607CCAA1
P 5300 2200
AR Path="/607CCAA1" Ref="U?"  Part="1" 
AR Path="/604F6A55/607CCAA1" Ref="U?"  Part="1" 
AR Path="/604F6A55/607B7273/607CCAA1" Ref="U?"  Part="1" 
F 0 "U?" H 5300 2525 50  0000 C CNN
F 1 "74LS32" H 5300 2434 50  0000 C CNN
F 2 "" H 5300 2200 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS32" H 5300 2200 50  0001 C CNN
	1    5300 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 2900 4550 2900
Wire Wire Line
	5000 3100 4650 3100
Wire Wire Line
	4650 3100 4650 3500
Wire Wire Line
	4650 3500 4550 3500
Wire Wire Line
	5000 2300 4550 2300
Wire Wire Line
	5000 2100 4650 2100
Wire Wire Line
	4650 2100 4650 1700
Wire Wire Line
	4650 1700 4550 1700
Wire Wire Line
	5600 2200 5700 2200
Wire Wire Line
	5700 2200 5700 2500
Wire Wire Line
	5700 3000 5600 3000
Wire Wire Line
	5700 2500 6000 2500
Wire Wire Line
	6000 2700 5700 2700
Wire Wire Line
	5700 2700 5700 3000
Text GLabel 3950 1600 0    50   Input ~ 0
A4
Text GLabel 3950 2200 0    50   Input ~ 0
A5
Text GLabel 3950 2800 0    50   Input ~ 0
A6
Text GLabel 3950 3400 0    50   Input ~ 0
A7
$Comp
L Device:Jumper_NO_Small JP?
U 1 1 607CCAB9
P 3000 1800
AR Path="/604F6A55/607CCAB9" Ref="JP?"  Part="1" 
AR Path="/604F6A55/607B7273/607CCAB9" Ref="JP?"  Part="1" 
F 0 "JP?" H 3000 1893 50  0000 C CNN
F 1 "Jumper_NO_Small" H 3000 1894 50  0001 C CNN
F 2 "" H 3000 1800 50  0001 C CNN
F 3 "~" H 3000 1800 50  0001 C CNN
	1    3000 1800
	1    0    0    -1  
$EndComp
$Comp
L Device:Jumper_NO_Small JP?
U 1 1 607CCABF
P 3000 2400
AR Path="/604F6A55/607CCABF" Ref="JP?"  Part="1" 
AR Path="/604F6A55/607B7273/607CCABF" Ref="JP?"  Part="1" 
F 0 "JP?" H 3000 2493 50  0000 C CNN
F 1 "Jumper_NO_Small" H 3000 2494 50  0001 C CNN
F 2 "" H 3000 2400 50  0001 C CNN
F 3 "~" H 3000 2400 50  0001 C CNN
	1    3000 2400
	1    0    0    -1  
$EndComp
$Comp
L Device:Jumper_NO_Small JP?
U 1 1 607CCAC5
P 3000 3000
AR Path="/604F6A55/607CCAC5" Ref="JP?"  Part="1" 
AR Path="/604F6A55/607B7273/607CCAC5" Ref="JP?"  Part="1" 
F 0 "JP?" H 3000 3093 50  0000 C CNN
F 1 "Jumper_NO_Small" H 3000 3094 50  0001 C CNN
F 2 "" H 3000 3000 50  0001 C CNN
F 3 "~" H 3000 3000 50  0001 C CNN
	1    3000 3000
	1    0    0    -1  
$EndComp
$Comp
L Device:Jumper_NO_Small JP?
U 1 1 607CCACB
P 3000 3600
AR Path="/604F6A55/607CCACB" Ref="JP?"  Part="1" 
AR Path="/604F6A55/607B7273/607CCACB" Ref="JP?"  Part="1" 
F 0 "JP?" H 3000 3693 50  0000 C CNN
F 1 "Jumper_NO_Small" H 3000 3694 50  0001 C CNN
F 2 "" H 3000 3600 50  0001 C CNN
F 3 "~" H 3000 3600 50  0001 C CNN
	1    3000 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	3950 3000 3550 3000
Wire Wire Line
	3100 3600 3750 3600
Wire Wire Line
	2900 1800 2700 1800
Wire Wire Line
	2700 1800 2700 2400
Wire Wire Line
	2700 3600 2900 3600
Wire Wire Line
	2900 3000 2700 3000
Connection ~ 2700 3000
Wire Wire Line
	2700 3000 2700 3600
Wire Wire Line
	2900 2400 2700 2400
Connection ~ 2700 2400
Wire Wire Line
	2700 2400 2700 3000
Text GLabel 2700 1600 0    50   Input ~ 0
+5V
Wire Wire Line
	2700 1600 2700 1800
Connection ~ 2700 1800
Wire Wire Line
	3100 2400 3350 2400
Connection ~ 3350 2400
Wire Wire Line
	3350 2400 3950 2400
Connection ~ 3550 3000
Wire Wire Line
	3550 3000 3100 3000
Connection ~ 3750 3600
Wire Wire Line
	3750 3600 3950 3600
Wire Wire Line
	3150 4000 3150 4200
Wire Wire Line
	3150 4200 3350 4200
Wire Wire Line
	3750 4200 3750 4000
Wire Wire Line
	3550 4000 3550 4200
Connection ~ 3550 4200
Wire Wire Line
	3550 4200 3750 4200
Wire Wire Line
	3350 4000 3350 4200
Connection ~ 3350 4200
Wire Wire Line
	3350 4200 3550 4200
Wire Wire Line
	3150 4200 2700 4200
Connection ~ 3150 4200
Text GLabel 2700 4200 0    50   Input ~ 0
GND
$Comp
L Device:R R?
U 1 1 607CCAF2
P 3350 3850
AR Path="/604F6A55/607CCAF2" Ref="R?"  Part="1" 
AR Path="/604F6A55/607B7273/607CCAF2" Ref="R?"  Part="1" 
F 0 "R?" H 3420 3896 50  0000 L CNN
F 1 "10K" V 3350 3750 50  0000 L CNN
F 2 "" V 3280 3850 50  0001 C CNN
F 3 "~" H 3350 3850 50  0001 C CNN
	1    3350 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	3350 3700 3350 2400
Wire Wire Line
	3150 1800 3950 1800
Wire Wire Line
	3100 1800 3150 1800
Connection ~ 3150 1800
$Comp
L Device:R R?
U 1 1 607CCAFC
P 3150 3850
AR Path="/604F6A55/607CCAFC" Ref="R?"  Part="1" 
AR Path="/604F6A55/607B7273/607CCAFC" Ref="R?"  Part="1" 
F 0 "R?" H 3220 3896 50  0000 L CNN
F 1 "10K" V 3150 3750 50  0000 L CNN
F 2 "" V 3080 3850 50  0001 C CNN
F 3 "~" H 3150 3850 50  0001 C CNN
	1    3150 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 3700 3150 1800
$Comp
L Device:R R?
U 1 1 607CCB03
P 3550 3850
AR Path="/604F6A55/607CCB03" Ref="R?"  Part="1" 
AR Path="/604F6A55/607B7273/607CCB03" Ref="R?"  Part="1" 
F 0 "R?" H 3620 3896 50  0000 L CNN
F 1 "10K" V 3550 3750 50  0000 L CNN
F 2 "" V 3480 3850 50  0001 C CNN
F 3 "~" H 3550 3850 50  0001 C CNN
	1    3550 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	3550 3700 3550 3000
$Comp
L Device:R R?
U 1 1 607CCB0A
P 3750 3850
AR Path="/604F6A55/607CCB0A" Ref="R?"  Part="1" 
AR Path="/604F6A55/607B7273/607CCB0A" Ref="R?"  Part="1" 
F 0 "R?" H 3820 3896 50  0000 L CNN
F 1 "10K" V 3750 3750 50  0000 L CNN
F 2 "" V 3680 3850 50  0001 C CNN
F 3 "~" H 3750 3850 50  0001 C CNN
	1    3750 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	3750 3700 3750 3600
Text GLabel 3200 4800 0    50   Input ~ 0
+5V
Text GLabel 4400 4800 0    50   Input ~ 0
+5V
$Comp
L 74xx:74LS32 U?
U 4 1 607CCB13
P 6300 3200
AR Path="/607CCB13" Ref="U?"  Part="1" 
AR Path="/604F6A55/607CCB13" Ref="U?"  Part="3" 
AR Path="/604F6A55/607B7273/607CCB13" Ref="U?"  Part="4" 
F 0 "U?" H 6300 3525 50  0000 C CNN
F 1 "74LS32" H 6300 3434 50  0000 C CNN
F 2 "" H 6300 3200 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS32" H 6300 3200 50  0001 C CNN
	4    6300 3200
	1    0    0    -1  
$EndComp
Text GLabel 6000 3100 0    50   Input ~ 0
A0
Text GLabel 6000 3300 0    50   Input ~ 0
A1
Text GLabel 7500 2500 0    50   Input ~ 0
A3
$Comp
L 74xx:74LS138 U?
U 1 1 607D101B
P 8000 2800
AR Path="/604F6A55/607D101B" Ref="U?"  Part="1" 
AR Path="/604F6A55/607B7273/607D101B" Ref="U?"  Part="1" 
F 0 "U?" H 8000 3581 50  0000 C CNN
F 1 "74LS138" H 8000 3490 50  0000 C CNN
F 2 "" H 8000 2800 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS138" H 8000 2800 50  0001 C CNN
	1    8000 2800
	1    0    0    -1  
$EndComp
Text Label 6650 2600 0    50   ~ 0
CRUBASE*
Wire Wire Line
	6600 2600 7100 2600
Text GLabel 8500 2600 2    50   Output ~ 0
CRUWE*
NoConn ~ 8500 2500
NoConn ~ 8500 2700
NoConn ~ 8500 2800
NoConn ~ 8500 2900
NoConn ~ 8500 3000
NoConn ~ 8500 3100
NoConn ~ 8500 3200
Text GLabel 8000 3500 0    50   Input ~ 0
GND
Text GLabel 8000 2200 0    50   Input ~ 0
+5V
Text GLabel 7500 2700 0    50   Input ~ 0
WE*
Wire Wire Line
	8500 2600 9600 2600
Wire Wire Line
	7500 3100 7100 3100
Wire Wire Line
	7100 3100 7100 2600
Text GLabel 7500 3000 0    50   Input ~ 0
MEMEN*
Wire Wire Line
	7500 3200 6600 3200
Text GLabel 7500 2600 0    50   Input ~ 0
A2
Text GLabel 8500 3000 2    50   Output ~ 0
CRUOE*
Text Label 6950 3200 2    50   ~ 0
A0A1*
Text GLabel 3200 5800 0    50   Input ~ 0
GND
Text GLabel 4400 5800 0    50   Input ~ 0
GND
$EndSCHEMATC
