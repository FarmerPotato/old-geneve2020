EESchema Schematic File Version 4
LIBS:forti-1-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 74xx:74LS259 U?
U 1 1 60C1FB44
P 5100 2900
AR Path="/604F6A55/60C1FB44" Ref="U?"  Part="1" 
AR Path="/604F6A55/60C1D1DF/60C1FB44" Ref="U6"  Part="1" 
F 0 "U6" H 5100 3781 50  0000 C CNN
F 1 "74LS259" H 5100 3690 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-16_3.9x9.9mm_Pitch1.27mm" H 5100 2900 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS259" H 5100 2900 50  0001 C CNN
	1    5100 2900
	1    0    0    -1  
$EndComp
Text GLabel 4600 3300 0    50   Input ~ 0
RESET*
Text GLabel 5100 3600 0    50   Input ~ 0
GND
Text GLabel 5100 2200 0    50   Input ~ 0
+5V
Text GLabel 4600 2500 0    50   Input ~ 0
A15
Text GLabel 4600 2900 0    50   Input ~ 0
A12
Text GLabel 4600 2800 0    50   Input ~ 0
A13
Text GLabel 4600 2700 0    50   Input ~ 0
A14
Wire Wire Line
	2800 3100 4600 3100
Text Label 5600 2500 0    50   ~ 0
CARDSEL
Wire Wire Line
	6000 1950 6000 2500
Wire Wire Line
	6000 2500 5600 2500
NoConn ~ 5600 2600
NoConn ~ 5600 2700
NoConn ~ 5600 2800
NoConn ~ 5600 2900
NoConn ~ 5600 3000
NoConn ~ 5600 3100
NoConn ~ 5600 3200
Text Notes 6150 3300 0    50   ~ 0
0 CARDSEL\n1 Ch1 0=M 1=L\n2 Ch2 0=M 1=L\n3 Ch3 0=M 1=R\n4 Ch4 0=M 1=R\n5 Ch1 Ch3 Clock divider LSBit\n6 Ch1 Ch3 Clock divider MSBit\n7 LED\n 
Wire Wire Line
	2800 3000 4250 3000
Wire Wire Line
	4250 3000 4250 1950
$EndSCHEMATC
