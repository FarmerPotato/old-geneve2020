EESchema Schematic File Version 4
LIBS:forti-1-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Memory_EPROM:27C64 U7
U 1 1 6063C8F6
P 5150 3300
F 0 "U7" H 5150 4481 50  0000 C CNN
F 1 "27C64" H 5150 4390 50  0000 C CNN
F 2 "Housings_DIP:DIP-28_W15.24mm_Socket_LongPads" H 5150 3300 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/11107M.pdf" H 5150 3300 50  0001 C CNN
	1    5150 3300
	1    0    0    -1  
$EndComp
Text GLabel 4750 3700 0    50   Input ~ 0
A3
Text GLabel 4750 3600 0    50   Input ~ 0
A4
Text GLabel 4750 3500 0    50   Input ~ 0
A5
Text GLabel 4750 3400 0    50   Input ~ 0
A6
Text GLabel 4750 3300 0    50   Input ~ 0
A7
Text GLabel 4750 3200 0    50   Input ~ 0
A8
Text GLabel 4750 3100 0    50   Input ~ 0
A9
Text GLabel 4750 3000 0    50   Input ~ 0
A10
Text GLabel 4750 2900 0    50   Input ~ 0
A11
Text GLabel 4750 2800 0    50   Input ~ 0
A12
Text GLabel 4750 2700 0    50   Input ~ 0
A13
Text GLabel 4750 2600 0    50   Input ~ 0
A14
Text GLabel 4750 2500 0    50   Input ~ 0
A15
Text GLabel 5550 3200 2    50   Output ~ 0
D0
Text GLabel 5550 3100 2    50   Output ~ 0
D1
Text GLabel 5550 3000 2    50   Output ~ 0
D2
Text GLabel 5550 2900 2    50   Output ~ 0
D3
Text GLabel 5550 2800 2    50   Output ~ 0
D4
Text GLabel 5550 2700 2    50   Output ~ 0
D5
Text GLabel 5550 2600 2    50   Output ~ 0
D6
Text GLabel 5550 2500 2    50   Output ~ 0
D7
Text GLabel 5150 4400 0    50   UnSpc ~ 0
GND
Text GLabel 5150 2300 0    50   UnSpc ~ 0
+5V
Text Notes 5200 1050 0    50   ~ 0
DSR\nA0=0\nA1=1\nA2=0\n
Text Notes 4550 1300 0    50   ~ 0
SND\nA0=1\nA1=0\nA2=0\nA3=0\nA4=0\nA5=1
Text Notes 4850 1150 0    50   ~ 0
CRU\nA0=0\nA1=0\nA2=0\nA3=1
Text GLabel 2050 2400 1    50   Input ~ 0
A0
Text GLabel 1450 4000 0    50   Input ~ 0
A1
Text GLabel 1450 3900 0    50   Input ~ 0
A2
Text GLabel 1450 4100 0    50   Input ~ 0
DBIN
Text GLabel 1450 4200 0    50   Input ~ 0
A3
Text GLabel 2450 2400 1    50   Input ~ 0
A4
Text GLabel 1950 2400 1    50   Input ~ 0
A5
Text GLabel 1450 4300 0    50   Input ~ 0
A6
Text GLabel 2850 3300 2    50   Input ~ 0
A7
Text GLabel 2850 3400 2    50   Input ~ 0
A15
Text GLabel 1450 3300 0    50   Input ~ 0
MEMEN*
$Comp
L Gemini:ATF22LV10C-28J U5
U 1 1 60799349
P 2050 3200
F 0 "U5" H 2894 3346 50  0000 L CNN
F 1 "ATF22LV10C-28J" H 1800 3200 50  0000 L CNN
F 2 "Sockets:PLCC28" H 2050 3150 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/doc0780.pdf" H 2050 3150 50  0001 C CNN
	1    2050 3200
	1    0    0    -1  
$EndComp
Text GLabel 2850 3100 2    50   UnSpc ~ 0
GND
Text GLabel 1450 3100 0    50   UnSpc ~ 0
GND
Text GLabel 2050 3800 3    50   UnSpc ~ 0
GND
Text GLabel 2150 3800 3    50   UnSpc ~ 0
GND
Text GLabel 2250 2400 1    50   UnSpc ~ 0
+5V
Text GLabel 2150 2400 1    50   UnSpc ~ 0
+5V
Text GLabel 4750 3900 0    50   UnSpc ~ 0
+5V
Text GLabel 4750 4000 0    50   UnSpc ~ 0
GND
Text Notes 10750 6850 2    100  ~ 0
Address decoder, CRU register and DSR ROM
Text HLabel 2850 3200 2    50   Output ~ 0
SNDEN*
Wire Wire Line
	1850 3800 1850 3900
Wire Wire Line
	1850 3900 1450 3900
Wire Wire Line
	1450 4000 1950 4000
Wire Wire Line
	1950 4000 1950 3800
Wire Wire Line
	1450 4100 2250 4100
Wire Wire Line
	2250 4100 2250 3800
Text HLabel 2850 3000 2    50   Output ~ 0
DSROE*
Text GLabel 1450 2900 0    50   Input ~ 0
CRUCLK*
Wire Wire Line
	1450 4200 2350 4200
Wire Wire Line
	2350 4200 2350 3800
Wire Wire Line
	1450 4300 2450 4300
Wire Wire Line
	2450 4300 2450 3800
Text GLabel 1450 3400 0    50   Input ~ 0
WE*
Text Notes 1900 700  2    50   ~ 0
CRU Base from 1800-1F00
Text GLabel 550  650  1    50   UnSpc ~ 0
+5V
$Comp
L Device:R_Small R1
U 1 1 60AFA063
P 1500 800
F 0 "R1" V 1500 1100 50  0000 C CNN
F 1 "10K" V 1500 1250 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" H 1500 800 50  0001 C CNN
F 3 "~" H 1500 800 50  0001 C CNN
	1    1500 800 
	0    1    1    0   
$EndComp
Text GLabel 1800 1100 2    50   UnSpc ~ 0
GND
$Comp
L Device:R_Small R2
U 1 1 60AFAF60
P 1500 900
F 0 "R2" V 1500 1200 50  0000 C CNN
F 1 "10K" V 1500 1350 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" H 1500 900 50  0001 C CNN
F 3 "~" H 1500 900 50  0001 C CNN
	1    1500 900 
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R3
U 1 1 60AFB08C
P 1500 1000
F 0 "R3" V 1500 1300 50  0000 C CNN
F 1 "10K" V 1500 1450 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" H 1500 1000 50  0001 C CNN
F 3 "~" H 1500 1000 50  0001 C CNN
	1    1500 1000
	0    1    1    0   
$EndComp
Wire Wire Line
	1700 1100 1800 1100
Wire Wire Line
	1600 800  1700 800 
Wire Wire Line
	1700 800  1700 900 
Wire Wire Line
	1700 900  1600 900 
Connection ~ 1700 900 
Wire Wire Line
	1700 900  1700 1000
Wire Wire Line
	1600 1000 1700 1000
Connection ~ 1700 1000
Wire Wire Line
	1700 1000 1700 1100
Text Label 1150 2000 1    50   ~ 0
BAS100
Text Label 1250 2000 1    50   ~ 0
BAS200
Text Label 1350 2000 1    50   ~ 0
BAS400
Text Notes 1100 4500 0    50   ~ 0
Setting any bit to the CRU block, enables the DSR
Text GLabel 1850 2400 1    50   Input ~ 0
RESET*
$Comp
L Device:C_Small C15
U 1 1 605A83FF
P 3300 950
F 0 "C15" H 3250 1150 50  0000 L CNN
F 1 "cap" H 3392 905 50  0001 L CNN
F 2 "Capacitors_SMD:C_0805" H 3300 950 50  0001 C CNN
F 3 "~" H 3300 950 50  0001 C CNN
	1    3300 950 
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C16
U 1 1 605A8D7A
P 3450 950
F 0 "C16" H 3400 1150 50  0000 L CNN
F 1 "cap" H 3542 905 50  0001 L CNN
F 2 "Capacitors_SMD:C_0805" H 3450 950 50  0001 C CNN
F 3 "~" H 3450 950 50  0001 C CNN
	1    3450 950 
	1    0    0    -1  
$EndComp
Text GLabel 3100 1050 0    50   UnSpc ~ 0
GND
Text GLabel 3100 850  0    50   UnSpc ~ 0
+5V
Wire Wire Line
	3100 850  3300 850 
Connection ~ 3300 850 
Wire Wire Line
	3300 850  3450 850 
Wire Wire Line
	3100 1050 3300 1050
Connection ~ 3300 1050
Wire Wire Line
	3300 1050 3450 1050
$Comp
L Device:LED_ALT D1
U 1 1 6063B35C
P 4100 1300
F 0 "D1" V 4139 1183 50  0000 R CNN
F 1 "LED" V 4048 1183 50  0000 R CNN
F 2 "LEDs:LED_Rectangular_W3.0mm_H2.0mm" H 4100 1300 50  0001 C CNN
F 3 "~" H 4100 1300 50  0001 C CNN
	1    4100 1300
	0    -1   -1   0   
$EndComp
Text GLabel 4100 1550 0    50   UnSpc ~ 0
GND
Text GLabel 4100 750  0    50   UnSpc ~ 0
+5V
Wire Wire Line
	4100 750  4100 850 
Wire Wire Line
	4100 1450 4100 1550
Text Label 4100 1100 0    50   ~ 0
led
Wire Wire Line
	4100 1050 4100 1150
Wire Wire Line
	1450 2800 1350 2800
Wire Wire Line
	1350 2800 1350 1000
Wire Wire Line
	1450 3000 1250 3000
Wire Wire Line
	1250 3000 1250 900 
Wire Wire Line
	1250 900  1400 900 
Wire Wire Line
	1150 3200 1450 3200
Text GLabel 2850 2800 2    50   Input ~ 0
DSREN*
Text GLabel 4750 4100 0    50   Input ~ 0
DSREN*
Text GLabel 4750 4200 0    50   Input ~ 0
DSROE*
Text Label 1450 3900 0    50   ~ 0
I8
Text Label 1450 4000 0    50   ~ 0
I9
Text Label 1450 4100 0    50   ~ 0
I10
Text Label 1450 4200 0    50   ~ 0
IO9
Text Label 1450 4300 0    50   ~ 0
IO8
$Comp
L Connector_Generic:Conn_02x03_Odd_Even J4
U 1 1 607A1177
P 750 900
F 0 "J4" H 800 1125 50  0000 C CNN
F 1 "Conn_02x03_Odd_Even" H 800 1126 50  0001 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x03_Pitch2.54mm" H 750 900 50  0001 C CNN
F 3 "~" H 750 900 50  0001 C CNN
	1    750  900 
	1    0    0    -1  
$EndComp
Connection ~ 1250 900 
Wire Wire Line
	550  650  550  800 
Wire Wire Line
	1150 1000 1150 3200
Connection ~ 1150 1000
Wire Wire Line
	1150 1000 1050 1000
Wire Wire Line
	550  800  550  900 
Connection ~ 550  800 
Connection ~ 550  900 
Wire Wire Line
	550  900  550  1000
Wire Wire Line
	1050 800  1350 800 
Wire Wire Line
	1050 900  1250 900 
Wire Wire Line
	1350 1000 1350 800 
Connection ~ 1350 800 
Wire Wire Line
	1350 800  1400 800 
Wire Wire Line
	1150 1000 1400 1000
$Comp
L Device:R_Small R10
U 1 1 6063D214
P 4100 950
F 0 "R10" V 4150 950 50  0000 C CNN
F 1 "470" V 4000 950 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" H 4100 950 50  0001 C CNN
F 3 "~" H 4100 950 50  0001 C CNN
	1    4100 950 
	-1   0    0    1   
$EndComp
Text GLabel 10550 4600 2    50   Input ~ 0
CLK1
Text GLabel 10550 2000 2    50   Input ~ 0
CLK2
Text GLabel 10550 2200 2    50   Input ~ 0
CLK3
Text GLabel 10550 3900 2    50   Input ~ 0
CLK4
Wire Wire Line
	6650 1800 6650 2100
Wire Wire Line
	6950 1800 6650 1800
Text GLabel 6950 2400 2    50   UnSpc ~ 0
GND
Text GLabel 6950 1800 2    50   UnSpc ~ 0
+5V
$Comp
L Oscillator:SG-615 X?
U 1 1 60790601
P 6950 2100
AR Path="/609CA5FB/60790601" Ref="X?"  Part="1" 
AR Path="/60790601" Ref="X?"  Part="1" 
AR Path="/604F6A55/60790601" Ref="X1"  Part="1" 
F 0 "X1" H 7150 2550 50  0000 L CNN
F 1 "SG-7050CCN" H 7150 2450 50  0000 L CNN
F 2 "Oscillators:Oscillator_SMD_SeikoEpson_SG8002CA-4pin_7.0x5.0mm" H 7650 1750 50  0001 C CNN
F 3 "https://support.epson.biz/td/api/doc_check.php?mode=dl&lang=en&Parts=SG-51P" H 6850 2100 50  0001 C CNN
	1    6950 2100
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 60790608
P 6500 2100
AR Path="/60790608" Ref="C?"  Part="1" 
AR Path="/604F6A55/60790608" Ref="C19"  Part="1" 
F 0 "C19" H 6450 2300 50  0000 L CNN
F 1 "cap" H 6592 2055 50  0001 L CNN
F 2 "Capacitors_SMD:C_0805" H 6500 2100 50  0001 C CNN
F 3 "~" H 6500 2100 50  0001 C CNN
	1    6500 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	6650 1800 6500 1800
Wire Wire Line
	6500 1800 6500 2000
Connection ~ 6650 1800
Wire Wire Line
	6500 2400 6950 2400
Wire Wire Line
	6500 2200 6500 2400
Text Notes 7600 1200 0    100  ~ 0
Clocks
$Comp
L 74xx:74LS74 U6
U 3 1 607961F1
P 6000 5900
F 0 "U6" H 6230 5946 50  0000 L CNN
F 1 "74HCT74" H 6230 5855 50  0000 L CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 6000 5900 50  0001 C CNN
F 3 "74xx/74hc_hct74.pdf" H 6000 5900 50  0001 C CNN
	3    6000 5900
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS74 U6
U 2 1 60795F11
P 9050 3050
F 0 "U6" H 9050 3531 50  0000 C CNN
F 1 "74HCT74" H 9050 3440 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 9050 3050 50  0001 C CNN
F 3 "74xx/74hc_hct74.pdf" H 9050 3050 50  0001 C CNN
	2    9050 3050
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS74 U6
U 1 1 60791861
P 8150 3050
F 0 "U6" H 8150 3531 50  0000 C CNN
F 1 "74HCT74" H 8150 3440 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 8150 3050 50  0001 C CNN
F 3 "74xx/74hc_hct74.pdf" H 8150 3050 50  0001 C CNN
	1    8150 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	7250 2100 7600 2100
Wire Wire Line
	7600 2100 7600 3050
Wire Wire Line
	7600 3050 7850 3050
Wire Wire Line
	7850 2950 7750 2950
Wire Wire Line
	7750 2950 7750 2450
Text GLabel 6000 6300 2    50   UnSpc ~ 0
GND
Text GLabel 6000 5500 2    50   UnSpc ~ 0
+5V
NoConn ~ 8450 2950
Wire Wire Line
	8550 3150 8450 3150
Wire Wire Line
	8550 2450 8550 3050
Wire Wire Line
	7750 2450 8550 2450
Wire Wire Line
	8750 3050 8550 3050
Connection ~ 8550 3050
Wire Wire Line
	8550 3050 8550 3150
Wire Wire Line
	8750 2950 8650 2950
Wire Wire Line
	8650 2950 8650 2450
Wire Wire Line
	8650 2450 9450 2450
Wire Wire Line
	9450 2450 9450 3150
Wire Wire Line
	9450 3150 9350 3150
Text GLabel 7250 3350 0    50   UnSpc ~ 0
+5V
Connection ~ 8150 3350
Wire Wire Line
	8150 3350 9050 3350
Wire Wire Line
	9050 2750 8150 2750
Connection ~ 8150 2750
Wire Wire Line
	8150 2750 7400 2750
Wire Wire Line
	7400 2750 7400 3350
Wire Wire Line
	7400 3350 8150 3350
Wire Wire Line
	7250 3350 7400 3350
Connection ~ 7400 3350
$Comp
L 74xx:74LS153 U16
U 1 1 607B3648
P 10050 4600
F 0 "U16" H 10050 5781 50  0000 C CNN
F 1 "74HCT153" H 10050 5690 50  0000 C CNN
F 2 "Package_SO:SOIC-16_3.9x9.9mm_P1.27mm" H 10050 4600 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS153" H 10050 4600 50  0001 C CNN
	1    10050 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	7600 2100 10550 2100
Connection ~ 7600 2100
Wire Wire Line
	10550 2100 10550 2200
Text GLabel 10050 3600 0    50   UnSpc ~ 0
+5V
Text GLabel 9550 5100 0    50   UnSpc ~ 0
GND
Text GLabel 9550 4400 0    50   UnSpc ~ 0
GND
Text GLabel 10050 5700 0    50   UnSpc ~ 0
GND
Connection ~ 7600 3050
Text Label 7900 2100 0    50   ~ 0
CLK3579
Wire Wire Line
	10550 2000 10550 2100
Connection ~ 10550 2100
Text Notes 9950 3000 0    50   ~ 0
S  CLK1 CLK4\n00   -    -\n01  /4    -\n10   -   /4\n11  /4   /4
Text GLabel 2850 2900 2    50   Output ~ 0
CLKS0
Text GLabel 2350 2400 1    50   Output ~ 0
CLKS1
Text GLabel 9550 5400 0    50   Input ~ 0
CLKS1
Text GLabel 9550 5300 0    50   Input ~ 0
CLKS0
Text Label 9350 3300 0    50   ~ 0
DIV4
Text Label 8550 3150 0    50   ~ 0
DIV2
Wire Wire Line
	7600 3050 7600 3900
Wire Wire Line
	9350 2950 9350 4100
Wire Wire Line
	9550 4600 7600 4600
Wire Wire Line
	9550 3900 7600 3900
Connection ~ 7600 3900
Wire Wire Line
	7600 3900 7600 4000
Wire Wire Line
	9550 4100 9350 4100
Connection ~ 9350 4100
Wire Wire Line
	9350 4100 9350 4200
Wire Wire Line
	9550 4200 9350 4200
Connection ~ 9350 4200
Wire Wire Line
	9350 4200 9350 4700
Wire Wire Line
	9550 4700 9350 4700
Connection ~ 9350 4700
Wire Wire Line
	9350 4700 9350 4900
Wire Wire Line
	9550 4900 9350 4900
Wire Wire Line
	9550 4800 7600 4800
Wire Wire Line
	7600 4800 7600 4600
Connection ~ 7600 4600
Wire Wire Line
	9550 4000 7600 4000
Connection ~ 7600 4000
Wire Wire Line
	7600 4000 7600 4600
$EndSCHEMATC
