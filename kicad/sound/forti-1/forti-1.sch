EESchema Schematic File Version 4
LIBS:forti-1-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Gemini:SN76489 U11
U 1 1 604F18B5
P 9350 4700
F 0 "U11" H 9350 5881 50  0000 C CNN
F 1 "SN76489" H 9350 5790 50  0000 C CNN
F 2 "Housings_DIP:DIP-16_W7.62mm_LongPads" H 9350 5100 50  0001 C CNN
F 3 "" H 9350 4700 50  0001 C CNN
	1    9350 4700
	1    0    0    -1  
$EndComp
$Comp
L Gemini:SN76489 U10
U 1 1 604F25B6
P 7900 4700
F 0 "U10" H 7900 5881 50  0000 C CNN
F 1 "SN76489" H 7900 5790 50  0000 C CNN
F 2 "Housings_DIP:DIP-16_W7.62mm_LongPads" H 7900 5100 50  0001 C CNN
F 3 "" H 7900 4700 50  0001 C CNN
	1    7900 4700
	1    0    0    -1  
$EndComp
$Sheet
S 3100 7100 550  600 
U 604F6A55
F0 "CRU" 50
F1 "cru-personality.sch" 50
F2 "SNDEN*" O R 3650 7300 50 
F3 "DSROE*" O L 3100 7600 50 
$EndSheet
$Comp
L 74xx:74LS32 U12
U 4 1 605B6E01
P 4050 3050
F 0 "U12" H 4050 3375 50  0000 C CNN
F 1 "74LS32" H 4050 3284 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 4050 3050 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS32" H 4050 3050 50  0001 C CNN
	4    4050 3050
	0    1    1    0   
$EndComp
Text GLabel 4150 2750 1    50   Input ~ 0
A11
$Comp
L Gemini:SN76489 U9
U 1 1 605EA175
P 6400 4700
F 0 "U9" H 6400 5881 50  0000 C CNN
F 1 "SN76489" H 6400 5790 50  0000 C CNN
F 2 "Housings_DIP:DIP-16_W7.62mm_LongPads" H 6400 5100 50  0001 C CNN
F 3 "" H 6400 4700 50  0001 C CNN
	1    6400 4700
	1    0    0    -1  
$EndComp
$Comp
L Gemini:SN76489 U8
U 1 1 605EA17F
P 4950 4700
F 0 "U8" H 4950 5881 50  0000 C CNN
F 1 "SN76489" H 4950 5790 50  0000 C CNN
F 2 "Housings_DIP:DIP-16_W7.62mm_LongPads" H 4950 5100 50  0001 C CNN
F 3 "" H 4950 4700 50  0001 C CNN
	1    4950 4700
	1    0    0    -1  
$EndComp
Text GLabel 2250 6350 2    50   Output ~ 0
D0
Text GLabel 2250 6550 2    50   Output ~ 0
D1
Text GLabel 2250 6750 2    50   Output ~ 0
D2
Text GLabel 2250 6950 2    50   Output ~ 0
D3
Text GLabel 2250 7050 2    50   Output ~ 0
D4
Text GLabel 2250 6850 2    50   Output ~ 0
D5
Text GLabel 2250 6650 2    50   Output ~ 0
D6
Text GLabel 2250 6450 2    50   Output ~ 0
D7
Text GLabel 1250 6450 0    50   Input ~ 0
B_D7
Text GLabel 1250 6650 0    50   Input ~ 0
B_D6
Text GLabel 1250 6850 0    50   Input ~ 0
B_D5
Text GLabel 1250 7050 0    50   Input ~ 0
B_D4
Text GLabel 1250 6950 0    50   Input ~ 0
B_D3
Text GLabel 1250 6750 0    50   Input ~ 0
B_D2
Text GLabel 1250 6550 0    50   Input ~ 0
B_D1
Text GLabel 1250 6350 0    50   Input ~ 0
B_D0
$Comp
L 74xx:74LS32 U12
U 3 1 6060F99A
P 5650 3000
F 0 "U12" V 5604 3188 50  0000 L CNN
F 1 "74LS32" V 5695 3188 50  0000 L CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 5650 3000 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS32" H 5650 3000 50  0001 C CNN
	3    5650 3000
	0    1    1    0   
$EndComp
Text GLabel 5750 2700 1    50   Input ~ 0
A12
$Comp
L 74xx:74LS32 U12
U 2 1 60616C1C
P 7150 3000
F 0 "U12" V 7104 3188 50  0000 L CNN
F 1 "74LS32" V 7195 3188 50  0000 L CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 7150 3000 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS32" H 7150 3000 50  0001 C CNN
	2    7150 3000
	0    1    1    0   
$EndComp
Text GLabel 7250 2700 1    50   Input ~ 0
A13
$Comp
L 74xx:74LS32 U12
U 1 1 6061F581
P 8600 3000
F 0 "U12" V 8554 3188 50  0000 L CNN
F 1 "74LS32" V 8645 3188 50  0000 L CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 8600 3000 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS32" H 8600 3000 50  0001 C CNN
	1    8600 3000
	0    1    1    0   
$EndComp
Text GLabel 8700 2700 1    50   Input ~ 0
A14
Text GLabel 2250 1000 2    50   Output ~ 0
A0
Text GLabel 2250 1100 2    50   Output ~ 0
A1
Text GLabel 2250 1200 2    50   Output ~ 0
A2
Text GLabel 2250 1600 2    50   Output ~ 0
A3
Text GLabel 2250 1400 2    50   Output ~ 0
A4
Text GLabel 2250 1300 2    50   Output ~ 0
A5
Text GLabel 2250 3050 2    50   Output ~ 0
A6
Text GLabel 2250 3150 2    50   Output ~ 0
A7
Text GLabel 2250 3250 2    50   Output ~ 0
A8
Text GLabel 2250 3350 2    50   Output ~ 0
A9
Text GLabel 2250 3450 2    50   Output ~ 0
A10
Text GLabel 2250 4550 2    50   Output ~ 0
A11
Text GLabel 2250 4650 2    50   Output ~ 0
A12
Text GLabel 2250 4750 2    50   Output ~ 0
A13
Text GLabel 2250 4850 2    50   Output ~ 0
A14
Text GLabel 2250 4950 2    50   Output ~ 0
A15
Text GLabel 1250 1000 0    50   Input ~ 0
B_A0
Text GLabel 1250 1100 0    50   Input ~ 0
B_A1
Text GLabel 1250 1200 0    50   Input ~ 0
B_A2
Text GLabel 1250 1600 0    50   Input ~ 0
B_A3
Text GLabel 1250 1400 0    50   Input ~ 0
B_A4
Text GLabel 1250 1300 0    50   Input ~ 0
B_A5
Text GLabel 1250 3050 0    50   Input ~ 0
B_A6
Text GLabel 1250 3150 0    50   Input ~ 0
B_A7
Text GLabel 1250 3250 0    50   Input ~ 0
B_A8
Text GLabel 1250 3350 0    50   Input ~ 0
B_A9
Text GLabel 1250 3450 0    50   Input ~ 0
B_A10
Text GLabel 1250 4550 0    50   Input ~ 0
B_A11
Text GLabel 1250 4650 0    50   Input ~ 0
B_A12
Text GLabel 1250 4750 0    50   Input ~ 0
B_A13
Text GLabel 1250 4850 0    50   Input ~ 0
B_A14
Text GLabel 1250 4950 0    50   Input ~ 0
B_A15
Text GLabel 1750 7650 2    50   UnSpc ~ 0
GND
Text GLabel 4400 4450 0    50   Input ~ 0
D0
Text GLabel 4400 5150 0    50   Input ~ 0
D7
Text GLabel 4400 5050 0    50   Input ~ 0
D6
Text GLabel 4400 4950 0    50   Input ~ 0
D5
Text GLabel 4400 4850 0    50   Input ~ 0
D4
Text GLabel 4400 4750 0    50   Input ~ 0
D3
Text GLabel 4400 4650 0    50   Input ~ 0
D2
Text GLabel 4400 4550 0    50   Input ~ 0
D1
Text GLabel 5850 4450 0    50   Input ~ 0
D0
Text GLabel 5850 5150 0    50   Input ~ 0
D7
Text GLabel 5850 5050 0    50   Input ~ 0
D6
Text GLabel 5850 4950 0    50   Input ~ 0
D5
Text GLabel 5850 4850 0    50   Input ~ 0
D4
Text GLabel 5850 4750 0    50   Input ~ 0
D3
Text GLabel 5850 4650 0    50   Input ~ 0
D2
Text GLabel 5850 4550 0    50   Input ~ 0
D1
Text GLabel 7350 4450 0    50   Input ~ 0
D0
Text GLabel 7350 5150 0    50   Input ~ 0
D7
Text GLabel 7350 5050 0    50   Input ~ 0
D6
Text GLabel 7350 4950 0    50   Input ~ 0
D5
Text GLabel 7350 4850 0    50   Input ~ 0
D4
Text GLabel 7350 4750 0    50   Input ~ 0
D3
Text GLabel 7350 4650 0    50   Input ~ 0
D2
Text GLabel 7350 4550 0    50   Input ~ 0
D1
Text GLabel 8800 4450 0    50   Input ~ 0
D0
Text GLabel 8800 5150 0    50   Input ~ 0
D7
Text GLabel 8800 5050 0    50   Input ~ 0
D6
Text GLabel 8800 4950 0    50   Input ~ 0
D5
Text GLabel 8800 4850 0    50   Input ~ 0
D4
Text GLabel 8800 4750 0    50   Input ~ 0
D3
Text GLabel 8800 4650 0    50   Input ~ 0
D2
Text GLabel 8800 4550 0    50   Input ~ 0
D1
Text GLabel 4400 3700 0    50   UnSpc ~ 0
+5V
Text GLabel 1750 6050 2    50   UnSpc ~ 0
+5V
Text GLabel 1750 4250 2    50   UnSpc ~ 0
+5V
Text GLabel 1750 2450 2    50   UnSpc ~ 0
+5V
Text GLabel 4400 6000 0    50   UnSpc ~ 0
GND
Text GLabel 1750 5850 2    50   UnSpc ~ 0
GND
Text GLabel 1750 4050 2    50   UnSpc ~ 0
GND
Text GLabel 1250 3750 0    50   UnSpc ~ 0
GND
Text GLabel 1250 5550 0    50   UnSpc ~ 0
GND
Text GLabel 1250 1900 0    50   UnSpc ~ 0
GND
Text GLabel 1750 600  2    50   UnSpc ~ 0
+5V
Text GLabel 1750 2200 2    50   UnSpc ~ 0
GND
Wire Wire Line
	9350 3700 7900 3700
Connection ~ 6400 3700
Wire Wire Line
	6400 3700 4950 3700
Connection ~ 7900 3700
Wire Wire Line
	7900 3700 6400 3700
Text GLabel 8800 4350 0    50   Input ~ 0
CLK4
Text GLabel 7350 4350 0    50   Input ~ 0
CLK3
Text GLabel 5850 4350 0    50   Input ~ 0
CLK2
Text GLabel 4400 4350 0    50   Input ~ 0
CLK1
Text GLabel 5550 5350 2    50   Output ~ 0
READY1
Text GLabel 7000 5350 2    50   Output ~ 0
READY2
Text GLabel 8500 5350 2    50   Output ~ 0
READY3
Text GLabel 9950 5350 2    50   Output ~ 0
READY4
Text GLabel 8800 4250 0    50   Input ~ 0
WE*
Text GLabel 7350 4250 0    50   Input ~ 0
WE*
Text GLabel 5850 4250 0    50   Input ~ 0
WE*
Text GLabel 4400 4250 0    50   Input ~ 0
WE*
Text Label 4050 3450 0    50   ~ 0
SNDEN1*
Text Label 5650 3450 0    50   ~ 0
SNDEN2*
Text Label 7150 3450 0    50   ~ 0
SNDEN3*
Text Label 8600 3450 0    50   ~ 0
SNDEN4*
Text GLabel 5550 5450 2    50   Output ~ 0
AUDIO1
Wire Wire Line
	8800 4150 8600 4150
Wire Wire Line
	8600 3300 8600 4150
Wire Wire Line
	5850 4150 5650 4150
Wire Wire Line
	5650 3300 5650 4150
Wire Wire Line
	4400 4150 4050 4150
Wire Wire Line
	4050 3350 4050 4150
Wire Wire Line
	7350 4150 7150 4150
Wire Wire Line
	7150 3300 7150 4150
Text GLabel 7000 5450 2    50   Output ~ 0
AUDIO2
Text GLabel 8500 5450 2    50   Output ~ 0
AUDIO3
Text GLabel 9950 5450 2    50   Output ~ 0
AUDIO4
$Comp
L 74xx:74LS125 U13
U 3 1 60897606
P 10150 800
F 0 "U13" H 10150 1117 50  0000 C CNN
F 1 "74LS125" H 10150 1026 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 10150 800 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS125" H 10150 800 50  0001 C CNN
	3    10150 800 
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS125 U13
U 4 1 60898BFC
P 10150 1450
F 0 "U13" H 10150 1767 50  0000 C CNN
F 1 "74LS125" H 10150 1676 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 10150 1450 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS125" H 10150 1450 50  0001 C CNN
	4    10150 1450
	1    0    0    -1  
$EndComp
Text GLabel 9650 1050 0    50   Output ~ 0
READY1
Text GLabel 9650 1700 0    50   Output ~ 0
READY2
$Comp
L 74xx:74LS125 U13
U 1 1 6089D6FF
P 10150 2100
F 0 "U13" H 10150 2417 50  0000 C CNN
F 1 "74LS125" H 10150 2326 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 10150 2100 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS125" H 10150 2100 50  0001 C CNN
	1    10150 2100
	1    0    0    -1  
$EndComp
Text GLabel 9650 2350 0    50   Output ~ 0
READY3
Text GLabel 9650 3000 0    50   Output ~ 0
READY4
$Comp
L 74xx:74LS125 U13
U 2 1 6089D709
P 10150 2750
F 0 "U13" H 10150 3067 50  0000 C CNN
F 1 "74LS125" H 10150 2976 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 10150 2750 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS125" H 10150 2750 50  0001 C CNN
	2    10150 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	9850 2100 9750 2100
Wire Wire Line
	9750 2100 9750 2350
Connection ~ 9750 2350
Wire Wire Line
	9750 2350 9650 2350
Wire Wire Line
	9750 2350 10150 2350
Wire Wire Line
	9850 2750 9750 2750
Wire Wire Line
	9750 2750 9750 3000
Connection ~ 9750 3000
Wire Wire Line
	9750 3000 9650 3000
Wire Wire Line
	9750 3000 10150 3000
Wire Wire Line
	9750 1450 9750 1700
Wire Wire Line
	9750 1450 9850 1450
Connection ~ 9750 1700
Wire Wire Line
	9750 1700 9650 1700
Wire Wire Line
	9750 1700 10150 1700
Wire Wire Line
	9850 800  9750 800 
Wire Wire Line
	9750 800  9750 1050
Connection ~ 9750 1050
Wire Wire Line
	9750 1050 9650 1050
Wire Wire Line
	9750 1050 10150 1050
Wire Wire Line
	10650 2100 10450 2100
Wire Wire Line
	10450 1450 10650 1450
Connection ~ 10650 1450
Wire Wire Line
	10650 1450 10650 1750
Wire Wire Line
	10650 2750 10450 2750
Wire Wire Line
	10650 2100 10650 2750
Connection ~ 10650 2100
Wire Wire Line
	10650 800  10650 1450
Wire Wire Line
	10450 800  10650 800 
Wire Wire Line
	10850 1750 10650 1750
Connection ~ 10650 1750
Wire Wire Line
	10650 1750 10650 2100
Wire Wire Line
	8500 2200 7050 2200
Wire Wire Line
	8500 2200 8500 2700
Wire Wire Line
	3950 2200 3950 2750
Connection ~ 5550 2200
Wire Wire Line
	5550 2200 5550 2700
Wire Wire Line
	5550 2200 3950 2200
Connection ~ 7050 2200
Wire Wire Line
	7050 2200 7050 2700
Wire Wire Line
	7050 2200 5550 2200
Text GLabel 10850 1750 2    50   Output ~ 0
B_READY
Wire Wire Line
	4400 6000 4950 6000
Wire Wire Line
	4950 6000 6400 6000
Connection ~ 4950 6000
Connection ~ 6400 6000
Wire Wire Line
	6400 6000 7900 6000
Connection ~ 7900 6000
Wire Wire Line
	7900 6000 9350 6000
Wire Wire Line
	4950 3700 4400 3700
Connection ~ 4950 3700
$Comp
L 74xx:74LS125 U13
U 5 1 60902D95
P 4750 7100
F 0 "U13" H 4980 7146 50  0000 L CNN
F 1 "74LS125" H 4980 7055 50  0000 L CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 4750 7100 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS125" H 4750 7100 50  0001 C CNN
	5    4750 7100
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS32 U12
U 5 1 60903E6C
P 4250 7100
F 0 "U12" H 4250 7425 50  0000 C CNN
F 1 "74LS32" H 4250 7334 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 4250 7100 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS32" H 4250 7100 50  0001 C CNN
	5    4250 7100
	1    0    0    -1  
$EndComp
Text GLabel 4050 7600 0    50   UnSpc ~ 0
GND
Wire Wire Line
	4050 7600 4250 7600
Text GLabel 4050 6600 0    50   UnSpc ~ 0
+5V
Wire Wire Line
	1750 7650 1250 7650
Wire Wire Line
	1150 7250 1150 7750
Wire Wire Line
	1250 7350 1250 7650
Wire Wire Line
	1150 7250 1250 7250
Wire Wire Line
	1150 7750 2900 7750
Wire Wire Line
	3100 7600 2900 7600
Wire Wire Line
	2900 7600 2900 7750
Wire Wire Line
	3950 2200 3800 2200
Wire Wire Line
	3800 2200 3800 7300
Wire Wire Line
	3800 7300 3650 7300
Connection ~ 3950 2200
Text GLabel 1250 2950 0    50   Input ~ 0
B_MEMEN*
Text GLabel 1250 2850 0    50   Input ~ 0
B_WE*
Text GLabel 1250 2750 0    50   Input ~ 0
B_CRUCLK*
Text GLabel 1250 1500 0    50   Input ~ 0
B_DBIN
Text GLabel 1250 900  0    50   Input ~ 0
B_RESET*
Text GLabel 2250 2950 2    50   Output ~ 0
MEMEN*
Text GLabel 2250 2850 2    50   Output ~ 0
WE*
Text GLabel 2250 2750 2    50   Output ~ 0
CRUCLK*
Text GLabel 2250 1500 2    50   Output ~ 0
DBIN
Text GLabel 2250 900  2    50   Output ~ 0
RESET*
$Comp
L 74xx:74HCT541 U1
U 1 1 6098F868
P 1750 1400
F 0 "U1" H 1750 2381 50  0000 C CNN
F 1 "74HCT541" H 1750 2290 50  0000 C CNN
F 2 "Gemini:SO-20_5.3x12.6mm_P1.27mm" H 1750 1400 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74HCT541" H 1750 1400 50  0001 C CNN
	1    1750 1400
	1    0    0    -1  
$EndComp
Text GLabel 1250 1800 0    50   UnSpc ~ 0
GND
$Comp
L 74xx:74HCT541 U2
U 1 1 60994FA6
P 1750 3250
F 0 "U2" H 1750 4231 50  0000 C CNN
F 1 "74HCT541" H 1750 4140 50  0000 C CNN
F 2 "Gemini:SO-20_5.3x12.6mm_P1.27mm" H 1750 3250 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74HCT541" H 1750 3250 50  0001 C CNN
	1    1750 3250
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74HCT541 U3
U 1 1 60995AAA
P 1750 5050
F 0 "U3" H 1650 6000 50  0000 C CNN
F 1 "74HCT541" H 1750 5940 50  0000 C CNN
F 2 "Gemini:SO-20_5.3x12.6mm_P1.27mm" H 1750 5050 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74HCT541" H 1750 5050 50  0001 C CNN
	1    1750 5050
	1    0    0    -1  
$EndComp
Text GLabel 1250 5450 0    50   UnSpc ~ 0
GND
Text GLabel 1250 3650 0    50   UnSpc ~ 0
GND
$Comp
L Gemini:74ALS645A U4
U 1 1 609976A5
P 1750 6850
F 0 "U4" H 1650 7800 50  0000 C CNN
F 1 "74LS645ANS" H 1750 7740 50  0000 C CNN
F 2 "Gemini:SO-20_5.3x12.6mm_P1.27mm" H 1750 6850 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS245" H 1750 6850 50  0001 C CNN
	1    1750 6850
	1    0    0    -1  
$EndComp
$Sheet
S 3600 850  1500 950 
U 609B277A
F0 "Side Port" 50
F1 "SidePort.sch" 50
$EndSheet
$Sheet
S 7650 950  1050 900 
U 60B1656F
F0 "Audio stage" 50
F1 "audio.sch" 50
$EndSheet
$Comp
L Device:C_Small C5
U 1 1 60588328
P 5350 7500
F 0 "C5" H 5300 7700 50  0000 L CNN
F 1 "cap" H 5442 7455 50  0001 L CNN
F 2 "Capacitors_SMD:C_0805" H 5350 7500 50  0001 C CNN
F 3 "~" H 5350 7500 50  0001 C CNN
	1    5350 7500
	1    0    0    -1  
$EndComp
Connection ~ 4250 6600
Wire Wire Line
	4250 6600 4050 6600
Connection ~ 4250 7600
Connection ~ 4750 6600
Connection ~ 4750 7600
Wire Wire Line
	4250 7600 4750 7600
Wire Wire Line
	4250 6600 4750 6600
$Comp
L Device:C_Small C6
U 1 1 6058FE40
P 5500 7500
F 0 "C6" H 5450 7700 50  0000 L CNN
F 1 "cap" H 5592 7455 50  0001 L CNN
F 2 "Capacitors_SMD:C_0805" H 5500 7500 50  0001 C CNN
F 3 "~" H 5500 7500 50  0001 C CNN
	1    5500 7500
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C7
U 1 1 60590094
P 5650 7500
F 0 "C7" H 5600 7700 50  0000 L CNN
F 1 "cap" H 5742 7455 50  0001 L CNN
F 2 "Capacitors_SMD:C_0805" H 5650 7500 50  0001 C CNN
F 3 "~" H 5650 7500 50  0001 C CNN
	1    5650 7500
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C8
U 1 1 605902F1
P 5800 7500
F 0 "C8" H 5750 7700 50  0000 L CNN
F 1 "cap" H 5892 7455 50  0001 L CNN
F 2 "Capacitors_SMD:C_0805" H 5800 7500 50  0001 C CNN
F 3 "~" H 5800 7500 50  0001 C CNN
	1    5800 7500
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C9
U 1 1 60590500
P 5950 7500
F 0 "C9" H 5900 7700 50  0000 L CNN
F 1 "cap" H 6042 7455 50  0001 L CNN
F 2 "Capacitors_SMD:C_0805" H 5950 7500 50  0001 C CNN
F 3 "~" H 5950 7500 50  0001 C CNN
	1    5950 7500
	1    0    0    -1  
$EndComp
Wire Wire Line
	4750 6600 5350 6600
Wire Wire Line
	5350 7400 5500 7400
Connection ~ 5500 7400
Wire Wire Line
	5500 7400 5650 7400
Connection ~ 5650 7400
Wire Wire Line
	5650 7400 5800 7400
Connection ~ 5800 7400
Wire Wire Line
	5800 7400 5950 7400
Connection ~ 5950 7400
Connection ~ 5500 7600
Wire Wire Line
	5500 7600 5350 7600
Connection ~ 5650 7600
Wire Wire Line
	5650 7600 5500 7600
Connection ~ 5800 7600
Wire Wire Line
	5800 7600 5650 7600
Connection ~ 5950 7600
Wire Wire Line
	5950 7600 5800 7600
Wire Wire Line
	4750 7600 5350 7600
Connection ~ 5350 7600
Wire Wire Line
	5350 7400 5350 6600
Connection ~ 5350 7400
Wire Wire Line
	5950 7400 6100 7400
Wire Wire Line
	5950 7600 6100 7600
$Comp
L Device:C_Small C10
U 1 1 605ACEAE
P 6100 7500
F 0 "C10" H 6050 7700 50  0000 L CNN
F 1 "cap" H 6192 7455 50  0001 L CNN
F 2 "Capacitors_SMD:C_0805" H 6100 7500 50  0001 C CNN
F 3 "~" H 6100 7500 50  0001 C CNN
	1    6100 7500
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C11
U 1 1 605ACEB8
P 6250 7500
F 0 "C11" H 6200 7700 50  0000 L CNN
F 1 "cap" H 6342 7455 50  0001 L CNN
F 2 "Capacitors_SMD:C_0805" H 6250 7500 50  0001 C CNN
F 3 "~" H 6250 7500 50  0001 C CNN
	1    6250 7500
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C12
U 1 1 605ACEC2
P 6400 7500
F 0 "C12" H 6350 7700 50  0000 L CNN
F 1 "cap" H 6492 7455 50  0001 L CNN
F 2 "Capacitors_SMD:C_0805" H 6400 7500 50  0001 C CNN
F 3 "~" H 6400 7500 50  0001 C CNN
	1    6400 7500
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C13
U 1 1 605ACECC
P 6550 7500
F 0 "C13" H 6500 7700 50  0000 L CNN
F 1 "cap" H 6642 7455 50  0001 L CNN
F 2 "Capacitors_SMD:C_0805" H 6550 7500 50  0001 C CNN
F 3 "~" H 6550 7500 50  0001 C CNN
	1    6550 7500
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C14
U 1 1 605ACED6
P 6700 7500
F 0 "C14" H 6650 7700 50  0000 L CNN
F 1 "cap" H 6792 7455 50  0001 L CNN
F 2 "Capacitors_SMD:C_0805" H 6700 7500 50  0001 C CNN
F 3 "~" H 6700 7500 50  0001 C CNN
	1    6700 7500
	1    0    0    -1  
$EndComp
Connection ~ 6100 7400
Wire Wire Line
	6100 7400 6250 7400
Connection ~ 6100 7600
Wire Wire Line
	6100 7600 6250 7600
Connection ~ 6250 7400
Wire Wire Line
	6250 7400 6400 7400
Connection ~ 6250 7600
Wire Wire Line
	6250 7600 6400 7600
Connection ~ 6400 7400
Wire Wire Line
	6400 7400 6550 7400
Connection ~ 6400 7600
Wire Wire Line
	6400 7600 6550 7600
Connection ~ 6550 7400
Wire Wire Line
	6550 7400 6700 7400
Connection ~ 6550 7600
Wire Wire Line
	6550 7600 6700 7600
Text GLabel 1250 5250 0    50   UnSpc ~ 0
+5V
Text GLabel 1250 5150 0    50   UnSpc ~ 0
+5V
Text GLabel 1250 5050 0    50   UnSpc ~ 0
+5V
NoConn ~ 2250 5050
NoConn ~ 2250 5150
NoConn ~ 2250 5250
Text GLabel 650  6350 0    50   Input ~ 0
B_D7
Text GLabel 650  6450 0    50   Input ~ 0
B_D6
Text GLabel 650  6550 0    50   Input ~ 0
B_D5
Text GLabel 650  6650 0    50   Input ~ 0
B_D4
Text GLabel 650  7050 0    50   Input ~ 0
B_D3
Text GLabel 650  6950 0    50   Input ~ 0
B_D2
Text GLabel 650  6850 0    50   Input ~ 0
B_D1
Text GLabel 650  6750 0    50   Input ~ 0
B_D0
Text GLabel 2650 6750 2    50   Output ~ 0
D0
Text GLabel 2650 6850 2    50   Output ~ 0
D1
Text GLabel 2650 6950 2    50   Output ~ 0
D2
Text GLabel 2650 7050 2    50   Output ~ 0
D3
Text GLabel 2650 6650 2    50   Output ~ 0
D4
Text GLabel 2650 6550 2    50   Output ~ 0
D5
Text GLabel 2650 6450 2    50   Output ~ 0
D6
Text GLabel 2650 6350 2    50   Output ~ 0
D7
$EndSCHEMATC
