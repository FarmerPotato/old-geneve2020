EESchema Schematic File Version 4
LIBS:forti-1-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 1600 3750 0    50   Input ~ 0
AUDIO1
Text GLabel 1600 3550 0    50   Input ~ 0
AUDIO2
Wire Wire Line
	1850 1900 1600 1900
Wire Wire Line
	1600 2100 1850 2100
Wire Wire Line
	1850 3550 1600 3550
Wire Wire Line
	1600 3750 1850 3750
Wire Wire Line
	2050 1900 2350 1900
Wire Wire Line
	2350 1900 2350 2000
Wire Wire Line
	2350 2000 2350 2100
Wire Wire Line
	2350 2100 2050 2100
Connection ~ 2350 2000
Wire Wire Line
	2050 3550 2350 3550
Wire Wire Line
	2050 3750 2350 3750
$Comp
L Device:R_Small R8
U 1 1 60B1F3D1
P 2950 2500
F 0 "R8" V 2850 2350 50  0000 C CNN
F 1 "10" V 2845 2500 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" H 2950 2500 50  0001 C CNN
F 3 "~" H 2950 2500 50  0001 C CNN
	1    2950 2500
	0    1    1    0   
$EndComp
$Comp
L power:GNDA #PWR01
U 1 1 60B24CB7
P 3150 5800
F 0 "#PWR01" H 3150 5550 50  0001 C CNN
F 1 "GNDA" H 3155 5627 50  0000 C CNN
F 2 "" H 3150 5800 50  0001 C CNN
F 3 "" H 3150 5800 50  0001 C CNN
	1    3150 5800
	1    0    0    -1  
$EndComp
Wire Wire Line
	6300 3650 5800 3650
Wire Wire Line
	6200 3550 6300 3550
Wire Wire Line
	6100 3750 6300 3750
Wire Wire Line
	6000 3950 6300 3950
$Comp
L Device:Ferrite_Bead_Small FB1
U 1 1 60B327AF
P 1850 5800
F 0 "FB1" V 1613 5800 50  0000 C CNN
F 1 "Ferrite_Bead_Small" V 1704 5800 50  0001 C CNN
F 2 "Gemini:Ferrite_L5.5_D3.5" V 1780 5800 50  0001 C CNN
F 3 "~" H 1850 5800 50  0001 C CNN
	1    1850 5800
	0    1    1    0   
$EndComp
Text GLabel 1500 5200 0    50   UnSpc ~ 0
+5V
$Comp
L Device:C C3
U 1 1 60B3E4F2
P 5450 2200
F 0 "C3" V 5198 2200 50  0000 C CNN
F 1 "C" V 5289 2200 50  0000 C CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 5488 2050 50  0001 C CNN
F 3 "~" H 5450 2200 50  0001 C CNN
	1    5450 2200
	0    1    1    0   
$EndComp
$Comp
L Device:C C4
U 1 1 60B3F3EB
P 5450 3850
F 0 "C4" V 5198 3850 50  0000 C CNN
F 1 "C" V 5289 3850 50  0000 C CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 5488 3700 50  0001 C CNN
F 3 "~" H 5450 3850 50  0001 C CNN
	1    5450 3850
	0    1    1    0   
$EndComp
Wire Wire Line
	5600 3850 6300 3850
Wire Wire Line
	5600 2200 5800 2200
$Comp
L Device:Ferrite_Bead_Small FB2
U 1 1 60B4553C
P 1850 5200
F 0 "FB2" V 1705 5200 50  0000 C CNN
F 1 "Ferrite_Bead_Small" V 1704 5200 50  0001 C CNN
F 2 "Gemini:Ferrite_L5.5_D3.5" V 1780 5200 50  0001 C CNN
F 3 "~" H 1850 5200 50  0001 C CNN
	1    1850 5200
	0    1    1    0   
$EndComp
$Comp
L Device:C C1
U 1 1 60B48116
P 2150 5550
F 0 "C1" H 2035 5504 50  0000 R CNN
F 1 "10uF" H 2035 5595 50  0000 R CNN
F 2 "Capacitors_SMD:C_0805" H 2188 5400 50  0001 C CNN
F 3 "~" H 2150 5550 50  0001 C CNN
	1    2150 5550
	-1   0    0    1   
$EndComp
$Comp
L Device:C C2
U 1 1 60B4DED0
P 2500 5550
F 0 "C2" H 2385 5504 50  0000 R CNN
F 1 "0.1uF" H 2385 5595 50  0000 R CNN
F 2 "Capacitors_SMD:C_0805" H 2538 5400 50  0001 C CNN
F 3 "~" H 2500 5550 50  0001 C CNN
	1    2500 5550
	-1   0    0    1   
$EndComp
Wire Wire Line
	1950 5200 2150 5200
Wire Wire Line
	2150 5200 2500 5200
Connection ~ 2150 5200
Wire Wire Line
	1750 5200 1500 5200
Text GLabel 1550 5800 0    50   UnSpc ~ 0
GND
Wire Wire Line
	2500 5200 2500 5400
Wire Wire Line
	2150 5200 2150 5400
Wire Wire Line
	2150 5700 2150 5800
Connection ~ 2150 5800
Wire Wire Line
	2150 5800 2500 5800
Wire Wire Line
	2500 5700 2500 5800
Connection ~ 2500 5800
Connection ~ 2500 5200
Wire Wire Line
	1550 5800 1750 5800
Wire Wire Line
	1950 5800 2150 5800
Connection ~ 3150 5800
$Comp
L power:GNDA #PWR0103
U 1 1 60B815B8
P 6000 3950
F 0 "#PWR0103" H 6000 3700 50  0001 C CNN
F 1 "GNDA" H 6005 3777 50  0000 C CNN
F 2 "" H 6000 3950 50  0001 C CNN
F 3 "" H 6000 3950 50  0001 C CNN
	1    6000 3950
	1    0    0    -1  
$EndComp
NoConn ~ 6300 2000
NoConn ~ 6300 2200
$Comp
L power:+5VA #PWR0104
U 1 1 60C2E7F4
P 3150 5200
F 0 "#PWR0104" H 3150 5050 50  0001 C CNN
F 1 "+5VA" H 3165 5373 50  0000 C CNN
F 2 "" H 3150 5200 50  0001 C CNN
F 3 "" H 3150 5200 50  0001 C CNN
	1    3150 5200
	1    0    0    -1  
$EndComp
Connection ~ 3150 5200
$Comp
L power:PWR_FLAG #FLG0103
U 1 1 60C2F346
P 2850 5200
F 0 "#FLG0103" H 2850 5275 50  0001 C CNN
F 1 "PWR_FLAG" H 2850 5373 50  0000 C CNN
F 2 "" H 2850 5200 50  0001 C CNN
F 3 "~" H 2850 5200 50  0001 C CNN
	1    2850 5200
	1    0    0    -1  
$EndComp
Text GLabel 4900 4300 2    50   Input ~ 0
B_AUDIOIN
NoConn ~ 4900 4300
Wire Wire Line
	2500 5800 2850 5800
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 60C35A13
P 2850 5800
F 0 "#FLG0102" H 2850 5875 50  0001 C CNN
F 1 "PWR_FLAG" H 2850 5973 50  0000 C CNN
F 2 "" H 2850 5800 50  0001 C CNN
F 3 "~" H 2850 5800 50  0001 C CNN
	1    2850 5800
	-1   0    0    1   
$EndComp
Connection ~ 2850 5800
Wire Wire Line
	2850 5800 3150 5800
Text Label 2250 3550 0    50   ~ 0
RIN
Text Label 2250 1900 0    50   ~ 0
LIN
Text Label 4400 4200 0    50   ~ 0
FBB-
Text Label 5600 2200 0    50   ~ 0
LOUT
Text Label 5650 3850 0    50   ~ 0
ROUT
$Comp
L Connector:AudioJack3_Ground_SwitchTR J3
U 1 1 6064244D
P 6500 3850
F 0 "J3" H 6220 3626 50  0000 R CNN
F 1 "Headphone" H 6220 3717 50  0000 R CNN
F 2 "Gemini:CUI_SJ1-3535NG-BE" H 6500 3850 50  0001 C CNN
F 3 "~" H 6500 3850 50  0001 C CNN
	1    6500 3850
	-1   0    0    1   
$EndComp
$Comp
L Connector:AudioJack3_Ground_SwitchTR J2
U 1 1 606445C8
P 6500 2300
F 0 "J2" H 6220 2076 50  0000 R CNN
F 1 "Line Out" H 6220 2167 50  0000 R CNN
F 2 "Gemini:CUI_SJ1-3535NG-BE" H 6500 2300 50  0001 C CNN
F 3 "~" H 6500 2300 50  0001 C CNN
	1    6500 2300
	-1   0    0    1   
$EndComp
Wire Wire Line
	6300 2100 6200 2100
Wire Wire Line
	6300 2300 6100 2300
Wire Wire Line
	6300 2400 6000 2400
NoConn ~ 6500 3350
NoConn ~ 6500 1800
Text Notes 2700 1400 0    50   ~ 0
Gain amp
Text Notes 2700 1250 0    50   ~ 0
Summing amp
Wire Wire Line
	5800 2200 5800 3650
Wire Wire Line
	6000 2400 6000 3950
Connection ~ 6000 3950
Wire Wire Line
	6200 2100 6200 3550
Wire Wire Line
	6100 2300 6100 3750
Connection ~ 2850 5200
Wire Wire Line
	2850 5200 2500 5200
Wire Wire Line
	2850 5200 3150 5200
Text Notes 6750 6200 0    50   ~ 0
Does OPA1679 provide the current that OPA1688 would?
Text Notes 5400 4950 0    50   ~ 0
https://www.digikey.com/en/articles/a-deep-dive-into-audio-jack-switches-and-configurations
Wire Wire Line
	3150 5200 3350 5200
Wire Wire Line
	3650 5800 3350 5800
Wire Wire Line
	3250 2100 3300 2100
Wire Wire Line
	2650 2200 2650 2500
Wire Wire Line
	2650 2500 2850 2500
Wire Wire Line
	3050 2500 3300 2500
Wire Wire Line
	3300 2500 3300 2100
Wire Wire Line
	2350 3550 2350 3650
Wire Wire Line
	2350 2000 2650 2000
Wire Wire Line
	2650 2500 2650 2600
Connection ~ 2650 2500
Text GLabel 2450 3000 0    50   UnSpc ~ 0
GNDA
Wire Wire Line
	2650 3000 2450 3000
Text Label 2650 2500 2    50   ~ 0
FBBL
Text Label 4550 2200 0    50   ~ 0
LOP
Wire Wire Line
	4500 2200 4550 2200
Wire Wire Line
	3900 2300 3900 2600
Wire Wire Line
	3900 2600 4100 2600
Wire Wire Line
	4300 2600 4550 2600
Wire Wire Line
	4550 2600 4550 2200
Wire Wire Line
	3900 2600 3900 2700
Connection ~ 3900 2600
Wire Wire Line
	3900 2900 3900 3000
Text Label 3900 2600 2    50   ~ 0
FBAL
Connection ~ 4550 2200
Wire Wire Line
	3900 2100 3300 2100
Connection ~ 3300 2100
Wire Wire Line
	2650 2800 2650 3000
Wire Wire Line
	2650 3000 3900 3000
Connection ~ 2650 3000
Wire Wire Line
	3250 3750 3300 3750
Wire Wire Line
	2650 3850 2650 4150
Wire Wire Line
	2650 4150 2850 4150
Wire Wire Line
	3050 4150 3300 4150
Wire Wire Line
	3300 4150 3300 3750
Wire Wire Line
	2350 3650 2650 3650
Wire Wire Line
	2650 4150 2650 4250
Connection ~ 2650 4150
Wire Wire Line
	2650 4650 2450 4650
Text Label 2650 4150 2    50   ~ 0
FBBR
Text Label 4550 3850 0    50   ~ 0
ROP
Wire Wire Line
	4500 3850 4550 3850
Wire Wire Line
	3900 3950 3900 4250
Wire Wire Line
	3900 4250 4100 4250
Wire Wire Line
	4300 4250 4550 4250
Wire Wire Line
	4550 4250 4550 3850
Wire Wire Line
	3900 4250 3900 4350
Connection ~ 3900 4250
Wire Wire Line
	3900 4550 3900 4650
Text Label 3900 4250 2    50   ~ 0
FBAR
Wire Wire Line
	4550 3850 4800 3850
Connection ~ 4550 3850
Wire Wire Line
	3900 3750 3300 3750
Connection ~ 3300 3750
Wire Wire Line
	2650 4450 2650 4650
Wire Wire Line
	2650 4650 3900 4650
Connection ~ 2650 4650
Connection ~ 2350 3650
Wire Wire Line
	2350 3650 2350 3750
Text Notes 2900 2650 0    50   ~ 0
Gain adjust
Text Notes 3950 1250 0    50   ~ 0
Driving Amp
$Comp
L Gemini:OPA1688 U15
U 2 1 6065472B
P 2950 2100
F 0 "U15" H 2950 2467 50  0000 C CNN
F 1 "OPA1688" H 2950 2376 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 2950 2100 50  0001 C CNN
F 3 "https://www.ti.com/lit/ds/symlink/opa1688.pdf" H 2950 2100 50  0001 C CNN
	2    2950 2100
	1    0    0    -1  
$EndComp
$Comp
L Gemini:OPA1688 U14
U 1 1 60655BBC
P 4200 3850
F 0 "U14" H 4200 4217 50  0000 C CNN
F 1 "OPA1688" H 4200 4126 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 4200 3850 50  0001 C CNN
F 3 "https://www.ti.com/lit/ds/symlink/opa1688.pdf" H 4200 3850 50  0001 C CNN
	1    4200 3850
	1    0    0    -1  
$EndComp
$Comp
L Gemini:OPA1688 U14
U 2 1 60657895
P 2950 3750
F 0 "U14" H 2950 4117 50  0000 C CNN
F 1 "OPA1688" H 2950 4026 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 2950 3750 50  0001 C CNN
F 3 "https://www.ti.com/lit/ds/symlink/opa1688.pdf" H 2950 3750 50  0001 C CNN
	2    2950 3750
	1    0    0    -1  
$EndComp
$Comp
L Gemini:OPA1688 U15
U 3 1 60658BEC
P 3750 5500
F 0 "U15" H 3708 5546 50  0000 L CNN
F 1 "OPA1688" H 3708 5455 50  0000 L CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 3750 5500 50  0001 C CNN
F 3 "https://www.ti.com/lit/ds/symlink/opa1688.pdf" H 3750 5500 50  0001 C CNN
	3    3750 5500
	1    0    0    -1  
$EndComp
$Comp
L Gemini:OPA1688 U14
U 3 1 60659B36
P 4650 5500
F 0 "U14" H 4608 5546 50  0000 L CNN
F 1 "OPA1688" H 4608 5455 50  0000 L CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 4650 5500 50  0001 C CNN
F 3 "https://www.ti.com/lit/ds/symlink/opa1688.pdf" H 4650 5500 50  0001 C CNN
	3    4650 5500
	1    0    0    -1  
$EndComp
Connection ~ 3650 5200
Connection ~ 3650 5800
$Comp
L Gemini:OPA1688 U15
U 1 1 60652C1F
P 4200 2200
F 0 "U15" H 4200 2567 50  0000 C CNN
F 1 "OPA1688" H 4200 2476 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 4200 2200 50  0001 C CNN
F 3 "https://www.ti.com/lit/ds/symlink/opa1688.pdf" H 4200 2200 50  0001 C CNN
	1    4200 2200
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R14
U 1 1 60665F01
P 3900 4450
F 0 "R14" V 3800 4300 50  0000 C CNN
F 1 "1K" V 3795 4450 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" H 3900 4450 50  0001 C CNN
F 3 "~" H 3900 4450 50  0001 C CNN
	1    3900 4450
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R15
U 1 1 60667B4E
P 4200 2600
F 0 "R15" V 4100 2450 50  0000 C CNN
F 1 "10" V 4095 2600 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" H 4200 2600 50  0001 C CNN
F 3 "~" H 4200 2600 50  0001 C CNN
	1    4200 2600
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R12
U 1 1 60668B5B
P 2950 4150
F 0 "R12" V 2850 4000 50  0000 C CNN
F 1 "10" V 2845 4150 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" H 2950 4150 50  0001 C CNN
F 3 "~" H 2950 4150 50  0001 C CNN
	1    2950 4150
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R16
U 1 1 6066915F
P 4200 4250
F 0 "R16" V 4100 4100 50  0000 C CNN
F 1 "10" V 4095 4250 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" H 4200 4250 50  0001 C CNN
F 3 "~" H 4200 4250 50  0001 C CNN
	1    4200 4250
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R11
U 1 1 6066AC21
P 2650 4350
F 0 "R11" V 2550 4200 50  0000 C CNN
F 1 "1K" V 2545 4350 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" H 2650 4350 50  0001 C CNN
F 3 "~" H 2650 4350 50  0001 C CNN
	1    2650 4350
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R9
U 1 1 6066B238
P 2650 2700
F 0 "R9" V 2550 2550 50  0000 C CNN
F 1 "1K" V 2545 2700 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" H 2650 2700 50  0001 C CNN
F 3 "~" H 2650 2700 50  0001 C CNN
	1    2650 2700
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R13
U 1 1 6066B885
P 3900 2800
F 0 "R13" V 3800 2650 50  0000 C CNN
F 1 "1K" V 3795 2800 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" H 3900 2800 50  0001 C CNN
F 3 "~" H 3900 2800 50  0001 C CNN
	1    3900 2800
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R7
U 1 1 6066D235
P 1950 3750
F 0 "R7" V 1850 3600 50  0000 C CNN
F 1 "10" V 1845 3750 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" H 1950 3750 50  0001 C CNN
F 3 "~" H 1950 3750 50  0001 C CNN
	1    1950 3750
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R6
U 1 1 6066D8C0
P 1950 3550
F 0 "R6" V 1850 3400 50  0000 C CNN
F 1 "10" V 1845 3550 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" H 1950 3550 50  0001 C CNN
F 3 "~" H 1950 3550 50  0001 C CNN
	1    1950 3550
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R5
U 1 1 6066E48B
P 1950 2100
F 0 "R5" V 1850 1950 50  0000 C CNN
F 1 "10" V 1845 2100 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" H 1950 2100 50  0001 C CNN
F 3 "~" H 1950 2100 50  0001 C CNN
	1    1950 2100
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R4
U 1 1 6066F07C
P 1950 1900
F 0 "R4" V 1850 1750 50  0000 C CNN
F 1 "10" V 1845 1900 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" H 1950 1900 50  0001 C CNN
F 3 "~" H 1950 1900 50  0001 C CNN
	1    1950 1900
	0    1    1    0   
$EndComp
Text Label 3550 3750 0    50   ~ 0
RADD
Text Label 3550 2100 0    50   ~ 0
LADD
Text GLabel 2450 4650 0    50   UnSpc ~ 0
GNDA
$Comp
L Device:C C17
U 1 1 6062EBF2
P 3350 5500
F 0 "C17" H 3235 5454 50  0000 R CNN
F 1 "0.1uF" H 3235 5545 50  0000 R CNN
F 2 "Capacitors_SMD:C_0805" H 3388 5350 50  0001 C CNN
F 3 "~" H 3350 5500 50  0001 C CNN
	1    3350 5500
	-1   0    0    1   
$EndComp
$Comp
L Device:C C18
U 1 1 6063229B
P 4250 5500
F 0 "C18" H 4135 5454 50  0000 R CNN
F 1 "0.1uF" H 4135 5545 50  0000 R CNN
F 2 "Capacitors_SMD:C_0805" H 4288 5350 50  0001 C CNN
F 3 "~" H 4250 5500 50  0001 C CNN
	1    4250 5500
	-1   0    0    1   
$EndComp
Wire Wire Line
	3350 5350 3350 5200
Connection ~ 3350 5200
Wire Wire Line
	3350 5200 3650 5200
Wire Wire Line
	3350 5650 3350 5800
Connection ~ 3350 5800
Wire Wire Line
	3350 5800 3150 5800
Wire Wire Line
	4250 5650 4250 5800
Wire Wire Line
	3650 5800 4250 5800
Wire Wire Line
	4250 5350 4250 5200
Wire Wire Line
	3650 5200 4250 5200
Wire Wire Line
	4250 5200 4550 5200
Connection ~ 4250 5200
Wire Wire Line
	4550 5800 4250 5800
Connection ~ 4250 5800
Text GLabel 1600 2100 0    50   Input ~ 0
AUDIO4
Text GLabel 1600 1900 0    50   Input ~ 0
AUDIO3
$Comp
L Connector:TestPoint TP1
U 1 1 6070FD0F
P 4800 3850
F 0 "TP1" H 4858 3968 50  0000 L CNN
F 1 "Rop" H 4858 3877 50  0000 L CNN
F 2 "Measurement_Points:Test_Point_Keystone_5000-5004_Miniature" H 5000 3850 50  0001 C CNN
F 3 "~" H 5000 3850 50  0001 C CNN
	1    4800 3850
	1    0    0    -1  
$EndComp
Connection ~ 4800 3850
Wire Wire Line
	4800 3850 5300 3850
Wire Wire Line
	4550 2200 4800 2200
$Comp
L Connector:TestPoint TP2
U 1 1 6073A901
P 4800 2200
F 0 "TP2" H 4858 2318 50  0000 L CNN
F 1 "Lop" H 4858 2227 50  0000 L CNN
F 2 "Measurement_Points:Test_Point_Keystone_5000-5004_Miniature" H 5000 2200 50  0001 C CNN
F 3 "~" H 5000 2200 50  0001 C CNN
	1    4800 2200
	1    0    0    -1  
$EndComp
Connection ~ 4800 2200
Wire Wire Line
	4800 2200 5300 2200
$Comp
L Connector:TestPoint TP3
U 1 1 607458EB
P 6100 1500
F 0 "TP3" H 6158 1618 50  0000 L CNN
F 1 "Rout" H 6158 1527 50  0000 L CNN
F 2 "Measurement_Points:Test_Point_Keystone_5000-5004_Miniature" H 6300 1500 50  0001 C CNN
F 3 "~" H 6300 1500 50  0001 C CNN
	1    6100 1500
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP4
U 1 1 60745E34
P 6200 1750
F 0 "TP4" H 6258 1868 50  0000 L CNN
F 1 "Lout" H 6258 1777 50  0000 L CNN
F 2 "Measurement_Points:Test_Point_Keystone_5000-5004_Miniature" H 6400 1750 50  0001 C CNN
F 3 "~" H 6400 1750 50  0001 C CNN
	1    6200 1750
	1    0    0    -1  
$EndComp
Connection ~ 6200 2100
Wire Wire Line
	6100 1500 6100 2300
Connection ~ 6100 2300
Wire Wire Line
	6200 1750 6200 2100
$EndSCHEMATC
