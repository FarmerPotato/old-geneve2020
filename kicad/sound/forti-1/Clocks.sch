EESchema Schematic File Version 4
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 74xx:74LS74 U15
U 3 1 609D1C0E
P 3800 6900
F 0 "U15" H 4030 6946 50  0000 L CNN
F 1 "74HCT74" H 4030 6855 50  0000 L CNN
F 2 "Housings_SOIC:SOIC-16_3.9x9.9mm_Pitch1.27mm" H 3800 6900 50  0001 C CNN
F 3 "74xx/74hc_hct74.pdf" H 3800 6900 50  0001 C CNN
	3    3800 6900
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS175 U16
U 1 1 60A09746
P 6550 2500
F 0 "U16" H 6550 3381 50  0000 C CNN
F 1 "74LS175" H 6550 3290 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-16_3.9x9.9mm_Pitch1.27mm" H 6550 2500 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS175" H 6550 2500 50  0001 C CNN
	1    6550 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	6050 1500 6050 2100
Text GLabel 7750 1100 2    50   Output ~ 0
CLK_DIV2
Wire Wire Line
	7150 2200 7150 1500
Wire Wire Line
	6050 1500 7150 1500
Wire Wire Line
	7050 2200 7150 2200
Wire Wire Line
	3450 2100 3450 2900
$Comp
L 74xx:74LS08 U14
U 1 1 60A6669E
P 4750 2300
F 0 "U14" H 4750 2625 50  0000 C CNN
F 1 "74LS08" H 4750 2534 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-16_3.9x9.9mm_Pitch1.27mm" H 4750 2300 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS08" H 4750 2300 50  0001 C CNN
	1    4750 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	5050 2300 6050 2300
Wire Wire Line
	4350 1300 4350 2200
Wire Wire Line
	4350 2200 4450 2200
Wire Wire Line
	4450 2400 4250 2400
Wire Wire Line
	4250 2400 4250 1200
Wire Wire Line
	7250 2300 7250 1400
Wire Wire Line
	7250 1400 5950 1400
Wire Wire Line
	5950 1400 5950 2500
Wire Wire Line
	5950 2500 6050 2500
Wire Wire Line
	7050 2300 7250 2300
Wire Wire Line
	7050 2400 7350 2400
Wire Wire Line
	7350 2400 7350 1300
Wire Wire Line
	7350 1300 4350 1300
Wire Wire Line
	7050 2500 8050 2500
Wire Wire Line
	8050 2500 8050 1200
Text GLabel 8300 1200 2    50   Output ~ 0
CLK_DIV3
Wire Wire Line
	7050 2600 7450 2600
Wire Wire Line
	7450 2600 7450 1200
Wire Wire Line
	7450 1200 4250 1200
Wire Wire Line
	7050 2100 7650 2100
Wire Wire Line
	7650 2100 7650 1100
Wire Wire Line
	8300 1200 8050 1200
Wire Wire Line
	7750 1100 7650 1100
$Comp
L Oscillator:SG-615 X1
U 1 1 60AA4249
P 2550 2900
F 0 "X1" H 2750 3350 50  0000 L CNN
F 1 "SG-7050CCN" H 2750 3250 50  0000 L CNN
F 2 "Oscillator:Oscillator_SMD_SeikoEpson_SG8002JA-4Pin_14.0x8.7mm" H 3250 2550 50  0001 C CNN
F 3 "https://support.epson.biz/td/api/doc_check.php?mode=dl&lang=en&Parts=SG-51P" H 2450 2900 50  0001 C CNN
	1    2550 2900
	1    0    0    -1  
$EndComp
Connection ~ 3450 2900
Wire Wire Line
	3450 2900 6050 2900
Wire Wire Line
	2850 2900 3450 2900
$Comp
L 74xx:74LS08 U14
U 5 1 60ABE97A
P 5250 6850
F 0 "U14" H 5480 6896 50  0000 L CNN
F 1 "74LS08" H 5480 6805 50  0000 L CNN
F 2 "Housings_SOIC:SOIC-16_3.9x9.9mm_Pitch1.27mm" H 5250 6850 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS08" H 5250 6850 50  0001 C CNN
	5    5250 6850
	1    0    0    -1  
$EndComp
Text GLabel 6550 1800 2    50   UnSpc ~ 0
+5V
Text GLabel 5250 6350 2    50   UnSpc ~ 0
+5V
Text GLabel 3800 6500 2    50   UnSpc ~ 0
+5V
Text GLabel 2550 2600 2    50   UnSpc ~ 0
+5V
Text GLabel 6550 3300 2    50   UnSpc ~ 0
GND
Text GLabel 2550 3200 2    50   UnSpc ~ 0
GND
Wire Wire Line
	2550 2600 2250 2600
Wire Wire Line
	2250 2600 2250 2900
NoConn ~ 7050 2700
NoConn ~ 7050 2800
Text GLabel 6050 3000 0    50   UnSpc ~ 0
+5V
Text GLabel 6050 2700 0    50   UnSpc ~ 0
GND
Text HLabel 3450 2100 2    50   Output ~ 0
SNDCLK
Connection ~ 7150 2200
Wire Wire Line
	7150 2200 8300 2200
Wire Wire Line
	8300 1500 8300 2100
Wire Wire Line
	9200 1300 9000 1300
Wire Wire Line
	9300 1500 10300 1500
Wire Wire Line
	8300 1500 9100 1500
Wire Wire Line
	10400 1400 10200 1400
Text GLabel 10400 1400 2    50   Output ~ 0
CLK_DIV8
Text GLabel 9200 1300 2    50   Output ~ 0
CLK_DIV4
Wire Wire Line
	9000 1300 9000 2100
Wire Wire Line
	10300 1500 10300 2300
Wire Wire Line
	9300 1500 9300 2100
Wire Wire Line
	9100 1500 9100 2200
Wire Wire Line
	9300 2100 9500 2100
Wire Wire Line
	9500 2200 9100 2200
Text GLabel 8600 1900 2    50   UnSpc ~ 0
+5V
Text GLabel 9800 2500 2    50   UnSpc ~ 0
+5V
Text GLabel 8600 2500 2    50   UnSpc ~ 0
+5V
Text GLabel 9800 1900 2    50   UnSpc ~ 0
+5V
Wire Wire Line
	8900 2100 9000 2100
Wire Wire Line
	8900 2300 9100 2300
Connection ~ 9100 2200
Wire Wire Line
	9100 2200 9100 2300
Wire Wire Line
	10100 2100 10200 2100
Wire Wire Line
	10200 2100 10200 1400
Wire Wire Line
	10100 2300 10300 2300
$Comp
L 74xx:74LS74 U15
U 1 1 609E0C5C
P 8600 2200
F 0 "U15" H 8600 2681 50  0000 C CNN
F 1 "74HCT74" H 8600 2590 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-16_3.9x9.9mm_Pitch1.27mm" H 8600 2200 50  0001 C CNN
F 3 "74xx/74hc_hct74.pdf" H 8600 2200 50  0001 C CNN
	1    8600 2200
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS74 U15
U 2 1 609E17F8
P 9800 2200
F 0 "U15" H 9800 2681 50  0000 C CNN
F 1 "74HCT74" H 9800 2590 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-16_3.9x9.9mm_Pitch1.27mm" H 9800 2200 50  0001 C CNN
F 3 "74xx/74hc_hct74.pdf" H 9800 2200 50  0001 C CNN
	2    9800 2200
	1    0    0    -1  
$EndComp
$EndSCHEMATC
