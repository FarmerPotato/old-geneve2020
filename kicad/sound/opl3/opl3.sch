EESchema Schematic File Version 4
LIBS:opl3-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
NoConn ~ 1950 1900
Text GLabel 1950 3000 2    50   Input ~ 0
3V3
NoConn ~ 1950 2200
NoConn ~ 1950 2300
NoConn ~ 1950 2400
NoConn ~ 1450 2400
NoConn ~ 1450 2300
NoConn ~ 1450 2200
Text GLabel 1450 2000 0    50   Input ~ 0
GND
Text GLabel 1950 2000 2    50   Input ~ 0
GND
Text GLabel 1950 2500 2    50   Input ~ 0
CD7
Text GLabel 1450 2500 0    50   Input ~ 0
CD6
Text GLabel 1950 2600 2    50   Input ~ 0
CD5
Text GLabel 1450 2600 0    50   Input ~ 0
CD4
Text GLabel 1950 2700 2    50   Input ~ 0
CD3
Text GLabel 1450 2700 0    50   Input ~ 0
CD2
Text GLabel 1950 2800 2    50   Input ~ 0
CD1
Text GLabel 1450 2800 0    50   Input ~ 0
CD0
Text GLabel 1950 1700 2    50   Input ~ 0
CSR*
Text GLabel 1450 1700 0    50   Input ~ 0
CSW*
Text GLabel 1950 1600 2    50   Input ~ 0
SS1
Text GLabel 1450 1600 0    50   Input ~ 0
SS0
Text GLabel 1450 1800 0    50   Input ~ 0
MODE0
Text GLabel 1950 1800 2    50   Input ~ 0
MODE1
Text GLabel 1450 3000 0    50   Input ~ 0
3V3
Text GLabel 1450 2900 0    50   Input ~ 0
GND
Text GLabel 1950 2900 2    50   Input ~ 0
GND
$Comp
L Connector_Generic:Conn_02x15_Odd_Even J?
U 1 1 5E6EDF08
P 1650 2300
AR Path="/5E6E9EF8/5E6EDF08" Ref="J?"  Part="1" 
AR Path="/5E6EDF08" Ref="J2"  Part="1" 
F 0 "J2" H 1700 3200 50  0000 C CNN
F 1 "Conn_02x15_Odd_Even" H 1700 3326 50  0001 C CNN
F 2 "Pin_Headers:Pin_Header_Angled_2x15_Pitch2.54mm" H 1650 2300 50  0001 C CNN
F 3 "~" H 1650 2300 50  0001 C CNN
	1    1650 2300
	1    0    0    1   
$EndComp
Text Notes 1750 5150 0    50   ~ 0
DIR=1 A->B
Text GLabel 1850 5450 2    50   Input ~ 0
GND
Text GLabel 3400 1800 0    50   Input ~ 0
3V3
Text GLabel 2900 2700 0    50   Input ~ 0
CD6
Text GLabel 2900 2500 0    50   Input ~ 0
CD4
Text GLabel 2900 2300 0    50   Input ~ 0
CD2
Text GLabel 2900 2100 0    50   Input ~ 0
CD0
Text GLabel 2900 3000 0    50   Input ~ 0
CSR1*
Text GLabel 3400 3400 2    50   Input ~ 0
GND
Text GLabel 2900 2800 0    50   Input ~ 0
CD7
Text GLabel 2900 2600 0    50   Input ~ 0
CD5
Text GLabel 2900 2400 0    50   Input ~ 0
CD3
Text GLabel 2900 2200 0    50   Input ~ 0
CD1
Text GLabel 1450 1900 0    50   Input ~ 0
VDPWAIT*
Text Notes 1500 1300 0    50   ~ 0
PMOD FPGA
NoConn ~ 1450 2100
NoConn ~ 1950 2100
Text Notes 2000 2150 0    50   ~ 0
3V3
Text Notes 1250 2150 0    50   ~ 0
3V3
$Comp
L 74xx:74LS138 U?
U 1 1 5E6EF3FF
P 3400 4550
AR Path="/5E6E9EF8/5E6EF3FF" Ref="U?"  Part="1" 
AR Path="/5E6EF3FF" Ref="U7"  Part="1" 
F 0 "U7" H 3400 5331 50  0000 C CNN
F 1 "74LS138" H 3400 5240 50  0000 C CNN
F 2 "Package_SO:SOIC-16W_5.3x10.2mm_P1.27mm" H 3400 4550 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS138" H 3400 4550 50  0001 C CNN
	1    3400 4550
	1    0    0    -1  
$EndComp
Text GLabel 2900 4750 0    50   Input ~ 0
SS0
Text GLabel 2900 4450 0    50   Input ~ 0
SS1
Wire Wire Line
	2900 5250 3400 5250
Text GLabel 3400 5250 2    50   Input ~ 0
GND
Text GLabel 3400 3950 0    50   Input ~ 0
+5V
NoConn ~ 3900 4550
NoConn ~ 3900 4650
NoConn ~ 3900 4950
NoConn ~ 3900 4750
NoConn ~ 3900 4850
Wire Wire Line
	2900 4850 2900 4950
$Comp
L power:GND #PWR0101
U 1 1 5E6F0FCB
P 4600 7150
F 0 "#PWR0101" H 4600 6900 50  0001 C CNN
F 1 "GND" H 4605 6977 50  0000 C CNN
F 2 "" H 4600 7150 50  0001 C CNN
F 3 "" H 4600 7150 50  0001 C CNN
	1    4600 7150
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 5E6F0FD1
P 900 6400
F 0 "C1" H 1015 6446 50  0000 L CNN
F 1 "0.1uF" H 1015 6355 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D3.0mm_W2.0mm_P2.50mm" H 938 6250 50  0001 C CNN
F 3 "~" H 900 6400 50  0001 C CNN
	1    900  6400
	1    0    0    -1  
$EndComp
$Comp
L Device:C C3
U 1 1 5E6F0FD7
P 5050 7000
F 0 "C3" H 5165 7046 50  0000 L CNN
F 1 "0.1uF" H 5165 6955 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D3.0mm_W2.0mm_P2.50mm" H 5088 6850 50  0001 C CNN
F 3 "~" H 5050 7000 50  0001 C CNN
	1    5050 7000
	1    0    0    -1  
$EndComp
Text GLabel 4400 7150 0    50   Input ~ 0
GND
$Comp
L power:+5V #PWR0102
U 1 1 5E6F0FDE
P 4600 6850
F 0 "#PWR0102" H 4600 6700 50  0001 C CNN
F 1 "+5V" H 4615 7023 50  0000 C CNN
F 2 "" H 4600 6850 50  0001 C CNN
F 3 "" H 4600 6850 50  0001 C CNN
	1    4600 6850
	1    0    0    -1  
$EndComp
Text GLabel 4400 6850 0    50   Input ~ 0
+5V
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 5E6F0FE5
P 3350 6850
F 0 "#FLG0101" H 3350 6925 50  0001 C CNN
F 1 "PWR_FLAG" V 3300 6800 50  0000 L CNN
F 2 "" H 3350 6850 50  0001 C CNN
F 3 "~" H 3350 6850 50  0001 C CNN
	1    3350 6850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0103
U 1 1 5E6F0FEB
P 3450 7150
F 0 "#PWR0103" H 3450 6900 50  0001 C CNN
F 1 "GND" H 3455 6977 50  0000 C CNN
F 2 "" H 3450 7150 50  0001 C CNN
F 3 "" H 3450 7150 50  0001 C CNN
	1    3450 7150
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 5E6F0FF1
P 3450 7000
F 0 "C2" H 3565 7046 50  0000 L CNN
F 1 "0.1uF" H 3565 6955 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D3.0mm_W2.0mm_P2.50mm" H 3488 6850 50  0001 C CNN
F 3 "~" H 3450 7000 50  0001 C CNN
	1    3450 7000
	1    0    0    -1  
$EndComp
Text GLabel 3250 7150 0    50   Input ~ 0
GND
Text GLabel 3250 6850 0    50   Input ~ 0
3V3
$Comp
L power:+3V3 #PWR0104
U 1 1 5E6F0FF9
P 3450 6850
F 0 "#PWR0104" H 3450 6700 50  0001 C CNN
F 1 "+3V3" H 3465 7023 50  0000 C CNN
F 2 "" H 3450 6850 50  0001 C CNN
F 3 "" H 3450 6850 50  0001 C CNN
	1    3450 6850
	1    0    0    -1  
$EndComp
$Comp
L Device:C C4
U 1 1 5E6F0FFF
P 5450 7000
F 0 "C4" H 5565 7046 50  0000 L CNN
F 1 "47uF" H 5565 6955 50  0000 L CNN
F 2 "Capacitors_ThroughHole:CP_Radial_D5.0mm_P2.50mm" H 5488 6850 50  0001 C CNN
F 3 "~" H 5450 7000 50  0001 C CNN
	1    5450 7000
	1    0    0    -1  
$EndComp
$Comp
L Device:C C5
U 1 1 5E6F1005
P 5750 7000
F 0 "C5" H 5865 7046 50  0000 L CNN
F 1 "47uF" H 5865 6955 50  0000 L CNN
F 2 "Capacitors_ThroughHole:CP_Radial_D5.0mm_P2.50mm" H 5788 6850 50  0001 C CNN
F 3 "~" H 5750 7000 50  0001 C CNN
	1    5750 7000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 7150 3450 7150
Wire Wire Line
	3250 6850 3350 6850
Wire Wire Line
	4400 6850 4600 6850
Wire Wire Line
	4400 7150 4600 7150
Connection ~ 3350 6850
Wire Wire Line
	3350 6850 3450 6850
Connection ~ 3450 6850
Wire Wire Line
	3450 6850 3900 6850
Connection ~ 3450 7150
Wire Wire Line
	3450 7150 3900 7150
Connection ~ 4600 6850
Wire Wire Line
	4600 6850 5050 6850
Connection ~ 4600 7150
Wire Wire Line
	4600 7150 5050 7150
Connection ~ 5050 6850
Wire Wire Line
	5050 6850 5450 6850
Connection ~ 5050 7150
Wire Wire Line
	5050 7150 5450 7150
Connection ~ 5450 6850
Wire Wire Line
	5450 6850 5750 6850
Connection ~ 5450 7150
Wire Wire Line
	5450 7150 5750 7150
$Comp
L 74xx:74LS245 U1
U 1 1 5E6F5E6C
P 3400 2600
F 0 "U1" H 3400 3581 50  0000 C CNN
F 1 "74LS245" H 3400 3490 50  0000 C CNN
F 2 "" H 3400 2600 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS245" H 3400 2600 50  0001 C CNN
	1    3400 2600
	1    0    0    -1  
$EndComp
Text GLabel 3900 2100 2    50   Input ~ 0
BCD0
Text GLabel 3900 2800 2    50   Input ~ 0
BCD7
Text GLabel 3900 2700 2    50   Input ~ 0
BCD6
Text GLabel 3900 2600 2    50   Input ~ 0
BCD5
Text GLabel 3900 2500 2    50   Input ~ 0
BCD4
Text GLabel 3900 2400 2    50   Input ~ 0
BCD3
Text GLabel 3900 2300 2    50   Input ~ 0
BCD2
Text GLabel 3900 2200 2    50   Input ~ 0
BCD1
$Comp
L Connector_Generic:Conn_02x06_Odd_Even J5
U 1 1 5E6F85A4
P 1650 3800
F 0 "J5" H 1700 3275 50  0000 C CNN
F 1 "Conn_02x06_Odd_Even" H 1700 3366 50  0000 C CNN
F 2 "" H 1650 3800 50  0001 C CNN
F 3 "~" H 1650 3800 50  0001 C CNN
	1    1650 3800
	1    0    0    1   
$EndComp
Connection ~ 2900 4950
Wire Wire Line
	2900 4950 2900 5250
Text GLabel 1450 4000 0    50   Input ~ 0
3V3
Text GLabel 1450 3900 0    50   Input ~ 0
GND
Text GLabel 1950 4000 2    50   Input ~ 0
3V3
Text GLabel 1950 3900 2    50   Input ~ 0
GND
Text GLabel 2900 4250 0    50   Input ~ 0
GND
Text GLabel 2900 4350 0    50   Input ~ 0
GND
Text GLabel 3900 4250 2    50   Input ~ 0
CS0*
Text GLabel 3900 4350 2    50   Input ~ 0
CS1*
NoConn ~ 3900 4450
Text GLabel 1950 3500 2    50   Input ~ 0
PHISY
Text GLabel 1950 3600 2    50   Input ~ 0
DOCD
Text GLabel 1450 3600 0    50   Input ~ 0
DOAB
Text GLabel 1950 3700 2    50   Input ~ 0
SMPAC
Text GLabel 1450 3700 0    50   Input ~ 0
SMPBD
Text GLabel 4700 1400 0    50   Input ~ 0
CS0*
Text GLabel 4700 2100 0    50   Input ~ 0
BCD0
Text GLabel 4700 2200 0    50   Input ~ 0
BCD1
Text GLabel 4700 1500 0    50   Input ~ 0
CSR*
Text GLabel 4700 1600 0    50   Input ~ 0
CSW*
Text GLabel 4700 1900 0    50   Input ~ 0
MODE0
Text GLabel 4700 1800 0    50   Input ~ 0
MODE1
Text GLabel 4700 1100 0    50   Input ~ 0
FMINT1
Text GLabel 4700 1200 0    50   Input ~ 0
RESET*
NoConn ~ 5750 3200
Text GLabel 5500 3200 2    50   Input ~ 0
GND
Text GLabel 1800 6950 2    50   Input ~ 0
GND
Text GLabel 1800 5850 0    50   Input ~ 0
VCC
Text GLabel 2900 3100 0    50   Input ~ 0
CS0*
NoConn ~ 1950 3800
NoConn ~ 1450 3800
NoConn ~ 1450 3500
$Comp
L Oscillator:XO32 X1
U 1 1 5E6FF760
P 1800 6400
F 0 "X1" H 1456 6446 50  0000 R CNN
F 1 "XO32" H 1456 6355 50  0000 R CNN
F 2 "Oscillator:Oscillator_SMD_EuroQuartz_XO32-4Pin_3.2x2.5mm" H 2500 6050 50  0001 C CNN
F 3 "http://cdn-reichelt.de/documents/datenblatt/B400/XO32.pdf" H 1700 6400 50  0001 C CNN
	1    1800 6400
	1    0    0    -1  
$EndComp
Text GLabel 4700 2300 0    50   Input ~ 0
BCD2
Text GLabel 4700 2400 0    50   Input ~ 0
BCD3
Text GLabel 4700 2500 0    50   Input ~ 0
BCD4
Text GLabel 4700 2600 0    50   Input ~ 0
BCD5
Text GLabel 4700 2700 0    50   Input ~ 0
BCD6
Text GLabel 4700 2800 0    50   Input ~ 0
BCD7
Wire Wire Line
	900  6250 900  6000
Wire Wire Line
	900  6000 1800 6000
Wire Wire Line
	1800 6000 1800 5850
Wire Wire Line
	1800 6100 1800 6000
Connection ~ 1800 6000
Wire Wire Line
	1800 6700 1800 6800
Wire Wire Line
	900  6550 900  6800
Wire Wire Line
	900  6800 1800 6800
Connection ~ 1800 6800
Wire Wire Line
	1800 6800 1800 6950
Text Label 2100 6400 0    50   ~ 0
MCLK
Text GLabel 7550 2650 2    50   Input ~ 0
GND
Wire Wire Line
	6300 1700 6950 1700
Text Label 6300 2000 0    50   ~ 0
DOAB1
Text Label 6300 2100 0    50   ~ 0
DOCD1
Text Label 6300 1900 0    50   ~ 0
SMPAC1
Text Label 6300 1800 0    50   ~ 0
SMPBD1
Text Label 6300 1700 0    50   ~ 0
PHISY1
Wire Wire Line
	6950 1800 6300 1800
Wire Wire Line
	6300 1900 6950 1900
Wire Wire Line
	6300 2000 6950 2000
Text Notes 6200 3250 0    50   ~ 0
Does it need another YAC for DOCD? \nYes\nor must you put all voices on AB
Text GLabel 8150 1700 2    50   Input ~ 0
AOUT1
$Comp
L Gemini:YAC512 U9
U 1 1 5E7FCA92
P 7550 2050
F 0 "U9" H 7850 3100 50  0000 C CNN
F 1 "YAC512" H 7950 3000 50  0000 C CNN
F 2 "Gemini:SOIC-24_5.0x15.24mm_P1.27mm" H 7550 2250 50  0001 C CNN
F 3 "" H 7550 2050 50  0001 C CNN
	1    7550 2050
	1    0    0    -1  
$EndComp
Text GLabel 8150 1600 2    50   Input ~ 0
MP1
Text GLabel 8150 1500 2    50   Input ~ 0
CV1
Text GLabel 8150 1800 2    50   Input ~ 0
SWIN1
Text GLabel 8150 1900 2    50   Input ~ 0
CH1R
Text GLabel 8150 2000 2    50   Input ~ 0
CH1L
$Comp
L Amplifier_Operational:TL074 U10
U 1 1 5E8174AA
P 9650 950
F 0 "U10" H 9650 1317 50  0000 C CNN
F 1 "TL074" H 9650 1226 50  0000 C CNN
F 2 "" H 9600 1050 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/tl071.pdf" H 9700 1150 50  0001 C CNN
	1    9650 950 
	1    0    0    -1  
$EndComp
Wire Wire Line
	7650 750  7450 750 
Wire Wire Line
	7350 750  7350 850 
Wire Wire Line
	7650 750  7650 850 
Wire Wire Line
	7450 850  7450 750 
Connection ~ 7450 750 
Wire Wire Line
	7450 750  7350 750 
Wire Wire Line
	7350 750  6850 750 
Wire Wire Line
	6850 750  6850 1500
Wire Wire Line
	6850 1500 6950 1500
Connection ~ 7350 750 
Wire Wire Line
	6600 600  6850 600 
Wire Wire Line
	6850 600  6850 750 
Connection ~ 6850 750 
$Comp
L Amplifier_Operational:TL074 U10
U 5 1 5E82B109
P 6450 7000
F 0 "U10" H 6408 7046 50  0000 L CNN
F 1 "TL074" H 6408 6955 50  0000 L CNN
F 2 "" H 6400 7100 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/tl071.pdf" H 6500 7200 50  0001 C CNN
	5    6450 7000
	1    0    0    -1  
$EndComp
Text GLabel 6350 7300 0    50   Input ~ 0
GND
Text GLabel 6350 6700 0    50   Input ~ 0
+5V
Text GLabel 5500 600  0    50   Input ~ 0
+5V
$Comp
L Gemini:YMF262-M U8
U 1 1 5E6F4CC0
P 5500 1650
F 0 "U8" H 5500 1000 50  0000 C CNN
F 1 "YMF262-M" H 5550 850 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-24W_7.5x15.4mm_Pitch1.27mm" H 5500 1650 50  0001 C CNN
F 3 "" H 5500 1650 50  0001 C CNN
	1    5500 1650
	1    0    0    -1  
$EndComp
Text GLabel 6600 600  0    50   Input ~ 0
+5V
Text GLabel 8350 850  0    50   Input ~ 0
GND
$Comp
L Device:CP1_Small C6
U 1 1 5E834C18
P 8550 850
F 0 "C6" V 8322 850 50  0000 C CNN
F 1 "10uF" V 8413 850 50  0000 C CNN
F 2 "Capacitors_THT:CP_Axial_L11.0mm_D5.0mm_P18.00mm_Horizontal" H 8550 850 50  0001 C CNN
F 3 "~" H 8550 850 50  0001 C CNN
	1    8550 850 
	0    1    1    0   
$EndComp
Wire Wire Line
	8350 850  8450 850 
$Comp
L Amplifier_Operational:TL074 U10
U 2 1 5E836B56
P 9650 1800
F 0 "U10" H 9650 2167 50  0000 C CNN
F 1 "TL074" H 9650 2076 50  0000 C CNN
F 2 "" H 9600 1900 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/tl071.pdf" H 9700 2000 50  0001 C CNN
	2    9650 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	8150 1700 9150 1700
$Comp
L Device:R_Small_US R6
U 1 1 5E84599C
P 8850 1900
F 0 "R6" V 8645 1900 50  0000 C CNN
F 1 "33" V 8736 1900 50  0000 C CNN
F 2 "" H 8850 1900 50  0001 C CNN
F 3 "~" H 8850 1900 50  0001 C CNN
	1    8850 1900
	0    1    1    0   
$EndComp
Wire Wire Line
	9050 1900 9350 1900
Wire Wire Line
	9050 1900 9050 2100
Wire Wire Line
	9050 2100 10050 2100
Wire Wire Line
	10050 2100 10050 1800
Wire Wire Line
	10050 1800 9950 1800
Wire Wire Line
	8750 1900 8550 1900
Wire Wire Line
	8550 1900 8550 1800
Wire Wire Line
	8550 1800 8150 1800
Wire Wire Line
	8950 1900 9050 1900
Connection ~ 9050 1900
Wire Wire Line
	8650 850  8750 850 
Wire Wire Line
	8750 1500 8150 1500
Connection ~ 8750 850 
Wire Wire Line
	8750 850  9350 850 
Wire Wire Line
	8750 850  8750 1500
Wire Wire Line
	8150 1600 8850 1600
Wire Wire Line
	8850 1250 8850 1600
Wire Wire Line
	9950 950  10050 950 
Wire Wire Line
	10050 950  10050 1250
Wire Wire Line
	8850 1250 8850 1050
Connection ~ 8850 1250
Wire Wire Line
	8850 1250 10050 1250
Wire Wire Line
	8850 1050 9350 1050
$Comp
L Device:CP1_Small C7
U 1 1 5E8675CD
P 9150 1500
F 0 "C7" H 9059 1454 50  0000 R CNN
F 1 "10uF" H 9059 1545 50  0000 R CNN
F 2 "Capacitors_THT:CP_Axial_L11.0mm_D5.0mm_P18.00mm_Horizontal" H 9150 1500 50  0001 C CNN
F 3 "~" H 9150 1500 50  0001 C CNN
	1    9150 1500
	-1   0    0    1   
$EndComp
Wire Wire Line
	9150 1600 9150 1700
Connection ~ 9150 1700
Wire Wire Line
	9150 1700 9350 1700
Text GLabel 9150 1400 0    50   Input ~ 0
GND
$Sheet
S 10300 2450 700  700 
U 5E8134DB
F0 "Amplifier" 50
F1 "opl3_amp.sch" 50
$EndSheet
$Sheet
S 6450 4600 850  750 
U 5F2FEF9D
F0 "Backplane" 50
F1 "Backplane.sch" 50
$EndSheet
$EndSCHEMATC
