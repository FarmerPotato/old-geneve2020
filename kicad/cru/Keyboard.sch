EESchema Schematic File Version 4
LIBS:Ruby-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 74xx:74LS595 U5
U 1 1 5FAB665F
P 4250 2850
F 0 "U5" H 4250 3631 50  0000 C CNN
F 1 "74LS595" H 4250 3540 50  0000 C CNN
F 2 "" H 4250 2850 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls595" H 4250 2850 50  0001 C CNN
	1    4250 2850
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS595 U18
U 1 1 5FAB6915
P 4250 4450
F 0 "U18" H 4250 5231 50  0000 C CNN
F 1 "74LS595" H 4250 5140 50  0000 C CNN
F 2 "" H 4250 4450 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls595" H 4250 4450 50  0001 C CNN
	1    4250 4450
	1    0    0    -1  
$EndComp
Text GLabel 1950 3050 0    50   Input ~ 0
KBD_CLR*
Wire Wire Line
	3850 4650 3300 4650
Wire Wire Line
	3300 4650 3300 3050
Connection ~ 3300 3050
Wire Wire Line
	3300 3050 3850 3050
Wire Wire Line
	3850 4550 3400 4550
Wire Wire Line
	3400 2950 3850 2950
Wire Wire Line
	3850 2750 3500 2750
Wire Wire Line
	3500 2750 3500 4350
Wire Wire Line
	3500 4350 3850 4350
Wire Wire Line
	3850 4250 3600 4250
Wire Wire Line
	3600 4250 3600 2650
Wire Wire Line
	3600 2650 3850 2650
Wire Wire Line
	4650 3350 4750 3350
Wire Wire Line
	4750 3350 4750 3700
Wire Wire Line
	4750 3700 3700 3700
Wire Wire Line
	3700 3700 3700 4050
Wire Wire Line
	3700 4050 3850 4050
Text Label 4900 2450 0    50   ~ 0
STOP_BIT
$Comp
L 74xx:74LS04 U3
U 2 1 5FAB9F13
P 2700 2650
F 0 "U3" H 2700 2967 50  0000 C CNN
F 1 "74LS04" H 2700 2876 50  0000 C CNN
F 2 "" H 2700 2650 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 2700 2650 50  0001 C CNN
	2    2700 2650
	1    0    0    -1  
$EndComp
Text GLabel 1950 2650 0    50   Input ~ 0
KBD_CLK
Text GLabel 1950 2150 0    50   Input ~ 0
KBD_DATA
$Comp
L 74xx:74LS04 U3
U 1 1 5FABA88E
P 2700 2150
F 0 "U3" H 2700 2467 50  0000 C CNN
F 1 "74LS04" H 2700 2376 50  0000 C CNN
F 2 "" H 2700 2150 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 2700 2150 50  0001 C CNN
	1    2700 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	1950 2150 2400 2150
Wire Wire Line
	2400 2650 2250 2650
Wire Wire Line
	3000 2650 3600 2650
Connection ~ 3600 2650
Wire Wire Line
	3000 2150 3600 2150
Wire Wire Line
	3600 2150 3600 2450
Wire Wire Line
	3600 2450 3850 2450
Text Label 4900 2550 0    50   ~ 0
PARITY
Text Label 4900 2650 0    50   ~ 0
D7
Text Label 4900 2750 0    50   ~ 0
D6
Text Label 4900 2850 0    50   ~ 0
D5
Text Label 4900 2950 0    50   ~ 0
D4
Text Label 4900 3050 0    50   ~ 0
D3
Text Label 4900 3150 0    50   ~ 0
D2
Text Label 4900 4050 0    50   ~ 0
D1
Text Label 4900 4150 0    50   ~ 0
D0
Text Label 4900 4250 0    50   ~ 0
START_BIT
Text Notes 5550 4950 0    50   ~ 0
Inverted SER Start bit \nshows as a 1\n\nInverted again for a falling \nedge to the interrupt pin.\n\nIf device starts transmitting\nagain, the interrupt will be lost.\nCRU wants it present for about 10 cycles?\na kbd cycle is 83 us which is a\nlot of CRU cycles.
$Comp
L 74xx:74LS04 U3
U 3 1 5FAC1B0A
P 5950 4250
F 0 "U3" H 5950 4567 50  0000 C CNN
F 1 "74LS04" H 5950 4476 50  0000 C CNN
F 2 "" H 5950 4250 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 5950 4250 50  0001 C CNN
	3    5950 4250
	1    0    0    -1  
$EndComp
Text GLabel 7300 4250 2    50   Output ~ 0
INT5*
Text Notes 1100 2650 0    50   ~ 0
Data is valid on falling edge \nof KBD_CLK.\n\nData is shifted in on rising edge \nof SRCLK.
Text Notes 1500 1800 0    50   ~ 0
Invert data so that\nstart bit of 0 is detected \nas a 1 on QC.
Text Notes 5750 2200 0    50   ~ 0
Parallel outputs could be transferred\nserially, or in one word read.
$Comp
L Gemini:74LS251 U21
U 1 1 5FACDC7F
P 6350 3250
F 0 "U21" H 6350 4117 50  0000 C CNN
F 1 "74LS251" H 6350 4026 50  0000 C CNN
F 2 "" H 6350 3250 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS251" H 6350 3250 50  0001 C CNN
	1    6350 3250
	1    0    0    -1  
$EndComp
Entry Wire Line
	5300 2450 5400 2550
Entry Wire Line
	5300 2550 5400 2650
Entry Wire Line
	5300 2650 5400 2750
Entry Wire Line
	5300 2750 5400 2850
Entry Wire Line
	5300 2850 5400 2950
Entry Wire Line
	5300 2950 5400 3050
Entry Wire Line
	5300 3050 5400 3150
Entry Wire Line
	5300 3150 5400 3250
Entry Wire Line
	5300 4050 5400 4150
Entry Wire Line
	5300 4150 5400 4250
Entry Wire Line
	5300 4250 5400 4350
Wire Wire Line
	4650 2550 5300 2550
Wire Wire Line
	4650 2650 5300 2650
Wire Wire Line
	4650 2750 5300 2750
Wire Wire Line
	4650 2850 5300 2850
Wire Wire Line
	4650 2950 5300 2950
Wire Wire Line
	4650 3050 5300 3050
Wire Wire Line
	4650 2450 5300 2450
Entry Wire Line
	5650 2550 5750 2650
Entry Wire Line
	5650 2650 5750 2750
Entry Wire Line
	5650 2750 5750 2850
Entry Wire Line
	5650 2850 5750 2950
Wire Wire Line
	5750 2650 5850 2650
Text Label 5750 2650 0    50   ~ 0
D3
Text Label 5750 2950 0    50   ~ 0
D0
Text Label 5750 2850 0    50   ~ 0
D1
Text Label 5750 2750 0    50   ~ 0
D2
Text Label 6850 2750 0    50   ~ 0
D4
Text Label 6850 2850 0    50   ~ 0
D5
Text Label 6850 2950 0    50   ~ 0
D6
Text Label 6850 3050 0    50   ~ 0
D7
Entry Wire Line
	6950 2750 7050 2650
Entry Wire Line
	6950 2850 7050 2750
Entry Wire Line
	6950 2950 7050 2850
Entry Wire Line
	6950 3050 7050 2950
Wire Wire Line
	6950 2750 6850 2750
Wire Wire Line
	6850 2850 6950 2850
Wire Wire Line
	6950 2950 6850 2950
Wire Wire Line
	6850 3050 6950 3050
Wire Bus Line
	5400 2300 5650 2300
Connection ~ 5650 2300
Wire Bus Line
	5650 2300 7050 2300
Wire Wire Line
	5750 2750 5850 2750
Wire Wire Line
	5750 2850 5850 2850
Wire Wire Line
	5750 2950 5850 2950
Wire Wire Line
	4650 3150 5300 3150
Wire Wire Line
	4650 4050 5300 4050
Wire Wire Line
	4650 4150 5300 4150
Text Notes 6400 3950 0    50   ~ 0
CPU need not\ninvert the data.
Wire Wire Line
	5850 3150 5650 3150
Text GLabel 7300 3750 2    50   Output ~ 0
CRUIN
Wire Wire Line
	5850 3250 5750 3250
Wire Wire Line
	5750 3250 5750 3650
Wire Wire Line
	5750 3650 7300 3650
Wire Wire Line
	7300 3750 5650 3750
Wire Wire Line
	5650 3150 5650 3750
Text GLabel 5850 3350 3    50   Input ~ 0
GND
Text GLabel 7300 3650 2    50   Input ~ 0
KBD_SERENA*
Text Label 3550 4650 0    50   ~ 0
RCLR*
Text GLabel 7300 3150 2    50   Input ~ 0
A0
Text GLabel 7300 3250 2    50   Input ~ 0
A1
Text GLabel 7300 3350 2    50   Input ~ 0
A2
Wire Wire Line
	6850 3150 7300 3150
Wire Wire Line
	7300 3250 6850 3250
Wire Wire Line
	6850 3350 7300 3350
Wire Wire Line
	1950 3050 3300 3050
Wire Wire Line
	6250 4250 7300 4250
Text Label 7150 4550 0    50   ~ 0
PARITY
Text Label 7150 4650 0    50   ~ 0
D7
Text Label 7150 4750 0    50   ~ 0
D6
Text Label 7150 4850 0    50   ~ 0
D5
Text Label 7150 4950 0    50   ~ 0
D4
Text Label 7150 5050 0    50   ~ 0
D3
Text Label 7150 5150 0    50   ~ 0
D2
Text Label 7150 5250 0    50   ~ 0
D1
Text Label 7150 5350 0    50   ~ 0
D0
Entry Wire Line
	7050 4450 7150 4550
Entry Wire Line
	7050 4550 7150 4650
Entry Wire Line
	7050 4650 7150 4750
Entry Wire Line
	7050 4750 7150 4850
Entry Wire Line
	7050 4850 7150 4950
Entry Wire Line
	7050 4950 7150 5050
Entry Wire Line
	7050 5050 7150 5150
Entry Wire Line
	7050 5150 7150 5250
Entry Wire Line
	7050 5250 7150 5350
Wire Wire Line
	7150 4550 7550 4550
Wire Wire Line
	7150 4650 7550 4650
Wire Wire Line
	7150 4750 7550 4750
Wire Wire Line
	7150 4850 7550 4850
Wire Wire Line
	7150 4950 7550 4950
Wire Wire Line
	7150 5050 7550 5050
Wire Wire Line
	7150 5150 7550 5150
Wire Wire Line
	7150 5250 7550 5250
Wire Wire Line
	7150 5350 7550 5350
$Comp
L 74xx:74LS165 U19
U 1 1 5FB1AD2E
P 4250 6650
F 0 "U19" H 4250 7731 50  0000 C CNN
F 1 "74LS165" H 4250 7640 50  0000 C CNN
F 2 "" H 4250 6650 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS165" H 4250 6650 50  0001 C CNN
	1    4250 6650
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3400 2900 2250 2900
Wire Wire Line
	2250 2900 2250 2650
Wire Wire Line
	3400 2900 3400 2950
Connection ~ 3400 2950
Wire Wire Line
	3400 2950 3400 4550
Connection ~ 2250 2650
Wire Wire Line
	2250 2650 1950 2650
$Comp
L 74xx:74LS165 U20
U 1 1 5FB21812
P 5600 6650
F 0 "U20" H 5600 7731 50  0000 C CNN
F 1 "74LS165" H 5600 7640 50  0000 C CNN
F 2 "" H 5600 6650 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS165" H 5600 6650 50  0001 C CNN
	1    5600 6650
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4650 4250 5650 4250
Wire Bus Line
	5650 2300 5650 2850
Wire Bus Line
	5400 2300 5400 4350
Wire Bus Line
	7050 2300 7050 5450
$EndSCHEMATC
