EESchema Schematic File Version 4
LIBS:Ruby-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 900  1100 0    100  ~ 20
RS232/1
NoConn ~ 3150 1950
Text GLabel 1550 1650 0    50   Input ~ 0
A0
Text GLabel 1550 1750 0    50   Input ~ 0
A1
Text GLabel 1550 1850 0    50   Input ~ 0
A2
Text GLabel 1550 1950 0    50   Input ~ 0
A3
Text GLabel 1550 2050 0    50   Input ~ 0
A4
Text GLabel 1550 2250 0    50   Input ~ 0
CLK3
Text GLabel 1550 2150 0    50   Input ~ 0
CRUCLK
Text GLabel 1550 2350 0    50   Input ~ 0
SEL_1340*
Text GLabel 1550 1550 0    50   Input ~ 0
CRUOUT
Text GLabel 1550 1450 0    50   Input ~ 0
CRUIN
NoConn ~ 3800 5600
Text GLabel 2200 5900 0    50   Input ~ 0
CLK3
Text GLabel 2200 5800 0    50   Input ~ 0
CRUCLK
Text GLabel 2200 6000 0    50   Input ~ 0
SEL_1380*
Text GLabel 2200 5200 0    50   Input ~ 0
CRUOUT
Text GLabel 2200 5100 0    50   Input ~ 0
CRUIN
Text GLabel 2200 5000 0    50   Input ~ 0
INT_1380*
Text GLabel 2900 4250 0    50   Input ~ 0
+5V
Text GLabel 2350 850  2    50   Input ~ 0
+5V
Text GLabel 7200 3750 2    50   Input ~ 0
GND
Text GLabel 3000 6500 0    50   Input ~ 0
GND
Text Label 3350 2350 2    50   ~ 0
XOUT1>
Text Label 3350 2250 2    50   ~ 0
RIN1<
Text Label 3350 2150 2    50   ~ 0
RTS1>
Text Label 3350 2050 2    50   ~ 0
CTS1<
Text Label 4300 6000 2    50   ~ 0
XOUT2
Text Label 4300 5700 2    50   ~ 0
RTS2
Text Label 4300 6100 2    50   ~ 0
CTS2
Text GLabel 2200 5300 0    50   Input ~ 0
A0
Text GLabel 2200 5400 0    50   Input ~ 0
A1
Text GLabel 2200 5500 0    50   Input ~ 0
A2
Text GLabel 2200 5600 0    50   Input ~ 0
A3
Text GLabel 2200 5700 0    50   Input ~ 0
A4
Text GLabel 1550 1350 0    50   Input ~ 0
INT_1340*
Text Notes 8600 5450 0    50   ~ 0
## Interrupts raised by 9901\nName Description\nINT0 CPU Reset\nINT1 (Backplane) VDP interrupt\nINT2 (CPU only)\n     MID (not maskable), \n     Arithmetic Overflow\nINT3 (Reserved for  TMS9995/9901) \n       Decrementer Interrupt\nINT4 (Backplane) Peripheral Interrupt. \n        Disk I/O?\nINT5 (CRU) Serial (any port)\nINT6 (Backplane) Available\nINT7 (CRU) Keyboard. Geneve 9640 \n         key scancode available.\nINT8 (CRU) Mouse\nINT9 (Backplane)\n\nBackplane has INT1, INT4, INT6, INT9\nINT10 is unused?\n\nPMOD1/2 is  [P0..P7] at >2020  disabling INT15\nPMOD3     is [P8..P11] at >2030 disabling [INT14..INT11]
Text Notes 1950 4650 0    100  ~ 20
RS232/2\n(Console)
Text Notes 9500 1100 0    50   ~ 0
Strategy: determine clock data rate\nfrom sampling a CRU pin. Set the\nUART to the rate.
Text Notes 5450 7350 0    50   ~ 0
APPLYING INPUT CURRENT TO AN\nOUTPUT PIN MAY DAMAGE THE 9901\n\nOutputs can drive 2 TTL loads
Text Notes 6800 1150 0    100  ~ 20
DE-9 Serial Port
Text Notes 9650 2100 0    50   ~ 0
1300 Reserved for PIO\n1340 RS232/1\n1380 RS232/2 FTDI Console\n13C0 Unused\n1400 \n1440 Keyboard\n1480 Mouse\n14C0 MIDI\n1500 PIO/2 (expansion)\n1540 RS232/3 (expansion)\n1580 RS232/4 (expansion)
$Comp
L Connector_Generic:Conn_01x06 J8
U 1 1 5F031F9C
P 4800 5800
F 0 "J8" V 4672 6080 50  0000 L CNN
F 1 "FTDI" V 4763 6080 50  0000 L CNN
F 2 "Pin_Headers:Pin_Header_Angled_1x06_Pitch2.54mm" H 4800 5800 50  0001 C CNN
F 3 "~" H 4800 5800 50  0001 C CNN
	1    4800 5800
	1    0    0    -1  
$EndComp
$Comp
L Interface_UART:MAX232 U5
U 1 1 5F03CE49
P 4550 2050
F 0 "U5" H 4550 3431 50  0000 C CNN
F 1 "MAX232" H 4550 3340 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-16_3.9x9.9mm_Pitch1.27mm" H 4600 1000 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/max232.pdf" H 4550 2150 50  0001 C CNN
	1    4550 2050
	1    0    0    -1  
$EndComp
Text Label 3350 1950 2    50   ~ 0
DSR1<
Wire Wire Line
	3150 1950 3350 1950
Wire Wire Line
	3150 2150 3750 2150
Wire Wire Line
	3150 2350 3750 2350
Wire Wire Line
	3450 2750 3450 2250
Wire Wire Line
	3150 2250 3450 2250
Wire Wire Line
	3450 2750 3750 2750
Wire Wire Line
	3550 2550 3550 2050
Wire Wire Line
	3150 2050 3550 2050
Wire Wire Line
	3550 2550 3750 2550
Wire Wire Line
	5500 1150 5500 1200
Wire Wire Line
	5350 1150 5500 1150
Wire Wire Line
	5350 1450 5500 1450
Wire Wire Line
	5500 1450 5500 1400
Wire Wire Line
	3750 1450 3600 1450
Wire Wire Line
	3600 1450 3600 1400
Wire Wire Line
	3600 1150 3750 1150
Wire Wire Line
	3600 1200 3600 1150
Text Label 5400 1650 0    50   ~ 0
8.5V
Text Label 5350 1950 0    50   ~ 0
-8.5V
Text Notes 7200 1300 0    50   ~ 0
Wired as DTE
Text GLabel 4600 5600 0    50   Input ~ 0
GND
Text Notes 3700 5700 0    50   ~ 0
CTS<
Text Notes 3700 5600 0    50   ~ 0
DSR<
Text Notes 3700 5800 0    50   ~ 0
RTS>
Text Notes 3700 5900 0    50   ~ 0
RX<
Text Notes 3700 6000 0    50   ~ 0
TX>
Wire Wire Line
	3800 5700 4000 5700
Text Label 4300 5900 2    50   ~ 0
RIN2
Text Notes 4600 6150 2    50   ~ 0
RTS>
Text Notes 4600 6050 2    50   ~ 0
RX<
Text Notes 4600 5950 2    50   ~ 0
TX>
Text Notes 4600 5750 2    50   ~ 0
CTS<
Text Notes 4600 5850 2    50   ~ 0
3.3V>
Text Notes 8550 7100 0    50   ~ 0
Pull the clock low.\nHold it low for 100 us\nPull KBD_DATA low\nRelease Clock\nWait for a KBD_CLK low\nBegin sending on KBD_DATA
$Comp
L Gemini:TMS9902 U13
U 1 1 5E17761C
P 3000 5500
F 0 "U13" H 3000 6681 50  0000 C CNN
F 1 "TMS9902" H 3000 6590 50  0000 C CNN
F 2 "Housings_DIP:DIP-18_W7.62mm_Socket_LongPads" H 3000 5500 50  0001 C CNN
F 3 "" H 3000 5500 50  0001 C CNN
	1    3000 5500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2350 2850 2350 3450
Wire Wire Line
	7200 3750 7200 3450
Wire Wire Line
	5350 1650 5900 1650
Wire Wire Line
	5350 1950 5700 1950
Wire Wire Line
	5700 2150 5700 3450
Connection ~ 5700 3450
Wire Wire Line
	5900 1850 5900 3450
Wire Wire Line
	5700 3450 5900 3450
Connection ~ 5900 3450
Wire Wire Line
	5900 3450 7200 3450
Text GLabel 4550 850  2    50   Input ~ 0
+5V
Wire Wire Line
	2350 3450 4550 3450
Wire Wire Line
	3800 6000 4600 6000
Wire Wire Line
	3800 5900 4600 5900
Wire Wire Line
	4000 5700 4000 6100
Wire Wire Line
	4600 6100 4000 6100
Wire Wire Line
	3800 5800 4100 5800
Text Notes 4650 5150 0    100  ~ 20
FTDI\nHeader
Text Notes 6000 3150 0    50   ~ 0
Second MAX232?\nDSR in\nDTR out
NoConn ~ 6700 2050
Text Label 7200 2050 0    50   ~ 0
RI
Text Label 7200 2150 0    50   ~ 0
RTS
Text Label 7200 2250 0    50   ~ 0
GND
Text Label 7200 2350 0    50   ~ 0
TXD
Text Label 7200 2450 0    50   ~ 0
DCD
Text Label 6700 2150 2    50   ~ 0
CTS
Text Label 6700 2250 2    50   ~ 0
DSR
Text Label 6700 2350 2    50   ~ 0
DTR
Text Label 6700 2450 2    50   ~ 0
RXD
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J2
U 1 1 5F015675
P 7000 2250
F 0 "J2" H 7050 1825 50  0000 C CNN
F 1 "COM1 Header" H 7050 1916 50  0000 C CNN
F 2 "Connector_IDC:IDC-Header_2x05_P2.54mm_Vertical" H 7000 2250 50  0001 C CNN
F 3 "~" H 7000 2250 50  0001 C CNN
	1    7000 2250
	-1   0    0    1   
$EndComp
Entry Wire Line
	6500 2350 6600 2450
Entry Wire Line
	6500 2050 6600 2150
Wire Wire Line
	6600 2150 6700 2150
Wire Wire Line
	6600 2250 6700 2250
Wire Wire Line
	6600 2350 6700 2350
Wire Wire Line
	6600 2450 6700 2450
NoConn ~ 6600 2250
NoConn ~ 6600 2350
NoConn ~ 7200 2450
NoConn ~ 7200 2050
Entry Wire Line
	7350 2350 7450 2250
Entry Wire Line
	7350 2150 7450 2050
Wire Wire Line
	7350 2350 7200 2350
Wire Wire Line
	7350 2150 7200 2150
Wire Bus Line
	6500 1700 7450 1700
Connection ~ 6500 1700
Entry Wire Line
	6100 2350 6200 2250
Entry Wire Line
	6100 2550 6200 2450
Entry Wire Line
	6100 2750 6200 2650
Entry Wire Line
	6100 2150 6200 2050
Wire Wire Line
	6100 2750 5350 2750
Wire Wire Line
	6100 2550 5350 2550
Wire Wire Line
	5350 2350 6100 2350
Wire Bus Line
	6200 1700 6500 1700
Wire Wire Line
	7400 2250 7400 3450
Wire Wire Line
	7200 2250 7400 2250
Wire Wire Line
	7200 3450 7400 3450
Connection ~ 7200 3450
$Comp
L Gemini:TMS9902 U17
U 1 1 5E0F3868
P 2350 1850
F 0 "U17" H 2350 3031 50  0000 C CNN
F 1 "TMS9902" H 2350 2940 50  0000 C CNN
F 2 "Housings_DIP:DIP-18_W7.62mm_Socket_LongPads" H 2350 1850 50  0001 C CNN
F 3 "" H 2350 1850 50  0001 C CNN
	1    2350 1850
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C21
U 1 1 5F48B46D
P 5500 1300
F 0 "C21" H 5592 1346 50  0000 L CNN
F 1 "1uF" H 5592 1255 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206" H 5500 1300 50  0001 C CNN
F 3 "~" H 5500 1300 50  0001 C CNN
	1    5500 1300
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C20
U 1 1 5F48B82B
P 3600 1300
F 0 "C20" H 3692 1346 50  0000 L CNN
F 1 "1uF" H 3692 1255 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206" H 3600 1300 50  0001 C CNN
F 3 "~" H 3600 1300 50  0001 C CNN
	1    3600 1300
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C22
U 1 1 5F48BB7D
P 5900 1750
F 0 "C22" H 5992 1796 50  0000 L CNN
F 1 "1uF" H 5992 1705 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206" H 5900 1750 50  0001 C CNN
F 3 "~" H 5900 1750 50  0001 C CNN
	1    5900 1750
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C23
U 1 1 5F48BF56
P 5700 2050
F 0 "C23" H 5792 2096 50  0000 L CNN
F 1 "1uF" H 5792 2005 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206" H 5700 2050 50  0001 C CNN
F 3 "~" H 5700 2050 50  0001 C CNN
	1    5700 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5350 2150 6100 2150
Text Label 5950 2350 0    50   ~ 0
TXD
Text Label 6100 2750 2    50   ~ 0
RXD
Text Label 6100 2550 2    50   ~ 0
CTS
Text Label 6100 2150 2    50   ~ 0
RTS
Wire Wire Line
	4400 5800 4600 5800
Wire Wire Line
	3000 4500 3000 4250
Wire Wire Line
	3000 4250 2900 4250
Wire Wire Line
	4100 5700 4100 5800
Wire Wire Line
	4100 5700 4600 5700
Text GLabel 4400 5800 0    50   Input ~ 0
+3V3
NoConn ~ 10700 -300
Wire Wire Line
	4550 3250 4550 3450
Connection ~ 4550 3450
Wire Wire Line
	4550 3450 5700 3450
Wire Bus Line
	7450 1700 7450 2250
Wire Bus Line
	6500 1700 6500 2350
Wire Bus Line
	6200 1700 6200 2750
$EndSCHEMATC
