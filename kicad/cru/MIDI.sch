EESchema Schematic File Version 4
LIBS:Ruby-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 8500 7050 0    50   ~ 0
The DC15 game port header is\nwasteful, but it may\nbecome joystick ports.\nPC analog stick takes more design.\nTwo Atari style, not so hard.\nThey could share PMOD port?
Text GLabel 6850 3500 2    50   Input ~ 0
B_MUS_CLK
Text GLabel 6850 3700 2    50   Input ~ 0
B_KBD_CLK
Text GLabel 6850 3900 2    50   Input ~ 0
MIDI_OUT
Text GLabel 6350 3400 0    50   Input ~ 0
GND
Text GLabel 6350 3500 0    50   Input ~ 0
GND
Text GLabel 6850 3800 2    50   Input ~ 0
MIDI_IN
Text GLabel 4550 3200 2    50   Input ~ 0
MIDI_OUT
Text GLabel 4550 3100 2    50   Input ~ 0
MIDI_IN
Wire Wire Line
	4550 3100 3800 3100
Text Notes 3700 3750 0    50   ~ 0
MIDI circuit is on\nthe front panel.\nDB15 carries just\nthe digital signal.
Wire Wire Line
	3800 3200 4550 3200
Text GLabel 3000 1700 2    50   Input ~ 0
+5V
NoConn ~ 3800 3000
NoConn ~ 3800 2900
Text Label 3800 3100 0    50   ~ 0
MIDI_IN
Text Notes 2250 1650 0    100  ~ 20
MIDI
$Comp
L Gemini:TMS9902 U?
U 1 1 5F9BB523
P 3000 2700
AR Path="/5EEDD9EE/5F9BB523" Ref="U?"  Part="1" 
AR Path="/5F9B7653/5F9BB523" Ref="U14"  Part="1" 
F 0 "U14" H 3000 3881 50  0000 C CNN
F 1 "TMS9902" H 3000 3790 50  0000 C CNN
F 2 "Housings_DIP:DIP-18_W7.62mm_Socket_LongPads" H 3000 2700 50  0001 C CNN
F 3 "" H 3000 2700 50  0001 C CNN
	1    3000 2700
	1    0    0    -1  
$EndComp
Text GLabel 2200 2500 0    50   Input ~ 0
A0
Text GLabel 2200 2600 0    50   Input ~ 0
A1
Text GLabel 2200 2700 0    50   Input ~ 0
A2
Text GLabel 2200 2800 0    50   Input ~ 0
A3
Text GLabel 2200 2900 0    50   Input ~ 0
A4
Text GLabel 2200 3100 0    50   Input ~ 0
CLK3
Text GLabel 2200 3000 0    50   Input ~ 0
CRUCLK
Text GLabel 2200 3200 0    50   Input ~ 0
SEL_14C0*
Text GLabel 2200 2400 0    50   Input ~ 0
CRUOUT
Text GLabel 2200 2300 0    50   Input ~ 0
CRUIN
Text GLabel 3000 3700 0    50   Input ~ 0
GND
Text GLabel 2200 2200 0    50   Input ~ 0
INT_14C0*
Text Notes 4300 3100 0    50   ~ 0
RX<
Text Label 4100 3200 2    50   ~ 0
MIDI_OUT
Text Notes 4350 3200 0    50   ~ 0
TX>
NoConn ~ 3800 2800
Text GLabel 6850 3600 2    50   Input ~ 0
B_KBD_DATA
Text GLabel 6850 3400 2    50   Input ~ 0
B_MUS_DATA
$Comp
L Connector_Generic:Conn_02x06_Odd_Even J4
U 1 1 5F1C5B19
P 6550 3600
F 0 "J4" H 6600 2975 50  0000 C CNN
F 1 "GamePortHdr" H 6600 2900 50  0000 C CNN
F 2 "Connector_IDC:IDC-Header_2x06_P2.54mm_Vertical" H 6500 4150 50  0001 C CNN
F 3 "~" H 6550 3600 50  0001 C CNN
	1    6550 3600
	1    0    0    -1  
$EndComp
Text GLabel 6350 3600 0    50   Input ~ 0
GND
Text GLabel 6350 3700 0    50   Input ~ 0
GND
Text GLabel 6350 3800 0    50   Input ~ 0
GND
Text GLabel 6350 3900 0    50   Input ~ 0
GND
$EndSCHEMATC
