EESchema Schematic File Version 4
LIBS:Ruby-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 700  2350 0    100  ~ 0
Card Select\n
Text GLabel 7950 4400 2    50   Input ~ 0
GND
Text Notes 7100 6950 0    50   ~ 0
All bit numbering is 0 LSBit.\nData bus lines are normalized for \n16-bit word D0-D15. 8-bit operations are \nin the MSB D8-D15, which is the even address.
$Comp
L Gemini:TMS9901 U10
U 1 1 5EE9F884
P 7950 2950
F 0 "U10" H 7950 4581 50  0000 C CNN
F 1 "TMS9901" H 7950 4490 50  0000 C CNN
F 2 "Housings_DIP:DIP-40_W15.24mm_Socket_LongPads" H 7950 2950 50  0001 C CNN
F 3 "" H 7950 2950 50  0001 C CNN
	1    7950 2950
	1    0    0    -1  
$EndComp
$Sheet
S 3500 1800 1150 600 
U 5EEDD9EE
F0 "Serial" 50
F1 "Connie.sch" 50
$EndSheet
Text GLabel 6900 2000 0    50   Input ~ 0
CRUOUT
Text GLabel 9000 3500 2    50   Input ~ 0
A0
Text GLabel 9000 3400 2    50   Input ~ 0
A1
Text GLabel 9000 2400 2    50   Input ~ 0
A2
Text GLabel 9000 2300 2    50   Input ~ 0
A3
Text GLabel 9000 2000 2    50   Input ~ 0
A4
Text GLabel 1900 2800 1    50   Output ~ 0
SEL_2000*
Text GLabel 1000 3600 0    50   Input ~ 0
A10
Text GLabel 1800 4200 3    50   Input ~ 0
A8
Text GLabel 1000 3200 0    50   Input ~ 0
SERENA*
Text GLabel 1000 3400 0    50   Input ~ 0
RDBENA*
$Comp
L Gemini:ATF22LV10C-28J U7
U 1 1 5EFD4091
P 1600 3600
F 0 "U7" H 2100 4300 50  0000 L CNN
F 1 "ATF22LV10C-28J" H 2300 4200 50  0000 L CNN
F 2 "Sockets:PLCC28" H 1600 3550 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/doc0780.pdf" H 1600 3550 50  0001 C CNN
	1    1600 3600
	1    0    0    -1  
$EndComp
Text GLabel 1000 3700 0    50   Input ~ 0
A6
Text GLabel 1400 4200 3    50   Input ~ 0
A5
Text GLabel 2400 3300 2    50   Output ~ 0
SEL_1340*
Text GLabel 2400 3400 2    50   Output ~ 0
SEL_1380*
Text GLabel 2400 3600 2    50   Output ~ 0
SEL_1440*
Text GLabel 2000 4200 3    50   Output ~ 0
SEL_1480*
Text GLabel 2400 3800 2    50   Output ~ 0
SEL_14C0*
Text GLabel 1600 4200 3    50   Input ~ 0
GND
Text GLabel 1700 4200 3    50   Input ~ 0
GND
Text GLabel 1000 3500 0    50   Input ~ 0
GND
Text GLabel 1700 2800 1    50   Input ~ 0
+5V
Text GLabel 1800 2800 1    50   Input ~ 0
+5V
Text GLabel 1600 2800 1    50   Input ~ 0
WE*
Text GLabel 2400 3500 2    50   Input ~ 0
GND
Text GLabel 7950 1500 0    50   Input ~ 0
+5V
$Sheet
S 3450 1000 1250 600 
U 5F4BC2EA
F0 "Backplane" 50
F1 "Backplane.sch" 50
$EndSheet
Text GLabel 9000 2700 2    50   Input ~ 0
B_INT9*
Text GLabel 9000 2500 2    50   Input ~ 0
INT7*
Text GLabel 9000 2600 2    50   Input ~ 0
INT8*
Text GLabel 1900 4200 3    50   Output ~ 0
CRUEN*
Text GLabel 6450 6750 2    50   Input ~ 0
B_INT1*
Text GLabel 6450 6650 2    50   Input ~ 0
B_INT6*
Text GLabel 6450 6450 2    50   Input ~ 0
B_INT9*
Wire Wire Line
	6350 6650 6450 6650
Wire Wire Line
	6350 6750 6450 6750
$Comp
L Connector_Generic:Conn_02x06_Odd_Even J5
U 1 1 5F827BBB
P 10400 2450
F 0 "J5" H 10450 2867 50  0000 C CNN
F 1 "PMOD1/2" H 10450 2776 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Angled_2x06_Pitch2.54mm" H 10400 2450 50  0001 C CNN
F 3 "~" H 10400 2450 50  0001 C CNN
	1    10400 2450
	1    0    0    -1  
$EndComp
Text GLabel 10700 2750 2    50   Input ~ 0
GND
Text GLabel 10200 2750 0    50   Input ~ 0
GND
Text GLabel 10200 2650 0    50   Input ~ 0
+3V3
Wire Wire Line
	4400 5800 4500 5800
Wire Wire Line
	3700 5600 3800 5600
Text GLabel 4600 5600 2    50   Input ~ 0
CLK3
Wire Wire Line
	3550 5700 3800 5700
Text GLabel 3550 5700 0    50   Input ~ 0
B_CLKOUT
$Comp
L 74xx:74LS74 U?
U 2 1 5F945820
P 4100 5700
AR Path="/5EEDD9EE/5F945820" Ref="U?"  Part="1" 
AR Path="/5F945820" Ref="U1"  Part="2" 
F 0 "U1" H 4100 6181 50  0000 C CNN
F 1 "74LS74" H 4100 6090 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 4100 5700 50  0001 C CNN
F 3 "74xx/74hc_hct74.pdf" H 4100 5700 50  0001 C CNN
	2    4100 5700
	1    0    0    -1  
$EndComp
Text Notes 3450 5100 0    100  ~ 0
3MHz Clock
Text GLabel 9000 5650 0    50   Input ~ 0
INT_1340*
Text GLabel 9000 6050 0    50   Input ~ 0
INT_1440*
Text GLabel 9000 5750 0    50   Input ~ 0
INT_1380*
Text Notes 8500 5350 0    100  ~ 0
Serial Interrupts
Text GLabel 9600 5750 2    50   Input ~ 0
INT5*
Text Notes 6050 6150 0    50   ~ 0
Pull-up Resistors
Text GLabel 9000 6150 0    50   Input ~ 0
INT_1480*
Text GLabel 10700 2650 2    50   Input ~ 0
+3V3
Text Label 9100 3700 0    50   ~ 0
P3
Text Label 9100 3800 0    50   ~ 0
P4
Text Label 9100 3600 0    50   ~ 0
P7
Text Label 9100 3300 0    50   ~ 0
P2
Text Label 9100 3200 0    50   ~ 0
P8
Text Label 9100 3100 0    50   ~ 0
P9
Text Label 9100 3000 0    50   ~ 0
P10
Text Label 9100 2900 0    50   ~ 0
P11
Entry Wire Line
	9350 3800 9450 3900
Entry Wire Line
	9350 3700 9450 3800
Entry Wire Line
	9350 3300 9450 3400
Entry Wire Line
	9350 3200 9450 3300
Entry Wire Line
	9350 3100 9450 3200
Entry Wire Line
	9350 3000 9450 3100
Entry Wire Line
	9350 2900 9450 3000
Entry Wire Line
	9350 3600 9450 3700
Entry Wire Line
	9350 2800 9450 2900
Text Label 9150 2100 0    50   ~ 0
P0
Text Label 9150 2200 0    50   ~ 0
P1
Entry Wire Line
	9350 2100 9450 2200
Entry Wire Line
	9350 2200 9450 2300
Text Label 10100 2550 2    50   ~ 0
P3
Text Label 10750 2250 0    50   ~ 0
P4
Text Label 10750 2550 0    50   ~ 0
P7
Entry Wire Line
	10950 2250 11050 2350
Entry Wire Line
	10950 2350 11050 2450
Entry Wire Line
	10950 2450 11050 2550
Entry Wire Line
	10950 2550 11050 2650
Wire Wire Line
	10700 2550 10950 2550
Wire Wire Line
	10950 2450 10700 2450
Wire Wire Line
	10700 2350 10950 2350
Wire Wire Line
	10950 2250 10700 2250
Wire Wire Line
	10200 2450 9950 2450
Wire Wire Line
	10200 2350 9950 2350
Wire Wire Line
	9950 2250 10200 2250
Wire Wire Line
	9950 2550 10200 2550
Entry Wire Line
	9850 2150 9950 2250
Entry Wire Line
	9850 2250 9950 2350
Entry Wire Line
	9850 2350 9950 2450
Entry Wire Line
	9850 2450 9950 2550
Wire Bus Line
	9850 2850 11050 2850
Wire Bus Line
	9450 2000 9850 2000
Text Label 10000 2250 0    50   ~ 0
P0
Text Label 10000 2350 0    50   ~ 0
P1
Text Label 10000 2450 0    50   ~ 0
P2
Text Label 10750 2450 0    50   ~ 0
P6
Text Label 10750 2350 0    50   ~ 0
P5
$Sheet
S 3500 2700 1200 1050
U 5F9B7653
F0 "MIDI" 50
F1 "MIDI.sch" 50
$EndSheet
Text GLabel 9000 5850 0    50   Input ~ 0
INT_14C0*
$Comp
L 74xx:74LS11 U8
U 1 1 5FA01666
P 9300 5750
F 0 "U8" H 9300 6075 50  0000 C CNN
F 1 "74LS11" H 9300 5984 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 9300 5750 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS11" H 9300 5750 50  0001 C CNN
	1    9300 5750
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS11 U8
U 2 1 5FA01DC4
P 7800 5700
F 0 "U8" H 7800 6025 50  0000 C CNN
F 1 "74LS11" H 7800 5934 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 7800 5700 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS11" H 7800 5700 50  0001 C CNN
	2    7800 5700
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS11 U8
U 3 1 5FA05ACC
P 7600 6350
F 0 "U8" H 7600 6675 50  0000 C CNN
F 1 "74LS11" H 7600 6584 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 7600 6350 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS11" H 7600 6350 50  0001 C CNN
	3    7600 6350
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS11 U8
U 4 1 5FA06275
P 10500 5800
F 0 "U8" H 10730 5846 50  0000 L CNN
F 1 "74LS11" H 10730 5755 50  0000 L CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 10500 5800 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS11" H 10500 5800 50  0001 C CNN
	4    10500 5800
	1    0    0    -1  
$EndComp
Text GLabel 10500 5300 0    50   Input ~ 0
+5V
Text GLabel 10500 6300 2    50   Input ~ 0
GND
Text GLabel 7500 5600 0    50   Input ~ 0
+5V
Text GLabel 7500 5800 0    50   Input ~ 0
+5V
Text GLabel 7500 5700 0    50   Input ~ 0
+5V
Text Notes 7300 5500 0    50   ~ 0
unused
Text Label 10000 4150 0    50   ~ 0
P8
Text Label 10000 4250 0    50   ~ 0
P9
Text Label 10000 4350 0    50   ~ 0
P10
Text Label 10000 4450 0    50   ~ 0
P11
Entry Wire Line
	9450 4050 9550 4150
Entry Wire Line
	9450 4150 9550 4250
Entry Wire Line
	9450 4250 9550 4350
Entry Wire Line
	9450 4350 9550 4450
Wire Wire Line
	9550 4150 10200 4150
Wire Wire Line
	9550 4250 10200 4250
Wire Wire Line
	9550 4350 10200 4350
Wire Wire Line
	9550 4450 10200 4450
Text GLabel 5950 6300 0    50   Input ~ 0
+5V
Text GLabel 6450 6550 2    50   Input ~ 0
B_INT4*
$Comp
L 74xx:74LS74 U?
U 3 1 5FAA23EC
P 5250 5750
AR Path="/5EEDD9EE/5FAA23EC" Ref="U?"  Part="3" 
AR Path="/5FAA23EC" Ref="U1"  Part="3" 
F 0 "U1" H 5480 5796 50  0000 L CNN
F 1 "74LS74" H 5480 5705 50  0000 L CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 5250 5750 50  0001 C CNN
F 3 "74xx/74hc_hct74.pdf" H 5250 5750 50  0001 C CNN
	3    5250 5750
	1    0    0    -1  
$EndComp
Text GLabel 5250 5350 0    50   Input ~ 0
+5V
Text GLabel 5250 6150 0    50   Input ~ 0
GND
Text GLabel 2400 3700 2    50   Output ~ 0
CRUCLK
Text Notes 8550 5900 2    63   ~ 0
RS232\nFTDI\nMIDI
Text GLabel 9600 6050 2    50   Input ~ 0
INT7*
Text GLabel 9600 6150 2    50   Input ~ 0
INT8*
Wire Wire Line
	9000 6050 9600 6050
Wire Wire Line
	9600 6150 9000 6150
Text Notes 8550 6200 2    63   ~ 0
Keyboard\nMouse
Text GLabel 7300 6250 0    50   Input ~ 0
+5V
Text GLabel 7300 6450 0    50   Input ~ 0
+5V
Text GLabel 7300 6350 0    50   Input ~ 0
+5V
Text Notes 7100 6150 0    50   ~ 0
unused
NoConn ~ 8100 5700
NoConn ~ 7900 6350
Text GLabel 10200 4150 2    50   Input ~ 0
KEY_CLK_O
Text GLabel 10200 4250 2    50   Input ~ 0
KEY_DATA_O
Text GLabel 10200 4350 2    50   Input ~ 0
MOUSE_CLK_O
Text GLabel 10200 4450 2    50   Input ~ 0
MOUSE_DATA_O
$Sheet
S 3500 4000 1150 500 
U 5F2EEAA0
F0 "KeyboardMouse" 50
F1 "KeyboardMouse.sch" 50
$EndSheet
Wire Wire Line
	4400 5600 4600 5600
Text GLabel 3600 5200 0    50   Input ~ 0
+5V
Wire Wire Line
	4100 5400 3600 5400
Wire Wire Line
	3600 5400 3600 6000
Wire Wire Line
	3600 6000 4100 6000
Wire Wire Line
	3700 5600 3700 6150
Wire Wire Line
	3700 6150 4500 6150
Wire Wire Line
	4500 6150 4500 5800
Wire Wire Line
	3600 5400 3600 5200
Connection ~ 3600 5400
Text Notes 2500 2950 0    50   ~ 0
CRUCLK = !WE*\nSaves an inverter
$Comp
L Device:R_Small R9
U 1 1 5EFE9D5C
P 6250 6450
F 0 "R9" V 6054 6450 50  0000 C CNN
F 1 "3.3K" V 6250 6450 50  0000 C CNN
F 2 "Resistors_SMD:R_1206" H 6250 6450 50  0001 C CNN
F 3 "~" H 6250 6450 50  0001 C CNN
	1    6250 6450
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R11
U 1 1 5EFEBD91
P 6250 6550
F 0 "R11" V 6054 6550 50  0000 C CNN
F 1 "3.3K" V 6250 6550 50  0000 C CNN
F 2 "Resistors_SMD:R_1206" H 6250 6550 50  0001 C CNN
F 3 "~" H 6250 6550 50  0001 C CNN
	1    6250 6550
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R10
U 1 1 5F00110A
P 6250 6650
F 0 "R10" V 6054 6650 50  0000 C CNN
F 1 "3.3K" V 6250 6650 50  0000 C CNN
F 2 "Resistors_SMD:R_1206" H 6250 6650 50  0001 C CNN
F 3 "~" H 6250 6650 50  0001 C CNN
	1    6250 6650
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R12
U 1 1 5F001114
P 6250 6750
F 0 "R12" V 6054 6750 50  0000 C CNN
F 1 "3.3K" V 6250 6750 50  0000 C CNN
F 2 "Resistors_SMD:R_1206" H 6250 6750 50  0001 C CNN
F 3 "~" H 6250 6750 50  0001 C CNN
	1    6250 6750
	0    1    1    0   
$EndComp
Wire Wire Line
	6350 6550 6450 6550
Wire Wire Line
	6450 6450 6350 6450
Wire Wire Line
	5950 6450 6150 6450
Wire Wire Line
	6150 6550 5950 6550
Connection ~ 5950 6550
Wire Wire Line
	5950 6550 5950 6450
Wire Wire Line
	6150 6650 5950 6650
Connection ~ 5950 6650
Wire Wire Line
	5950 6650 5950 6550
Wire Wire Line
	6150 6750 5950 6750
Wire Wire Line
	5950 6750 5950 6650
Text Notes -100 2850 0    50   ~ 0
Want PLD. All Combinatorial.\ninputs\nSERENA*\n7 Inputs: A13-A7 -> ABxx\n2 Inputs? A6-A5  useful for \nanti-aliasing and decoding \nUART selects.\n\nRDBENA*\n11 Total\n\nOutputs: \nSEL_2000*\n\nSEL_1300*\nSEL_1340*\nSEL_1380*\nSEL_13C0*\n\nSEL_1500*\nSEL_1540*\nSEL_1580*\nSEL_15C0*\n\nCRUEN*\n\n6 left over\n\nTotal: 11 inputs, 10 outputs\n\n
Connection ~ 5950 6450
Wire Wire Line
	5950 6450 5950 6300
$Comp
L Device:C_Small C?
U 1 1 5F560AF3
P 2050 7450
AR Path="/5EEDD9EE/5F560AF3" Ref="C?"  Part="1" 
AR Path="/5F560AF3" Ref="C1"  Part="1" 
F 0 "C1" H 2000 7600 50  0000 L CNN
F 1 "cap" H 1950 7250 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206" H 2088 7300 50  0001 C CNN
F 3 "~" H 2050 7450 50  0001 C CNN
	1    2050 7450
	1    0    0    -1  
$EndComp
Wire Wire Line
	2050 7350 2250 7350
Text GLabel 1850 6700 0    50   Input ~ 0
+5V
Text GLabel 1100 7700 0    50   Input ~ 0
GND
Wire Wire Line
	2050 7350 1850 7350
Connection ~ 2050 7350
$Comp
L Device:C_Small C?
U 1 1 5F56C2D6
P 2250 7450
AR Path="/5EEDD9EE/5F56C2D6" Ref="C?"  Part="1" 
AR Path="/5F56C2D6" Ref="C2"  Part="1" 
F 0 "C2" H 2200 7600 50  0000 L CNN
F 1 "cap" H 2150 7250 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206" H 2288 7300 50  0001 C CNN
F 3 "~" H 2250 7450 50  0001 C CNN
	1    2250 7450
	1    0    0    -1  
$EndComp
Connection ~ 2250 7350
Wire Wire Line
	2250 7350 2450 7350
$Comp
L Device:C_Small C?
U 1 1 5F56C3EE
P 2450 7450
AR Path="/5EEDD9EE/5F56C3EE" Ref="C?"  Part="1" 
AR Path="/5F56C3EE" Ref="C3"  Part="1" 
F 0 "C3" H 2400 7600 50  0000 L CNN
F 1 "cap" H 2350 7250 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206" H 2488 7300 50  0001 C CNN
F 3 "~" H 2450 7450 50  0001 C CNN
	1    2450 7450
	1    0    0    -1  
$EndComp
Connection ~ 2450 7350
$Comp
L Device:C_Small C?
U 1 1 5F56C4FD
P 2650 7450
AR Path="/5EEDD9EE/5F56C4FD" Ref="C?"  Part="1" 
AR Path="/5F56C4FD" Ref="C4"  Part="1" 
F 0 "C4" H 2600 7600 50  0000 L CNN
F 1 "cap" H 2550 7250 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206" H 2688 7300 50  0001 C CNN
F 3 "~" H 2650 7450 50  0001 C CNN
	1    2650 7450
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5F56C9D4
P 2850 7450
AR Path="/5EEDD9EE/5F56C9D4" Ref="C?"  Part="1" 
AR Path="/5F56C9D4" Ref="C5"  Part="1" 
F 0 "C5" H 2800 7600 50  0000 L CNN
F 1 "cap" H 2750 7250 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206" H 2888 7300 50  0001 C CNN
F 3 "~" H 2850 7450 50  0001 C CNN
	1    2850 7450
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5F56C9DE
P 3050 7450
AR Path="/5EEDD9EE/5F56C9DE" Ref="C?"  Part="1" 
AR Path="/5F56C9DE" Ref="C6"  Part="1" 
F 0 "C6" H 3000 7600 50  0000 L CNN
F 1 "cap" H 2950 7250 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206" H 3088 7300 50  0001 C CNN
F 3 "~" H 3050 7450 50  0001 C CNN
	1    3050 7450
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5F56C9E8
P 3250 7450
AR Path="/5EEDD9EE/5F56C9E8" Ref="C?"  Part="1" 
AR Path="/5F56C9E8" Ref="C7"  Part="1" 
F 0 "C7" H 3200 7600 50  0000 L CNN
F 1 "cap" H 3150 7250 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206" H 3288 7300 50  0001 C CNN
F 3 "~" H 3250 7450 50  0001 C CNN
	1    3250 7450
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5F56C9F2
P 3450 7450
AR Path="/5EEDD9EE/5F56C9F2" Ref="C?"  Part="1" 
AR Path="/5F56C9F2" Ref="C8"  Part="1" 
F 0 "C8" H 3400 7600 50  0000 L CNN
F 1 "cap" H 3350 7250 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206" H 3488 7300 50  0001 C CNN
F 3 "~" H 3450 7450 50  0001 C CNN
	1    3450 7450
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5F56F684
P 3850 7450
AR Path="/5EEDD9EE/5F56F684" Ref="C?"  Part="1" 
AR Path="/5F56F684" Ref="C10"  Part="1" 
F 0 "C10" H 3800 7600 50  0000 L CNN
F 1 "cap" H 3750 7250 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206" H 3888 7300 50  0001 C CNN
F 3 "~" H 3850 7450 50  0001 C CNN
	1    3850 7450
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5F56F68E
P 4050 7450
AR Path="/5EEDD9EE/5F56F68E" Ref="C?"  Part="1" 
AR Path="/5F56F68E" Ref="C11"  Part="1" 
F 0 "C11" H 4000 7600 50  0000 L CNN
F 1 "cap" H 3950 7250 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206" H 4088 7300 50  0001 C CNN
F 3 "~" H 4050 7450 50  0001 C CNN
	1    4050 7450
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5F56F698
P 4250 7450
AR Path="/5EEDD9EE/5F56F698" Ref="C?"  Part="1" 
AR Path="/5F56F698" Ref="C12"  Part="1" 
F 0 "C12" H 4200 7600 50  0000 L CNN
F 1 "cap" H 4150 7250 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206" H 4288 7300 50  0001 C CNN
F 3 "~" H 4250 7450 50  0001 C CNN
	1    4250 7450
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5F56F6A2
P 4450 7450
AR Path="/5EEDD9EE/5F56F6A2" Ref="C?"  Part="1" 
AR Path="/5F56F6A2" Ref="C13"  Part="1" 
F 0 "C13" H 4400 7600 50  0000 L CNN
F 1 "cap" H 4350 7250 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206" H 4488 7300 50  0001 C CNN
F 3 "~" H 4450 7450 50  0001 C CNN
	1    4450 7450
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5F56F6AC
P 4650 7450
AR Path="/5EEDD9EE/5F56F6AC" Ref="C?"  Part="1" 
AR Path="/5F56F6AC" Ref="C14"  Part="1" 
F 0 "C14" H 4600 7600 50  0000 L CNN
F 1 "cap" H 4550 7250 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206" H 4688 7300 50  0001 C CNN
F 3 "~" H 4650 7450 50  0001 C CNN
	1    4650 7450
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5F56F6B6
P 4850 7450
AR Path="/5EEDD9EE/5F56F6B6" Ref="C?"  Part="1" 
AR Path="/5F56F6B6" Ref="C15"  Part="1" 
F 0 "C15" H 4800 7600 50  0000 L CNN
F 1 "cap" H 4750 7250 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206" H 4888 7300 50  0001 C CNN
F 3 "~" H 4850 7450 50  0001 C CNN
	1    4850 7450
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5F56F6C0
P 5050 7450
AR Path="/5EEDD9EE/5F56F6C0" Ref="C?"  Part="1" 
AR Path="/5F56F6C0" Ref="C16"  Part="1" 
F 0 "C16" H 5000 7600 50  0000 L CNN
F 1 "cap" H 4950 7250 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206" H 5088 7300 50  0001 C CNN
F 3 "~" H 5050 7450 50  0001 C CNN
	1    5050 7450
	1    0    0    -1  
$EndComp
Connection ~ 2850 7350
Wire Wire Line
	2850 7350 3050 7350
Connection ~ 3050 7350
Wire Wire Line
	3050 7350 3250 7350
Connection ~ 3250 7350
Wire Wire Line
	3250 7350 3450 7350
Connection ~ 3450 7350
Connection ~ 3850 7350
Wire Wire Line
	3850 7350 4050 7350
Connection ~ 4050 7350
Wire Wire Line
	4050 7350 4250 7350
Connection ~ 4250 7350
Wire Wire Line
	4250 7350 4450 7350
Connection ~ 4450 7350
Wire Wire Line
	4450 7350 4650 7350
Connection ~ 4650 7350
Wire Wire Line
	4650 7350 4850 7350
Connection ~ 4850 7350
Wire Wire Line
	4850 7350 5050 7350
Wire Wire Line
	5050 7550 4850 7550
Connection ~ 2050 7550
Connection ~ 2250 7550
Wire Wire Line
	2250 7550 2050 7550
Connection ~ 2450 7550
Wire Wire Line
	2450 7550 2250 7550
Connection ~ 2650 7550
Wire Wire Line
	2650 7550 2450 7550
Connection ~ 2850 7550
Wire Wire Line
	2850 7550 2650 7550
Connection ~ 3050 7550
Wire Wire Line
	3050 7550 2850 7550
Connection ~ 3250 7550
Wire Wire Line
	3250 7550 3050 7550
Connection ~ 3450 7550
Wire Wire Line
	3450 7550 3250 7550
Connection ~ 3850 7550
Connection ~ 4050 7550
Wire Wire Line
	4050 7550 3850 7550
Connection ~ 4250 7550
Wire Wire Line
	4250 7550 4050 7550
Connection ~ 4450 7550
Wire Wire Line
	4450 7550 4250 7550
Connection ~ 4650 7550
Wire Wire Line
	4650 7550 4450 7550
Connection ~ 4850 7550
Wire Wire Line
	4850 7550 4650 7550
$Comp
L Device:C_Small C?
U 1 1 5F57D0E9
P 5250 7450
AR Path="/5EEDD9EE/5F57D0E9" Ref="C?"  Part="1" 
AR Path="/5F57D0E9" Ref="C17"  Part="1" 
F 0 "C17" H 5200 7600 50  0000 L CNN
F 1 "cap" H 5150 7250 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206" H 5288 7300 50  0001 C CNN
F 3 "~" H 5250 7450 50  0001 C CNN
	1    5250 7450
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5F57D0F3
P 3650 7450
AR Path="/5EEDD9EE/5F57D0F3" Ref="C?"  Part="1" 
AR Path="/5F57D0F3" Ref="C9"  Part="1" 
F 0 "C9" H 3600 7600 50  0000 L CNN
F 1 "cap" H 3550 7250 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206" H 3688 7300 50  0001 C CNN
F 3 "~" H 3650 7450 50  0001 C CNN
	1    3650 7450
	1    0    0    -1  
$EndComp
Wire Wire Line
	5050 7350 5250 7350
Connection ~ 5050 7350
Connection ~ 5250 7350
Wire Wire Line
	5250 7350 5450 7350
Wire Wire Line
	5450 7550 5250 7550
Connection ~ 5050 7550
Connection ~ 5250 7550
Wire Wire Line
	5250 7550 5050 7550
Wire Wire Line
	2450 7350 2650 7350
Connection ~ 2650 7350
Wire Wire Line
	2650 7350 2850 7350
Wire Wire Line
	3450 7350 3650 7350
Wire Wire Line
	3450 7550 3650 7550
$Comp
L Connector:Conn_01x20_Female J6
U 1 1 5F5F5312
P 8950 2900
F 0 "J6" H 8800 1750 50  0000 L CNN
F 1 "Conn_01x20_Female" H 8978 2785 50  0001 L CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x20_Pitch2.54mm" H 8950 2900 50  0001 C CNN
F 3 "~" H 8950 2900 50  0001 C CNN
	1    8950 2900
	1    0    0    1   
$EndComp
Wire Wire Line
	8750 2000 9000 2000
Connection ~ 8750 2000
Wire Wire Line
	9000 2300 8750 2300
Connection ~ 8750 2300
Wire Wire Line
	8750 2400 9000 2400
Connection ~ 8750 2400
Wire Wire Line
	9000 2500 8750 2500
Connection ~ 8750 2500
Wire Wire Line
	8750 2600 9000 2600
Connection ~ 8750 2600
Wire Wire Line
	9000 2700 8750 2700
Connection ~ 8750 2700
Wire Wire Line
	9000 3400 8750 3400
Connection ~ 8750 3400
Wire Wire Line
	8750 3500 9000 3500
Connection ~ 8750 3500
Text Label 9100 2800 0    50   ~ 0
P12
Wire Wire Line
	8750 3800 9350 3800
Connection ~ 8750 3800
Wire Wire Line
	9350 3700 8750 3700
Connection ~ 8750 3700
Wire Wire Line
	8750 3600 9350 3600
Connection ~ 8750 3600
Wire Wire Line
	9350 3300 8750 3300
Connection ~ 8750 3300
Wire Wire Line
	9350 3200 8750 3200
Connection ~ 8750 3200
Wire Wire Line
	8750 3100 9350 3100
Connection ~ 8750 3100
Wire Wire Line
	9350 3000 8750 3000
Connection ~ 8750 3000
Wire Wire Line
	8750 2900 9350 2900
Connection ~ 8750 2900
Wire Wire Line
	9350 2800 8750 2800
Connection ~ 8750 2800
Wire Wire Line
	8750 2200 9350 2200
Connection ~ 8750 2200
Wire Wire Line
	9350 2100 8750 2100
Connection ~ 8750 2100
Text Notes 9200 8400 0    50   ~ 0
## Interrupts raised by 9901\nName Description\nINT0 CPU Reset\nINT1 (Backplane) VDP interrupt\nINT2 (CPU only)    \n     MID (not maskable), \n     Arithmetic Overflow\nINT3 (Reserved for  TMS9995/9901) \n       Decrementer Interrupt\nINT4 (Backplane) Peripheral Interrupt. \n        Disk I/O?\nINT5 (CRU) Serial (any port)\nINT6 (Backplane) Available\nINT7 (CRU) Keyboard. Geneve 9640 \n         key scancode available.\nINT8 (CRU) Mouse\nINT9 (Backplane)\n\nBackplane has INT1, INT4, INT6, INT9\nINT10 is unused?\n\nPMOD1/2 is  [P0..P7] at >2020  disabling INT15\nPMOD3     is [P8..P11] at >2030 disabling [INT14..INT11]
Entry Wire Line
	6300 3700 6400 3800
Text Label 6500 3800 0    50   ~ 0
P5
Text Label 6500 3700 0    50   ~ 0
P6
Text GLabel 6900 2500 0    50   Input ~ 0
INT5*
Text GLabel 6900 2200 0    50   Input ~ 0
CRUIN
Text GLabel 6900 2100 0    50   Input ~ 0
CRUCLK
Text GLabel 6900 2800 0    50   Input ~ 0
CLK3
Text GLabel 6900 2900 0    50   Input ~ 0
INTREQ*
Text GLabel 6900 2600 0    50   Input ~ 0
B_INT4*
Text GLabel 6900 2400 0    50   Input ~ 0
B_INT6*
Text GLabel 6900 3500 0    50   Input ~ 0
B_INT1*
Text GLabel 6900 1900 0    50   Input ~ 0
RESOUT*
Text GLabel 6900 2300 0    50   Input ~ 0
SEL_2000*
$Comp
L Connector:Conn_01x20_Female J7
U 1 1 5F6CD94A
P 6950 2800
F 0 "J7" H 6800 3900 50  0000 L CNN
F 1 "Conn_01x20_Female" H 6978 2685 50  0001 L CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x20_Pitch2.54mm" H 6950 2800 50  0001 C CNN
F 3 "~" H 6950 2800 50  0001 C CNN
	1    6950 2800
	-1   0    0    -1  
$EndComp
Entry Wire Line
	6300 3600 6400 3700
Wire Bus Line
	6300 4550 9450 4550
Wire Wire Line
	6900 1900 7150 1900
Connection ~ 7150 1900
Wire Wire Line
	7150 2000 6900 2000
Connection ~ 7150 2000
Wire Wire Line
	6900 2100 7150 2100
Connection ~ 7150 2100
Wire Wire Line
	7150 2200 6900 2200
Connection ~ 7150 2200
Wire Wire Line
	6900 2300 7150 2300
Connection ~ 7150 2300
Wire Wire Line
	7150 2400 6900 2400
Connection ~ 7150 2400
Wire Wire Line
	6900 2500 7150 2500
Connection ~ 7150 2500
Wire Wire Line
	7150 2600 6900 2600
Connection ~ 7150 2600
Wire Wire Line
	6900 2800 7150 2800
Connection ~ 7150 2800
Wire Wire Line
	7150 2900 6900 2900
Connection ~ 7150 2900
Wire Wire Line
	6900 3000 7150 3000
Connection ~ 7150 3000
Wire Wire Line
	7150 3100 6900 3100
Connection ~ 7150 3100
Wire Wire Line
	6900 3200 7150 3200
Connection ~ 7150 3200
Wire Wire Line
	7150 3300 6900 3300
Connection ~ 7150 3300
Wire Wire Line
	6900 3500 7150 3500
Connection ~ 7150 3500
Wire Wire Line
	6400 3700 7150 3700
Connection ~ 7150 3700
Wire Wire Line
	6400 3800 7150 3800
Connection ~ 7150 3800
Text GLabel 6900 3400 0    50   Input ~ 0
GND
Wire Wire Line
	6900 3400 7150 3400
Text GLabel 6900 3600 0    50   Input ~ 0
GND
Text GLabel 6900 2700 0    50   Input ~ 0
GND
Wire Wire Line
	6900 2700 7150 2700
Connection ~ 7150 2700
Wire Wire Line
	7150 3600 6900 3600
Connection ~ 7150 3600
Text GLabel 9000 1900 2    50   Input ~ 0
+5V
Wire Wire Line
	8750 1900 9000 1900
$Comp
L Regulator_Linear:TLV713285PDBV U?
U 1 1 5F836A48
P 1100 7250
AR Path="/5EEDD9EE/5F836A48" Ref="U?"  Part="1" 
AR Path="/5F836A48" Ref="U12"  Part="1" 
F 0 "U12" H 1100 7592 50  0000 C CNN
F 1 "TLV713285PDBV" H 1100 7501 50  0000 C CNN
F 2 "TO_SOT_Packages_SMD:SOT-23-5" H 1100 7550 50  0001 C CIN
F 3 "http://www.ti.com/lit/ds/symlink/tlv713p.pdf" H 1100 7250 50  0001 C CNN
	1    1100 7250
	-1   0    0    -1  
$EndComp
Text GLabel 800  6700 2    50   Output ~ 0
+3V3
Wire Wire Line
	1850 7350 1850 7150
Wire Wire Line
	1400 7250 1400 7150
Wire Wire Line
	1400 7150 1850 7150
Connection ~ 1400 7150
Connection ~ 1850 7150
Wire Wire Line
	1850 7150 1850 6850
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 5F86ECC0
P 1850 6850
F 0 "#FLG0101" H 1850 6925 50  0001 C CNN
F 1 "PWR_FLAG" V 1850 6978 50  0000 L CNN
F 2 "" H 1850 6850 50  0001 C CNN
F 3 "~" H 1850 6850 50  0001 C CNN
	1    1850 6850
	0    1    1    0   
$EndComp
Connection ~ 1850 6850
Wire Wire Line
	1850 6850 1850 6700
Wire Wire Line
	800  6700 800  7150
Text GLabel 1500 2800 1    50   Input ~ 0
A12
Text GLabel 1500 4200 3    50   Input ~ 0
A7
Text GLabel 1000 3800 0    50   Input ~ 0
A11
Text GLabel 1400 2800 1    50   Input ~ 0
A9
Wire Wire Line
	2400 3200 2400 2700
Text GLabel 2400 2700 1    50   Input ~ 0
LED0
Text Notes 2500 4200 0    50   ~ 0
SERENA* implies\nA13=0 to save a bit
NoConn ~ 2000 2800
Text GLabel 1000 3300 0    50   Input ~ 0
A13
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 5F683000
P 900 7550
F 0 "#FLG0102" H 900 7625 50  0001 C CNN
F 1 "PWR_FLAG" V 900 7678 50  0000 L CNN
F 2 "" H 900 7550 50  0001 C CNN
F 3 "~" H 900 7550 50  0001 C CNN
	1    900  7550
	0    -1   -1   0   
$EndComp
Wire Wire Line
	900  7550 1100 7550
Connection ~ 1100 7550
Wire Wire Line
	1100 7550 2050 7550
Wire Wire Line
	1100 7550 1100 7700
Text Notes 5600 3150 0    50   ~ 0
Should IC0-3\nhave bus drivers?\nYes
Text GLabel 6900 3000 0    50   Output ~ 0
IC3
Text GLabel 6900 3100 0    50   Output ~ 0
IC2
Text GLabel 6900 3200 0    50   Output ~ 0
IC1
Text GLabel 6900 3300 0    50   Output ~ 0
IC0
Connection ~ 3650 7350
Wire Wire Line
	3650 7350 3850 7350
Connection ~ 3650 7550
Wire Wire Line
	3650 7550 3850 7550
Text Notes 9500 2900 0    50   ~ 0
unused
Wire Bus Line
	6300 1150 6300 4550
Wire Bus Line
	9850 2000 9850 2850
Wire Bus Line
	11050 2350 11050 2850
Wire Bus Line
	9450 2000 9450 4550
$EndSCHEMATC
