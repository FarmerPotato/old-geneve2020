EESchema Schematic File Version 4
LIBS:NewCard-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 5250 5150 0    50   ~ 0
MSB
Text GLabel 5450 4650 0    50   Input ~ 0
AD4
Text GLabel 5450 4950 0    50   Input ~ 0
AD1
Text GLabel 5450 4850 0    50   Input ~ 0
AD2
Text GLabel 5450 4750 0    50   Input ~ 0
AD3
Text GLabel 5450 4550 0    50   Input ~ 0
AD5
Text GLabel 5450 4450 0    50   Input ~ 0
AD6
Text GLabel 5450 4350 0    50   Input ~ 0
AD7
Text GLabel 6450 2300 2    50   Input ~ 0
LPSEL*
Text GLabel 6450 2400 2    50   Input ~ 0
LA14
Text GLabel 6450 2500 2    50   Input ~ 0
LA13
Text GLabel 6450 2600 2    50   Input ~ 0
LA12
Text GLabel 6450 2700 2    50   Input ~ 0
LA11
Text GLabel 6450 2800 2    50   Input ~ 0
LA10
Text GLabel 6450 2900 2    50   Input ~ 0
LA9
Text GLabel 6450 3000 2    50   Input ~ 0
LA8
Text GLabel 5450 5350 0    50   Input ~ 0
GND
Text GLabel 5450 5250 0    50   Input ~ 0
ALATCH
Text GLabel 5950 3600 2    50   Input ~ 0
GND
$Comp
L 74xx:74LS573 U?
U 1 1 5ED146C2
P 5950 2800
AR Path="/5ED146C2" Ref="U?"  Part="1" 
AR Path="/5E905665/5ED146C2" Ref="U?"  Part="1" 
AR Path="/5E96C96E/5ED146C2" Ref="U?"  Part="1" 
AR Path="/5E6D9FBE/5ED146C2" Ref="U?"  Part="1" 
F 0 "U?" H 5950 3781 50  0000 C CNN
F 1 "74HCT573" H 5950 3690 50  0000 C CNN
F 2 "Package_SO:SO-20_12.8x7.5mm_P1.27mm" H 5950 2800 50  0001 C CNN
F 3 "74xx/74hc573.pdf" H 5950 2800 50  0001 C CNN
	1    5950 2800
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS573 U?
U 1 1 5ED146C8
P 5950 4850
AR Path="/5ED146C8" Ref="U?"  Part="1" 
AR Path="/5E905665/5ED146C8" Ref="U?"  Part="1" 
AR Path="/5E96C96E/5ED146C8" Ref="U?"  Part="1" 
AR Path="/5E6D9FBE/5ED146C8" Ref="U?"  Part="1" 
F 0 "U?" H 5950 5831 50  0000 C CNN
F 1 "74HCT573" H 5700 5500 50  0000 C CNN
F 2 "Package_SO:SO-20_12.8x7.5mm_P1.27mm" H 5950 4850 50  0001 C CNN
F 3 "74xx/74hc573.pdf" H 5950 4850 50  0001 C CNN
	1    5950 4850
	1    0    0    -1  
$EndComp
Text GLabel 5450 3200 0    50   Input ~ 0
ALATCH
Text GLabel 5450 3300 0    50   Input ~ 0
GND
Text GLabel 6450 4950 2    50   Input ~ 0
LA1
Text GLabel 6450 5050 2    50   Input ~ 0
LA0
Text GLabel 6450 4850 2    50   Input ~ 0
LA2
Text GLabel 6450 4750 2    50   Input ~ 0
LA3
Text GLabel 6450 4650 2    50   Input ~ 0
LA4
Text GLabel 6450 4550 2    50   Input ~ 0
LA5
Text GLabel 6450 4450 2    50   Input ~ 0
LA6
Text GLabel 6450 4350 2    50   Input ~ 0
LA7
Text GLabel 5450 3000 0    50   Input ~ 0
AD8
Text GLabel 5950 5650 2    50   Input ~ 0
GND
Text GLabel 5950 4050 0    50   Input ~ 0
VCC
Text Notes 5000 2450 0    50   ~ 0
LSB
Text Notes 6650 5100 0    50   ~ 0
MSB
Text Notes 6700 2450 0    50   ~ 0
LSB
Text GLabel 5450 2300 0    50   Input ~ 0
PSEL_AD15_OUT
Text GLabel 5450 2900 0    50   Input ~ 0
AD9
Text GLabel 5450 2800 0    50   Input ~ 0
AD10
Text GLabel 5450 2700 0    50   Input ~ 0
AD11
Text GLabel 5450 2600 0    50   Input ~ 0
AD12
Text GLabel 5450 2500 0    50   Input ~ 0
AD13
Text GLabel 5450 2400 0    50   Input ~ 0
AD14
Text Notes 4450 2050 0    100  ~ 0
Address Latches\n(Option)
Text GLabel 5950 2000 0    50   Input ~ 0
VCC
Text GLabel 5450 5050 0    50   Input ~ 0
AD0_IN
Text Notes 7050 7050 0    79   ~ 0
Used to latch AD lines from D0-D14, instead of buffering LA0-14.\nD0-D14 are the address on falling ALATCH, until falling RD* or WR*.
$EndSCHEMATC
