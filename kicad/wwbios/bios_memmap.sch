EESchema Schematic File Version 4
LIBS:wwbios-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Memory_RAM:628128_DIP32_SSOP32 U?
U 1 1 60339F44
P 5100 3600
F 0 "U?" H 5100 4781 50  0000 C CNN
F 1 "TC551001_128Kx8" H 5100 4690 50  0000 C CNN
F 2 "Housings_DIP:DIP-32_W15.24mm_Socket_LongPads" H 5100 3600 50  0001 C CNN
F 3 "http://www.futurlec.com/Datasheet/Memory/628128.pdf" H 5100 3600 50  0001 C CNN
	1    5100 3600
	1    0    0    -1  
$EndComp
Text GLabel 5600 2800 2    50   Input ~ 0
D0
Text GLabel 5600 2900 2    50   Input ~ 0
D1
Text GLabel 5600 3000 2    50   Input ~ 0
D2
Text GLabel 5600 3100 2    50   Input ~ 0
D3
Text GLabel 5600 3200 2    50   Input ~ 0
D4
Text GLabel 5600 3300 2    50   Input ~ 0
D5
Text GLabel 5600 3400 2    50   Input ~ 0
D6
Text GLabel 5600 3500 2    50   Input ~ 0
D7
Text GLabel 5100 4600 0    50   Input ~ 0
GND
Text GLabel 7150 4600 0    50   Input ~ 0
GND
Text GLabel 5100 2600 2    50   Input ~ 0
+5V
Text GLabel 7150 2600 2    50   Input ~ 0
+5V
$Comp
L Memory_RAM:628128_DIP32_SSOP32 U?
U 1 1 60339F56
P 7150 3600
F 0 "U?" H 7150 4781 50  0000 C CNN
F 1 "TC551001_128Kx8" H 7150 4690 50  0000 C CNN
F 2 "Housings_DIP:DIP-32_W15.24mm_Socket_LongPads" H 7150 3600 50  0001 C CNN
F 3 "http://www.futurlec.com/Datasheet/Memory/628128.pdf" H 7150 3600 50  0001 C CNN
	1    7150 3600
	1    0    0    -1  
$EndComp
Text Notes 3250 2550 0    50   ~ 0
PSEL* is independent of PRV.\nInterrupts are guaranteed to \nclear MS / output PSEL*=1\n
Text GLabel 5600 3900 2    50   Input ~ 0
RD*
Text GLabel 5600 4000 2    50   Input ~ 0
WE*
Text GLabel 5600 3700 2    50   Input ~ 0
RAMSEL*
Text Notes 7950 3900 0    50   ~ 0
RAM maps into \n>8000->FFFE
Text GLabel 5600 3800 2    50   Input ~ 0
A14
Text GLabel 7650 3900 2    50   Input ~ 0
RD*
Text GLabel 7650 4000 2    50   Input ~ 0
WE*
Text GLabel 7650 3700 2    50   Input ~ 0
RAMSEL*
Text GLabel 7650 3800 2    50   Input ~ 0
A14
Text GLabel 6650 2900 0    50   Input ~ 0
A0
Text GLabel 6650 3000 0    50   Input ~ 0
A1
Text GLabel 6650 3100 0    50   Input ~ 0
A2
Text GLabel 6650 3200 0    50   Input ~ 0
A3
Text GLabel 6650 3300 0    50   Input ~ 0
A4
Text GLabel 6650 3400 0    50   Input ~ 0
A5
Text GLabel 6650 3500 0    50   Input ~ 0
A6
Text GLabel 6650 3600 0    50   Input ~ 0
A7
Text GLabel 6650 3700 0    50   Input ~ 0
A8
Text GLabel 6650 3800 0    50   Input ~ 0
A9
Text GLabel 6650 3900 0    50   Input ~ 0
A10
Text GLabel 6650 4000 0    50   Input ~ 0
A11
Text GLabel 6650 4100 0    50   Input ~ 0
A12
Text GLabel 7650 2800 2    50   Input ~ 0
D8
Text GLabel 7650 2900 2    50   Input ~ 0
D9
Text GLabel 7650 3100 2    50   Input ~ 0
D11
Text GLabel 7650 3200 2    50   Input ~ 0
D12
Text GLabel 7650 3300 2    50   Input ~ 0
D13
Text GLabel 7650 3400 2    50   Input ~ 0
D14
Text GLabel 7650 3500 2    50   Input ~ 0
D15
Text GLabel 7650 3000 2    50   Input ~ 0
D10
Text GLabel 4600 2900 0    50   Input ~ 0
A0
Text GLabel 4600 4100 0    50   Input ~ 0
A12
Text GLabel 4600 4000 0    50   Input ~ 0
A11
Text GLabel 4600 3900 0    50   Input ~ 0
A10
Text GLabel 4600 3800 0    50   Input ~ 0
A9
Text GLabel 4600 3700 0    50   Input ~ 0
A8
Text GLabel 4600 3600 0    50   Input ~ 0
A7
Text GLabel 4600 3500 0    50   Input ~ 0
A6
Text GLabel 4600 3400 0    50   Input ~ 0
A5
Text GLabel 4600 3300 0    50   Input ~ 0
A4
Text GLabel 4600 3200 0    50   Input ~ 0
A3
Text GLabel 4600 3100 0    50   Input ~ 0
A2
Text GLabel 4600 3000 0    50   Input ~ 0
A1
Text GLabel 4600 2800 0    50   Input ~ 0
ZERO
Text GLabel 6650 2800 0    50   Input ~ 0
ONE
Text GLabel 4600 4200 0    50   Input ~ 0
A13
Text GLabel 6650 4200 0    50   Input ~ 0
A13
Text GLabel 4600 4300 0    50   Input ~ 0
ONE
Text GLabel 4600 4400 0    50   Input ~ 0
ZERO
Text GLabel 6650 4300 0    50   Input ~ 0
ONE
Text GLabel 6650 4400 0    50   Input ~ 0
ZERO
$EndSCHEMATC
