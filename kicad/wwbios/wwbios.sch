EESchema Schematic File Version 4
LIBS:wwbios-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Memory_RAM:628128_DIP32_SSOP32 U10
U 1 1 5DFE9735
P 7600 1950
F 0 "U10" H 7600 3131 50  0000 C CNN
F 1 "TC551001_128Kx8" H 7600 3040 50  0000 C CNN
F 2 "Housings_DIP:DIP-32_W15.24mm_Socket_LongPads" H 7600 1950 50  0001 C CNN
F 3 "http://www.futurlec.com/Datasheet/Memory/628128.pdf" H 7600 1950 50  0001 C CNN
	1    7600 1950
	1    0    0    -1  
$EndComp
Text GLabel 8100 1150 2    50   Input ~ 0
D0
Text GLabel 8100 1250 2    50   Input ~ 0
D1
Text GLabel 8100 1350 2    50   Input ~ 0
D2
Text GLabel 8100 1450 2    50   Input ~ 0
D3
Text GLabel 8100 1550 2    50   Input ~ 0
D4
Text GLabel 8100 1650 2    50   Input ~ 0
D5
Text GLabel 8100 1750 2    50   Input ~ 0
D6
Text GLabel 8100 1850 2    50   Input ~ 0
D7
$Comp
L Memory_EPROM:27C010 U11
U 1 1 5DFF257B
P 7600 4950
F 0 "U11" H 7600 6331 50  0000 C CNN
F 1 "27C010" H 7600 6240 50  0000 C CNN
F 2 "Housings_DIP:DIP-32_W15.24mm_Socket" H 7600 4950 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/doc0321.pdf" H 7600 4950 50  0001 C CNN
	1    7600 4950
	1    0    0    -1  
$EndComp
Text GLabel 7600 2950 0    50   Input ~ 0
GND
Text GLabel 7600 6250 0    50   Input ~ 0
GND
Text GLabel 9650 2950 0    50   Input ~ 0
GND
Text GLabel 7600 950  2    50   Input ~ 0
+5V
Text GLabel 9650 950  2    50   Input ~ 0
+5V
$Comp
L Memory_EPROM:27C010 U12
U 1 1 5DFF6D13
P 9650 4950
F 0 "U12" H 9650 6331 50  0000 C CNN
F 1 "27C010" H 9650 6240 50  0000 C CNN
F 2 "Housings_DIP:DIP-32_W15.24mm_Socket" H 9650 4950 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/doc0321.pdf" H 9650 4950 50  0001 C CNN
	1    9650 4950
	1    0    0    -1  
$EndComp
Text GLabel 9650 6250 0    50   Input ~ 0
GND
Text Notes 7150 3800 2    50   ~ 0
One ROM image,\nA0 = even byte
Text GLabel 7200 3950 0    50   Input ~ 0
ZERO
Text GLabel 9650 3750 2    50   Input ~ 0
+5V
Text GLabel 7600 3750 2    50   Input ~ 0
+5V
Text Notes 9100 3800 2    50   ~ 0
One ROM image,\nA0 = odd byte
$Comp
L Memory_RAM:628128_DIP32_SSOP32 U13
U 1 1 5DFEE7D9
P 9650 1950
F 0 "U13" H 9650 3131 50  0000 C CNN
F 1 "TC551001_128Kx8" H 9650 3040 50  0000 C CNN
F 2 "Housings_DIP:DIP-32_W15.24mm_Socket_LongPads" H 9650 1950 50  0001 C CNN
F 3 "http://www.futurlec.com/Datasheet/Memory/628128.pdf" H 9650 1950 50  0001 C CNN
	1    9650 1950
	1    0    0    -1  
$EndComp
Text Notes 4800 4200 0    50   ~ 0
EPROM Map - 128Kbytes\n 0000- 3FFE  Page 0. Interrupt and XOP vectors. \n                        Supervisor code. Macrostore.\n 4000-7FFE   Page 1. Supervisor code.\n 8000-FFFE   Page 2-3\n10000-1FFFE Page 4-7\n\nRAM Usage - 256K in 16K pages\n00000-03FFE  Page 0\n04000-7FFFE Page 1\n\nMemory Map\n0000-3FFE  ROM \n8000-FFFE  RAM 
Text Notes 5750 900  0    50   ~ 0
PSEL* is independent of PRV.\nInterrupts are guaranteed to \nclear MS / output PSEL*=1\n
$Sheet
S 4950 5050 1250 500 
U 5EF2AEDB
F0 "Backplane" 50
F1 "Backplane.sch" 50
$EndSheet
Text GLabel 8100 2250 2    50   Input ~ 0
RD*
Text GLabel 8100 2350 2    50   Input ~ 0
WE*
Text GLabel 9250 3950 0    50   Input ~ 0
ONE
Text GLabel 10050 3950 2    50   Input ~ 0
D8
Text GLabel 10050 4050 2    50   Input ~ 0
D9
Text GLabel 10050 4250 2    50   Input ~ 0
D11
Text GLabel 10050 4350 2    50   Input ~ 0
D12
Text GLabel 10050 4450 2    50   Input ~ 0
D13
Text GLabel 10050 4550 2    50   Input ~ 0
D14
Text GLabel 10050 4650 2    50   Input ~ 0
D15
Text GLabel 9250 5950 0    50   Input ~ 0
ROMSEL*
Text GLabel 950  1200 0    50   Input ~ 0
A14
Text GLabel 7200 5950 0    50   Input ~ 0
ROMSEL*
Text GLabel 8100 2050 2    50   Input ~ 0
RAMSEL*
Text Notes 10450 2250 0    50   ~ 0
RAM maps into \n>8000->FFFE
Text Notes 8700 6300 0    50   ~ 0
ROM maps into \n>0000->7FFE
Text GLabel 9050 5850 0    50   Output ~ 0
+5V
Text GLabel 9050 5750 0    50   Output ~ 0
+5V
Text GLabel 7200 5850 0    50   Output ~ 0
+5V
Text GLabel 7200 5750 0    50   Output ~ 0
+5V
$Comp
L power:PWR_FLAG #FLG01
U 1 1 5F144476
P 1200 7300
F 0 "#FLG01" H 1200 7375 50  0001 C CNN
F 1 "PWR_FLAG" V 1200 7427 50  0000 L CNN
F 2 "" H 1200 7300 50  0001 C CNN
F 3 "~" H 1200 7300 50  0001 C CNN
	1    1200 7300
	0    -1   -1   0   
$EndComp
$Comp
L power:PWR_FLAG #FLG02
U 1 1 5F144791
P 1200 7500
F 0 "#FLG02" H 1200 7575 50  0001 C CNN
F 1 "PWR_FLAG" V 1200 7627 50  0000 L CNN
F 2 "" H 1200 7500 50  0001 C CNN
F 3 "~" H 1200 7500 50  0001 C CNN
	1    1200 7500
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR03
U 1 1 5F1458A7
P 1200 7500
F 0 "#PWR03" H 1200 7250 50  0001 C CNN
F 1 "GND" H 1205 7327 50  0000 C CNN
F 2 "" H 1200 7500 50  0001 C CNN
F 3 "" H 1200 7500 50  0001 C CNN
	1    1200 7500
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR02
U 1 1 5F145103
P 1200 7300
F 0 "#PWR02" H 1200 7150 50  0001 C CNN
F 1 "+5V" H 1215 7473 50  0000 C CNN
F 2 "" H 1200 7300 50  0001 C CNN
F 3 "" H 1200 7300 50  0001 C CNN
	1    1200 7300
	1    0    0    -1  
$EndComp
Wire Wire Line
	9050 5750 9250 5750
Wire Wire Line
	9250 5850 9050 5850
Text Notes 7000 3350 0    50   ~ 0
MACRO ROM is unified \nwith Page 0 ROM
$Comp
L Device:LED D?
U 1 1 5F0F13C8
P 3850 6400
AR Path="/5EB213FB/5F0F13C8" Ref="D?"  Part="1" 
AR Path="/5F0F13C8" Ref="D2"  Part="1" 
F 0 "D2" V 3797 6478 50  0000 L CNN
F 1 "LED" V 3888 6478 50  0000 L CNN
F 2 "LEDs:LED_Rectangular_W5.0mm_H2.0mm" H 3850 6400 50  0001 C CNN
F 3 "~" H 3850 6400 50  0001 C CNN
	1    3850 6400
	0    1    -1   0   
$EndComp
Text GLabel 8100 2150 2    50   Input ~ 0
A14
Text GLabel 10150 2250 2    50   Input ~ 0
RD*
Text GLabel 10150 2350 2    50   Input ~ 0
WE*
Text GLabel 10150 2050 2    50   Input ~ 0
RAMSEL*
Text GLabel 10150 2150 2    50   Input ~ 0
A14
Text GLabel 7200 4050 0    50   Input ~ 0
A0
Text GLabel 7200 4150 0    50   Input ~ 0
A1
Text GLabel 7200 4250 0    50   Input ~ 0
A2
Text GLabel 7200 4350 0    50   Input ~ 0
A3
Text GLabel 7200 4450 0    50   Input ~ 0
A4
Text GLabel 7200 4550 0    50   Input ~ 0
A5
Text GLabel 7200 4650 0    50   Input ~ 0
A6
Text GLabel 7200 4750 0    50   Input ~ 0
A7
Text GLabel 7200 4850 0    50   Input ~ 0
A8
Text GLabel 7200 4950 0    50   Input ~ 0
A9
Text GLabel 7200 5050 0    50   Input ~ 0
A10
Text GLabel 7200 5150 0    50   Input ~ 0
A11
Text GLabel 7200 5250 0    50   Input ~ 0
A12
Text GLabel 9250 4050 0    50   Input ~ 0
A0
Text GLabel 9250 4150 0    50   Input ~ 0
A1
Text GLabel 9250 4250 0    50   Input ~ 0
A2
Text GLabel 9250 4350 0    50   Input ~ 0
A3
Text GLabel 9250 4450 0    50   Input ~ 0
A4
Text GLabel 9250 4550 0    50   Input ~ 0
A5
Text GLabel 9250 4650 0    50   Input ~ 0
A6
Text GLabel 9250 4750 0    50   Input ~ 0
A7
Text GLabel 9250 4850 0    50   Input ~ 0
A8
Text GLabel 9250 4950 0    50   Input ~ 0
A9
Text GLabel 9250 5050 0    50   Input ~ 0
A10
Text GLabel 9250 5150 0    50   Input ~ 0
A11
Text GLabel 9250 5250 0    50   Input ~ 0
A12
Text GLabel 9150 1250 0    50   Input ~ 0
A0
Text GLabel 9150 1350 0    50   Input ~ 0
A1
Text GLabel 9150 1450 0    50   Input ~ 0
A2
Text GLabel 9150 1550 0    50   Input ~ 0
A3
Text GLabel 9150 1650 0    50   Input ~ 0
A4
Text GLabel 9150 1750 0    50   Input ~ 0
A5
Text GLabel 9150 1850 0    50   Input ~ 0
A6
Text GLabel 9150 1950 0    50   Input ~ 0
A7
Text GLabel 9150 2050 0    50   Input ~ 0
A8
Text GLabel 9150 2150 0    50   Input ~ 0
A9
Text GLabel 9150 2250 0    50   Input ~ 0
A10
Text GLabel 9150 2350 0    50   Input ~ 0
A11
Text GLabel 9150 2450 0    50   Input ~ 0
A12
Text GLabel 8000 3950 2    50   Input ~ 0
D0
Text GLabel 8000 4050 2    50   Input ~ 0
D1
Text GLabel 8000 4150 2    50   Input ~ 0
D2
Text GLabel 8000 4250 2    50   Input ~ 0
D3
Text GLabel 8000 4350 2    50   Input ~ 0
D4
Text GLabel 8000 4450 2    50   Input ~ 0
D5
Text GLabel 8000 4550 2    50   Input ~ 0
D6
Text GLabel 8000 4650 2    50   Input ~ 0
D7
Text GLabel 10050 4150 2    50   Input ~ 0
D10
Text GLabel 10150 1150 2    50   Input ~ 0
D8
Text GLabel 10150 1250 2    50   Input ~ 0
D9
Text GLabel 10150 1450 2    50   Input ~ 0
D11
Text GLabel 10150 1550 2    50   Input ~ 0
D12
Text GLabel 10150 1650 2    50   Input ~ 0
D13
Text GLabel 10150 1750 2    50   Input ~ 0
D14
Text GLabel 10150 1850 2    50   Input ~ 0
D15
Text GLabel 10150 1350 2    50   Input ~ 0
D10
Text GLabel 1400 7500 3    50   Input ~ 0
ZERO
$Comp
L Device:LED D?
U 1 1 5F33BEF4
P 4150 6400
AR Path="/5EB213FB/5F33BEF4" Ref="D?"  Part="1" 
AR Path="/5F33BEF4" Ref="D1"  Part="1" 
F 0 "D1" V 4097 6478 50  0000 L CNN
F 1 "LED" V 4188 6478 50  0000 L CNN
F 2 "LEDs:LED_Rectangular_W5.0mm_H2.0mm" H 4150 6400 50  0001 C CNN
F 3 "~" H 4150 6400 50  0001 C CNN
	1    4150 6400
	0    1    -1   0   
$EndComp
$Comp
L Device:R R1
U 1 1 5F341E69
P 4150 6000
F 0 "R1" H 4220 6046 50  0000 L CNN
F 1 "R" H 4220 5955 50  0000 L CNN
F 2 "Resistors_SMD:R_1206" V 4080 6000 50  0001 C CNN
F 3 "~" H 4150 6000 50  0001 C CNN
	1    4150 6000
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4150 6150 4150 6250
$Comp
L Device:R R2
U 1 1 5F346171
P 3850 6000
F 0 "R2" H 3920 6046 50  0000 L CNN
F 1 "R" H 3920 5955 50  0000 L CNN
F 2 "Resistors_SMD:R_1206" V 3780 6000 50  0001 C CNN
F 3 "~" H 3850 6000 50  0001 C CNN
	1    3850 6000
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3850 6150 3850 6250
Text GLabel 1400 7300 1    50   Input ~ 0
ONE
$Comp
L 74xx:74LS259 U7
U 1 1 5F604AAE
P 1450 5450
F 0 "U7" H 1450 6331 50  0000 C CNN
F 1 "74LHCT259" H 1450 6240 50  0000 C CNN
F 2 "Housings_DIP:DIP-16_W7.62mm_Socket" H 1450 5450 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS259" H 1450 5450 50  0001 C CNN
	1    1450 5450
	1    0    0    -1  
$EndComp
Text GLabel 1450 6150 0    50   Input ~ 0
GND
Text GLabel 1450 4750 2    50   Input ~ 0
+5V
Text GLabel 950  5350 0    50   Input ~ 0
A1
Text GLabel 950  5050 0    50   Input ~ 0
CRUOUT
Text GLabel 950  5450 0    50   Input ~ 0
A2
Text GLabel 950  5250 0    50   Input ~ 0
A0
Text GLabel 950  5850 0    50   Input ~ 0
+5V
$Comp
L Device:LED D?
U 1 1 5F65A8F4
P 3250 6400
AR Path="/5EB213FB/5F65A8F4" Ref="D?"  Part="1" 
AR Path="/5F65A8F4" Ref="D4"  Part="1" 
F 0 "D4" V 3197 6478 50  0000 L CNN
F 1 "LED" V 3288 6478 50  0000 L CNN
F 2 "LEDs:LED_Rectangular_W5.0mm_H2.0mm" H 3250 6400 50  0001 C CNN
F 3 "~" H 3250 6400 50  0001 C CNN
	1    3250 6400
	0    1    -1   0   
$EndComp
$Comp
L Device:LED D?
U 1 1 5F65A908
P 3550 6400
AR Path="/5EB213FB/5F65A908" Ref="D?"  Part="1" 
AR Path="/5F65A908" Ref="D3"  Part="1" 
F 0 "D3" V 3497 6478 50  0000 L CNN
F 1 "LED" V 3588 6478 50  0000 L CNN
F 2 "LEDs:LED_Rectangular_W5.0mm_H2.0mm" H 3550 6400 50  0001 C CNN
F 3 "~" H 3550 6400 50  0001 C CNN
	1    3550 6400
	0    1    -1   0   
$EndComp
$Comp
L Device:R R3
U 1 1 5F65A91C
P 3550 6000
F 0 "R3" H 3620 6046 50  0000 L CNN
F 1 "R" H 3620 5955 50  0000 L CNN
F 2 "Resistors_SMD:R_1206" V 3480 6000 50  0001 C CNN
F 3 "~" H 3550 6000 50  0001 C CNN
	1    3550 6000
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3550 6150 3550 6250
$Comp
L Device:R R4
U 1 1 5F65A927
P 3250 6000
F 0 "R4" H 3320 6046 50  0000 L CNN
F 1 "R" H 3320 5955 50  0000 L CNN
F 2 "Resistors_SMD:R_1206" V 3180 6000 50  0001 C CNN
F 3 "~" H 3250 6000 50  0001 C CNN
	1    3250 6000
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3250 6150 3250 6250
$Comp
L Device:LED D?
U 1 1 5F66E4F2
P 2650 6400
AR Path="/5EB213FB/5F66E4F2" Ref="D?"  Part="1" 
AR Path="/5F66E4F2" Ref="D6"  Part="1" 
F 0 "D6" V 2597 6478 50  0000 L CNN
F 1 "LED" V 2688 6478 50  0000 L CNN
F 2 "LEDs:LED_Rectangular_W5.0mm_H2.0mm" H 2650 6400 50  0001 C CNN
F 3 "~" H 2650 6400 50  0001 C CNN
	1    2650 6400
	0    1    -1   0   
$EndComp
$Comp
L Device:LED D?
U 1 1 5F66E506
P 2950 6400
AR Path="/5EB213FB/5F66E506" Ref="D?"  Part="1" 
AR Path="/5F66E506" Ref="D5"  Part="1" 
F 0 "D5" V 2897 6478 50  0000 L CNN
F 1 "LED" V 2988 6478 50  0000 L CNN
F 2 "LEDs:LED_Rectangular_W5.0mm_H2.0mm" H 2950 6400 50  0001 C CNN
F 3 "~" H 2950 6400 50  0001 C CNN
	1    2950 6400
	0    1    -1   0   
$EndComp
$Comp
L Device:R R5
U 1 1 5F66E51A
P 2950 6000
F 0 "R5" H 3020 6046 50  0000 L CNN
F 1 "R" H 3020 5955 50  0000 L CNN
F 2 "Resistors_SMD:R_1206" V 2880 6000 50  0001 C CNN
F 3 "~" H 2950 6000 50  0001 C CNN
	1    2950 6000
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2950 6150 2950 6250
$Comp
L Device:R R6
U 1 1 5F66E525
P 2650 6000
F 0 "R6" H 2720 6046 50  0000 L CNN
F 1 "R" H 2720 5955 50  0000 L CNN
F 2 "Resistors_SMD:R_1206" V 2580 6000 50  0001 C CNN
F 3 "~" H 2650 6000 50  0001 C CNN
	1    2650 6000
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2650 6150 2650 6250
$Comp
L Device:LED D?
U 1 1 5F66E530
P 2050 6400
AR Path="/5EB213FB/5F66E530" Ref="D?"  Part="1" 
AR Path="/5F66E530" Ref="D8"  Part="1" 
F 0 "D8" V 1997 6478 50  0000 L CNN
F 1 "LED" V 2088 6478 50  0000 L CNN
F 2 "LEDs:LED_Rectangular_W5.0mm_H2.0mm" H 2050 6400 50  0001 C CNN
F 3 "~" H 2050 6400 50  0001 C CNN
	1    2050 6400
	0    1    -1   0   
$EndComp
$Comp
L Device:LED D?
U 1 1 5F66E544
P 2350 6400
AR Path="/5EB213FB/5F66E544" Ref="D?"  Part="1" 
AR Path="/5F66E544" Ref="D7"  Part="1" 
F 0 "D7" V 2297 6478 50  0000 L CNN
F 1 "LED" V 2388 6478 50  0000 L CNN
F 2 "LEDs:LED_Rectangular_W5.0mm_H2.0mm" H 2350 6400 50  0001 C CNN
F 3 "~" H 2350 6400 50  0001 C CNN
	1    2350 6400
	0    1    -1   0   
$EndComp
$Comp
L Device:R R7
U 1 1 5F66E558
P 2350 6000
F 0 "R7" H 2420 6046 50  0000 L CNN
F 1 "R" H 2420 5955 50  0000 L CNN
F 2 "Resistors_SMD:R_1206" V 2280 6000 50  0001 C CNN
F 3 "~" H 2350 6000 50  0001 C CNN
	1    2350 6000
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2350 6150 2350 6250
$Comp
L Device:R R8
U 1 1 5F66E563
P 2050 6000
F 0 "R8" H 2120 6046 50  0000 L CNN
F 1 "R" H 2120 5955 50  0000 L CNN
F 2 "Resistors_SMD:R_1206" V 1980 6000 50  0001 C CNN
F 3 "~" H 2050 6000 50  0001 C CNN
	1    2050 6000
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2050 6150 2050 6250
Wire Wire Line
	4150 6550 4150 6700
Wire Wire Line
	4150 6700 3850 6700
Wire Wire Line
	2050 6700 2050 6550
Wire Wire Line
	2350 6550 2350 6700
Connection ~ 2350 6700
Wire Wire Line
	2350 6700 2050 6700
Wire Wire Line
	2650 6550 2650 6700
Connection ~ 2650 6700
Wire Wire Line
	2650 6700 2350 6700
Wire Wire Line
	2950 6550 2950 6700
Connection ~ 2950 6700
Wire Wire Line
	2950 6700 2650 6700
Wire Wire Line
	3250 6550 3250 6700
Connection ~ 3250 6700
Wire Wire Line
	3250 6700 2950 6700
Wire Wire Line
	3550 6550 3550 6700
Connection ~ 3550 6700
Wire Wire Line
	3550 6700 3250 6700
Wire Wire Line
	3850 6550 3850 6700
Connection ~ 3850 6700
Wire Wire Line
	3850 6700 3550 6700
Text GLabel 2050 6700 0    50   Input ~ 0
GND
Wire Wire Line
	1950 5750 2050 5750
Wire Wire Line
	2050 5750 2050 5850
Wire Wire Line
	2350 5850 2350 5650
Wire Wire Line
	2350 5650 1950 5650
Wire Wire Line
	2650 5850 2650 5550
Wire Wire Line
	2650 5550 1950 5550
Wire Wire Line
	2950 5850 2950 5450
Wire Wire Line
	2950 5450 1950 5450
Wire Wire Line
	3250 5850 3250 5350
Wire Wire Line
	3250 5350 1950 5350
Wire Wire Line
	3550 5850 3550 5250
Wire Wire Line
	3550 5250 1950 5250
Wire Wire Line
	1950 5150 3850 5150
Wire Wire Line
	3850 5150 3850 5850
Wire Wire Line
	4150 5850 4150 5050
Wire Wire Line
	4150 5050 1950 5050
Text Notes 850  4700 0    50   ~ 0
LEDs
Text Notes 7100 6800 0    50   ~ 0
Simple Wire Wrap BIOS\n32K ROM, 32K RAM in supervisor space
Text GLabel 7100 1250 0    50   Input ~ 0
A0
Text GLabel 7100 2450 0    50   Input ~ 0
A12
Text GLabel 7100 2350 0    50   Input ~ 0
A11
Text GLabel 7100 2250 0    50   Input ~ 0
A10
Text GLabel 7100 2150 0    50   Input ~ 0
A9
Text GLabel 7100 2050 0    50   Input ~ 0
A8
Text GLabel 7100 1950 0    50   Input ~ 0
A7
Text GLabel 7100 1850 0    50   Input ~ 0
A6
Text GLabel 7100 1750 0    50   Input ~ 0
A5
Text GLabel 7100 1650 0    50   Input ~ 0
A4
Text GLabel 7100 1550 0    50   Input ~ 0
A3
Text GLabel 7100 1450 0    50   Input ~ 0
A2
Text GLabel 7100 1350 0    50   Input ~ 0
A1
Text GLabel 7100 1150 0    50   Input ~ 0
ZERO
Text GLabel 9150 1150 0    50   Input ~ 0
ONE
Text GLabel 7100 2550 0    50   Input ~ 0
A13
Text GLabel 9150 2550 0    50   Input ~ 0
A13
Text GLabel 7100 2650 0    50   Input ~ 0
ONE
Text GLabel 7100 2750 0    50   Input ~ 0
ZERO
Text GLabel 9150 2650 0    50   Input ~ 0
ONE
Text GLabel 9150 2750 0    50   Input ~ 0
ZERO
Text GLabel 7200 5450 0    50   Input ~ 0
ZERO
Text GLabel 7200 5550 0    50   Input ~ 0
ZERO
Text GLabel 9250 5450 0    50   Input ~ 0
ZERO
Text GLabel 9250 5550 0    50   Input ~ 0
ZERO
Text GLabel 9250 5350 0    50   Input ~ 0
A13
Text GLabel 7200 5350 0    50   Input ~ 0
A13
$Comp
L 74xx:74LS138 U1
U 1 1 5F7A913D
P 1450 1500
F 0 "U1" H 1450 2281 50  0000 C CNN
F 1 "74LS138" H 1450 2190 50  0000 C CNN
F 2 "Housings_DIP:DIP-16_W7.62mm_Socket" H 1450 1500 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS138" H 1450 1500 50  0001 C CNN
	1    1450 1500
	1    0    0    -1  
$EndComp
Text GLabel 950  1900 0    50   Input ~ 0
MEM*
Text GLabel 950  1300 0    50   Input ~ 0
PSEL*
Text GLabel 1950 1400 2    50   Input ~ 0
ROMSEL*
Text GLabel 1950 1500 2    50   Input ~ 0
RAMSEL*
NoConn ~ 1950 1200
NoConn ~ 1950 1300
NoConn ~ 1950 1600
NoConn ~ 1950 1700
NoConn ~ 1950 1800
NoConn ~ 1950 1900
Text GLabel 950  1700 0    50   Input ~ 0
ONE
Text GLabel 950  1800 0    50   Input ~ 0
MEM*
Text GLabel 1450 900  2    50   Input ~ 0
+5V
Text GLabel 1450 2200 0    50   Input ~ 0
GND
Text Notes 650  850  0    50   ~ 0
Chip Select
Text Notes 2000 1250 0    50   ~ 0
user space
Text GLabel 9250 6050 0    50   Input ~ 0
RD*
Text GLabel 7200 6050 0    50   Input ~ 0
RD*
Text GLabel 950  1400 0    50   Input ~ 0
ZERO
$Sheet
S 2750 3450 1150 1050
U 600110B6
F0 "Connie" 50
F1 "Connie.sch" 50
$EndSheet
Wire Wire Line
	1200 7300 1400 7300
Connection ~ 1200 7300
Wire Wire Line
	1400 7500 1200 7500
Connection ~ 1200 7500
Text GLabel 950  5650 0    50   Input ~ 0
SEL_1400*
$EndSCHEMATC
