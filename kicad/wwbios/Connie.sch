EESchema Schematic File Version 4
LIBS:wwbios-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
NoConn ~ 6350 2050
Text GLabel 4750 2350 0    50   Input ~ 0
CLKOUT
Text GLabel 4750 2450 0    50   Input ~ 0
SEL_1380*
Text GLabel 4750 1650 0    50   Input ~ 0
CRUOUT
Text GLabel 4750 1550 0    50   Input ~ 0
CRUIN
Text GLabel 5450 700  0    50   Input ~ 0
+5V
Text GLabel 5550 2950 0    50   Input ~ 0
GND
Text Label 6850 2450 2    50   ~ 0
XOUT2
Text Label 6850 2150 2    50   ~ 0
RTS2
Text Label 6850 2550 2    50   ~ 0
CTS2
Text GLabel 4750 1750 0    50   Input ~ 0
A0
Text GLabel 4750 1850 0    50   Input ~ 0
A1
Text GLabel 4750 1950 0    50   Input ~ 0
A2
Text GLabel 4750 2050 0    50   Input ~ 0
A3
Text GLabel 4750 2150 0    50   Input ~ 0
A4
Text Notes 4500 1100 0    100  ~ 20
RS232/2\n(Console)
Text Notes 9500 1100 0    50   ~ 0
Strategy: determine clock data rate\nfrom sampling a CRU pin. Set the\nUART to the rate.
Text Notes 5450 7350 0    50   ~ 0
APPLYING INPUT CURRENT TO AN\nOUTPUT PIN MAY DAMAGE THE 9901\n\nOutputs can drive 2 TTL loads
Text Notes 9650 2100 0    50   ~ 0
1300 Reserved for PIO\n1340 RS232/1\n1380 RS232/2 FTDI Console\n13C0 Unused\n1400 \n1440 Keyboard\n1480 Mouse\n14C0 MIDI\n1500 PIO/2 (expansion)\n1540 RS232/3 (expansion)\n1580 RS232/4 (expansion)
$Comp
L Connector_Generic:Conn_01x06 J4
U 1 1 5F031F9C
P 7350 2250
F 0 "J4" V 7222 2530 50  0000 L CNN
F 1 "FTDI" V 7313 2530 50  0000 L CNN
F 2 "Pin_Headers:Pin_Header_Angled_1x06_Pitch2.54mm" H 7350 2250 50  0001 C CNN
F 3 "~" H 7350 2250 50  0001 C CNN
	1    7350 2250
	1    0    0    -1  
$EndComp
Text GLabel 7150 2050 0    50   Input ~ 0
GND
Text Notes 6250 2150 0    50   ~ 0
CTS<
Text Notes 6250 2050 0    50   ~ 0
DSR<
Text Notes 6250 2250 0    50   ~ 0
RTS>
Text Notes 6250 2350 0    50   ~ 0
RX<
Text Notes 6250 2450 0    50   ~ 0
TX>
Wire Wire Line
	6350 2150 6550 2150
Text Label 6850 2350 2    50   ~ 0
RIN2
Text Notes 7150 2600 2    50   ~ 0
RTS>
Text Notes 7150 2500 2    50   ~ 0
RX<
Text Notes 7150 2400 2    50   ~ 0
TX>
Text Notes 7150 2200 2    50   ~ 0
CTS<
Text Notes 7150 2300 2    50   ~ 0
3.3V>
Text Notes 8550 7100 0    50   ~ 0
Pull the clock low.\nHold it low for 100 us\nPull KBD_DATA low\nRelease Clock\nWait for a KBD_CLK low\nBegin sending on KBD_DATA
$Comp
L Gemini:TMS9902 U2
U 1 1 5E17761C
P 5550 1950
F 0 "U2" H 5550 3131 50  0000 C CNN
F 1 "TMS9902" H 5550 3040 50  0000 C CNN
F 2 "Housings_DIP:DIP-18_W7.62mm_Socket" H 5550 1950 50  0001 C CNN
F 3 "" H 5550 1950 50  0001 C CNN
	1    5550 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 2450 7150 2450
Wire Wire Line
	6350 2350 7150 2350
Wire Wire Line
	6550 2150 6550 2550
Wire Wire Line
	7150 2550 6550 2550
Wire Wire Line
	6350 2250 6650 2250
Text Notes 7200 1600 0    100  ~ 20
FTDI\nHeader
Wire Wire Line
	6950 2250 7150 2250
Wire Wire Line
	5550 950  5550 700 
Wire Wire Line
	5550 700  5450 700 
Wire Wire Line
	6650 2150 6650 2250
Wire Wire Line
	6650 2150 7150 2150
Text GLabel 6950 2250 0    50   Input ~ 0
+3V3
NoConn ~ 10700 -300
$Comp
L 74xx:74LS138 U3
U 1 1 600190BF
P 6800 3900
F 0 "U3" H 6800 4681 50  0000 C CNN
F 1 "74LS138" H 6800 4590 50  0000 C CNN
F 2 "Housings_DIP:DIP-16_W7.62mm_Socket" H 6800 3900 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS138" H 6800 3900 50  0001 C CNN
	1    6800 3900
	1    0    0    -1  
$EndComp
Text GLabel 6300 4300 0    50   Input ~ 0
SERENA*
Text GLabel 6300 4200 0    50   Input ~ 0
A10
Text GLabel 6300 4100 0    50   Input ~ 0
A11
Text GLabel 6300 3800 0    50   Input ~ 0
A9
Text GLabel 6300 3600 0    50   Input ~ 0
A5
Text GLabel 6300 3700 0    50   Input ~ 0
A6
Text GLabel 6800 4600 0    50   Input ~ 0
GND
Text GLabel 6800 3300 0    50   Input ~ 0
+5V
Text GLabel 4750 2250 0    50   Input ~ 0
CRUCLK*
NoConn ~ 7300 3700
NoConn ~ 7300 3900
NoConn ~ 7300 4100
NoConn ~ 7300 4300
NoConn ~ 4750 1450
Text Label 7300 3700 0    50   ~ 0
Y1
Text Label 7300 3800 0    50   ~ 0
Y2
Text Label 7300 3900 0    50   ~ 0
Y3
Text Label 7300 4000 0    50   ~ 0
Y4
Text Label 7300 4100 0    50   ~ 0
Y5
Text Label 7300 4300 0    50   ~ 0
Y7
Text Label 4750 1450 0    50   ~ 0
INT*
Text GLabel 7300 3800 2    50   Input ~ 0
SEL_1380*
NoConn ~ 7300 3600
Text Label 7300 3600 0    50   ~ 0
Y0
Text Label 7300 4200 0    50   ~ 0
Y6
NoConn ~ 7300 4200
Text GLabel 7300 4000 2    50   Input ~ 0
SEL_1400*
$Comp
L 74xx:74LS125 U?
U 1 1 60AB77F4
P 8850 3150
AR Path="/5EF2AEDB/60AB77F4" Ref="U?"  Part="1" 
AR Path="/600110B6/60AB77F4" Ref="U4"  Part="1" 
F 0 "U4" H 8850 3467 50  0000 C CNN
F 1 "74LS125" H 8850 3376 50  0000 C CNN
F 2 "Housings_DIP:DIP-14_W7.62mm_Socket" H 8850 3150 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS125" H 8850 3150 50  0001 C CNN
	1    8850 3150
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS125 U?
U 5 1 60AB77FA
P 8850 4250
AR Path="/5EF2AEDB/60AB77FA" Ref="U?"  Part="5" 
AR Path="/600110B6/60AB77FA" Ref="U4"  Part="5" 
F 0 "U4" H 9080 4296 50  0000 L CNN
F 1 "74LS125" H 9080 4205 50  0000 L CNN
F 2 "Housings_DIP:DIP-14_W7.62mm_Socket" H 8850 4250 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS125" H 8850 4250 50  0001 C CNN
	5    8850 4250
	1    0    0    -1  
$EndComp
Text GLabel 8850 3750 0    50   Input ~ 0
+5V
Text GLabel 8850 4750 0    50   Input ~ 0
GND
Text GLabel 8850 3400 0    50   Input ~ 0
SEL_1380*
Text GLabel 8550 3150 0    50   Input ~ 0
CRUIN
Text GLabel 9150 3150 2    50   Input ~ 0
B_D15_IN
Text Notes 9100 3350 0    50   ~ 0
Unnecessary--9902 tristates its output
$EndSCHEMATC
