EESchema Schematic File Version 4
LIBS:wwbios-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 600  4500 0    50   ~ 0
In 8-bit  CRU access we\nwant the byte on D0-D7\nnot D8-D15\nDefine D0-D7 as the even word.\nThough D7 is still most significant.
Text Notes 600  5150 0    50   ~ 0
Address word is rotated 1 right.\nPSEL* is moved to \nA15 MSBit of address\n(not symmetric with data bus)
Text Notes 600  5650 0    50   ~ 0
BMO   Bus Master Override\nPARENA Parallel CRU Enable (except /IORQ)\nSERENA  Serial CRU Enable
Text Notes 650  3900 0    50   ~ 0
Memory mapper outputs effective address \non A16-A28.   A0 is LSB here.\n\nBank.................... Word Address\nA14 A13 A12 A11 A10..A0\n8 bit Page. ..4K\nA23 .. A16  A15\nExtended Address\n\nA0  \\n...  } Word Address\nA10 /\n\nA11  \\nA12  4} Bank Reg#\nA13   }\nA14  /\nA15  } PSEL* or Map Enable\n\nEffective Address\n\nA16  FA11 4K half-page\n\nA17  FA12 \\n...       8} 8K page#\nA24  FA19 /\n\nA25  \\nA26   } 32MB Expansion\nA27   }\nA28  /\n     \ .. 3 mode bits ..\nA29  } RO      Read-Only\nA30  } PBPTHP  P-Box Pass-Thru Page\nA31  } SPAMM   Set page memory-mapped
Text Notes 600  5950 0    50   ~ 0
HALT is unused right?
Text GLabel 4100 6400 0    50   Input ~ 0
B_FA11
Text GLabel 4100 6500 0    50   Input ~ 0
B_FA12
Text GLabel 4100 6600 0    50   Input ~ 0
B_FA13
Text GLabel 4100 6900 0    50   Input ~ 0
B_FA16
NoConn ~ 4100 6400
NoConn ~ 4100 6500
NoConn ~ 4100 6600
NoConn ~ 4100 6700
NoConn ~ 4100 6800
NoConn ~ 4100 6900
NoConn ~ 4100 7000
NoConn ~ 4100 7100
NoConn ~ 4100 7200
NoConn ~ 4100 7300
NoConn ~ 4100 7400
NoConn ~ 4100 7500
NoConn ~ 4100 7600
NoConn ~ 4550 6400
NoConn ~ 4550 6500
NoConn ~ 4550 6600
NoConn ~ 4550 6900
NoConn ~ 4550 7000
NoConn ~ 4550 7100
NoConn ~ 4550 7200
Text Notes 5650 7600 0    50   ~ 0
B_D15 D7    MSBit\nB_D14 D6\nB_D13 D5\nB_D12 D4\nB_D11 D3\nB_D10 D2\nB_D9  D1\nB_D8  D0\nB_D7 D15\nB_D6 D14\nB_D5 D13\nB_D4 D12\nB_D3 D11\nB_D2 D10\nB_D1 D9\nB_D0 D8      LSBit\n
Text Notes 2600 1600 0    50   ~ 0
Kontron Bus used in ECB RetroBrewComputing.net\n\nPins are numbered with 0 least significant.\nData bus 0-7 is from 8-bit bus, so it becomes the even (assuming big-endian), MSB of 16-bit bus.\nA0 is the LSBit of a word address. There is no byte address bit because 16-bit bus.\n99105 IN and OUT pins are in the expected place in the data bus word (not the addr).\n\nMEM* asserts for a CPU memory cycle. CRU requires decoding BST1-3 for IO.\nSERENA* and PARENA* assert for a bit-serial or byte/word-parallel CRU cycle.\nIORQ* asserts for a byte/word-parallel cycle to addresses 00-1FE (MSX style port)\n\nA card can use the data bus as AD0-AD15/PSEL of the 99105,\nor use separate address and data buses.
Text Notes 7050 2229 2    79   ~ 0
All Cards Except CPU will name least significiant \nwith 0! B_A0 is least significant word address (9900\n and 99105 have no A15) B_D0 is least significant data \nbit.\n
NoConn ~ 4550 7300
NoConn ~ 4550 7400
NoConn ~ 4550 7500
NoConn ~ 4550 7600
Text GLabel 5050 6400 0    50   Input ~ 0
B_NMI*
Text GLabel 5050 6500 0    50   Input ~ 0
B_INTREQ*
Text GLabel 5050 6600 0    50   Input ~ 0
B_CLKOUT
Text GLabel 5050 6700 0    50   Input ~ 0
B_RESET*
Text GLabel 5050 6800 0    50   Input ~ 0
B_INTA*
Text GLabel 5050 6900 0    50   Input ~ 0
B_READY
Text GLabel 5050 7000 0    50   Input ~ 0
B_HOLD*
Text GLabel 5050 7100 0    50   Input ~ 0
B_IAQ*
Text GLabel 5050 7300 0    50   Input ~ 0
B_ALATCH
Text GLabel 5050 7200 0    50   Input ~ 0
B_IORQ*
Text GLabel 5050 7400 0    50   Input ~ 0
B_HOLDA*
NoConn ~ 5050 7200
NoConn ~ 5050 7300
NoConn ~ 5050 7400
NoConn ~ 5050 6400
NoConn ~ 5050 6500
NoConn ~ 5050 6600
NoConn ~ 5050 6700
NoConn ~ 5050 6800
NoConn ~ 5050 6900
NoConn ~ 5050 7000
NoConn ~ 5050 7100
Text Notes 600  7700 0    50   ~ 0
MEM* BST1 2 3  Name      Description\n 0      0 0 0  SOPL       \n 0      0 0 1  SOP        \n 0      0 1 0  IOP        \n 0      0 1 1  IAQ*       \n 0      1 0 0  DOP        \n 0      1 0 1  INTA       \n 0      1 1 0  WS         \n 0      1 1 1  GM         General memory transfer\n 1      0 0 0  AUMSL      Internal ALU op or Macrostore access +MPICLK \n 1      0 0 1  AUMS       Internal ALU op or Macrostore access.\n 1      0 1 0  RESET      Reset. The RESET* line is pulled low.\n 1      0 1 1  IO         I/O transfer (a CRU cycle)\n 1      1 0 0  WP         Workspace pointer update\n 1      1 0 1  ST         Status register update\n 1      1 1 0  MID        Macroinstruction detected. APP* sampled on READY\n 1      1 1 1  HOLDA      Hold acknowledge\n
Text Notes 7250 6850 0    100  ~ 0
Backplane signals\nFrom BIOS card schematic
Text Notes 4450 6350 2    50   ~ 0
Unused:
Text GLabel 4550 6600 0    50   Input ~ 0
B_PBPTHP
Text GLabel 4550 6500 0    50   Input ~ 0
B_PRO
Text GLabel 4550 6400 0    50   Input ~ 0
B_SPAMM
Text GLabel 5050 7500 0    50   Input ~ 0
B_PARENA*
NoConn ~ 5050 7500
Text GLabel 5050 7600 0    50   Input ~ 0
B_BST3
NoConn ~ 5050 7600
Text GLabel 4550 6800 0    50   Input ~ 0
B_BMO
Text GLabel 4550 7600 0    50   Input ~ 0
B_IC0
Text GLabel 4550 7500 0    50   Input ~ 0
B_IC1
Text GLabel 4550 7400 0    50   Input ~ 0
B_IC2
Text GLabel 4550 7300 0    50   Input ~ 0
B_IC3
Text GLabel 4550 7200 0    50   Input ~ 0
B_INT1
Text GLabel 4550 7100 0    50   Input ~ 0
B_INT2
Text GLabel 4550 7000 0    50   Input ~ 0
B_INT3
Text GLabel 4550 6900 0    50   Input ~ 0
B_INT4
Text GLabel 4100 7200 0    50   Input ~ 0
B_FA19
Text GLabel 4100 7300 0    50   Input ~ 0
B_FA20
Text GLabel 4100 7400 0    50   Input ~ 0
B_FA21
Text GLabel 4100 7500 0    50   Input ~ 0
B_FA22
Text GLabel 4100 7600 0    50   Input ~ 0
B_FA23
Text GLabel 4100 6800 0    50   UnSpc ~ 0
B_FA15
Text GLabel 4100 7000 0    50   Input ~ 0
B_FA17
Text GLabel 4100 7100 0    50   Input ~ 0
B_FA18
Text GLabel 4100 6700 0    50   Input ~ 0
B_FA14
NoConn ~ 4550 6800
Text GLabel 5550 6700 0    50   Input ~ 0
B_RESOUT*
NoConn ~ 5550 6700
Text GLabel 8300 2100 0    50   Input ~ 0
B_D2
Text GLabel 8300 2300 0    50   Input ~ 0
B_D4
Text GLabel 8300 2500 0    50   Input ~ 0
B_D6
Text GLabel 8300 2600 0    50   Input ~ 0
B_D7
Text GLabel 8300 2400 0    50   Input ~ 0
B_D5
Text GLabel 8300 2200 0    50   Input ~ 0
B_D3
Text GLabel 8300 2000 0    50   Input ~ 0
B_D1
Text GLabel 9450 1900 2    50   Input ~ 0
A0
Text GLabel 9450 2000 2    50   Input ~ 0
A1
Text GLabel 9450 2100 2    50   Input ~ 0
A2
Text GLabel 9450 2200 2    50   Input ~ 0
A3
Text GLabel 9450 2300 2    50   Input ~ 0
A4
Text GLabel 9450 2400 2    50   Input ~ 0
A5
Text GLabel 9450 2500 2    50   Input ~ 0
A6
Text GLabel 9450 2600 2    50   Input ~ 0
A7
Text GLabel 9450 2800 2    50   Input ~ 0
A8
Text GLabel 9450 2900 2    50   Input ~ 0
A9
Text GLabel 9450 3000 2    50   Input ~ 0
A10
Text GLabel 9450 3100 2    50   Input ~ 0
A11
Text GLabel 9450 3200 2    50   Input ~ 0
A12
Text GLabel 9450 3300 2    50   Input ~ 0
A13
Text GLabel 8500 2800 2    50   Input ~ 0
D8
Text GLabel 8500 2900 2    50   Input ~ 0
D9
Text GLabel 8500 3100 2    50   Input ~ 0
D11
Text GLabel 8500 3200 2    50   Input ~ 0
D12
Text GLabel 8500 3300 2    50   Input ~ 0
D13
Text GLabel 8500 3400 2    50   Input ~ 0
D14
Text GLabel 8500 3500 2    50   Input ~ 0
D15
Text GLabel 8500 3000 2    50   Input ~ 0
D10
Text GLabel 8500 1900 2    50   Input ~ 0
D0
Text GLabel 8500 2000 2    50   Input ~ 0
D1
Text GLabel 8500 2100 2    50   Input ~ 0
D2
Text GLabel 8500 2200 2    50   Input ~ 0
D3
Text GLabel 8500 2300 2    50   Input ~ 0
D4
Text GLabel 8500 2400 2    50   Input ~ 0
D5
Text GLabel 8500 2500 2    50   Input ~ 0
D6
Text GLabel 8500 2600 2    50   Input ~ 0
D7
Text GLabel 10500 2000 2    50   Input ~ 0
RD*
Text GLabel 10500 2100 2    50   Input ~ 0
WE*
Text GLabel 9450 3400 2    50   Input ~ 0
A14
Text GLabel 10300 2200 0    50   Input ~ 0
B_PSEL*
Text GLabel 10300 2300 0    50   Input ~ 0
B_SERENA*
Text GLabel 10300 1900 0    50   UnSpc ~ 0
B_MEM*
Text GLabel 10300 2100 0    50   Input ~ 0
B_WE*
Text GLabel 10300 2000 0    50   Input ~ 0
B_RD*
Text GLabel 9250 3300 0    50   Input ~ 0
B_A13
Text GLabel 9250 2900 0    50   Input ~ 0
B_A9
Text GLabel 8300 2800 0    50   Input ~ 0
B_D8
Text GLabel 8300 3000 0    50   Input ~ 0
B_D10
Text GLabel 9250 1900 0    50   Input ~ 0
B_A0
Text GLabel 9250 2400 0    50   Input ~ 0
B_A5
Text GLabel 9250 2600 0    50   Input ~ 0
B_A7
Text GLabel 9250 2200 0    50   Input ~ 0
B_A3
Text GLabel 9250 2300 0    50   Input ~ 0
B_A4
Text GLabel 9250 2000 0    50   Input ~ 0
B_A1
Text GLabel 9250 2800 0    50   Input ~ 0
B_A8
Text GLabel 9250 2100 0    50   Input ~ 0
B_A2
Text GLabel 9250 2500 0    50   Input ~ 0
B_A6
Text GLabel 8300 3500 0    50   Input ~ 0
B_D15_IN
Text GLabel 8300 2900 0    50   Input ~ 0
B_D9
Text GLabel 9250 3100 0    50   Input ~ 0
B_A11
Text GLabel 9250 3000 0    50   Input ~ 0
B_A10
Text GLabel 9250 3400 0    50   Input ~ 0
B_A14
Text GLabel 9250 3200 0    50   Input ~ 0
B_A12
Text GLabel 8300 3400 0    50   Input ~ 0
B_D14
Text GLabel 8300 3300 0    50   Input ~ 0
B_D13
Text GLabel 8300 3200 0    50   Input ~ 0
B_D12
Text GLabel 8300 3100 0    50   Input ~ 0
B_D11
Text GLabel 8300 1900 0    50   Input ~ 0
B_D0_OUT
Text GLabel 10500 2200 2    50   Input ~ 0
PSEL*
Text GLabel 10500 2300 2    50   Input ~ 0
SERENA*
Text GLabel 10500 1900 2    50   UnSpc ~ 0
MEM*
Wire Wire Line
	8300 1900 8500 1900
Wire Wire Line
	8300 2000 8500 2000
Wire Wire Line
	8300 2100 8500 2100
Wire Wire Line
	8300 2200 8500 2200
Wire Wire Line
	8300 2300 8500 2300
Wire Wire Line
	8300 2400 8500 2400
Wire Wire Line
	8300 2500 8500 2500
Wire Wire Line
	8300 2600 8500 2600
Wire Wire Line
	8300 2800 8500 2800
Wire Wire Line
	8300 2900 8500 2900
Wire Wire Line
	8300 3000 8500 3000
Wire Wire Line
	8300 3100 8500 3100
Wire Wire Line
	8300 3200 8500 3200
Wire Wire Line
	8300 3300 8500 3300
Wire Wire Line
	8300 3400 8500 3400
Wire Wire Line
	8300 3500 8500 3500
Wire Wire Line
	9250 1900 9450 1900
Wire Wire Line
	9250 2000 9450 2000
Wire Wire Line
	9250 2100 9450 2100
Wire Wire Line
	9250 2200 9450 2200
Wire Wire Line
	9250 2300 9450 2300
Wire Wire Line
	9250 2400 9450 2400
Wire Wire Line
	9250 2500 9450 2500
Wire Wire Line
	9250 2600 9450 2600
Wire Wire Line
	9250 2800 9450 2800
Wire Wire Line
	9250 2900 9450 2900
Wire Wire Line
	9250 3000 9450 3000
Wire Wire Line
	9250 3100 9450 3100
Wire Wire Line
	9250 3200 9450 3200
Wire Wire Line
	9250 3300 9450 3300
Wire Wire Line
	9250 3400 9450 3400
Wire Wire Line
	10300 1900 10500 1900
Wire Wire Line
	10300 2000 10500 2000
Wire Wire Line
	10300 2100 10500 2100
Wire Wire Line
	10300 2200 10500 2200
Wire Wire Line
	10300 2300 10500 2300
Text GLabel 5550 6600 0    50   Input ~ 0
B_RDBENA*
NoConn ~ 5550 6600
Text GLabel 5400 7150 0    50   Input ~ 0
B_BST2
Text GLabel 5400 7250 0    50   Input ~ 0
B_BST3
Text GLabel 5400 6950 0    50   Input ~ 0
B_BST1
NoConn ~ 5400 6950
NoConn ~ 5400 7150
NoConn ~ 5400 7250
Text GLabel 10500 2550 2    50   Input ~ 0
CRUCLK*
Text GLabel 10300 2550 0    50   Input ~ 0
B_WE*
Wire Wire Line
	10300 2550 10500 2550
Text GLabel 10300 2850 0    50   Input ~ 0
B_CLKOUT
Text GLabel 10500 2850 2    50   Input ~ 0
CLKOUT
Wire Wire Line
	10300 2850 10500 2850
Text GLabel 5150 5950 2    50   Input ~ 0
+5V
Text GLabel 4000 3950 2    50   Input ~ 0
B_INTREQ*
Text GLabel 5150 2850 2    50   Input ~ 0
GND
Text GLabel 4000 4050 2    50   Input ~ 0
B_NMI*
Text GLabel 5150 5250 2    50   Input ~ 0
B_D2
Text GLabel 5150 5050 2    50   Input ~ 0
B_D4
Text GLabel 5150 4850 2    50   Input ~ 0
B_D6
Text GLabel 5150 5450 2    50   Input ~ 0
B_D0_OUT
Text GLabel 6300 3250 2    50   Input ~ 0
B_ALATCH
Text GLabel 4000 2850 2    50   Input ~ 0
GND
Text GLabel 4000 5950 2    50   Input ~ 0
+5V
Text GLabel 4000 2950 2    50   Input ~ 0
B_RESET*
Text GLabel 6300 3550 2    50   Input ~ 0
B_BST2
Text GLabel 5150 4750 2    50   Input ~ 0
B_D7
Text GLabel 5150 4950 2    50   Input ~ 0
B_D5
Text GLabel 5150 5150 2    50   Input ~ 0
B_D3
Text GLabel 5150 5350 2    50   Input ~ 0
B_D1
Text GLabel 6300 5150 2    50   Input ~ 0
B_A6
Text GLabel 4000 5250 2    50   Input ~ 0
B_A8
Text GLabel 6300 4250 2    50   Input ~ 0
B_A14
Text GLabel 4000 3350 2    50   Input ~ 0
B_A12
Text GLabel 6300 5350 2    50   Input ~ 0
B_A4
Text GLabel 6300 5450 2    50   Input ~ 0
B_A2
Text GLabel 6300 3150 2    50   Input ~ 0
B_A13
Text GLabel 4000 5150 2    50   Input ~ 0
B_A7
Text GLabel 6300 3050 2    50   Input ~ 0
B_A9
Text GLabel 6300 5250 2    50   Input ~ 0
B_A5
Text GLabel 4000 5450 2    50   Input ~ 0
B_A3
Text GLabel 4000 5350 2    50   Input ~ 0
B_A1
Text GLabel 4000 3250 2    50   Input ~ 0
B_PSEL*
NoConn ~ 4000 4450
NoConn ~ 4000 4950
NoConn ~ 6300 4350
NoConn ~ 6300 4850
Text GLabel 6300 2850 2    50   Input ~ 0
GND
Text GLabel 6300 5950 2    50   Input ~ 0
+5V
Text GLabel 4000 3150 2    50   Input ~ 0
B_CLKOUT
Text GLabel 6300 5050 2    50   Input ~ 0
B_READY
Text GLabel 6300 3450 2    50   Input ~ 0
B_BST3
Text GLabel 6300 3750 2    50   Input ~ 0
B_BST1
Text GLabel 4000 3850 2    50   Input ~ 0
B_WE*
Text GLabel 4000 3650 2    50   Input ~ 0
B_RD*
$Comp
L Gemini:DIN-41612_Kontron_Card J?
U 3 1 5F02AAE1
P 4000 2850
AR Path="/5F02AAE1" Ref="J?"  Part="3" 
AR Path="/5F025AB5/5F02AAE1" Ref="J2"  Part="3" 
AR Path="/5ED05929/5F02AAE1" Ref="J?"  Part="3" 
AR Path="/5EF2AEDB/5F02AAE1" Ref="J1"  Part="3" 
F 0 "J1" V 3347 1300 50  0000 C CNN
F 1 "DIN-41612_Kontron_Card" V 3256 1300 50  0000 C CNN
F 2 "Connector_DIN:DIN41612_C_3x32_Male_Horizontal_THT" H 4400 -350 50  0001 L CNN
F 3 "https://www.arrow.com/en/products/86092645113755elf/amphenol-fci" H 3150 -250 50  0001 L CNN
	3    4000 2850
	1    0    0    -1  
$EndComp
$Comp
L Gemini:DIN-41612_Kontron_Card J?
U 1 1 5F02AAE7
P 6300 2850
AR Path="/5F02AAE7" Ref="J?"  Part="1" 
AR Path="/5F025AB5/5F02AAE7" Ref="J2"  Part="1" 
AR Path="/5ED05929/5F02AAE7" Ref="J?"  Part="1" 
AR Path="/5EF2AEDB/5F02AAE7" Ref="J1"  Part="1" 
F 0 "J1" V 5647 1300 50  0000 C CNN
F 1 "DIN-41612_Kontron_Card" V 5556 1300 50  0000 C CNN
F 2 "Connector_DIN:DIN41612_C_3x32_Male_Horizontal_THT" H 6700 -350 50  0001 L CNN
F 3 "https://www.arrow.com/en/products/86092645113755elf/amphenol-fci" H 5450 -250 50  0001 L CNN
	1    6300 2850
	1    0    0    -1  
$EndComp
$Comp
L Gemini:DIN-41612_Kontron_Card J?
U 2 1 5F02AAED
P 5150 2850
AR Path="/5F02AAED" Ref="J?"  Part="2" 
AR Path="/5F025AB5/5F02AAED" Ref="J2"  Part="2" 
AR Path="/5ED05929/5F02AAED" Ref="J?"  Part="2" 
AR Path="/5EF2AEDB/5F02AAED" Ref="J1"  Part="2" 
F 0 "J1" V 4527 1300 50  0000 C CNN
F 1 "DIN-41612_Kontron_Card" V 4436 1300 50  0000 C CNN
F 2 "Connector_DIN:DIN41612_C_3x32_Male_Horizontal_THT" H 5550 -350 50  0001 L CNN
F 3 "https://www.arrow.com/en/products/86092645113755elf/amphenol-fci" H 4300 -250 50  0001 L CNN
	2    5150 2850
	1    0    0    -1  
$EndComp
Text GLabel 6300 5750 2    50   Input ~ 0
B_D14
Text GLabel 6300 5850 2    50   Input ~ 0
B_D13
Text GLabel 6300 5550 2    50   Input ~ 0
B_D12
Text GLabel 6300 5650 2    50   Input ~ 0
B_D11
Text GLabel 4000 5650 2    50   Input ~ 0
B_D10
Text GLabel 4000 4650 2    50   Input ~ 0
B_D9
Text GLabel 4000 5850 2    50   Input ~ 0
B_D8
Text GLabel 4000 5750 2    50   Input ~ 0
B_D15_IN
Text GLabel 4000 3050 2    50   UnSpc ~ 0
B_MEM*
Text GLabel 6300 4950 2    50   Input ~ 0
B_HOLD*
Text GLabel 4000 5050 2    50   Input ~ 0
B_FA11
Text GLabel 4000 4850 2    50   Input ~ 0
B_FA12
Text GLabel 4000 4750 2    50   Input ~ 0
B_FA13
Text GLabel 4000 3750 2    50   UnSpc ~ 0
B_FA15
Text GLabel 4000 4150 2    50   Input ~ 0
B_FA16
Text GLabel 6300 4650 2    50   Input ~ 0
B_FA14
Text GLabel 6300 3950 2    50   Input ~ 0
B_FA17
Text GLabel 6300 3850 2    50   Input ~ 0
B_FA18
Text GLabel 6300 3350 2    50   Input ~ 0
B_IORQ*
Text GLabel 5150 3750 2    50   Input ~ 0
B_BMO
Text GLabel 5150 3550 2    50   Input ~ 0
B_INTA*
Text GLabel 5150 5850 2    50   Input ~ 0
B_FA19
Text GLabel 5150 5750 2    50   Input ~ 0
B_FA20
Text GLabel 5150 5650 2    50   Input ~ 0
B_FA21
Text GLabel 5150 5550 2    50   Input ~ 0
B_FA22
Text GLabel 5150 3050 2    50   Input ~ 0
B_FA23
Text GLabel 5150 2950 2    50   Input ~ 0
B_PRO
Text GLabel 6300 4450 2    50   Input ~ 0
B_PBPTHP
Text GLabel 4000 4550 2    50   Input ~ 0
B_SPAMM
Text GLabel 5150 3950 2    50   Input ~ 0
B_IC0
Text GLabel 5150 4050 2    50   Input ~ 0
B_IC1
Text GLabel 5150 4150 2    50   Input ~ 0
B_IC2
Text GLabel 5150 4250 2    50   Input ~ 0
B_IC3
Text GLabel 5150 4350 2    50   Input ~ 0
B_INT1
Text GLabel 5150 4450 2    50   Input ~ 0
B_INT2
Text GLabel 5150 4550 2    50   Input ~ 0
B_INT3
Text GLabel 5150 4650 2    50   Input ~ 0
B_INT4
Text GLabel 6300 4050 2    50   Input ~ 0
B_IAQ*
Text GLabel 6300 2950 2    50   Input ~ 0
B_HOLDA*
Text GLabel 5150 3250 2    50   Input ~ 0
B_SERENA*
Text GLabel 5150 3150 2    50   Input ~ 0
B_PARENA*
NoConn ~ 6300 4750
NoConn ~ 6300 4150
NoConn ~ 6300 3650
NoConn ~ 5150 3350
NoConn ~ 5150 3450
NoConn ~ 5150 3650
NoConn ~ 5150 3850
Text GLabel 4000 3550 2    50   Input ~ 0
B_RDBENA*
NoConn ~ 6300 4550
Text GLabel 4000 3450 2    50   Input ~ 0
B_RESOUT*
Text Notes 3650 5850 2    50   ~ 0
LSBit of MSByte (even byte)
Text Notes 3650 5750 2    50   ~ 0
MSBit of MSByte (even byte)
Text Notes 3650 5550 2    50   ~ 0
LSBit, A0 addressing the word
Text GLabel 4000 4350 2    50   Input ~ 0
B_A11
Text GLabel 4000 4250 2    50   Input ~ 0
B_A10
Text GLabel 4000 5550 2    50   Input ~ 0
B_A0
Text GLabel 4000 3450 2    50   Input ~ 0
B_RESOUT*
$EndSCHEMATC
