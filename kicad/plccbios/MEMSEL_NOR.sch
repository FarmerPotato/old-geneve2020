EESchema Schematic File Version 4
LIBS:BIOS-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 4250 2150 0    50   Input ~ 0
B_BST1
Text GLabel 4250 2250 0    50   Input ~ 0
B_BST2
$Comp
L 74xx:74LS04 U?
U 2 1 5ED95445
P 4750 3350
F 0 "U?" H 4750 3667 50  0000 C CNN
F 1 "74LS04" H 4750 3576 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 4750 3350 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 4750 3350 50  0001 C CNN
	2    4750 3350
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS04 U?
U 1 1 5ED9544B
P 4750 2700
F 0 "U?" H 4750 3017 50  0000 C CNN
F 1 "74LS04" H 4750 2926 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 4750 2700 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 4750 2700 50  0001 C CNN
	1    4750 2700
	1    0    0    -1  
$EndComp
Text GLabel 4250 2700 0    50   Input ~ 0
B_MEM*
Text GLabel 4300 3350 0    50   Input ~ 0
B_PSEL*
$Comp
L 74xx:74LS27 U?
U 1 1 5ED95453
P 5800 2250
F 0 "U?" H 5800 2575 50  0000 C CNN
F 1 "74LS27" H 5800 2484 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 5800 2250 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS27" H 5800 2250 50  0001 C CNN
	1    5800 2250
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS27 U?
U 2 1 5ED95459
P 5800 3350
F 0 "U?" H 5800 3675 50  0000 C CNN
F 1 "74LS27" H 5800 3584 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 5800 3350 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS27" H 5800 3350 50  0001 C CNN
	2    5800 3350
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS27 U?
U 3 1 5ED9545F
P 6550 2800
F 0 "U?" H 6550 3125 50  0000 C CNN
F 1 "74LS27" H 6550 3034 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 6550 2800 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS27" H 6550 2800 50  0001 C CNN
	3    6550 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	5500 3450 5250 3450
Wire Wire Line
	5250 3450 5250 3850
Wire Wire Line
	5250 3850 4250 3850
Wire Wire Line
	4300 3350 4450 3350
Wire Wire Line
	4250 2700 4350 2700
Wire Wire Line
	6100 2700 6250 2700
Wire Wire Line
	6250 2800 6100 2800
Wire Wire Line
	6250 3850 5250 3850
Connection ~ 5250 3850
Text GLabel 4300 3850 0    50   Input ~ 0
GND
Wire Wire Line
	5050 3350 5500 3350
Wire Wire Line
	5500 3250 5250 3250
Wire Wire Line
	5250 3250 5250 3000
Wire Wire Line
	4350 3000 4350 2700
Wire Wire Line
	4350 3000 5250 3000
Connection ~ 4350 2700
Wire Wire Line
	4250 2150 5500 2150
Wire Wire Line
	5500 2250 4250 2250
Wire Wire Line
	5250 2350 5500 2350
Text GLabel 7100 2800 2    50   Input ~ 0
MEMSEL*
Wire Wire Line
	7100 2800 6850 2800
Wire Wire Line
	6100 2800 6100 3350
Wire Wire Line
	6250 2900 6250 3850
Wire Wire Line
	5250 2700 5250 2350
Wire Wire Line
	6100 2250 6100 2700
Text Notes 5600 3800 0    50   ~ 0
BIOS is selected by \nGM : MEM=0, PSEL*=1\nor MACRO : MEM=1,BST1=0, BST2=0\n
Text Label 6100 3350 0    50   ~ 0
GM
Text Label 6100 2250 0    50   ~ 0
MACRO
Wire Wire Line
	5050 2700 5250 2700
Wire Wire Line
	4350 2700 4450 2700
Text Notes 4200 4300 0    100  ~ 0
Using NOR logic instead of NAND
$EndSCHEMATC
