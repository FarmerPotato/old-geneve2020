EESchema Schematic File Version 4
LIBS:BIOS-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Memory_RAM:628128_DIP32_SSOP32 U10
U 1 1 5DFE9735
P 7600 1950
F 0 "U10" H 7600 3131 50  0000 C CNN
F 1 "TC551001_128Kx8" H 7600 3040 50  0000 C CNN
F 2 "Housings_DIP:DIP-32_W15.24mm_Socket_LongPads" H 7600 1950 50  0001 C CNN
F 3 "http://www.futurlec.com/Datasheet/Memory/628128.pdf" H 7600 1950 50  0001 C CNN
	1    7600 1950
	1    0    0    -1  
$EndComp
Text GLabel 8100 1150 2    50   Input ~ 0
D0
Text GLabel 8100 1250 2    50   Input ~ 0
D1
Text GLabel 8100 1350 2    50   Input ~ 0
D2
Text GLabel 8100 1450 2    50   Input ~ 0
D3
Text GLabel 8100 1550 2    50   Input ~ 0
D4
Text GLabel 8100 1650 2    50   Input ~ 0
D5
Text GLabel 8100 1750 2    50   Input ~ 0
D6
Text GLabel 8100 1850 2    50   Input ~ 0
D7
$Comp
L Memory_EPROM:27C010 U11
U 1 1 5DFF257B
P 7600 4950
F 0 "U11" H 7600 6331 50  0000 C CNN
F 1 "27C010" H 7600 6240 50  0000 C CNN
F 2 "Socket:DIP_Socket-32_W11.9_W12.7_W15.24_W17.78_W18.5_3M_232-1285-00-0602J" H 7600 4950 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/doc0321.pdf" H 7600 4950 50  0001 C CNN
	1    7600 4950
	1    0    0    -1  
$EndComp
Text GLabel 7600 2950 0    50   Input ~ 0
GND
Text GLabel 7600 6250 0    50   Input ~ 0
GND
Text GLabel 9650 2950 0    50   Input ~ 0
GND
Text GLabel 7600 950  2    50   Input ~ 0
+5V
Text GLabel 9650 950  2    50   Input ~ 0
+5V
$Comp
L Memory_EPROM:27C010 U12
U 1 1 5DFF6D13
P 9650 4950
F 0 "U12" H 9650 6331 50  0000 C CNN
F 1 "27C010" H 9650 6240 50  0000 C CNN
F 2 "Socket:DIP_Socket-32_W11.9_W12.7_W15.24_W17.78_W18.5_3M_232-1285-00-0602J" H 9650 4950 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/doc0321.pdf" H 9650 4950 50  0001 C CNN
	1    9650 4950
	1    0    0    -1  
$EndComp
Text GLabel 9650 6250 0    50   Input ~ 0
GND
Text Notes 7150 3800 2    50   ~ 0
One ROM image,\nA0 = even byte
Text GLabel 7200 3950 0    50   Input ~ 0
ZERO
Text GLabel 9650 3750 2    50   Input ~ 0
+5V
Text GLabel 7600 3750 2    50   Input ~ 0
+5V
Text Notes 9100 3800 2    50   ~ 0
One ROM image,\nA0 = odd byte
$Comp
L Memory_RAM:628128_DIP32_SSOP32 U13
U 1 1 5DFEE7D9
P 9650 1950
F 0 "U13" H 9650 3131 50  0000 C CNN
F 1 "TC551001_128Kx8" H 9650 3040 50  0000 C CNN
F 2 "Housings_DIP:DIP-32_W15.24mm_Socket_LongPads" H 9650 1950 50  0001 C CNN
F 3 "http://www.futurlec.com/Datasheet/Memory/628128.pdf" H 9650 1950 50  0001 C CNN
	1    9650 1950
	1    0    0    -1  
$EndComp
Text GLabel 7100 2450 0    50   Input ~ 0
PAGE0
Text Notes 4850 4400 0    50   ~ 0
EPROM Map - 128Kbytes\n 0000- 3FFE  Page 0. Interrupt and XOP vectors. \n                        Supervisor code. Macrostore.\n 4000-7FFE   Page 1. Supervisor code.\n 8000-FFFE   Page 2-3\n10000-1FFFE Page 4-7\n\nRAM Usage - 256K in 16K pages\n00000-03FFE  Page 0\n04000-3FFFE Pages 1-15\n\nMemory Map\n0000-3FFE  ROM Page 0\n4000-7FFE  ROM Paged area\n8000-BFFE  RAM Page 0\nC000-FFFE  RAM Paged area
Text Notes 5750 900  0    50   ~ 0
PSEL* is independent of PRV.\nInterrupts are guaranteed to \nclear MS / output PSEL*=1\n
$Sheet
S 4950 5050 1250 500 
U 5EF2AEDB
F0 "Backplane" 50
F1 "Backplane.sch" 50
$EndSheet
Text GLabel 8100 2250 2    50   Input ~ 0
RD*
Text GLabel 8100 2350 2    50   Input ~ 0
WE*
Text GLabel 9250 3950 0    50   Input ~ 0
ONE
Text GLabel 10050 3950 2    50   Input ~ 0
D8
Text GLabel 10050 4050 2    50   Input ~ 0
D9
Text GLabel 10050 4250 2    50   Input ~ 0
D11
Text GLabel 10050 4350 2    50   Input ~ 0
D12
Text GLabel 10050 4450 2    50   Input ~ 0
D13
Text GLabel 10050 4550 2    50   Input ~ 0
D14
Text GLabel 10050 4650 2    50   Input ~ 0
D15
Text GLabel 9250 5950 0    50   Input ~ 0
MEMSEL*
Text GLabel 9250 6050 0    50   Input ~ 0
A14
Text GLabel 7200 6050 0    50   Input ~ 0
A14
Text GLabel 7200 5950 0    50   Input ~ 0
MEMSEL*
Text GLabel 8100 2050 2    50   Input ~ 0
MEMSEL*
Text Notes 10450 2250 0    50   ~ 0
RAM maps into \n>8000->FFFE
Text Notes 8700 6300 0    50   ~ 0
ROM maps into \n>0000->7FFE
Text GLabel 9150 2450 0    50   Input ~ 0
PAGE0
Text GLabel 7100 2550 0    50   Input ~ 0
PAGE1
Text GLabel 7100 2650 0    50   Input ~ 0
PAGE2
Text GLabel 7100 2750 0    50   Input ~ 0
PAGE3
Text GLabel 9050 5850 0    50   Output ~ 0
+5V
Text GLabel 9050 5750 0    50   Output ~ 0
+5V
Text GLabel 9150 2550 0    50   Input ~ 0
PAGE1
Text GLabel 9150 2650 0    50   Input ~ 0
PAGE2
Text GLabel 9150 2750 0    50   Input ~ 0
PAGE3
Text GLabel 7200 5850 0    50   Output ~ 0
+5V
Text GLabel 7200 5750 0    50   Output ~ 0
+5V
$Comp
L power:PWR_FLAG #FLG01
U 1 1 5F144476
P 1200 7300
F 0 "#FLG01" H 1200 7375 50  0001 C CNN
F 1 "PWR_FLAG" V 1200 7427 50  0000 L CNN
F 2 "" H 1200 7300 50  0001 C CNN
F 3 "~" H 1200 7300 50  0001 C CNN
	1    1200 7300
	0    -1   -1   0   
$EndComp
$Comp
L power:PWR_FLAG #FLG02
U 1 1 5F144791
P 1200 7500
F 0 "#FLG02" H 1200 7575 50  0001 C CNN
F 1 "PWR_FLAG" V 1200 7627 50  0000 L CNN
F 2 "" H 1200 7500 50  0001 C CNN
F 3 "~" H 1200 7500 50  0001 C CNN
	1    1200 7500
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR03
U 1 1 5F1458A7
P 1200 7500
F 0 "#PWR03" H 1200 7250 50  0001 C CNN
F 1 "GND" H 1205 7327 50  0000 C CNN
F 2 "" H 1200 7500 50  0001 C CNN
F 3 "" H 1200 7500 50  0001 C CNN
	1    1200 7500
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR02
U 1 1 5F145103
P 1200 7300
F 0 "#PWR02" H 1200 7150 50  0001 C CNN
F 1 "+5V" H 1215 7473 50  0000 C CNN
F 2 "" H 1200 7300 50  0001 C CNN
F 3 "" H 1200 7300 50  0001 C CNN
	1    1200 7300
	1    0    0    -1  
$EndComp
Connection ~ 1200 7300
Connection ~ 1200 7500
Wire Wire Line
	1550 7500 1200 7500
Wire Wire Line
	1550 7300 1200 7300
$Comp
L Device:C_Small C1
U 1 1 5F14787B
P 1550 7400
F 0 "C1" H 1500 7250 50  0000 L CNN
F 1 "0.1uF" H 1642 7355 50  0001 L CNN
F 2 "Capacitors_SMD:C_1206" H 1550 7400 50  0001 C CNN
F 3 "~" H 1550 7400 50  0001 C CNN
	1    1550 7400
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C11
U 1 1 5ED9AB48
P 3550 7400
F 0 "C11" H 3500 7250 50  0000 L CNN
F 1 "0.1uF" H 3642 7355 50  0001 L CNN
F 2 "Capacitors_SMD:C_1206" H 3550 7400 50  0001 C CNN
F 3 "~" H 3550 7400 50  0001 C CNN
	1    3550 7400
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C3
U 1 1 5ED9ACCE
P 1950 7400
F 0 "C3" H 1900 7250 50  0000 L CNN
F 1 "0.1uF" H 2042 7355 50  0001 L CNN
F 2 "Capacitors_SMD:C_1206" H 1950 7400 50  0001 C CNN
F 3 "~" H 1950 7400 50  0001 C CNN
	1    1950 7400
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C4
U 1 1 5ED9AF09
P 2150 7400
F 0 "C4" H 2100 7250 50  0000 L CNN
F 1 "0.1uF" H 2242 7355 50  0001 L CNN
F 2 "Capacitors_SMD:C_1206" H 2150 7400 50  0001 C CNN
F 3 "~" H 2150 7400 50  0001 C CNN
	1    2150 7400
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C5
U 1 1 5ED9B162
P 2350 7400
F 0 "C5" H 2300 7250 50  0000 L CNN
F 1 "0.1uF" H 2442 7355 50  0001 L CNN
F 2 "Capacitors_SMD:C_1206" H 2350 7400 50  0001 C CNN
F 3 "~" H 2350 7400 50  0001 C CNN
	1    2350 7400
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C7
U 1 1 5ED9B482
P 2750 7400
F 0 "C7" H 2700 7250 50  0000 L CNN
F 1 "0.1uF" H 2842 7355 50  0001 L CNN
F 2 "Capacitors_SMD:C_1206" H 2750 7400 50  0001 C CNN
F 3 "~" H 2750 7400 50  0001 C CNN
	1    2750 7400
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C8
U 1 1 5ED9C1BA
P 2950 7400
F 0 "C8" H 2900 7250 50  0000 L CNN
F 1 "0.1uF" H 3042 7355 50  0001 L CNN
F 2 "Capacitors_SMD:C_1206" H 2950 7400 50  0001 C CNN
F 3 "~" H 2950 7400 50  0001 C CNN
	1    2950 7400
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C9
U 1 1 5ED9C1C4
P 3150 7400
F 0 "C9" H 3100 7250 50  0000 L CNN
F 1 "0.1uF" H 3242 7355 50  0001 L CNN
F 2 "Capacitors_SMD:C_1206" H 3150 7400 50  0001 C CNN
F 3 "~" H 3150 7400 50  0001 C CNN
	1    3150 7400
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C10
U 1 1 5ED9C1CE
P 3350 7400
F 0 "C10" H 3300 7250 50  0000 L CNN
F 1 "0.1uF" H 3442 7355 50  0001 L CNN
F 2 "Capacitors_SMD:C_1206" H 3350 7400 50  0001 C CNN
F 3 "~" H 3350 7400 50  0001 C CNN
	1    3350 7400
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C14
U 1 1 5ED9C1D8
P 4150 7400
F 0 "C14" H 4100 7250 50  0000 L CNN
F 1 "0.1uF" H 4242 7355 50  0001 L CNN
F 2 "Capacitors_SMD:C_1206" H 4150 7400 50  0001 C CNN
F 3 "~" H 4150 7400 50  0001 C CNN
	1    4150 7400
	1    0    0    -1  
$EndComp
Connection ~ 1550 7300
Connection ~ 1950 7300
Wire Wire Line
	1950 7300 2150 7300
Connection ~ 2150 7300
Wire Wire Line
	2150 7300 2350 7300
Connection ~ 2350 7300
Connection ~ 2750 7300
Wire Wire Line
	2750 7300 2950 7300
Connection ~ 2950 7300
Wire Wire Line
	2950 7300 3150 7300
Connection ~ 3150 7300
Wire Wire Line
	3150 7300 3350 7300
Connection ~ 3350 7300
Connection ~ 1550 7500
Connection ~ 1950 7500
Connection ~ 2150 7500
Wire Wire Line
	2150 7500 1950 7500
Connection ~ 2350 7500
Wire Wire Line
	2350 7500 2150 7500
Connection ~ 2750 7500
Connection ~ 2950 7500
Wire Wire Line
	2950 7500 2750 7500
Connection ~ 3150 7500
Wire Wire Line
	3150 7500 2950 7500
Connection ~ 3350 7500
Wire Wire Line
	3350 7500 3150 7500
Wire Wire Line
	3350 7300 3550 7300
Wire Wire Line
	3350 7500 3550 7500
NoConn ~ 4650 7400
NoConn ~ 4650 7500
Wire Wire Line
	9050 5750 9250 5750
Wire Wire Line
	9250 5850 9050 5850
Wire Wire Line
	2350 7300 2550 7300
Wire Wire Line
	2350 7500 2550 7500
Text GLabel 7200 5350 0    50   Input ~ 0
PAGE0
Text GLabel 7200 5450 0    50   Input ~ 0
PAGE1
Text GLabel 7200 5550 0    50   Input ~ 0
PAGE2
Text GLabel 9250 5350 0    50   Input ~ 0
PAGE0
Text GLabel 9250 5450 0    50   Input ~ 0
PAGE1
Text GLabel 9250 5550 0    50   Input ~ 0
PAGE2
Text Notes 7000 3350 0    50   ~ 0
MACRO ROM is unified \nwith Page 0 ROM
$Comp
L Device:C_Small C6
U 1 1 5EFA0E8A
P 2550 7400
F 0 "C6" H 2500 7250 50  0000 L CNN
F 1 "0.1uF" H 2642 7355 50  0001 L CNN
F 2 "Capacitors_SMD:C_1206" H 2550 7400 50  0001 C CNN
F 3 "~" H 2550 7400 50  0001 C CNN
	1    2550 7400
	1    0    0    -1  
$EndComp
Connection ~ 2550 7300
Wire Wire Line
	2550 7300 2750 7300
Connection ~ 2550 7500
Wire Wire Line
	2550 7500 2750 7500
NoConn ~ 5450 7400
NoConn ~ 5450 7500
$Comp
L Gemini:Keystone_7790_RA_Screw J2
U 1 1 5EE12DA3
P 4650 7400
F 0 "J2" H 5050 7665 50  0000 C CNN
F 1 "Keystone_7790_RA_Screw" H 5050 7574 50  0000 C CNN
F 2 "Gemini:Keystone_7790_and_9203" H 5300 7500 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/1/7790.pdf" H 5300 7400 50  0001 L CNN
F 4 "Terminals KEYSTONE PCVB METRIC SCREW TRM HORIZ" H 5300 7300 50  0001 L CNN "Description"
F 5 "7.9" H 5300 7200 50  0001 L CNN "Height"
F 6 "534-7790" H 5300 7100 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.com/Search/Refine.aspx?Keyword=534-7790" H 5300 7000 50  0001 L CNN "Mouser Price/Stock"
F 8 "Keystone Electronics" H 5300 6900 50  0001 L CNN "Manufacturer_Name"
F 9 "7790" H 5300 6800 50  0001 L CNN "Manufacturer_Part_Number"
	1    4650 7400
	1    0    0    -1  
$EndComp
Text Notes 5800 6300 0    50   ~ 0
Convention: SBO 0\nDSR ROM Enable\nAn indicator light\nQ0 says page 1 is active\nQ7 is a spare bit
$Comp
L Device:LED D?
U 1 1 5F0F13C8
P 3850 6400
AR Path="/5EB213FB/5F0F13C8" Ref="D?"  Part="1" 
AR Path="/5F0F13C8" Ref="D2"  Part="1" 
F 0 "D2" V 3797 6478 50  0000 L CNN
F 1 "LED" V 3888 6478 50  0000 L CNN
F 2 "LEDs:LED_Rectangular_W5.0mm_H2.0mm" H 3850 6400 50  0001 C CNN
F 3 "~" H 3850 6400 50  0001 C CNN
	1    3850 6400
	0    1    -1   0   
$EndComp
Text GLabel 3000 3950 0    50   Input ~ 0
SERENA*
Text GLabel 3900 2350 3    50   Input ~ 0
WE*
Text GLabel 8100 2150 2    50   Input ~ 0
A14
Text GLabel 10150 2250 2    50   Input ~ 0
RD*
Text GLabel 10150 2350 2    50   Input ~ 0
WE*
Text GLabel 10150 2050 2    50   Input ~ 0
MEMSEL*
Text GLabel 10150 2150 2    50   Input ~ 0
A14
Text GLabel 7200 4050 0    50   Input ~ 0
A0
Text GLabel 7200 4150 0    50   Input ~ 0
A1
Text GLabel 7200 4250 0    50   Input ~ 0
A2
Text GLabel 7200 4350 0    50   Input ~ 0
A3
Text GLabel 7200 4450 0    50   Input ~ 0
A4
Text GLabel 7200 4550 0    50   Input ~ 0
A5
Text GLabel 7200 4650 0    50   Input ~ 0
A6
Text GLabel 7200 4750 0    50   Input ~ 0
A7
Text GLabel 7200 4850 0    50   Input ~ 0
A8
Text GLabel 7200 4950 0    50   Input ~ 0
A9
Text GLabel 7200 5050 0    50   Input ~ 0
A10
Text GLabel 7200 5150 0    50   Input ~ 0
A11
Text GLabel 7200 5250 0    50   Input ~ 0
A12
Text GLabel 9250 4050 0    50   Input ~ 0
A0
Text GLabel 9250 4150 0    50   Input ~ 0
A1
Text GLabel 9250 4250 0    50   Input ~ 0
A2
Text GLabel 9250 4350 0    50   Input ~ 0
A3
Text GLabel 9250 4450 0    50   Input ~ 0
A4
Text GLabel 9250 4550 0    50   Input ~ 0
A5
Text GLabel 9250 4650 0    50   Input ~ 0
A6
Text GLabel 9250 4750 0    50   Input ~ 0
A7
Text GLabel 9250 4850 0    50   Input ~ 0
A8
Text GLabel 9250 4950 0    50   Input ~ 0
A9
Text GLabel 9250 5050 0    50   Input ~ 0
A10
Text GLabel 9250 5150 0    50   Input ~ 0
A11
Text GLabel 9250 5250 0    50   Input ~ 0
A12
Text GLabel 7100 1150 0    50   Input ~ 0
A0
Text GLabel 7100 1250 0    50   Input ~ 0
A1
Text GLabel 7100 1350 0    50   Input ~ 0
A2
Text GLabel 7100 1450 0    50   Input ~ 0
A3
Text GLabel 7100 1550 0    50   Input ~ 0
A4
Text GLabel 7100 1650 0    50   Input ~ 0
A5
Text GLabel 7100 1750 0    50   Input ~ 0
A6
Text GLabel 7100 1850 0    50   Input ~ 0
A7
Text GLabel 7100 1950 0    50   Input ~ 0
A8
Text GLabel 7100 2050 0    50   Input ~ 0
A9
Text GLabel 7100 2150 0    50   Input ~ 0
A10
Text GLabel 7100 2250 0    50   Input ~ 0
A11
Text GLabel 7100 2350 0    50   Input ~ 0
A12
Text GLabel 9150 1150 0    50   Input ~ 0
A0
Text GLabel 9150 1250 0    50   Input ~ 0
A1
Text GLabel 9150 1350 0    50   Input ~ 0
A2
Text GLabel 9150 1450 0    50   Input ~ 0
A3
Text GLabel 9150 1550 0    50   Input ~ 0
A4
Text GLabel 9150 1650 0    50   Input ~ 0
A5
Text GLabel 9150 1750 0    50   Input ~ 0
A6
Text GLabel 9150 1850 0    50   Input ~ 0
A7
Text GLabel 9150 1950 0    50   Input ~ 0
A8
Text GLabel 9150 2050 0    50   Input ~ 0
A9
Text GLabel 9150 2150 0    50   Input ~ 0
A10
Text GLabel 9150 2250 0    50   Input ~ 0
A11
Text GLabel 9150 2350 0    50   Input ~ 0
A12
Text GLabel 8000 3950 2    50   Input ~ 0
D0
Text GLabel 8000 4050 2    50   Input ~ 0
D1
Text GLabel 8000 4150 2    50   Input ~ 0
D2
Text GLabel 8000 4250 2    50   Input ~ 0
D3
Text GLabel 8000 4350 2    50   Input ~ 0
D4
Text GLabel 8000 4450 2    50   Input ~ 0
D5
Text GLabel 8000 4550 2    50   Input ~ 0
D6
Text GLabel 8000 4650 2    50   Input ~ 0
D7
Text GLabel 10050 4150 2    50   Input ~ 0
D10
Text GLabel 10150 1150 2    50   Input ~ 0
D8
Text GLabel 10150 1250 2    50   Input ~ 0
D9
Text GLabel 10150 1450 2    50   Input ~ 0
D11
Text GLabel 10150 1550 2    50   Input ~ 0
D12
Text GLabel 10150 1650 2    50   Input ~ 0
D13
Text GLabel 10150 1750 2    50   Input ~ 0
D14
Text GLabel 10150 1850 2    50   Input ~ 0
D15
Text GLabel 10150 1350 2    50   Input ~ 0
D10
Text Notes 8800 5550 2    50   ~ 0
A13 goes\ninto PAGE\nlogic
Text Notes 8850 2650 2    50   ~ 0
A13 goes\ninto PAGE\nlogic
Text GLabel 3700 950  1    50   Input ~ 0
A2
Text GLabel 4000 950  1    50   Input ~ 0
A1
Text GLabel 4000 2350 3    50   Input ~ 0
A0
Text GLabel 3900 950  1    50   Input ~ 0
GND
Text GLabel 3800 950  1    50   Input ~ 0
GND
Text GLabel 4500 1650 2    50   Input ~ 0
GND
Text GLabel 3100 1650 0    50   Input ~ 0
GND
Text GLabel 3800 950  1    50   Input ~ 0
GND
Text GLabel 3800 2350 3    50   Input ~ 0
+5V
Text GLabel 3700 2350 3    50   Input ~ 0
+5V
Text GLabel 3500 2350 3    50   Input ~ 0
CRUIN
$Comp
L Gemini:Keystone_7790_RA_Screw J3
U 1 1 5F3003CB
P 4650 6750
F 0 "J3" H 5050 7015 50  0000 C CNN
F 1 "Keystone_7790_RA_Screw" H 5050 6924 50  0000 C CNN
F 2 "Gemini:Keystone_7790_and_9203" H 5300 6850 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/1/7790.pdf" H 5300 6750 50  0001 L CNN
F 4 "Terminals KEYSTONE PCVB METRIC SCREW TRM HORIZ" H 5300 6650 50  0001 L CNN "Description"
F 5 "7.9" H 5300 6550 50  0001 L CNN "Height"
F 6 "534-7790" H 5300 6450 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.com/Search/Refine.aspx?Keyword=534-7790" H 5300 6350 50  0001 L CNN "Mouser Price/Stock"
F 8 "Keystone Electronics" H 5300 6250 50  0001 L CNN "Manufacturer_Name"
F 9 "7790" H 5300 6150 50  0001 L CNN "Manufacturer_Part_Number"
	1    4650 6750
	1    0    0    -1  
$EndComp
NoConn ~ 4650 6750
NoConn ~ 4650 6850
NoConn ~ 5450 6750
NoConn ~ 5450 6850
Connection ~ 3550 7300
Connection ~ 3550 7500
$Comp
L Device:C_Small C12
U 1 1 5F3C4DB8
P 3750 7400
F 0 "C12" H 3700 7250 50  0000 L CNN
F 1 "0.1uF" H 3842 7355 50  0001 L CNN
F 2 "Capacitors_SMD:C_1206" H 3750 7400 50  0001 C CNN
F 3 "~" H 3750 7400 50  0001 C CNN
	1    3750 7400
	1    0    0    -1  
$EndComp
Connection ~ 3750 7300
Connection ~ 3750 7500
$Comp
L Device:C_Small C13
U 1 1 5F3C5C67
P 3950 7400
F 0 "C13" H 3900 7250 50  0000 L CNN
F 1 "0.1uF" H 4042 7355 50  0001 L CNN
F 2 "Capacitors_SMD:C_1206" H 3950 7400 50  0001 C CNN
F 3 "~" H 3950 7400 50  0001 C CNN
	1    3950 7400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3550 7300 3750 7300
Wire Wire Line
	3550 7500 3750 7500
Wire Wire Line
	3750 7300 3950 7300
Wire Wire Line
	3750 7500 3950 7500
Text GLabel 1400 7500 3    50   Input ~ 0
ZERO
Text Notes 800  3050 0    50   ~ 0
Page:\nCan't have registered outputs\n that are also combinatorial.\nNeed 7 outs, and external multiplexer.
$Comp
L 74xx:74LS157 U14
U 1 1 5EFCF721
P 1400 1550
F 0 "U14" H 1400 2631 50  0000 C CNN
F 1 "74LS157" H 1400 2540 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-16_3.9x9.9mm_Pitch1.27mm" H 1400 1550 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS157" H 1400 1550 50  0001 C CNN
	1    1400 1550
	-1   0    0    -1  
$EndComp
Text GLabel 900  950  0    50   Input ~ 0
PAGE0
Text GLabel 900  1250 0    50   Input ~ 0
PAGE1
Text GLabel 900  1550 0    50   Input ~ 0
PAGE2
Text GLabel 900  1850 0    50   Input ~ 0
PAGE3
Text GLabel 4100 950  1    50   Input ~ 0
D0
Text GLabel 2150 2150 2    50   Input ~ 0
A14
Text Label 1900 1950 0    50   ~ 0
RAMPAGE3
Text Label 1900 1650 0    50   ~ 0
RAMPAGE2
Text Label 1900 1550 0    50   ~ 0
ROMPAGE2
Text Label 1900 1250 0    50   ~ 0
ROMPAGE1
Text Label 3100 1750 2    50   ~ 0
RAMPAGE0
Text Label 3100 1950 2    50   ~ 0
ROMPAGE0
Text Label 1900 1350 0    50   ~ 0
RAMPAGE1
Text Notes 2050 2400 0    50   ~ 0
Not paged when A13=0
Wire Wire Line
	2150 2150 1900 2150
Text GLabel 1400 2550 0    50   Input ~ 0
GND
Text GLabel 1400 650  0    50   Input ~ 0
+5V
Text Label 1950 2150 0    50   ~ 0
ROMRAM
Wire Wire Line
	2100 2250 1900 2250
Text Label 1900 950  0    50   ~ 0
ROMPAGE0
Text Label 1900 1050 0    50   ~ 0
RAMPAGE0
Wire Wire Line
	1900 1850 2000 1850
Wire Wire Line
	2000 1850 2000 2550
Wire Wire Line
	1400 2550 2000 2550
Wire Wire Line
	3100 1550 1900 1550
Wire Wire Line
	3100 1350 3050 1350
Wire Wire Line
	3050 1350 3050 1250
Wire Wire Line
	3050 1250 1900 1250
Wire Wire Line
	1900 1650 2600 1650
Wire Wire Line
	1900 1050 2700 1050
Wire Wire Line
	2700 1050 2700 1750
Wire Wire Line
	2700 1750 3100 1750
Wire Wire Line
	1900 1950 2500 1950
Wire Wire Line
	3100 1950 2800 1950
Wire Wire Line
	2800 1950 2800 950 
Wire Wire Line
	2900 1350 2900 950 
Wire Wire Line
	2900 950  3500 950 
Wire Wire Line
	1900 1350 2900 1350
Wire Wire Line
	1900 950  2800 950 
Text Notes 4900 2550 0    50   ~ 0
CPLD\nCRU Address decode (saves 688)\nA7 A8 A9 A10 A11 A12 A13 \nif SERENA* also implied A13=0, all \nblock decoders are 1 less bit\nWrite access\nSERENA*\nA0 A1 A2\nWE*\nCRUOUT (input)\n\nRegisters Out\nIO0, IO2, IO3, IO4, IO5,  IO7, IO8\nCombinatorial Out\nIO1\nFree\nIO6 - LED7 or A13\nfor read out, need CRUIN, CRUEN*\nOr external 251 and CRUEN*\n
$Comp
L Gemini:ATF22LV10C-28J U9
U 1 1 5EED1162
P 3900 1550
F 0 "U9" H 3400 2300 50  0000 L CNN
F 1 "ATF22LV10C-28J" H 3650 1550 50  0000 L CNN
F 2 "Sockets:PLCC28" H 3900 1500 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/doc0780.pdf" H 3900 1500 50  0001 C CNN
	1    3900 1550
	-1   0    0    1   
$EndComp
$Comp
L Device:LED D?
U 1 1 5F33BEF4
P 4150 6400
AR Path="/5EB213FB/5F33BEF4" Ref="D?"  Part="1" 
AR Path="/5F33BEF4" Ref="D1"  Part="1" 
F 0 "D1" V 4097 6478 50  0000 L CNN
F 1 "LED" V 4188 6478 50  0000 L CNN
F 2 "LEDs:LED_Rectangular_W5.0mm_H2.0mm" H 4150 6400 50  0001 C CNN
F 3 "~" H 4150 6400 50  0001 C CNN
	1    4150 6400
	0    1    -1   0   
$EndComp
$Comp
L Device:R R1
U 1 1 5F341E69
P 4150 6000
F 0 "R1" H 4220 6046 50  0000 L CNN
F 1 "R" H 4220 5955 50  0000 L CNN
F 2 "Resistors_SMD:R_1206" V 4080 6000 50  0001 C CNN
F 3 "~" H 4150 6000 50  0001 C CNN
	1    4150 6000
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4150 6150 4150 6250
$Comp
L Device:R R2
U 1 1 5F346171
P 3850 6000
F 0 "R2" H 3920 6046 50  0000 L CNN
F 1 "R" H 3920 5955 50  0000 L CNN
F 2 "Resistors_SMD:R_1206" V 3780 6000 50  0001 C CNN
F 3 "~" H 3850 6000 50  0001 C CNN
	1    3850 6000
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3850 6150 3850 6250
Wire Wire Line
	3100 1450 2600 1450
Wire Wire Line
	2600 1450 2600 1650
Wire Wire Line
	3100 1850 2500 1850
Wire Wire Line
	2500 1850 2500 1950
Text Notes 4100 1100 0    50   ~ 0
D0 is CRUOUT
Wire Wire Line
	3950 7300 4150 7300
Wire Wire Line
	1550 7300 1750 7300
Wire Wire Line
	1550 7500 1750 7500
Connection ~ 3950 7300
Wire Wire Line
	4150 7500 3950 7500
Connection ~ 3950 7500
$Comp
L Device:C_Small C2
U 1 1 5F4935ED
P 1750 7400
F 0 "C2" H 1700 7250 50  0000 L CNN
F 1 "0.1uF" H 1842 7355 50  0001 L CNN
F 2 "Capacitors_SMD:C_1206" H 1750 7400 50  0001 C CNN
F 3 "~" H 1750 7400 50  0001 C CNN
	1    1750 7400
	1    0    0    -1  
$EndComp
Connection ~ 1750 7300
Wire Wire Line
	1750 7300 1950 7300
Connection ~ 1750 7500
Wire Wire Line
	1750 7500 1950 7500
Text GLabel 2100 2250 2    50   Input ~ 0
!A13
Text GLabel 1400 7300 1    50   Input ~ 0
ONE
Text GLabel 3600 2350 3    50   Input ~ 0
!A13
Text GLabel 4500 1450 2    50   Input ~ 0
PSEL*
Text GLabel 4500 1950 2    50   Input ~ 0
BST2
Text GLabel 4500 1850 2    50   Input ~ 0
BST1
Text GLabel 4500 1750 2    50   UnSpc ~ 0
MEM*
Text GLabel 3600 950  1    50   Input ~ 0
MEMSEL*
Text GLabel 4500 1350 2    50   Input ~ 0
SEL2100*
Text GLabel 4000 3350 2    50   Input ~ 0
SEL2100*
Text GLabel 4100 2350 3    50   Input ~ 0
A13
Text GLabel 4500 1550 2    50   Input ~ 0
A10
$Comp
L 74xx:74LS138 U8
U 1 1 5F5EAFCA
P 3500 3550
F 0 "U8" H 3500 4331 50  0000 C CNN
F 1 "74LS138" H 3500 4240 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-16_3.9x9.9mm_Pitch1.27mm" H 3500 3550 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS138" H 3500 3550 50  0001 C CNN
	1    3500 3550
	1    0    0    -1  
$EndComp
Text GLabel 3000 3750 0    50   Input ~ 0
A12
Text GLabel 3000 3850 0    50   Input ~ 0
A11
Text GLabel 4000 3450 2    50   Input ~ 0
SEL2200*
NoConn ~ 4000 3250
NoConn ~ 4000 3550
NoConn ~ 4000 3650
NoConn ~ 4000 3750
NoConn ~ 4000 3850
NoConn ~ 4000 3950
Text GLabel 3000 3350 0    50   Input ~ 0
A8
Text GLabel 3000 3450 0    50   Input ~ 0
A9
Text GLabel 3000 3250 0    50   Input ~ 0
A7
Text GLabel 3500 4250 0    50   Input ~ 0
GND
Text GLabel 3500 2950 2    50   Input ~ 0
+5V
$Comp
L 74xx:74LS259 U7
U 1 1 5F604AAE
P 1450 5450
F 0 "U7" H 1450 6331 50  0000 C CNN
F 1 "74LHCT59" H 1450 6240 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-16_3.9x9.9mm_Pitch1.27mm" H 1450 5450 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS259" H 1450 5450 50  0001 C CNN
	1    1450 5450
	1    0    0    -1  
$EndComp
Text GLabel 1450 6150 0    50   Input ~ 0
GND
Text GLabel 1450 4750 2    50   Input ~ 0
+5V
Text GLabel 950  5350 0    50   Input ~ 0
A1
Text GLabel 950  5050 0    50   Input ~ 0
D0
Text GLabel 950  5450 0    50   Input ~ 0
A2
Text GLabel 950  5250 0    50   Input ~ 0
A0
Text GLabel 950  5650 0    50   Input ~ 0
SEL2400*
Text GLabel 950  5850 0    50   Input ~ 0
+5V
$Comp
L Device:LED D?
U 1 1 5F65A8F4
P 3250 6400
AR Path="/5EB213FB/5F65A8F4" Ref="D?"  Part="1" 
AR Path="/5F65A8F4" Ref="D4"  Part="1" 
F 0 "D4" V 3197 6478 50  0000 L CNN
F 1 "LED" V 3288 6478 50  0000 L CNN
F 2 "LEDs:LED_Rectangular_W5.0mm_H2.0mm" H 3250 6400 50  0001 C CNN
F 3 "~" H 3250 6400 50  0001 C CNN
	1    3250 6400
	0    1    -1   0   
$EndComp
$Comp
L Device:LED D?
U 1 1 5F65A908
P 3550 6400
AR Path="/5EB213FB/5F65A908" Ref="D?"  Part="1" 
AR Path="/5F65A908" Ref="D3"  Part="1" 
F 0 "D3" V 3497 6478 50  0000 L CNN
F 1 "LED" V 3588 6478 50  0000 L CNN
F 2 "LEDs:LED_Rectangular_W5.0mm_H2.0mm" H 3550 6400 50  0001 C CNN
F 3 "~" H 3550 6400 50  0001 C CNN
	1    3550 6400
	0    1    -1   0   
$EndComp
$Comp
L Device:R R3
U 1 1 5F65A91C
P 3550 6000
F 0 "R3" H 3620 6046 50  0000 L CNN
F 1 "R" H 3620 5955 50  0000 L CNN
F 2 "Resistors_SMD:R_1206" V 3480 6000 50  0001 C CNN
F 3 "~" H 3550 6000 50  0001 C CNN
	1    3550 6000
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3550 6150 3550 6250
$Comp
L Device:R R4
U 1 1 5F65A927
P 3250 6000
F 0 "R4" H 3320 6046 50  0000 L CNN
F 1 "R" H 3320 5955 50  0000 L CNN
F 2 "Resistors_SMD:R_1206" V 3180 6000 50  0001 C CNN
F 3 "~" H 3250 6000 50  0001 C CNN
	1    3250 6000
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3250 6150 3250 6250
$Comp
L Device:LED D?
U 1 1 5F66E4F2
P 2650 6400
AR Path="/5EB213FB/5F66E4F2" Ref="D?"  Part="1" 
AR Path="/5F66E4F2" Ref="D6"  Part="1" 
F 0 "D6" V 2597 6478 50  0000 L CNN
F 1 "LED" V 2688 6478 50  0000 L CNN
F 2 "LEDs:LED_Rectangular_W5.0mm_H2.0mm" H 2650 6400 50  0001 C CNN
F 3 "~" H 2650 6400 50  0001 C CNN
	1    2650 6400
	0    1    -1   0   
$EndComp
$Comp
L Device:LED D?
U 1 1 5F66E506
P 2950 6400
AR Path="/5EB213FB/5F66E506" Ref="D?"  Part="1" 
AR Path="/5F66E506" Ref="D5"  Part="1" 
F 0 "D5" V 2897 6478 50  0000 L CNN
F 1 "LED" V 2988 6478 50  0000 L CNN
F 2 "LEDs:LED_Rectangular_W5.0mm_H2.0mm" H 2950 6400 50  0001 C CNN
F 3 "~" H 2950 6400 50  0001 C CNN
	1    2950 6400
	0    1    -1   0   
$EndComp
$Comp
L Device:R R5
U 1 1 5F66E51A
P 2950 6000
F 0 "R5" H 3020 6046 50  0000 L CNN
F 1 "R" H 3020 5955 50  0000 L CNN
F 2 "Resistors_SMD:R_1206" V 2880 6000 50  0001 C CNN
F 3 "~" H 2950 6000 50  0001 C CNN
	1    2950 6000
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2950 6150 2950 6250
$Comp
L Device:R R6
U 1 1 5F66E525
P 2650 6000
F 0 "R6" H 2720 6046 50  0000 L CNN
F 1 "R" H 2720 5955 50  0000 L CNN
F 2 "Resistors_SMD:R_1206" V 2580 6000 50  0001 C CNN
F 3 "~" H 2650 6000 50  0001 C CNN
	1    2650 6000
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2650 6150 2650 6250
$Comp
L Device:LED D?
U 1 1 5F66E530
P 2050 6400
AR Path="/5EB213FB/5F66E530" Ref="D?"  Part="1" 
AR Path="/5F66E530" Ref="D8"  Part="1" 
F 0 "D8" V 1997 6478 50  0000 L CNN
F 1 "LED" V 2088 6478 50  0000 L CNN
F 2 "LEDs:LED_Rectangular_W5.0mm_H2.0mm" H 2050 6400 50  0001 C CNN
F 3 "~" H 2050 6400 50  0001 C CNN
	1    2050 6400
	0    1    -1   0   
$EndComp
$Comp
L Device:LED D?
U 1 1 5F66E544
P 2350 6400
AR Path="/5EB213FB/5F66E544" Ref="D?"  Part="1" 
AR Path="/5F66E544" Ref="D7"  Part="1" 
F 0 "D7" V 2297 6478 50  0000 L CNN
F 1 "LED" V 2388 6478 50  0000 L CNN
F 2 "LEDs:LED_Rectangular_W5.0mm_H2.0mm" H 2350 6400 50  0001 C CNN
F 3 "~" H 2350 6400 50  0001 C CNN
	1    2350 6400
	0    1    -1   0   
$EndComp
$Comp
L Device:R R7
U 1 1 5F66E558
P 2350 6000
F 0 "R7" H 2420 6046 50  0000 L CNN
F 1 "R" H 2420 5955 50  0000 L CNN
F 2 "Resistors_SMD:R_1206" V 2280 6000 50  0001 C CNN
F 3 "~" H 2350 6000 50  0001 C CNN
	1    2350 6000
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2350 6150 2350 6250
$Comp
L Device:R R8
U 1 1 5F66E563
P 2050 6000
F 0 "R8" H 2120 6046 50  0000 L CNN
F 1 "R" H 2120 5955 50  0000 L CNN
F 2 "Resistors_SMD:R_1206" V 1980 6000 50  0001 C CNN
F 3 "~" H 2050 6000 50  0001 C CNN
	1    2050 6000
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2050 6150 2050 6250
Wire Wire Line
	4150 6550 4150 6700
Wire Wire Line
	4150 6700 3850 6700
Wire Wire Line
	2050 6700 2050 6550
Wire Wire Line
	2350 6550 2350 6700
Connection ~ 2350 6700
Wire Wire Line
	2350 6700 2050 6700
Wire Wire Line
	2650 6550 2650 6700
Connection ~ 2650 6700
Wire Wire Line
	2650 6700 2350 6700
Wire Wire Line
	2950 6550 2950 6700
Connection ~ 2950 6700
Wire Wire Line
	2950 6700 2650 6700
Wire Wire Line
	3250 6550 3250 6700
Connection ~ 3250 6700
Wire Wire Line
	3250 6700 2950 6700
Wire Wire Line
	3550 6550 3550 6700
Connection ~ 3550 6700
Wire Wire Line
	3550 6700 3250 6700
Wire Wire Line
	3850 6550 3850 6700
Connection ~ 3850 6700
Wire Wire Line
	3850 6700 3550 6700
Text GLabel 2050 6700 0    50   Input ~ 0
GND
Wire Wire Line
	1950 5750 2050 5750
Wire Wire Line
	2050 5750 2050 5850
Wire Wire Line
	2350 5850 2350 5650
Wire Wire Line
	2350 5650 1950 5650
Wire Wire Line
	2650 5850 2650 5550
Wire Wire Line
	2650 5550 1950 5550
Wire Wire Line
	2950 5850 2950 5450
Wire Wire Line
	2950 5450 1950 5450
Wire Wire Line
	3250 5850 3250 5350
Wire Wire Line
	3250 5350 1950 5350
Wire Wire Line
	3550 5850 3550 5250
Wire Wire Line
	3550 5250 1950 5250
Wire Wire Line
	1950 5150 3850 5150
Wire Wire Line
	3850 5150 3850 5850
Wire Wire Line
	4150 5850 4150 5050
Wire Wire Line
	4150 5050 1950 5050
Text Notes 4450 3400 0    50   ~ 0
Banks
Text Notes 4450 3500 0    50   ~ 0
LEDs
$Sheet
S 1350 3250 950  600 
U 5F58D2F7
F0 "Sheet5F58D2F6" 50
F1 "MEMSEL_NOR.sch" 50
$EndSheet
$EndSCHEMATC
