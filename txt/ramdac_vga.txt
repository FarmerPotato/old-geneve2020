
Today's crazy/creative idea for the VDP!

The V9938/58 has a color bus output. It emits 8 bit color codes. The digital output could be used by an FPGA to construct a VGA output. This would be an alternative to analog RGB output.

The color bus is poorly documented. It has a digitize clock output DG, 8 bits of color C0-C7, maybe uses DH/DLCLK for timing. 

If the digitize function is any clue, then the outputs are, by mode:

G4: 256x212  4 bits, C0-C3
G5: 512x212  2 bits, C0-C1, on clock 1, next pixel 2 bits, C2-C3, on clock 0
G6: 512x212  4 bits, C4-7, on clock 1, next pixel 4 bits, C0-C3, on clock 0
G7: 256x212  GGG RRR BB, C0-C7 
YJK: this is a mess: every 4 bytes encode 4 pixels: 4 values for Y=luminance, one JK=average color. Buggy.

I *guess* that these are exactly the VRAM bits. The block diagram indicates this.

Schematic:

FPGA input:
10 bits: HSYNC + CSYNC + color bus
*or VSYNC, extracted from CSYNC by LM1881
2 bits: DLCLK, DHCLK: dot clocks for low, high resolution. 5.37 or 10.74 MHz.
1 bit:  Decode BLEO, even/odd signal. 
1 bit:  CBDR: color bus direction of I/O
==
14 bits

FPGA output: HSYNC, VSYNC. Color bits to external DAC/RAMDAC, RGB to VGA. 

Some issues:

Text40, Text80, G1, G2, G3.  No idea if they are supported (this is a killer)  Experiment: check the color bus output in Text modes vs G4.

Palette is on the V9958. Solution: FPGA must intercept palette register writes to V9958, have default values.

Clock domains: latching pixels at 10.74 MHz DHCLK. Assumes FPGA clock is a multiple of 10.74. Assuming another PLL for the VGA clock of 31.5 MHz, I don't know how to share a data buffer across two clock domains. And I think the second PLL is disabled, so only one clock frequency can be configured. (Open source IceStorm "unlocks" the full 8K of the ICE40HX-4K, but not the other PLL.) 

Interlace - would need whole frame buffer, a giant cost. Otherwise a line buffer would be adequate, if 60 Hz RGB -> to 60 Hz VGA.

Framing: solved by centering 512x424 inside 640x480. 

New features:

G7 could become an indexed color mode.  Color index in G7 is one of 256 palette colors.
The palette could be loaded into a RAMDAC, in higher resolution than 3,3,3.
This is suggested by the V9938 block diagram, to use an external DAC.
Alternative to V9958 YJK (which is broken anyway.)
This would be immediately useful for a PNG or GIF decoder.


Analog output would still be sent over SCART, SVideo, and Composite.

For the present, I'm going to continue to focus on the analog output.






Possible RAMDAC

CMOS Monolithic 256x18 Color Palette RAM-DAC
Three 6-bit or 8-bit Digital-to-Analog Converters
256-Word Color Palette RAM

Plastic 28-pin DIP Package


LD1104-65  , 65MHz , SILICON LOGIC

Bt476KP66  , 66 MHz , Brooktree

ADV476KN66E  , 66 MHz , Analog Devices

TR9C1710-66PCA  , 66 MHz , Music Semiconductors

KDA0476CN-66  , 66 MHz , Samsung Electronics

GS0276AP-65  , 65 MHz , SUNTAC

Synthesize a dual-port RAM in different clock domains

Is it possible to synthesize a dual-port RAM where the ports are in different clock domains? Say, 10.77 MHz writer and 25.175 MHz reader?

I want to make an RGB to VGA converter on BlackIce II. 

The source is a Yamaha V9958 from 1990. In addition to analog RGB, it outputs an 8-bit color bus and dot clock DHCLK at 10.77 MHz. This is for a 15.75 kHz horizontal scan rate. Pixel resolution is max 512x212.  262.5 scan lines at 60 Hz.

The destination is VGA, 640x480 60 Hz. Maybe using an old RAMDAC (because I want 8-bit indexed color.)

I think it can be matched up if source and destination are both 60 frames/second.

I think that I should try to read in a line of 512 pixels. Inputs VSYNC and HSYNC tell me when to start. On the edge of DHCLK, I latch a pixel, 8 parallel inputs C0-C7. 512 pixels are written to a line buffer, and then it is marked ready. The writer can begin a second line buffer after that.

Meanwhile, a "reader" begins to output a VGA frame, signalled by VSYNC. It begins to output a VGA frame, putting black pixels in the visible lines, unless/until it sees the "ready" flag raised. It emits HSYNC at the VGA rate of 31.46875 kHz (why is this not exactly 525*60?)

The reader is going to frame the 512 pixels inside 640 visible pixels, inside a whole scanline of 800 clocks, or 31.777 us.

From http://tinyvga.com/vga-timing/640x480@60Hz

One scanline is:

 96 Sync pulse 
 16 Front porch 
640 Visible area:
    64 black pixels
   512 pixels from the line buffer
    64 black pixels
 48 Back porch

Each scan line is doubled--it uses the line buffer twice. 

In the whole frame, where there are 480 lines of visible area, the reader emits black lines until it sees a Ready flag, maybe around visible line 35 (some part of the 480-2*212 unused lines.) 

When the Ready flag is flipped, the "reader" switches to reading from the other line buffer.

In fact, if the line buffers were filled with black before/after the RGB visible area, an A/B Ready bit would get the job done.

The scheme hinges on being able to synthesize a module that writes a buffer on edges of DHCLK, 10.77 MHz, and a reader that accesses it on edges of its 31.5 MHz VGA clock.

