
/* Output both:
   the key scan code and status valid, makeBreak
   the 4A key 8x8 matrix
   */
   
module keyboard_mapper(
  input      CLOCK_50, 
  output reg valid, makeBreak, extended,
  output reg [7:0] outCode,
  input      [0:2] key_row,  // 4A matrix row selector
  output reg row_bits,       // 4A matrix one row out
  input      PS2_DAT, // PS2 data line
  input      PS2_CLK, // PS2 clock line
  input      reset
);

parameter EXTENDED = 0x80;

// Bit flags
reg [255:0] pressed;


keyboard_press_driver driver(CLOCK_50, valid, makeBreak, extended, outCode, PS2_DAT, PS2_CLK, reset);

always @(posedge valid or posedge reset)
    if (reset) 
        pressed <= 0;
    else
        if (extended)
            pressed[EXTENDED + outCode] = makeBreak;
        else
            pressed[outCode] = makeBreak;

//
// 4A Matrix output
//
// m_ wires take care of mapping multiple PS2 keys to one 4A key. (there's more than one way to press the 4A key)
//
wire [0:7] cru_06_bits = {m_equal,  m_period, m_comma, m_m,    m_n,    m_slash,     j1_fire,  j2_fire};   wire cru_06 = cru_06_bits[key_row];
wire [0:7] cru_08_bits = {m_space,  m_l,      m_k,     m_j,    m_h,    m_semicolon, j1_left,  j2_left};   wire cru_08 = cru_08_bits[key_row];
wire [0:7] cru_0a_bits = {m_enter,  m_o,      m_i,     m_u,    m_y,    m_p,         j1_right, j2_right};  wire cru_0a = cru_0a_bits[key_row];
wire [0:7] cru_0c_bits = {   1'b0,  m_9,      m_8,     m_7,    m_6,    m_0,         j1_down,  j2_down};   wire cru_0c = cru_0c_bits[key_row];
wire [0:7] cru_0e_bits = { m_fctn,  m_2,      m_3,     m_4,    m_5,    m_1,         j1_up,    j2_up};     wire cru_0e = cru_0e_bits[key_row];
wire [0:7] cru_10_bits = {m_shift,  m_s,      m_d,     m_f,    m_g,    m_a,         1'b0,     1'b0};      wire cru_10 = cru_10_bits[key_row];
wire [0:7] cru_12_bits = { m_ctrl,  m_w,      m_e,     m_r,    m_t,    m_q,         1'b0,     1'b0};      wire cru_12 = cru_12_bits[key_row];
wire [0:7] cru_14_bits = {   1'b0,  m_x,      m_c,     m_v,    m_b,    m_z,         1'b0,     1'b0};      wire cru_14 = cru_14_bits[key_row];

row_bits = { cru_06, cru_08, cru_0a, cru_0c, cru_0e, cru_10, cru_12, cru_14 };

// combinations of a real or implied keypress:


// Keys that are obtained with shift on PS2 but that are Fctn keys on 4A
wire m_tilde       = k_shift & k_backquote;
wire m_left_brace  = k_shift & k_left_bracket;
wire m_right_brace = k_shift & k_right_bracket;
wire m_underscore  = k_shift & k_minus);      // Later, m_underscore will imply FCTN-U
wire m_capslock = k_caps_lock;

// 4A keys by row
// * Plain characters with no FCTN keycaps
// * Several keys are also fctn keypresses on 4A keyboard--these imply m_fctn too

wire m_1 = k_1 | m_f1;
wire m_2 = k_2 | m_f2;
wire m_3 = k_3 | m_f3;
wire m_4 = k_4 | m_f4;
wire m_5 = k_5 | m_f5;
wire m_6 = k_6 | m_f6;
wire m_7 = k_7 | m_f7 | k_tab;
wire m_8 = k_8 | m_f8 | k_keypad_asterisk;
wire m_9 = k_9 | m_f9;
wire m_0 = k_0;
wire m_equal = k_equal | k_keypad_plus;

wire m_q = k_q;
wire m_w = k_w | m_tilde;
wire m_e = k_e | m_cursor_up;                 // m_cursor_up implies both FCTN and E
wire m_r = k_r | k_left_bracket;
wire m_t = k_t | k_right_bracket;
wire m_y = k_y;
wire m_u = k_u | m_underscore;
wire m_i = k_i | m_question;
wire m_o = k_o | k_singlequote;
wire m_p = k_p | m_doublequote;
wire m_slash = k_slash  | k_keypad_slash;

wire m_a = k_a | m_separator;
wire m_s = k_s | k_cursor_left | k_backspace;
wire m_d = k_d | k_cursor_right;
wire m_f = k_f | m_left_brace;
wire m_g = k_g | m_right_brace;
wire m_h = k_h;
wire m_j = k_j;
wire m_k = k_k;
wire m_l = k_l;
wire m_semicolon = k_semicolon;
wire m_enter = k_enter | k_return;

wire m_z = k_z | k_backslash;
wire m_x = k_x | k_cursor_down;
wire m_c = k_c | k_backquote;
wire m_v = k_v;
wire m_b = k_b;
wire m_n = k_n;
wire m_m = k_m;
wire m_comma = k_comma;
wire m_period = k_period;

wire m_space = k_space;



// real and implied fctn key
wire m_fctn = k_left_alt | k_right_alt
            | m_f1 | m_f2 | m_f3 | m_f4 | m_f5 | m_f6 | m_f7 | m_f8 | m_f9 | m_f10 
            | k_tilde | k_left_bracket | k_right_bracket | m_underscore | k_question | k_single_quote | k_double_quote 
            | k_right_brace | k_left_brace | k_separator | k_backslash 
            | m_cursor_left | m_cursor_right | m_cursor_up | m_cursor_down // keypad and arrows combined
            | k_backquote | k_backspace
;
// not used: F11 F12


// real and implied shift keys
wire m_shift = k_left_shift    | k_right_shift;
             | k_keypad_asterisk // shift 8
             | k_keypad_plus // shift =
;

wire m_ctrl  = k_left_control | k_right_control;


// Keypad / * - +
// NumLock modified keys
// keypad period is DEL which will go into fctn-1

m_cursor_up    = k_cursor_up    | (!k_number_lock & k_keypad_8);
m_cursor_down  = k_cursor_down  | (!k_number_lock & k_keypad_2);
m_cursor_left  = k_cursor_left  | (!k_number_lock & k_keypad_4);
m_cursor_right = k_cursor_right | (!k_number_lock & k_keypad_6);
m_page_up      = k_page_up      | (!k_number_lock & k_keypad_9);
m_page_down    = k_page_down    | (!k_number_lock & k_keypad_3);
m_insert       = k_insert       | (!k_number_lock & k_keypad_0);
m_delete       = k_delete       | (!k_number_lock & k_keypad_period);
m_home         = k_home         | (!k_number_lock & k_keypad_7);
m_end          = k_end          | (!k_number_lock & k_keypad_1);

// any good ideas for keys that should map to these?
m_erase = 0;
m_break = 0;
m_redo = m_home;
m_back = m_end;

// Mapped to function keys
m_f1 = k_f1 | m_delete;
m_f2 = k_f2 | m_insert;
m_f3 = k_f3 | m_erase;
m_f4 = k_f4 | m_break | m_page_up;  // this makes page_up break out of a basic program!
m_f5 = k_f5 | m_begin;
m_f6 = k_f6 | m_page_down;
m_f7 = k_f7 | m_tab;
m_f8 = k_f8 | m_redo;
m_f9 = k_f9 | m_back;
m_f0 = k_f10 | m_;


// Names of actual scan codes
//
// References:
// https://wiki.osdev.org/PS/2_Keyboard
// https://techdocs.altium.com/display/FPGA/PS2+Keyboard+Scan+Codes

wire k_f9  	= pressed[0x01];
wire k_f5  	= pressed[0x03];
wire k_f3  	= pressed[0x04];
wire k_f1  	= pressed[0x05];
wire k_f2  	= pressed[0x06];
wire k_f12  = pressed[0x07];
wire k_f10  = pressed[0x09];
wire k_f8  	= pressed[0x0a];
wire k_f6  	= pressed[0x0b];
wire k_f4  	= pressed[0x0c];
wire k_tab  = pressed[0x0d];
wire k_backtick = pressed[0x0e];
wire k_left_alt  = pressed[0x11];
wire k_left_shift = pressed[0x12];
wire k_left_control = pressed[0x14];
wire k_q  	= pressed[0x15];
wire k_1  	= pressed[0x16];
wire k_z  	= pressed[0x1a];
wire k_s  	= pressed[0x1b];
wire k_a  	= pressed[0x1c];
wire k_w  	= pressed[0x1d];
wire k_2  	= pressed[0x1e];
wire k_c  	= pressed[0x21];
wire k_x  	= pressed[0x22];
wire k_d  	= pressed[0x23];
wire k_e  	= pressed[0x24];
wire k_4  	= pressed[0x25];
wire k_3  	= pressed[0x26];
wire k_space = pressed[0x29];
wire k_v  	= pressed[0x2a];
wire k_f  	= pressed[0x2b];
wire k_t  	= pressed[0x2c];
wire k_r  	= pressed[0x2d];
wire k_5  	= pressed[0x2e];
wire k_n  	= pressed[0x31];
wire k_b  	= pressed[0x32];
wire k_h  	= pressed[0x33];
wire k_g  	= pressed[0x34];
wire k_y  	= pressed[0x35];
wire k_6  	= pressed[0x36];
wire k_m  	= pressed[0x3a];
wire k_j  	= pressed[0x3b];
wire k_u  	= pressed[0x3c];
wire k_7  	= pressed[0x3d];
wire k_8  	= pressed[0x3e];
wire k_comma = pressed[0x41];
wire k_k  	= pressed[0x42];
wire k_i  	= pressed[0x43];
wire k_o  	= pressed[0x44];
wire k_0  	= pressed[0x45];
wire k_9  	= pressed[0x46];
wire k_period = pressed[0x49];
wire k_slash = pressed[0x4a];
wire k_l  	= pressed[0x4b];
wire k_semicolon = pressed[0x4c];
wire k_p  	= pressed[0x4d];
wire k_minus         = pressed[0x4e];
wire k_single_quote 	= pressed[0x52];
wire k_left_bracket 	= pressed[0x54];
wire k_equals        = pressed[0x55];
wire k_caps_lock  	= pressed[0x58];
wire k_right_shift  	= pressed[0x59];
wire k_enter         = pressed[0x5a];
wire k_right_bracket = pressed[0x5b];
wire k_backslash  	= pressed[0x5d];
wire k_backspace  	= pressed[0x66];

wire k_keypad_1  	= pressed[0x69];
wire k_keypad_4  	= pressed[0x6b];
wire k_keypad_7  	= pressed[0x6c];
wire k_keypad_0  	= pressed[0x70];
wire k_keypad_period = pressed[0x71];
wire k_keypad_2  	= pressed[0x72];
wire k_keypad_5  	= pressed[0x73];
wire k_keypad_6  	= pressed[0x74];
wire k_keypad_8  	= pressed[0x75];

wire k_escape  	    = pressed[0x76];
wire k_number_lock  	= pressed[0x77];
wire k_f11  	        = pressed[0x78];
wire k_keypad_plus 	= pressed[0x79];
wire k_keypad_3  	= pressed[0x7a];
wire k_keypad_minus 	= pressed[0x7b];
wire k_keypad_asterisk 	= pressed[0x7c];
wire k_keypad_9  	= pressed[0x7d];
wire k_scroll_lock  	= pressed[0x7e];
wire k_f7  	        = pressed[0x83];

// Extended keys - put them beyond largest value 0x83 by adding EXTENDED = 0x80

wire k_right_alt = pressed[EXTENDED + 0x11];
wire k_right_control = pressed[EXTENDED + 0x14];
wire k_end = pressed[EXTENDED + 0x69];
wire k_cursor_left = pressed[EXTENDED + 0x6B];
wire k_home = pressed[EXTENDED + 0x6C];	
wire k_insert = pressed[EXTENDED + 0x70];
wire k_delete = pressed[EXTENDED + 0x71];
wire k_cursor_down = pressed[EXTENDED + 0x72];
wire k_cursor_right = pressed[EXTENDED + 0x74];
wire k_cursor_up = pressed[EXTENDED + 0x75];
wire k_page_down = pressed[EXTENDED + 0x7A];
wire k_page_up = pressed[EXTENDED + 0x7D];


endmodule 
