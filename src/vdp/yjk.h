// Theory: for human perception it is better to preserve luminance than hue.
// YJK subtracts the average luminance from a line of 4 pixels and encodes
// the color differences in JK axes.  This almost gives 15-bit color.
// Because of a design error, the 9958 YJK format gets luminance wrong.
// The green and blue components were swapped.
// Really Blue contributes least to luminance.

// All values are float [0.0, 1.0]
// 
struct rgb { float r,g,b; };
struct yjk { float y,j,k; };
typedef struct rgb rgb4[4];
typedef struct yjk yjk4[4];  // y must be common


float true_y(const rgb& p) {
  return 0.587*p.g + 0.114*p.b;
}

float msx_y(const rgb& p) {
  return p.b/2 + p.r/4 + p.g/8;
}

// Y given already computed J and K - this only has to be a guess (initial starting point)
float msx_y(const rgb& p, float j, float k) {
//  return p.b/2 + p.r/4 + p.g/8;
  // two answers implied
  float y1 = p.r - j;
  float y2 = p.g - j;
  return (y1+y2)/2;
}

float msx_j(const rgb& p, float y) {
  return p.r - y;
}

float msx_k(const rgb& p, float y) {
  return p.g - y;
}

// The MSX formulas
yjk rgb_to_yjk(const rgb& p) {
  yjk p2;
  p2.y = p.b/2 + p.r/4 + p.g/8;
  p2.j = p.r - p2.y;
  p2.k = p.g - p2.y;
  return p2;
}


rgb yjk_to_rgb(const yjk& p) {
  rgb p2;
  p2.r = p.y + p.j; 
  p2.g = p.y + p.k;
  p2.b = p.y * 1.25 - p.j/2 - p.k/4;
  return p2;
}

// Average of 4 rgb pixels
rgb average_rgb(const rgb* rgbs)
{
  // average rgb color
  rgb avg;
  avg.r = 0;
  avg.g = 0;
  avg.b = 0;
  for (int i=0; i<4; ++i) {
    avg.r += rgbs[i].r/4;
    avg.g += rgbs[i].g/4;
    avg.b += rgbs[i].b/4;
  }
  return avg;
}
