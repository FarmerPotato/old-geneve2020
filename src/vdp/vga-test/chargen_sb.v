module chargen(
  input wire clk,
  input wire reset,
	input wire [7:0] char_num,  // ascii 0-255 (valid defs are 32-95)
	input wire [2:0] char_y,  // row in 8x8 char
	input wire [2:0] char_x,  // column in 8x8 char
	output reg pixel_out          // pixel out, 1 or 0
);

  wire [8:0] addr;
	wire [7:0] bits;
  wire       pixel_out_next;

  // fold to 0-63
  assign addr = {~char_num[5], char_num[4:0], char_y[2:0]};
  
	char_rom_sb rom(
		.clk(clk), 
		.reset(reset), 
		.addr(addr), 
		.dout(bits)
	);

  assign pixel_out_next = bits[char_x];
	
	// addr is latched(?) into BRAM on posedge clk, data is valid on negedge clk
	always @(negedge clk)
	  pixel_out <= pixel_out_next;
	
endmodule

/*
   I never got this to make sense. How are the bits laid out?
   
   A block ram acting as 512x8 rom. 
   It can hold 64 8x8 pixel chars.
   Initialized with a font for ascii 32-95.
   If you want lowercase, it will take another block ram.
*/
module char_rom_sb(
  input wire clk,
  input wire reset,
	input wire [8:0] addr, 
	output wire [7:0] dout
);
  wire [15:0] rd;

	// mode 1, data width 8, uses bits 14, 12, 10, 8, 6, 4, 2, 0
	// where was this documented again?

  // indexes are reversed, so that char bits can be adressed by view_x[2:0]
  // where bits[0] should be the leftmost pixel.
//  assign dout = { rd[0], rd[2], rd[4], rd[6], rd[8], rd[10], rd[12], rd[14] };
  assign dout = { rd[1], rd[3], rd[5], rd[7], rd[9], rd[11], rd[13], rd[15] };

//  assign  dout = rd[15:8];
  
  SB_RAM40_4K #(
    .WRITE_MODE(1),  // 512x8
    .READ_MODE(1),   // 512x8
//    .WRITE_MODE(0),  // 256x16
//    .READ_MODE(0),   // 256x16
    .INIT_0(256'h00000000000000000010101010001000002424000000000000247E24247E2400),
    .INIT_1(256'h00083E283E0A3E080062640810264600001028102A443A000008100000000000),
    .INIT_2(256'h00040808080804000020101010102000000014083E081400000008083E080800),
    .INIT_3(256'h0000000000080810000000003E00000000000000001818000000020408102000),
    .INIT_4(256'h003C464A52623C000018280808083E00003C42023C407E00003C420C02423C00),
    .INIT_5(256'h00081828487E0800007E407C02423C00003C407C42423C00007E020408101000),
    .INIT_6(256'h003C423C42423C00003C42423E023C0000000010000010000000100000101020),
    .INIT_7(256'h00000408100804000000003E003E00000000100804081000003C420408000800),
    .INIT_8(256'h003C4A565E403C00003C42427E424200007C427C42427C00003C424040423C00),
    .INIT_9(256'h0078444242447800007E407C40407E00007E407C40404000003C42404E423C00),
    .INIT_A(256'h0042427E42424200003E080808083E000002020242423C000044487048444200),
    .INIT_B(256'h0040404040407E000042665A42424200004262524A464200003C424242423C00),
    .INIT_C(256'h007C42427C404000003C4242524A3C00007C42427C444200003C403C02423C00),
    .INIT_D(256'h00FE1010101010000042424242423E00004242424224180000424242425A2400),
    .INIT_E(256'h00422418182442000082442810101000007E040810207E00000E080808080E00),
    .INIT_F(256'h0000402010080400007010101010700000103854101010000000000000000000),

  ) rom_inst (
    .RDATA(rd),
    //.RADDR({2'b0, addr}),
    .RADDR({2'b0, addr[8:0]}),
    .RCLK(clk & ~reset), .RCLKE(1'b1), .RE(1'b1),
    .WCLK(1'b0), .WCLKE(1'b0), .WE(1'b0),
    .WADDR(0),
    .MASK(9'h00), .WDATA(8'h00));


endmodule

