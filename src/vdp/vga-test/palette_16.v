module palette_16
	(
	//input reset,
	input wire [3:0]  index,
	output reg [11:0] rgb
	);
	
	always @* begin
	case(index)
		0: rgb =  12'h000; // transparent
		1: rgb =  12'h000; // black
		2: rgb =  12'h2c3; // medium green
		3: rgb =  12'h5d6; // light green
		4: rgb =  12'h54f; // dark blue
		5: rgb =  12'h76f; // light blue
		6: rgb =  12'hd54; // dark red
		7: rgb =  12'h4ef; // cyan 
		8: rgb =  12'hf54; // medium red
		9: rgb =  12'hf76; // light red
		10: rgb =  12'hdc3; // dark yellow
		11: rgb =  12'hed6; // light yellow
		12: rgb =  12'h2b2; // dark green
		13: rgb =  12'hc5c; // magenta
		14: rgb =  12'hccc; // gray
		15: rgb =  12'hfff; // white 
  endcase
  end
  
	
endmodule
