module debounce_tb;

  reg clk = 1; // rising edge is on 10
  reg reset = 0;

	reg start_value = 1;
  reg d = 0;
  wire q;
  wire [3:0] buffer;
  
  reg [7:0] tests_failed = 0;
  
  // input and output sequences
  reg [0:19] ds = 20'b00001111000011110000;
  reg [0:19] qs = 20'b11110000111100001111;
  
	reg [7:0] i;
    
	debounce uut(clk, reset, start_value, d, q, buffer);
	
	event terminate_sim;
  
  // regular clock
  always #5    clk = !clk;

  // test
  initial begin
    d = start_value;
    reset = 1;
    #10 reset = 0;
		for (i = 0; i < 20; i = i + 1) begin
				d = ds[i];
				if (q != qs[i])
					tests_failed = tests_failed + 1; // hmm, this shows up on the next $display
				#10 begin end
		end
  
    #0 -> terminate_sim;
  end
  
  // Progress message
	initial
	  $monitor("At time %t, d = %d, q = %d, buffer = %4b, fails = %d", $time, d, q, buffer, tests_failed); 
  
  // Termination Message  
  initial
  @ (terminate_sim)  begin
   $display ("Terminating simulation");
   if (tests_failed == 0) begin
     $display ("Simulation Result : PASSED");
   end
   else begin
     $display ("Simulation Result : FAILED");
   end
   $finish;
  end
  
						
endmodule
