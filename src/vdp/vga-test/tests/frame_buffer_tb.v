module frame_buffer_tb;

  reg clk = 1; // rising edge is on 10
  reg reset = 0;

	reg start_value = 1;
  reg d = 0;
  wire q;
  wire [3:0] buffer;
  
  reg [7:0] tests_failed = 0;
  
  localparam BIT_DEPTH = 4;
  
  // read from buffer (these are screen, not vga, coordinates)
  reg [7:0] read_pixel_x, read_pixel_y;
	wire [BIT_DEPTH-1:0] read_index;
	reg read;

  // write to buffer (these are screen, not vga, coordinates)
	reg [7:0] write_pixel_x, write_pixel_y;
	reg [BIT_DEPTH-1:0] write_index;
	reg write;

	frame_buffer uut(
		.clk(clk), 
		.reset(reset), 
		.read_pixel_x(read_pixel_x),
		.read_pixel_y(read_pixel_y),
		.read_index(read_index),
		.read(read),
		.write_pixel_x(write_pixel_x),
		.write_pixel_y(write_pixel_y),
		.write_index(write_index),
		.write(write)
	);

	event terminate_sim;

  // regular clock
  always #5    clk = !clk;

	wire [7:0] read_pixel_x_next, read_pixel_y_next;
	wire [7:0] write_pixel_x_next, write_pixel_y_next;
  reg [15:0] i;
  
  assign read_pixel_x_next  = read_pixel_x + 1;
  assign read_pixel_y_next  = read_pixel_y + 1;
  
  assign write_pixel_x_next = write_pixel_x + 1;
  assign write_pixel_y_next = write_pixel_y + 1;
  
	always @(posedge reset, posedge clk)
	begin
		if (reset)
			begin
				read_pixel_y  = 0;
				read_pixel_x  = 0;
				write_pixel_y = 0;
				write_pixel_x = 1;
				
				read  = 1'b0;
				write = 1'b0;
				write_index = 1;
			end
		else
			begin

//				read_pixel_y  <= read_pixel_y_next;
//				read_pixel_x  <= read_pixel_x_next;
//				write_pixel_y <= write_pixel_y_next;
//				write_pixel_x <= write_pixel_x_next;
			end
	end
		
  // Test
  // this was tested with a bad value, works correctly.
  localparam cnt = 256;
  localparam mask = 15;
  
  initial begin
    d = start_value;
    #10 reset = 1;
    #10 reset = 0;
		for (i = 0; i < cnt; i = i + 1) begin
			write_pixel_x = i;
			write_index = i;
			#10 write = 1'b1;
			#10 write = 1'b0;
		end

		for (i = 0; i < cnt; i = i + 1) begin
			read_pixel_x = i;
			#10 read = 1'b1;
		  #10 read = 1'b0;
			if (read_index != (i & mask))
				tests_failed = tests_failed + 1;
		end
  
    #0 -> terminate_sim;
  end

  // Progress message
	initial
	  $monitor("At time %t, reset = %d, we=%d write[%d] = %x, rd=%d read[%d] = %x, fails = %d",
	  	$time, 
	  	reset,
	  	write,
	  	write_pixel_x,
	  	write_index, 
	  	read,
	  	read_pixel_x,
	  	read_index, 
	  	tests_failed
	  ); 
  
  // Termination Message  
  initial
  @ (terminate_sim)  begin
   $display ("Terminating simulation");
   if (tests_failed == 0) begin
     $display ("Simulation Result : PASSED");
   end
   else begin
     $display ("Simulation Result : FAILED");
   end
   $finish;
  end

endmodule
