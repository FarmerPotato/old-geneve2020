// The frame buffer, with read and write ports.
// Test 1. BRAM. Use  16 x 1024x4. They store 4 pixel rows each, so 64 rows.
//
// Add a text overlay, for debugging output.
//
// Test 2. SDRAM on BlackIce.  Display image from SDRAM.
//
// Test 3. Test multiplexing to SDRAM. In blank interval, take writes from FIFO. 
// Code FIFO and fifo_tb.
//
// Test 4. Implement read from colorbus, store in FIFO.
//
// Test 5. PCB rev2. Use VRAM chips. Connect to 478 palette.
//   In VRAM, use 1 byte per pixel, clock either 256 (half line) or 512.
//   So in 256x192, 48K expands to 96K. That's OK because we have 2x 256kx4 chips.
//

module frame_buffer#(
		parameter BIT_DEPTH = 4,
		parameter SCREEN_WIDTH = 256,
		parameter SCREEN_HEIGHT = 192
	) (
		input wire clk,
		input wire reset,

		// read from buffer (these are screen, not vga, coordinates)
		input wire [7:0] read_pixel_x, read_pixel_y,
		output reg [BIT_DEPTH-1:0] read_index, 
		input wire read,

		// write to buffer (these are screen, not vga, coordinates)
		input wire [7:0] write_pixel_x, write_pixel_y,
		input wire [BIT_DEPTH-1:0] write_index,
		input wire write

	);

	// The frame buffer. Full frame would be 24K (256x192).
	// BRAMs, 32 of them, come in 1024x4 or 4 pixels rows each.
	// Go for 24 x 4 = 96 pixels, and double the image.
  // But first try 1 BRAM.
  //
	reg [BIT_DEPTH-1:0] frame[1023:0]; // enough for 4 lines

	// asynchronous. latch on rising edges.
  always @(posedge read)
			if (read)
				read_index = frame[ {read_pixel_y[1:0], read_pixel_x[7:0] } ];

  always @(posedge write)
			if (write)
				frame[ {write_pixel_y[1:0], write_pixel_x[7:0] } ] = write_index;
			
endmodule
