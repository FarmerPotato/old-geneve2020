module color_bars
	(
	input wire [7:0] view_x,
	output wire [3:0] color_index
	);
	
	assign color_index = view_x[7:4];
	
endmodule
