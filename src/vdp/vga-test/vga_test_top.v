module vga_test_top
	(
	input wire CLK_OSC100, greset,
  output wire [55:4] PMOD,
//  output wire [55:32] PMOD,


  //
  // unused in this design
  //
  
	// Input lines from STM32/Done can be used to signal to Ice40 logic
	input DONE, // could be used as interupt in post programming
	input DBG1, // Could be used to select coms via STM32 or RPi etc..
  // SRAM Memory lines
	output [17:0] ADR,
	output [15:0] DAT,
	output RAMOE,
	output RAMWE,
	output RAMCS,
	output RAMLB,
	output RAMUB,
	// All PMOD outputs
	output PMOD0,
	output PMOD1,
	input  UART_RX,
	output UART_TX,
//	output [55:4] PMOD,
	input B1,
	input B2, 
	// QUAD SPI pins
	input QSPICSN,
	input QSPICK,
	output [3:0] QSPIDQ
  );

  localparam MHZ = 50.4; // double the VGA clock

  wire clk_2vga;

/**
 * PLL configuration
 *
 * This Verilog header file was generated automatically
 * using the icepll tool from the IceStorm project.
 * It is intended for use with FPGA primitives SB_PLL40_CORE,
 * SB_PLL40_PAD, SB_PLL40_2_PAD, SB_PLL40_2F_CORE or SB_PLL40_2F_PAD.
 * Use at your own risk.
 *
 * Given input frequency:       100.000 MHz
 * Requested output frequency:   50.400 MHz
 * Achieved output frequency:    50.625 MHz
 */


  SB_PLL40_CORE #(.FEEDBACK_PATH("SIMPLE"),
                  .PLLOUT_SELECT("GENCLK"),
									.DIVR(4'b1001),         // DIVR =  9
									.DIVF(7'b1010000),      // DIVF = 80
									.DIVQ(3'b100),          // DIVQ =  4
									.FILTER_RANGE(3'b001)   // FILTER_RANGE = 1
                 ) uut (
                         .REFERENCECLK(CLK_OSC100),
                         .PLLOUTCORE(clk_2vga),
                         //.PLLOUTGLOBAL(clk),
                         // .LOCK(D5),
                         .RESETB(1'b1),
                         .BYPASS(1'b0)
                      );


	// VGA PMOD on PMOD9/10 and 11/12
	// 12 11
	// =====
	// b0 r0    44 40
	// b1 r1    45 41
	// b2 r2    46 42
	// b3 r3    47 43
	// gnd gnd
	// 3v3 3v3

	// 10  9
	// hs g0    36 32 
	// vs g1    37 33
	// nc g2    38 34
	// nc g3    39 35
	// gnd gnd
	// 3v3 3v3



	wire [11:0] rgb;
	wire hs, vs;


  // green
	assign PMOD[32] = rgb[4];
	assign PMOD[33] = rgb[5];
	assign PMOD[34] = rgb[6];
	assign PMOD[35] = rgb[7];
  
  // sync	
	assign PMOD[36] = hs;
	assign PMOD[37] = vs;
	// no connection on vga pmod
	assign PMOD[38] 	= 1'b1;
	assign PMOD[39] 	= 1'b1;

  // red
	assign PMOD[40] = rgb[8];
	assign PMOD[41] = rgb[9];
	assign PMOD[42] = rgb[10];
	assign PMOD[43] = rgb[11];

  // blue
	assign PMOD[44] = rgb[0];
	assign PMOD[45] = rgb[1];
	assign PMOD[46] = rgb[2];
	assign PMOD[47] = rgb[3];

	
 	// Necessary workaround to allow BRAM to be ready
	wire delayed_reset;
	reset_delay resetclk(
	  .clk(CLK_OSC100), 
	  .reset_in(greset),
	  .reset_out(delayed_reset)
	);
	
	vga_test test_unit
	(
		.clk(clk_2vga),
		.reset(delayed_reset),
		.reset(greset),
		.hsync(hs), 
		.vsync(vs), 
		.rgb(rgb)
	);
	
  // leds:
	// 55 red 54 yellow 53 green 52 blue

  wire green_led;
  assign PMOD[53] = green_led;
  
	blink heartbeat(.clk(clk_2vga), .reset(greset), .led(green_led));


	// Set unused pins to default
	assign QSPIDQ[3:0] = {4{1'b1}};
			
	assign PMOD[55:54] 	= {2{1'b1}};
	assign PMOD[52:48] 	= {5{1'b1}};
  assign PMOD[31:4]   = {28{1'b1}};
  assign PMOD1 		= 1'bz;  // 0 ok, was bz; shared with CHM340 RTS
	assign PMOD0 		= 1'bz;  // 0 ok, was bz; shared with CHM340 CTS
	assign UART_TX = 1'b0;   // shared with PMOD[2]. UART_RX is shared with PMOD[3].
	// "Warning: Yosys has only limited support for tri-state logic at the moment"
	
	// SRAM signals are not use in this design, lets set them to default values
	assign ADR[17:0] = {18{1'b1}};
	assign DAT[15:0] = {16{1'b1}};
	assign RAMOE = 1'b1;
	assign RAMWE = 1'b1;
	assign RAMCS = 1'b1;
	assign RAMLB = 1'b1;
	assign RAMUB = 1'b1;
	
	
endmodule
