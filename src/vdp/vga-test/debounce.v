// debounce the signal d through 4 clks
// the rise time of external TTL is 20ns
// expected cycle time at 10 MHz = 100ns, 5 MHz 200ns
// so peak time might be 30 ns? 60 ns?

module debounce #(parameter NUM_CLKS = 4)
	(
		input wire clk, reset, start_value,
		input wire d,
		output reg q,
		output reg [NUM_CLKS-1:0] buffer
	);

  localparam [NUM_CLKS-1:0] ones = ~0;
  localparam [NUM_CLKS-1:0] zeroes = 0;
  
  wire [NUM_CLKS-1:0] buffer_next;
  
  assign buffer_next = { buffer[NUM_CLKS-2:0], d }; // shift left
  assign clr = (buffer_next == zeroes);
  assign set = (buffer_next == ones );

  always @(posedge clk, posedge reset)
		if (reset)
			begin
				buffer <= start_value ? ~0 : 0;
				q      <= start_value;
			end
		else
			begin
				buffer <= buffer_next;
				if (set)
					q <= 1'b1;
				else if (clr)
				  q <= 1'b0;
				// else unch
			end
			
endmodule

