// Implement graphics mode 1: 24x32 screen image table.
// But only chars 32-95 are defined in chargen.v.
// Use transparent background.

module screen#(
	parameter BIT_DEPTH = 4,
	parameter SCREEN_WIDTH = 256,
	parameter SCREEN_HEIGHT = 192,
	parameter RGB_DEPTH = 12  // for output
) (
  input wire clk,
  input wire reset,
  input wire pixel_tick,
	input wire [7:0] pixel_x, pixel_y,
	input wire [BIT_DEPTH-1:0] screen,
	output reg [RGB_DEPTH-1:0] rgb
	);

	// Screen image table
	
  // index means a color code, 0-15 (4 bits)
  wire [BIT_DEPTH-1:0] char_index, color_index, pixel_index;
  wire [7:0] char_num;
  reg  [RGB_DEPTH-1:0] rgb_next;

  wire char_pixel;

  // Repeating charset generator
	assign char_num = {2'b00, view_y[3], view_x[7:3]} - 4;
	assign char_index = bar_index + 2; // color combo
	assign pixel_index = char_pixel ? char_index : bar_index;

	// char_pixel is 1 or 0 for a given char_num and y,x
	chargen chars(
	  .clk(clk),   // pixel_tick too late? this works ok (or do we need clk?)
	  .reset(reset),
		.char_num(char_num), 
		.char_y(view_y[2:0]), 
		.char_x(view_x[2:0]), 
		.pixel_out(char_pixel)
	);


  assign color_index = outer_box ? border2 : (inner_box ? border1 : pixel_index);

	palette_16 palette(color_index, rgb_next);

  // changes happen on pixel_tick=1	
 	always @(negedge pixel_tick)
 	begin  
   		rgb <= rgb_next; //if(~pixel_tick) begin
	end
	
endmodule
