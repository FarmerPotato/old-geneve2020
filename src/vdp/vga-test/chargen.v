module chargen(
  input wire clk,
  input wire reset,
	input wire [7:0] char_num,  // ascii 0-255 (valid defs are 32-95)
	input wire [2:0] char_y,  // row in 8x8 char
	input wire [2:0] char_x,  // column in 8x8 char
	output reg pixel_out          // pixel out, 1 or 0
);

  wire [8:0] addr;
	wire [7:0] bits;
  wire       pixel_out_next;

  // fold to 0-63
  assign addr = {~char_num[5], char_num[4:0], char_y[2:0]};
  
	char_rom rom(
		.clk(clk), 
		.reset(reset), 
		.addr(addr), 
		.dout(bits)
	);

  assign pixel_out_next = bits[7-char_x];
	
	// addr is latched(?) into BRAM on posedge clk, data is valid on negedge clk
	always @(negedge clk)
	  pixel_out <= pixel_out_next;
	
endmodule


/* let yosys infer a bram */
module char_rom(
  input wire clk,
  input wire reset,
	input wire [8:0] addr, 
	output wire [7:0] dout
);

  reg [7:0] mem[511:0];

  initial $readmemh("chara1.hex", mem);
  
  always @(posedge clk)
  	if (!reset)
  		dout <= mem[addr];

endmodule
