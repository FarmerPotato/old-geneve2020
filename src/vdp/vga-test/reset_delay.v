module reset_delay(
  input wire  clk,
  input wire  reset_in, 
  output reg  reset_out
);

  // delay to let SRAM ready before first access to rom (or we get all 1s)
  // https://github.com/cliffordwolf/icestorm/issues/76#issuecomment-289270411

	reg [7:0] reset_count;

	always @(posedge clk) begin
		if (reset_in) begin
		  reset_count <= 0;
		  reset_out <= 1;
		end else 
		  reset_count <= reset_count + 1;

		if (reset_count == 36) // <-- set this to 36 to work around the issue
			reset_out <= 0;
	end
	// how did this ever work? reset_out is active low?
endmodule
