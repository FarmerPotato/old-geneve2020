module graphic #(
	parameter BIT_DEPTH = 4,
	parameter SCREEN_WIDTH = 256,
	parameter RGB_DEPTH = 12
) (
  input wire clk,
  input wire reset,
  input wire pixel_tick,
	input wire [9:0] pixel_x, pixel_y,
	input wire [BIT_DEPTH-1:0] border1,
	input wire [BIT_DEPTH-1:0] border2,
	input wire [BIT_DEPTH-1:0] screen,
	output reg [RGB_DEPTH-1:0] rgb
	);
	  
	// put something interesting inside the 640x480 box
	// frame 512x384: 64 on each side, 48 on top/bottom
	// yellow frame: 32 on each side , 24 on top/bottom
	// gray   frame: 32 on each side , 24 on top/bottom
	
	// for some reason, Acer is not showing the yellow top 24 pixel border! bottom black

	// use reg; later use bram
	reg [BIT_DEPTH-1:0] buf1[SCREEN_WIDTH-1:0]; // single bits, odd lines
	reg [BIT_DEPTH-1:0] buf2[SCREEN_WIDTH-1:0]; // single bits, even lines
	wire [7:0] view_x, view_y;
  wire odd, outer_box, inner_box;
  
  // index means a color code, 0-15 for now
  wire [BIT_DEPTH-1:0] color_index, bar_index, buf_color_index, char_index, pixel_index;
  wire [7:0] char_num;
  reg  [RGB_DEPTH-1:0] rgb_next;

  wire char_pixel;

  // coordinate on 192x256 view ; needs (64,48) subtracted (borders) but too slow
  assign view_x = (pixel_x) >> 1;
	assign view_y = (pixel_y) >> 1;
	
	color_bars bars(view_x, bar_index);

	assign char_num = {2'b00, view_y[3], view_x[7:3]} - 4;
	assign char_index = bar_index + 2; // color combo
	assign pixel_index = char_pixel ? char_index : bar_index;
	// could change ? multiplexer to masks and/or
		
	chargen chars(
	  .clk(clk),   // pixel_tick too late? this works ok (or do we need clk?)
	  .reset(reset),
		.char_num(char_num), 
		.char_y(view_y[2:0]), 
		.char_x(view_x[2:0]), 
		.pixel_out(char_pixel)
	);
	// reasoning: two clks per pixel_tick. read data is valid on second clk.
	// stars...
	// and cars...
	

  assign odd    = pixel_y[1]; // every other double line
  
  // adding >= and view_x = pixel_x-24 broke the timing 
  assign outer_box = (pixel_y < 24 || pixel_y >= 456 || pixel_x < 32 || pixel_x >= 608);
  assign inner_box = (pixel_y < 48 || pixel_y >= 432 || pixel_x < 64 || pixel_x >= 576);
  // take pixel from current buffer
  assign buf_color_index = odd ? buf1[view_x] : buf2[view_x];
  // ignore that and draw color bars with ascii chars over
  assign color_index = outer_box ? border2 : (inner_box ? border1 : pixel_index);

	palette_16 palette(color_index, rgb_next);

  // changes happen on pixel_tick=1	
 	always @(negedge pixel_tick)
 	begin  
   		rgb <= rgb_next; //if(~pixel_tick) begin
	end
	
endmodule
