all: chip.bin

VGADIR  = /Users/olsone/TI99/geneve2020/src/vdp/vga-test

VERILOG_SRC = \
	$(VGADIR)/vga_test_top.v \
	$(VGADIR)/vga_test.v \
	$(VGADIR)/vga_sync.v \
	$(VGADIR)/pll.v \
	$(VGADIR)/blink.v \
	$(VGADIR)/graphic.v \
	$(VGADIR)/palette_16.v \
	$(VGADIR)/color_bars.v \
	$(VGADIR)/chargen.v \
	$(VGADIR)/reset_delay.v \

ROM_FILES = chara1.hex \

INPUT_FREQ = 100
OUTPUT_FREQ = 50.4

$(VGADIR)/pll.v: Makefile
	icepll -i $(INPUT_FREQ) -o $(OUTPUT_FREQ) -m -f $@

# hub 2
#USB1=/dev/cu.usbmodem14121
#USB2=/dev/cu.wchusbserial14121
# hub 1 (hub plugged into left side of macbook)
USB1=/dev/cu.usbmodem14111
USB2=/dev/cu.wchusbserial14120


chip.bin: $(VERILOG_SRC) $(ROM_FILES) blackice-ii.pcf Makefile
	yosys  -q -p "synth_ice40 -top vga_test_top -abc2 -blif chip.blif" $(VERILOG_SRC)
	arachne-pnr  -d 8k -P tq144:4k  -p blackice-ii.pcf chip.blif -o chip.txt
	#icebox_explain chip.txt > chip.ex
	icepack chip.txt chip.bin
	#icemulti -p0 chip0.bin > chip.bin && rm chip0.bin


upload: chip.bin
	cat chip.bin > $(USB1)
        
clean:
	rm -f chip.blif chip.txt chip.bin

.PHONY: clean
.PHONY: chip
.PHONY: upload

