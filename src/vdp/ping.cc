#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <arpa/inet.h>
#include <assert.h>
#include "zlib.h"

/**
  * Implementation of a PNG decoder using zlib.
  * I needed to get at the uncompressed data of an image, to put in EPROM.
  * I compute what size it would be with simple RLE or Huffman encoding.
  * I can't put zlib in the Geneve2020 BIOS - not enough ROM or working RAM.
  * (not to mention it's in C.)
  */

struct rgb {
	unsigned char r, g, b, x;
};


// Required coroutines during zlib inflating
unsigned char* readChunk(uint32_t* len, uint32_t* chunkType, uint32_t* crc, FILE* fp, FILE *fpout);
unsigned char* inflateIDAT(unsigned char* data, size_t len, FILE *fp, size_t* outlen);
rgb* initPalette(unsigned char* data, size_t len, uint32_t *ncolors);
// My analysis routines
void smash(unsigned char* in, unsigned char* out, size_t len, rgb* palette);
void histogram(unsigned char* data, size_t len, rgb* palette);
uint32_t rle(unsigned char* data, size_t len);
uint32_t huffman(unsigned char* data, size_t len);
void summary(uint32_t n, const char** methods, uint32_t* lens);

const char* methods[6] = {"Raw", "RLE", "RLE2", "Huffman", "Huffman2", "Zlib" };
uint32_t lens[5];

// Standard PNG chunk ids I expect
uint32_t chunk_text = 0x74584574;
uint32_t chunk_ihdr = 0x52444849;
uint32_t chunk_idat = 0x54414449;
uint32_t chunk_plte = 0x45544c50;

// pretty print a text/binary stream with numbers for unprintable chars
void printData(unsigned char* data, size_t len, FILE* fp)
{
	for (int i=0; i<len; ++i) {
		unsigned char c = data[i];
		if (isprint(c)) {
			putc(c, fp); 
		} else  {
			fprintf(fp," %02x ", c); 
		}
	}
	fprintf(fp, "\n");

}

int main(int argc, const char **argv)
{
	const char *filename;
	if (argc<2) {
	 	fprintf(stderr, "Usage: %s file\n", argv[0]);
		filename = "muteswan_332.png";
		//exit(1);
	} else {
		filename = argv[1];
	}

	
	FILE *fp = fopen(filename, "rb");
	if (!fp) {
		fprintf(stderr, "can't open file %s", argv[1]);
		exit(1);
	}
        const char* outfilename = "output.bin";
	FILE *fpout = fopen(outfilename, "wb");
	if (!fpout) {
		fprintf(stderr, "can't open file %s", outfilename);
		exit(1);
	}

	struct stat buf;
	int status = fstat(fileno(fp), &buf);

	size_t sz = buf.st_size;

	unsigned char magic[8];
	status = fread(&magic[0], 8, 1, fp);

	printf("Magic:");
	printData(magic, 8, stdout);

	uint32_t len, chunkType, crc;
	while(!feof(fp)) {
		unsigned char* data = readChunk(&len, &chunkType, &crc, fp, fpout);
	}
	summary(6, &methods[0], &lens[0]);

	
}


struct header {
	uint32_t width;
	uint32_t height;
	uint8_t  depth;
	uint8_t  colorType;
	uint8_t  compressionMethod;
	uint8_t  filterMethod;
	uint8_t  interlaceMethod;
};


struct header* printHeader(unsigned char* data)
{
	struct header* hdr = (struct header*)data;

	hdr->width = ntohl(hdr->width);
	hdr->height = ntohl(hdr->height);

	printf("  width: %d\n", hdr->width);
	printf("  height: %d\n", hdr->height);
	printf("  depth: %d\n", hdr->depth);
	printf("  colorType: %d\n", hdr->colorType);
	printf("  compressionMethod: %d\n", hdr->compressionMethod);
	printf("  filterMethod: %d\n", hdr->filterMethod);
	printf("  interlaceMethod: %d\n", hdr->interlaceMethod);

	return hdr;
}

unsigned char* readChunk(uint32_t* len, uint32_t* chunkType, uint32_t* crc, FILE* fp, FILE *outfp)
{
	unsigned char* data = NULL;
	int  status;

	static rgb* palette;
	static uint32_t  ncolors;

	status = fread(len, 4, 1, fp);
	status = fread(chunkType, 4, 1, fp);
	*len = ntohl(*len);
	
        size_t n;
	if (*len) {
		data = (unsigned char*)malloc(*len);
		n = fread(data, *len, 1, fp);
	}
		
	
	fread(crc, 4, 1, fp);
	char* t = (char*)chunkType;
	printf("* Got chunk len %d with type %8x or '%c%c%c%c'\n", *len, *chunkType, t[0], t[1], t[2], t[3]);
	if (*chunkType == chunk_text) {
		for (uint32_t cnt = 0; cnt < *len; cnt += strlen((char*)data+cnt)+1) {
			printf("text: '%s'\n", data + cnt);
		}
	} else if (*chunkType == chunk_ihdr) {
		printHeader(data);
	} else if (*chunkType == chunk_idat) {
		uint32_t tot_zlib = *len;
                size_t tot_raw = 0;
		unsigned char* indexed = inflateIDAT(data, *len, outfp, &tot_raw);
		unsigned char* pixeled = (unsigned char*)malloc(*len);
		smash(indexed, pixeled, *len, palette);
	        histogram(indexed, n, palette);
		lens[0] = tot_raw;
		lens[1] = rle(indexed, tot_raw);
		lens[2] = rle(pixeled, tot_raw);
		lens[3] = huffman(indexed, tot_raw);
		lens[4] = huffman(pixeled, tot_raw);
		lens[5] = tot_zlib;

	} else if (*chunkType == chunk_plte) {
		palette = initPalette(data, *len, &ncolors);
	}
	return data;
}

unsigned char* inflateIDAT(unsigned char* data, size_t len, FILE *fpout, size_t *outlen)
{
    #define CHUNK 1000000
    int ret, flush;
    unsigned have;
    z_stream strm;
    unsigned char chunk[CHUNK];

    /* allocate deflate state */
    strm.zalloc = Z_NULL;
    strm.zfree = Z_NULL;
    strm.opaque = Z_NULL;
    strm.avail_in = len;
    strm.next_in = data;
    strm.avail_out = sizeof(chunk);
    strm.next_out  = chunk; 

    ret = inflateInit(&strm);
    assert(ret != Z_STREAM_ERROR);  /* state not clobbered */

    ret = inflate(&strm, Z_NO_FLUSH);
    assert(ret != Z_STREAM_ERROR);  /* state not clobbered */

    switch (ret) {
    case Z_NEED_DICT:
        fprintf(stderr, "Error: Z needs dictionary\n");
	ret = Z_DATA_ERROR;     /* and fall through */
	return NULL;
    case Z_DATA_ERROR:
    case Z_MEM_ERROR:
        fprintf(stderr, "Error: Z data or memory error\n");
	(void)inflateEnd(&strm);
	return NULL;
    default:
        break;
    }
    int n = CHUNK  - strm.avail_out;
    n = fwrite(chunk, 1, n, fpout);
    printf("Wrote %d bytes to output\n", n);
    inflateEnd(&strm);

    *outlen = n;
    unsigned char* out = (unsigned char*)  malloc(n);
    memcpy(out, chunk, n);
    return out;
}

/** 
 * From a palette chunk,
 * Store the palette data into an array
 * **/
rgb* initPalette(unsigned char* data, size_t len, uint32_t *ncolors)
{
        uint32_t n = len/3;
	*ncolors = n;
        rgb* out = (rgb*)malloc(sizeof(rgb)*n);
        for (int i=0; i<n; ++i) {
		unsigned char r = data[i*3];
		unsigned char g = data[i*3+1];
		unsigned char b = data[i*3+2];
		out[i].r = r;
		out[i].g = g;
		out[i].b = b;
		int rs = (int)(r*7.0/255.0);
		int gs = (int)(g*7.0/255.0);
		int bs = (int)(b*3.0/255.0);
		out[i].x = (rs<<5) + (gs<<2) + bs;
	}
	return out;

}

#define BYTE_TO_BINARY_PATTERN "%c%c%c %c%c%c %c%c"
#define BYTE_TO_BINARY(byte)  \
  (byte & 0x80 ? '1' : '0'), \
  (byte & 0x40 ? '1' : '0'), \
  (byte & 0x20 ? '1' : '0'), \
  (byte & 0x10 ? '1' : '0'), \
  (byte & 0x08 ? '1' : '0'), \
  (byte & 0x04 ? '1' : '0'), \
  (byte & 0x02 ? '1' : '0'), \
  (byte & 0x01 ? '1' : '0') 

/**
 * Report the frequency of each index in the color palette
 **/
void histogram(unsigned char* data, size_t len, rgb* palette)
{
	uint32_t bucket[256];
	for (int i=0; i<256; ++i)  {
		bucket[i] = 0;
	}
	// count them
	for (int i=0; i<len; ++i)
	{
		unsigned char c = data[i];
		++bucket[c];
	}
	// print summary
	for (int i=0; i<256 && bucket[i]>0; ++i)  {
		rgb* s = palette+i;
		printf("%02x rgb: %02x %02x %02x x: %02x   " BYTE_TO_BINARY_PATTERN " n: %8d\n", i, s->r, s->g, s->b, s->x, BYTE_TO_BINARY(s->x), bucket[i]);
	}
}
/** 
 * Replace indexes with the x byte (mode G7 pixel) before compression
 * Many RGB colors collapse together when reduced to rrrgggbb 8-bit pixels.
 */
void smash(unsigned char* in, unsigned char* out, size_t len, rgb* palette) {
	for (int i=0; i<len; ++i) {
		out[i] = palette[in[i]].x;
	}
}

/**
 *  Calculate the RLE size of this chunk
**/
uint32_t rle(unsigned char* data, size_t len)
{
	unsigned char c = 0xff;
	unsigned char last = 0xff;
	int total = 0;
	int run = 0;
	for (int i=0; i<len; ++i)
	{
		c = data[i];
		if (c == last) {
			++run;
		} else {
			last = c;
			if (run>0) {
				total += 2;
				run = 0;
			} else {
				total += 1;
			}
		}

	}
	float pct = 1.0-(float)total/(float)len;
	printf("RLE encoded length: %d of %d saves %f\n", total, len, 100.0*pct);
	return total;
}

/**
 * Compute what the Huffman encoded size would be  (estimate without overhead)
 * Each time two nodes are joined, file size increases by sum of subtrees * 1 bit
 **/


 /*
 *  insert_sort: move data[0] to its correct position
 * */
void insert_sort(uint32_t* data, size_t len) {
	uint32_t x = data[0];
	for(int i=0; i<len-1; ++i) {
		if (x>data[i+1]) {
			data[i] = data[i+1];
			data[i+1] = x;
		}
	}
}

/* lexi: the sort order co-routine */
int lexi(const void* a, const void* b)
{
	uint32_t c1 = *(const uint32_t*)a;
	uint32_t c2 = *(const uint32_t*)b;
	return c1-c2;
}


uint32_t huffman(unsigned char* data, size_t len)
{
	printf("\nhuffman\n\n");

	uint32_t bucket[256];
	for (int i=0; i<256; ++i)  {
		bucket[i] = 0;
	}
	// Buckets hold the frequency of that char
	for (int i=0; i<len; ++i)
	{
		unsigned char c = data[i];
		++bucket[c];
	}
	qsort(&bucket[0], 256, sizeof(uint32_t), lexi);
	// print just for fun
	int next = 0;
	for (int i=0; i<256; ++i)  {
		if ( bucket[i]==0) {
			next = i+1;
		} else {
			printf("%d %8d\n", i, bucket[i]);
		}
	}
	// now iterate joining nodes
	printf("Joining buckets, accumulating total bits\n");
	printf("%3s %4s %6s %3s %10s\n", "i", "c[i]", "c[i+1]", "sum", "total");
	uint32_t total = 0; // total bits
	for (int i=next ; i<255; ++i) {
		uint32_t c1 = bucket[i];
		uint32_t c2 = bucket[i+1];
		uint32_t sum = bucket[i] + bucket[i+1];
		total += sum;         // we just added 1 bit for each of those chars
		bucket[i+1] = sum;
		insert_sort(&bucket[i+1], 256-i);
		printf("%3d %4d %6d %3d %10d\n", i, c1, c2, sum, total);
	}
	int bytes = (total+7)/8;
	float pct = 1.0-(float)bytes/(float)len;
	printf ("Huffman: Total %d bytes or %f pct\n", bytes, 100.0*pct);
	return bytes;
}

void summary(uint32_t n, const char** methods, uint32_t* lens) {
	printf ("%-10s %5s %6s\n", "Method", "Total", "Pct");
	for (int i=0; i<n; ++i) {
		printf ("%-10s %5d %6.2f\n", methods[i], lens[i], 100.0*lens[i]/lens[0]);
	}
}
