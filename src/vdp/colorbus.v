
/**
**   VDP register shadowing
**/

FPGA required

RAMDAC destination - on video card but attached to FPGA?

FPGA must process color bus and send it to RAMDAC.
Palette registers could be emulated on the FPGA.

Need to capture: change of video mode and change of palette register.
In most modes, the 16 palette registers start in the default and change independently.
In G7, the whole palette changes to 8-bit indexed.

There are two ways the graphics mode changes.

Direct write to a register using port#1.
Indirect write: set the register# in VR#17 and write bytes to port #3 (MSB flag in VR#17 causes autoincrement)

Features needed:

Shadow mode bits from VR#0,1,8,9 (Yamaha calls these mode registers 0,1,2,3)
Shadow VR#16 palette number
Shadow VR#17 indirect register# (MSB=0 means autoincrement)
Shadow 16 palette registers used in G1-G6
Create a new interface for setting 256 palette registers. (this is a natural)

Load 256 defaults to RAMDAC palette when entering G7 (until they are altered again)
Load 16 cached palette registers to RAMDAC palette when exiting G7

Capture writes to >8C02 - write address, common register writes. Might as well keep a full register file?
Captures writes to >8C04 and >8C06
indirect writes: set VR17, then send data to port#3. If R#17 MSB set, then aiutoincrement.
R#17 cannot be written to during indirect addressing.

Indirect addressing can set the mode registers.

// How to detect a change to/from mode G7? That's when the RAMDAC palette must be reloaded.
// Mode G7 is
// VR#0  0  X  X  X M5 M4 M3  X
// VR#1  X  X  X M1 M2  X  X  X
// VR#8  X  X  X  X  X  X  X  X
// VR#9 LN  X  X  X  X  X  X  X

//     M5 M4 M3 M2 M1 (note order is M1,M2 in VR#1)
// T40  0  0  0  0  1   9918
// T80  0  1  0  0  1
// MC   0  0  0  1  0   9918
// G1   0  0  0  0  0   9918
// G2   0  0  1  0  0   9918A
// G3   0  1  0  0  0   
// G4   0  1  1  0  0
// G5   1  0  0  0  0
// G6   1  0  1  0  0  
// G7   1  1  1  0  0 

So only VR#0 is required to detect G7 entry/exit

// Q. what does 9938 do in half-bitmap mode?

// VR#0  0  X  X  X  1  1  1  X
// VR#1  X  X  X  0  0  X  X  X
// VR#8  X  X  X  X  X  X  X  X
// VR#9 LN  X  X  X  X  X  X  X
when LN is 1, screen height is 212 pixels


Set palette register number in VR#16, send 2 bytes to port2
VR#16   0  0  0  0 C3 C2 C1 C0   Palette #
Port#2  0 R2 R1 R0  0 B2 B1 B0   First byte
        0  0  0  0  0 G2 G2 G2   Second byte

// Port usage
//
// Write:
// 0 write data byte
// 1 write register (2 cycles)
// 2 write palette number
// 3 write palette value (2 cycles)

// Read:
// 0 read data byte
// 1 read status byte
// 2
// 3

// Port#1 - write register

state: idle, half, ready

// wr, rd  should be already debounced by parent.

input port_addr_i[1:0]
input wr_i
input rd_i

// Combinatorial Logic

port0_write_latch = port_addr_i == b'00 && last_port0_wr == 1 and !wr_i
port1_write_latch = port_addr_i == b'01 && last_port1_wr == 1 and !wr_i
port2_write_latch = port_addr_i == b'10 && last_port2_wr == 1 and !wr_i
port3_write_latch = port_addr_i == b'11 && last_port3_wr == 1 and !wr_i
// reading is weirder. do we ever need to read?

// should there be a:
//
// last_port1_wr = port1_wr
// port1_wr = next_port1_wr
// next_port1_wr = port1_wr_i

// or can wr_i be used directly?


next_port1_wr_en = port1_wr_en_i


// next state calculated in one expression

next_port1_state = port1_state==idle  & port1_write_latch ? half 
                 : port1_state==half  & port1_write_latch ? ready
                 : port1_state==ready ? idle
                 : port1_state

// register moves happen as a function of state and inputs

next_port1_lsb = port1_write_latch & port1_state==idle ? port1_i : port1_lsb;
next_port1_msb = port1_write_latch & port1_state==half ? port1_i : port1_lsb;


// If MSB was set, then the LSB goes into a register # pointed to by the lower 6 bits
// We only care about the registers that contain mode bits.
// 16 bit value: {VG, VA, A13..A0 }
// VG=1,VA=1 invalid
// VG=1,VA=0 write to register
// VG=0,VA=1 set vdpaddr and mode for write (auto-increment after write)
// VG=0,VA=0 set vdpaddr and mode for read (pre-fetch and increment)
 
// These are all the mode registers, but only VR#0 is needed to detect G7 entry/exit

next_vreg0       = port1_state==ready & port1_msb == 'h80 ? port1_lsb 
                 : port3_write_latch  & vreg17    == 'h00 ? port3_i
                 : vreg0
next_vreg1       = port1_state==ready & port1_msb == 'h81 ? port1_lsb 
                 : port3_write_latch  & vreg17    == 'h01 ? port3_i
                 : vreg1
next_vreg8       = port1_state==ready & port1_msb == 'h88 ? port1_lsb 
                 : port3_write_latch  & vreg17    == 'h08 ? port3_i
                 : vreg8
next_vreg9       = port1_state==ready & port1_msb == 'h89 ? port1_lsb 
                 : port3_write_latch  & vreg17    == 'h09 ? port3_i
                 : vreg9

// vreg17 is the pointer to indirect write a register. there are really only 6 bits register#
// vreg[7] is the autoincrement (negative logic)
next_vreg17      = port1_state==ready & port1_msb == 'h91 ? port1_lsb
                 : port3_write_latch  & !vreg17[7]        ? vreg17 + 1
                 : vreg17

// vdp read/write data address, in case we ever want those.
next_vaddr       = port1_state==ready & port1_msb[7] == 0 ? {port1_msb[5:0], port1_lsb} : vaddr_reg
next_vaddrmode   = port1_state==ready & port1_msb[7] == 0 ? port1_msb[6] : vaddrmode;

// there is no state for port3 but it can autoincrement vr17

// register transfers:

always(@posedge clk)
  last_port1_wr_en <= port1_wr_en

  port1_wr_en      <= next_port1_wr_en
  port1_lsb        <= next_port1_lsb
  port1_msb        <= next_port1_msb


    port1_reg = port1_reg_next
    port1_last_reg = port1_last_reg_next

... etc


//
// Palette section
//

// palette number to write to:
next_vreg16      = port1_state==ready & port1_msb == 'h90 ? port1_lsb 
                 : port3_write_latch  & vreg17    == 'h10 ? port3_i
                 : vreg16

// allocate reds[256] for color palette.. let's have up to 8 bits (since RAMDAC takes bytes)

reg [7:0] reds[256];
reg [7:0] greens[256];
reg [7:0] blues[256];

state = redblue, green, ready

next_color_state = port1_state==redblue & port2_write_latch ? green 
                 : port1_state==green   & port2_write_latch ? ready
                 : port1_state==ready   ? redblue

// port2 input 2 bytes:
// 0 R1 R2 R3 0 B1 B2 B3 
// 0  0  0  0 0 G1 G2 G3

// RAMDAC AD476: 
// 0  0 R1 R2 R3 R4 R5 R6
// use these so that white=1 1 1 1 1 1
// 0  0 R1 R2 R3 R1 R2 R3

next_red         <= port1_state==redblue & port2_write_latch ? {'b0,'b0,port2_i[6:4],port2_i[6:4]} : red_reg
next_blue        <= port1_state==redblue & port2_write_latch ? {'b0,'b0,port2_i[2:0],port2_i[2:0]} : blue_reg
next_green       <= port1_state==green   & port2_write_latch ? {'b0,'b0,port2_i[2:0],port2_i[2:0]} : green_reg

if (color_state == ready)
  reds[ vreg16 ] <= red_reg  
  blues[ vreg16 ] <= blue_reg  
  greens[ vreg16 ] <= green_reg  


