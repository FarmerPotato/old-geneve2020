
( VDP.fs words )

( J1A FORTH-cpu io port addresses

            bit READ            WRITE

      0001  0   CDATA in        CDATA in
      0002  1   CDATA out       CDATA out
      0004  2   CDATA dir       CDATA dir

      0010  4   SS in           
      0020  5   SS out          SS out
      0040  6   SS dir          SS dir
)


( control data port ss )
: ss@ $10 io@ ;
: ss! $20 io! ;
: sso $5f $40 io! ;



( cpu data port  )
: cd! $2 io! ;
: cd@ $1 io@ $ff and ;

( set dir of cpu data )
: cdo $00ff $4 io! ;
: cdi $0000 $4 io! ;


( ss register:    ss1 ss0 csr csw m1 m0 nc(i) wait(i)    )
( with bug workaround: ss1 and csw must be switched )
( ss register:    csw ss0 csr ss1 m1 m0 nc(i) wait(i)    )
( something went crazy . port pins are no longer mapped like i think they were? )
( ss register:    w ss1 nc csr m0 ss0 m1 csw )

( bits mask -- )
: modebits ss@ and or ss! ;

: port0 ( data )     $0 $f5 modebits ;
: port1 ( addr/sta ) $8 $f5 modebits ;
: port2 ( pltt )     $2 $f5 modebits ;
: port3 ( regs )     $a $f5 modebits ;

( chip selects. s1s0=10 is vdp1 )
: ss00 $00 $bb modebits ;
: ss01 $04 $bb modebits ;
: ss10 $40 $bb modebits ;
: ss11 $44 $bb modebits ;

( tightest loop still takes 1+1 microsecond )
: 2us 4 0 do loop ;
: 5us 10 0 do loop ;
: 8us 16 0 do loop ;
: us ( n -- ) 1 lshift 0 do loop ;

( n measured actual us )
( 1 2 )
( 8 5 )

: vdpwait@ ss@ 1 and ;

( example. loop until WAIT goes high )
( do vdpwait@ 0= while ; )

( raise/lower csw,csr. workaround hardware bug )
: csw1 $11 $ee modebits ;
: csw0 $10 $ee modebits ;
: csr1 $11 $ee modebits ;
: csr0 $01 $ee modebits ;


( write a byte to the current port )
: v! ( n -- ) ( toggle vdp csw low-high )
  8us ( wait gigantic amount ) 
  cdo
  $ff and  ( training wheels )
  cd! 
  csw0 
  2us csw1 ;

( read a byte from the current port )
: v@ ( -- n )
	cdi
	8us
	csr0 
	2us
	cd@ 
	csr1 ;



( set the vdp address. assume high bits are correct. )
: setva ( n -- )
  port1
  dup $ff and v!
  8 rshift v! ;

( traditional register names r0-r2 are used. ) 
( r0 address )
( r1 data byte in lsb )
( r2 length )

: vsbw ( r1 r0 -- )
    $3fff and $4000 or setva
    port0 v! ;

: vsbr ( r0 -- r1 )
    $3fff and setva
    port0 v@ ;

: vwtr ( yy n -- ) ( set register n to value yy )
  port1
  swap
  $ff and v!
  $3f and $80 or v! ;

: vdpsta  ( n -- n ) ( read status register n. first set R#15. )
  $0f and $8f00 or setva
  ( port1 ) v@ ;

: vsbwr ( r1 r0 -- b ) ( write then read byte, compare )
  over over
  vsbw
  vsbr
  = ;

( bug: '138 should have been: G2A* enable on ss00 = 0 )
: vdpinit cdo sso ss01 port0 
  ( registers )
  $00 0 vwtr (  0  DG  IE2 IE1 M5  M4  M3   0  )
  $60 1 vwtr ( 16K BL  IE0 M1  M2   0  SI  MAG )
  $00 2 vwtr 
  $00 3 vwtr
  $00 4 vwtr
  $00 5 vwtr
  $00 6 vwtr
  $f4 7 vwtr ( fg/bg color )
  $08 8 vwtr ( MS  LP  TP  CB  64K  0  SPD BW  )
  $00 9 vwtr ( LN   0  S1  S0  IL  EO  *NT DC  )
  $00 $E vwtr  ( A16 A15 A14  )
  $00 $F vwtr ( vdpsta #0 )
  $24 $19 vwtr ( enable WAIT and VDS* )
  $00 $2D vwtr ; ( 0 MXC ... )
;

( Graphic Modes )

: text1
  $00 0 vwtr
  $70 1 vwtr  ( BL|IE0|M1 )
  $00 2 vwtr  ( SIT  $0000 )
  $01 4 vwtr  ( PDT  $0800 )  
  $f4 7 vwtr  ( fg/bg color )
;

: text2
  $04 0 vwtr  ( M4 )
  $70 1 vwtr  ( BL|IE0|M1 )
  $03 2 vwtr  ( SIT  $0000 )
  $02 4 vwtr  ( PDT  $1000 )  
  $f4 7 vwtr  ( fg/bg color )
  $4f $C vwtr ( alt color )
  $0f $D vwtr ( blink time )
  $2f 3 vwtr  ( color tbl $0a00 )
  $00 $A vwtr ( color tbl msb ) 
;

: graphics1
  $00 0 vwtr  (  )
  $60 1 vwtr  ( BL|IE0 )
  $00 2 vwtr  ( SIT  $0000 )
  $01 4 vwtr  ( PDT  $0800 )  
  $17 7 vwtr  ( fg/bg color )
  $2f 3 vwtr  ( color tbl $0780 )
  $00 $A vwtr ( color tbl msb )
  $06 5 vwtr  ( SATR $0300 )
  $00 $B vwtr ( SATR msb )
  $01 6 vwtr  ( SPDT $0800 )  
  $08 8 vwtr  ( 64K  )
;


: graphics7   ( 256 x 192 x 8bpp )
  $0e 0 vwtr  ( M5|M4|M3  )
  $60 1 vwtr  ( BL|IE0 )
  $1f 2 vwtr  ( SIT  $0000 )
  $1F 7 vwtr  ( fg/bg color )
  $f7 5 vwtr  ( SATR $fa00 )
  $01 $B vwtr ( SATR msb )
  $1e 6 vwtr  ( SPDT $f000 )  
  $0a 8 vwtr ( 64K|SPD  )
;

: vfill ( n vaddr vaddr )  do i vsbw loop ;
  
( Tests )



: ck = if ." pass " else ." fail " then ;

: tst1
  $ff $0 vsbwr
  if ." pass " else ." fail " then
;


: tst2
  $aa $0000 vsbw
  $55 $1000 vsbw

  $0000 vsbr $aa ck
  $1000 vsbr $55 ck ( only this one passes because all reads = the last write ) 
;

( why is this broken when tst4 is not. bug? )
( write alternate FF 00 bytes to , always read addr-1 )
: tst3 $ff 0 vsbw
  $10 1 do 
  i 1 and 0= i vsbw 
  $aa cd! ( scramble pins )
  i 1- ( prior addr ) 
  dup 1 and swap vsbr 0 ck 
  loop ;

( known bugs in 9958 PCB )
( vdpwait 5V0 is connected directly to 3V3 input )
( CSR* fails because '138 has address bits backwards. Make software workaround here )
( really, PMODs should be connected in standard order, but left-right is so beautiful )
( trimmer resistor is unreliable, but definitely moves VSYNC closer to CSYNC detect )
( 220 uF coupling caps are way too big? )
( V/H/CSYNC needed a transistor/driver: breadboard fix)
( not a bug: vb was windowed past the trigger/area of interest )



( test range of addresses with values 00-FF )
( write )
: tstup ( vaddr vaddr -- )   do i $ff and i vsbw loop ;
: tstdn ( vaddr vaddr -- n )    0 -rot do i $ff and i vsbr = if else 1+ then loop ;
  

: tst4 ( vaddr vaddr -- n )   2dup  tstup  tstdn  dup . 0 ck ;
      
( test address msb in VR#14 )


( set vdp address 3 bytes )
: setvad ( 0|1 n $4000|0 )
  port1
  over $3fff and or setva
  14 rshift
  swap 2 lshift +  ( 3 top bits )
  $e vwtr ( VRAM base address register )
;

( set to read a 17-bit address )
: setvadr
  0 setvad
;

( set to write a 17-bit address )
: setvadw ( 0|1 n -- )
  $4000 setvad
;


( set even vdp address, 16 bits << 1 )
( most of the time all you need is even addr. )
: seteva ( n $4000|0 )
  port1
  over $1fff and 1 lshift or setva
  13 rshift
  $e vwtr ( VRAM base address register )
;

: setreva ( n -- )
  $4000 seteva
;

: setweva ( n -- )
  0 seteva
;


( if the msb is set in VR#14, the vfill won't affect the test )
: tst5 
  0 $e vwtr
  $4000 0 tstup
  0 $4000 setvadw
  $ff $4000 0 vfill
  0 $0000 setvadw
  $4000 0 testdn
;
  
( graphics 7 color bars test pattern )

: g7test
  graphics7
  ( 8x16 pixel spots )
  ( cycle colors 0-1f which is black, 3 blues, all the reds... next row add greenish )
  ( color is x>>3 + y&70<<1 )
  0 0 setvad
  port0
  192 0 do
    256 0 do 
      i 3 rshift
      j $70 and 1 lshift
      + ( color )
      8 0 do dup v! loop
      drop
    8 +loop
  loop
;
 
( for a text40 test, need min font 26 chars, 208 bytes )
