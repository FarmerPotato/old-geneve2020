#include <gsl/gsl_multimin.h>
#include <gsl/gsl_vector.h>
#include "yjk.h"

// Solve for some best-fit values of YJK among four pixels.

// define x as j,k,y0,y1,y2,y3
// context: rgbs
float errfunc(float* x, void* context)
{
  rgb* rgbs = (rgb*)context;
  float result = 0;
  for (int i=0; i<4; ++i) {
    // construct yjk, convert to rgb, check error in true y
    // do anything with chrominance error? weight it less than y?
    yjk p;
    p.y = x[2+i];
    p.j = x[0];
    p.k = x[1];
    rgb p2 = yjk_to_rgb(p);
    float y1 = true_y(p2);
    float y0 = true_y(rgbs[i]);
    float dy = (y1-y0);
    result += dy*dy;
  }
  return result;
}
  
// initial guess
float* guess(const rgb* rgbs)
{
  float* x = (float*)malloc(6*sizeof(float));
  
  rgb avg = average_rgb(rgbs);
  float y = msx_y(avg);
  float j = msx_j(avg, y);
  float k = msx_k(avg, y);
  x[0] = j;
  x[1] = k;
  for (int i=0; i<4; ++i) {
    x[2+i] = msx_y(rgbs[i], j, k);
  }
  return x;
}

double my_f(const gsl_vector *v, void *params)
{
  double x, y;
  double *p = (double *)params;

  x = gsl_vector_get(v, 0);
  y = gsl_vector_get(v, 1);

  return p[2] * (x - p[0]) * (x - p[0]) +
           p[3] * (y - p[1]) * (y - p[1]) + p[4];
}

/* The gradient of f, df = (df/dx, df/dy). */
void
my_df (const gsl_vector *v, void *params,
       gsl_vector *df)
{
  double x, y;
  double *p = (double *)params;

  x = gsl_vector_get(v, 0);
  y = gsl_vector_get(v, 1);

  gsl_vector_set(df, 0, 2.0 * p[2] * (x - p[0]));
  gsl_vector_set(df, 1, 2.0 * p[3] * (y - p[1]));
}

/* Compute both f and df together. */
void
my_fdf (const gsl_vector *x, void *params,
        double *f, gsl_vector *df)
{
  *f = my_f(x, params);
  my_df(x, params, df);
}

int main()
{
	gsl_multimin_function_fdf my_func;

// gsl example code
	/* Paraboloid center at (1,2), scale factors (10, 20),
	   minimum value 30 */
	double p[5] = { 1.0, 2.0, 10.0, 20.0, 30.0 };

	my_func.n = 2;  /* number of function components */
	my_func.f = &my_f;
	my_func.df = &my_df;
	my_func.fdf = &my_fdf;
	my_func.params = (void *)p;

	return 0;

}

