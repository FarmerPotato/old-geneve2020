/* memory mapper */

/* resembles the ls610/ls612/tim990x in that it replaces the top 4 bits of an address with 
   an extended 13 bits (610 gives 12 bits) for a maximum reach of 32MB.

  Geneve has a mapper that replaces the top 3 bits with 8, for a reach of 8MB.
  
  Geneve2020 has CRU parellel access to the 16-bit map registers. 
  To configure MDOS or GPL, it chooses an 8-bit prefix or a slice of 2MB.
  MDOS can only write to the lower 8 bits and this happens through its usual
  addresses F100-F107. Because mdos assumes 8 8K pages, a write goes to two
  registers. a read comes from the even register (assumes they are always equal.)
  mdos will frequently save and restore 2 registers with MOV.
  
  reg_next[i] = 
  /* serial access via 2100
  parallel access to mdos/gpl byte via 8000
  parallel access to bios 16-bits via a000
  spamm converts memory cycle to byte-parallel io

  so, incorporates its own select logic, from the full address bus and bus status
*/


`default_nettype none
module memorymapper (
    input wire reset,

  	input wire clk,
    input wire write,
    input wire read,
    input wire [15:0] data_write,       // the data to write
    output wire [15:0] data_read,       // the data that's been read
    input wire [17:0] address,          // address to write to
    output wire ready,                  // high when ready for next operation
    
    input wire [3:0] bst,               // mem, bst1, bst2, bst3

// bus interface, one step removed
    output wire [14:0] address_in,      // address pins for the memory/cru cycle
    input  wire [15:0] data_in,         // in serial, only lsb counts
    output wire [15:0] data_out,        // in serial, only msb counts
    output wire OE,                     // output enable - low to enable
    output wire WE,                     // write enable - low to enable
    output wire CS                      // chip select - low to enable
);


    assign cru_cycle = bst == b4'b;  // check this
    assign memory_cycle


		localparam SER_BASE = 0x3000; /* 256 bits, 16 16-bit registers */
		localparam PAR_BASE = 0x8000; /* 16 16-bit registers */
		localparam GPL_BASE = 0x8000; /* 8 1-byte registers */
		localparam MDOS_BASE = 0xF100; * 8 1-byte registers */
  
    localparam STATE_IDLE = 0;
    localparam STATE_WRITE = 1;
    localparam STATE_WRITE_SETUP = 4;
    localparam STATE_READ_SETUP = 2;
    localparam STATE_READ = 3;

    reg output_enable;
    reg write_enable;
    reg chip_select;

    reg [4:0] state;
    reg [15:0] data_read_reg;
    reg [15:0] data_write_reg;

    assign data_pins_out_en = (state == STATE_WRITE) ? 1 : 0; // turn on output pins when writing data
    assign address_pins = address;
    assign data_pins_out = data_write_reg;
    assign data_read = data_read_reg;
    assign OE = output_enable;
    assign WE = write_enable;
    assign CS = chip_select;

    assign ready = (!reset && state == STATE_IDLE) ? 1 : 0; 

    initial begin
        state <= STATE_IDLE;
        output_enable <= 1;
        chip_select <= 1;
        write_enable <= 1;
    end

	always@(posedge clk) begin
        if( reset == 1 ) begin
            state <= STATE_IDLE;
            output_enable <= 1;

        end
        else begin
            case(state)
                STATE_IDLE: begin
                    output_enable <= 1;
                    chip_select <= 1;
                    write_enable <= 1;
                    if(write) state <= STATE_WRITE_SETUP;
                    else if(read) state <= STATE_READ_SETUP;
                end
                STATE_WRITE_SETUP: begin
                    chip_select <= 0;
                    data_write_reg <= data_write;
                    state <= STATE_WRITE;
                end
                STATE_WRITE: begin
                    write_enable <= 0;
                    state <= STATE_IDLE;
                end
                STATE_READ_SETUP: begin
                    output_enable <= 0;
                    chip_select <= 0;
                    state <= STATE_READ;
                end
                STATE_READ: begin
                    data_read_reg <= data_pins_in;
                    state <= STATE_IDLE;
                end
            endcase
        end
    end


endmodule
