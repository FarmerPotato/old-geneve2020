/* dram controller

  inputs from bus and memorymapper
  
  rw  direction of this operation 1=r 0=w 
  
  positive logic everywhere else
  
  we       bus write strobe
  rd       bus read strobe
  clkout   ready is sampled on rising? edge, or data must be ready
           wait state can be inserted or cleared when clkout falls
  ready
  
  // declare
  dramcontroller dram( );
  
  
  any idea how refresh cycle is going to work? blocks memory cycle or interrupts it?
*/


`default_nettype none
module dramcontroller (
    input wire reset,
    input wire enable                   // positive on memory access to dram
  	input wire clk,                     // fpga clock
  	input wire readwrite                // early 0=write 1=read. valid on alatch
    input wire write,                   // positive, we pulse
    input wire read,                    // positive, rd pulse
    input wire [15:0] data_write,       // the data to write
    output wire [15:0] data_read,       // the data that's been read
    input wire [23:0] address,          // address to write to 32Mb reach in bytes
    output wire ready,                  // high when ready for next operation

    // DRAM pins
    output wire [11:0] address_pins,    // address pins of the DRAM
    input  wire [15:0] data_pins_in,    // parity bits are 17:16 data word is 15:0
    output wire [15:0] data_pins_out,
    input  wire [1:0]  parity_pins_in;
    input  wire [1:0]  parity_pins_out;
    // should those be unified as io or does the host take care of that?
    output wire ras,
    output wire cas,
    output wire OE,                     // output enable - low to enable
    output wire WE,                     // write enable - low to enable
    output wire CS                      // chip select - low to enable
);


  /* state model
    idle
      * enable (owner has seen alatch psel mem bst, and this is a memory cycle)
      * rw is valid at this time
    row_setup
      * row address out
      * x cycles then ras
    ras_cycle
      * assert ras
    col_setup
      * ras change?
      * col address out
      * x cycle than cas
    cas cycle
      * assert cas
    
    if (rw)
    read_data
    else
    write_data
    
      this is where fpm could be used to implement look-ahead cache. repeat cas
    release
          
    back to idle
      
  */
  
    localparam STATE_IDLE = 0;
    localparam STATE_WRITE = 1;
    localparam STATE_WRITE_SETUP = 4;
    localparam STATE_READ_SETUP = 2;
    localparam STATE_READ = 3;

  
// refresh logic
	// decide. does refresh logic steal idle cycles only
	// or does it block the bus to finish the refresh, or just defer the refresh?
	// whats the worst case?
	// stop incrementing refresh_counter when both refresh and memory access are active
    reg[23:0] refresh_counter;
    // dont constantly refresh, avoid wasting power
    // refresh can be the top bits = 0
    // be outputting a refresh cycle when refresh is true
    wire refresh = refresh_counter[22:12] == 12b'0;
    wire refresh_row = refresh_counter[]

// memory-to-memory logic


    reg [4:0] state;
    reg [15:0] data_read_reg;
    reg [15:0] data_write_reg;

    assign ready = (!reset && state == STATE_IDLE) ? 1 : 0; 

// this is from the sram module

    initial begin
        state <= STATE_IDLE;
        output_enable <= 1;
        chip_select <= 1;
        write_enable <= 1;
    end

	always@(posedge clk) begin
        if( reset == 1 ) begin
            state <= STATE_IDLE;
            output_enable <= 1;
            chip_select <= 1;
            write_enable <= 1;
        end
        else begin
            case(state)
                STATE_IDLE: begin
                    output_enable <= 1;
                    chip_select <= 1;
                    write_enable <= 1;
                    if(write) state <= STATE_WRITE_SETUP;
                    else if(read) state <= STATE_READ_SETUP;
                end
                STATE_WRITE_SETUP: begin
                    chip_select <= 0;
                    data_write_reg <= data_write;
                    state <= STATE_WRITE;
                end
                STATE_WRITE: begin
                    write_enable <= 0;
                    state <= STATE_IDLE;
                end
                STATE_READ_SETUP: begin
                    output_enable <= 0;
                    chip_select <= 0;
                    state <= STATE_READ;
                end
                STATE_READ: begin
                    data_read_reg <= data_pins_in;
                    state <= STATE_IDLE;
                end
            endcase
        end
    end


endmodule
